---
title: 'Managing Page Menus: Two Valuable WordPress Plugins'
date: 2007-07-09T18:44:51.000Z
slug: managing-page-menus-two-valuable-wordpress-plugins
tags:
  - wordpress
  - wordpress plugins
---

**This post has been deprecated with features built into newer versions of Wordpress. I'm leaving this post up for legacy information purposes, but I recommend using the integrated Wordpress menu tools to achieve the outcome these plugins provided.**

One of the nicest aspects of Wordpress is its ability to be used as a complete web site management tool, rather than just a blog. Its ease of use makes it a great solution for people unfamiliar with web site management, and daunted by the complexity of larger content management systems. However, there is something missing from the default Wordpress installation: the ability to easily manage page menus. There are a number of plugins that let you do this, but I've found two that I think are the most valuable to have.

What makes these plugins important is a couple of things: ease of use, and functionality. Both of these plugins make it very simple to achieve the desired result, without giving you too many options to confuse you or get in your way. Sometimes, less really is more.

[My Page Order](http://geekyweekly.com/mypageorder/) is a great tool to help you manage, well, the order of pages in your menu. Wordpress does natively support page ordering, but doing this is a little cumbersome. My Page Order eliminates the need to manually set the order value of each individual page, and instead gives you a clean, easy-to-use ordering tool in the Wordpress admin area. simply move the pages up and down with the buttons, and click Order Pages when you're done. It even supports ordering of subpages.

[Fold Page List](http://www.webspaceworks.com/resources/wordpress/30/) is another vital tool to anybody managing a site with more than a couple basic pages. If you have content categorized, and make use of subpages, this is a must-have. This plugin dynamically expands and collapses your subpage menus when you navigate to the parent page. That way, you don't have to have a page menu that extends endlessly down the page displaying all your subpages. Simply replace the Wordpress default page list function with the Fold Page List function, and you're good to go. It supports all the Wordpress parameters, and integrates seamlessly into your site. It also gives your site a much more professional feel.

These plugins will make site management much easier, as well as more user-friendly for visitors. Also, be sure to check back to the plugin pages for updates, as they add capability with newer versions of Wordpress.
