---
title: 'Make Links Look & Feel Like Buttons'
date: 2007-07-13T02:56:23.000Z
slug: make-links-look-feel-like-buttons
tags:
  - web design
---

Ever wanted to make your links look like buttons, to give your site a more desktop application-like feel? If you've tried making an HTML form just to navigate the user to a new page, you know how frustrating this can be. Luckily, CSS offers an easy way to do this.

First, let's write the HTML code we'll need for this:

```html
<html>
    <head>
        <title>Button Navigation</title>
    </head>
    <body>
        <div id="menu">
            <ul>
                <li><a href="#">Example 1</a></li>
                <li><a href="#">Example 2</a></li>
                <li><a href="#">Example 3</a></li>
            </ul>
        </div>
    </body>
</html>
```

Now that we have our HTML, let's go to our CSS document and make those links look button-like:

```css
#menu ul {
    margin: 0px;
    padding: 0px;
    padding-top: 5px;
    list-style: none;
}

#menu li {
    display: inline;
}

#menu a:link, #menu a:visited {
    margin-right: 2px;
    padding: 3px 10px 2px 10px;
    color: #000000;
    background-color: #CCCCCC;
    text-decoration: none;
    border-top: 1px solid #FFFFFF;
    border-left: 1px solid #FFFFFF;
    border-bottom: 1px solid #717171;
    border-right: 1px solid #717171;
}

#menu a:active {
    border-top: 1px solid #717171;
    border-left: 1px solid #717171;
    border-bottom: 1px solid #FFFFFF;
    border-right: 1px solid #FFFFFF;
}
```

This will give your link the appearance of a button on the page, and will make it appear that it is being pushed when you click on it.
