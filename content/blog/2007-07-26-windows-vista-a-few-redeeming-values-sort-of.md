---
title: 'Windows Vista: A Few Redeeming Values (Sort Of)'
date: 2007-07-26T05:34:14.000Z
slug: windows-vista-a-few-redeeming-values-sort-of
tags:
  - windows
  - windows vista
---

I recently got a new computer, which came with Windows Vista. I'm a Linux guy at heart, but like most of us, I'm forced to use Windows by the nature of my work. So, I decided to give Vista a shot, and see what it had to show for itself.

To be honest, I really haven't been impressed...at least not the way I should be after five years since the last version of the Windows operating system was released. I'm used to a release cycle of about six months, where even inside of that time frame improvements are still noticeable. So, after _five years_, a part of me (albeit, the part of me that doesn't live in reality) was expecting the holy grail of operating systems. Needless to say, that wasn't what was delivered.

For the average user, nice graphics and GUIs are all that's needed to have the appearance of a revolutionary new operating system. Throw in a few new menu layouts, and you not only have the appearance of a new design, but maybe even new functionality. Unfortunately, surface is about all that was added...and some higher processing power requirements. In order to enjoy all the new "benefits" of Vista, you have to install the DVD-sized operating system on a computer that has at least a 1GHz processor, 512 MB of RAM (1GB for anything other than Home Basic), and 15GB available on the hard drive (a 20GB hard drive is required for Home Basic; 40GB for everything else). How much is the user benefitting if the minimum requirements for their computer are so ridiculously high, just to run the operating system? It makes you wonder what all's running on the computer... especially when the actual operating system doesn't differ all that much from XP.

Yet, there are a few neat features that, while they themselves don't make the case for upgrading, are nice bonuses if you already have. Windows DVD Maker is very handy, as it allows you to create a DVD movie from any video file you have on your computer, in any format you have a codec for, complete with animated menus all with a few clicks of the mouse. That is a program I can get comfortable with, even if it is a 100% Microsoft product.

My new computer is also built to be much more multimedia-friendly, with media buttons right on the keyboard and even a remote control. So, naturally, the new version of Windows Media Center is a nice complement to these hardware features. It looks and feels a lot like a full-service media server you might find as part of a highly expensive home theater system. Granted, it really wouldn't do much for me if I didn't have the hardware add-ons to really make it convenient, but in its own right it does its job well.

For anybody who is a productivity user, and doesn't really do anything multimedia-related on their computer, such as most of the business world, perhaps, there really isn't much reason to spend the money to upgrade to Vista. If anything, the hardware and software compatibility issues that have surfaced since Vista's release make it more of an IT nightmare than a solution. For the personal user, though, it may have a few things to offer.

Overall, I've been underwhelmed with Vista. However, a few things have made me more hesitant to "upgrade" to XP. Hopefully by the time the next version of Windows comes out, they'll have fixed the problems with Vista. I'm not holding my breath, though.
