---
title: 'U.S. Coast Guard Insertion & Extraction Drill at MMA'
date: 2007-09-10T13:19:49.000Z
slug: us-coast-guard-insertion-extraction-drill-at-mma

tags:
  - us coast guard
  - mass maritime
  - ts enterprise
---

<video controls="">
  <source src="/files/blog/us-coast-guard-insertion-extraction-drill-at-mma/uscg-drill.mp4" type="video/mp4">
</video>

U.S. Coast Guard from Air Station Cape Cod ran a series of airborne insertion and extraction drills at Massachusetts Maritime Academy last week in conjunction with the T.S. Enterprise.