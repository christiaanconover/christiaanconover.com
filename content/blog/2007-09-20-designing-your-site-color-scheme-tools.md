---
title: 'Designing Your Site: Color Scheme Tools'
date: 2007-09-20T14:13:59.000Z
slug: designing-your-site-color-scheme-tools
tags:
  - web design
---

When designing a web site, one of the most important things to consider is the color scheme. Making use of color effectively is crucial in attracting visitors, and keeping them at your site. Obnoxious, clashing colors can drive people away faster than the page can even finish loading. A well-planned, appealing scheme can keep people at your site longer, and help them remember your site as a well-designed product.

Picking the right colors can be a little challenging, especially if you're not very familiar with the color wheel and the different properties of color. This is especially true when it comes to web design. Not all colors are good choices for use with web sites, so it's important not to use the ones that aren't. One of the best ways to avoid color problems is to use a color scheme generator. This will allow you to pick out the base color you'd like to use, from a choice of predesignated "web-safe" colors, and create a color scheme for you from those. Some are more powerful and flexible than others, so I'll give you a variety to choose from.

[Color Scheme Generator 2](http://wellstyled.com/tools/colorscheme2/index-en.html): Simple color scheme generator, using a color wheel and some basic color combination techniques, such as complimentary and triad. This is very useful if you already have a color in mind.

[Color Palette Generator](http://www.degraeve.com/color-palette/index.php): This allows you to create a color scheme from an image, such as a photograph. Simply provide the URL to the photo, and the software generates a color scheme based on the colors found in the image.

[Color Schemer](http://www.colorschemer.com/): Downloadable software to help you create color schemes. Similar to Color Scheme Generator 2, but a little more powerful. My favorite feature of this software is that in addition to picking out colors for you, it shows typical site layouts and how you could use the colors in the layouts.

[ColorZilla](http://www.iosart.com/firefox/colorzilla/): An extension for Firefox which helps you pick colors based on colors used on other sites. Includes a feature called Eyedropper, which will tell you the exact color code in RGB and Hex for any pixel on a page, even in an image, to help you determine exactly what color you'd like to use.

These tools can make color scheming much easier, and can help to improve the overall visual appeal of your site. Happy scheming!
