---
title: Photos of the T.S. Enterprise
date: 2007-09-27T04:12:53.000Z
slug: photos-of-the-ts-enterprise
tags:
  - mass maritime
  - photos
  - ts enterprise
---

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise01.jpg "T.S. Enterprise")

I took some pictures of the T.S. Enterprise at Massachusetts Maritime Academy last week, shown below. They were taken near sunset, to get the light coming in low across the port bow.

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise02.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise03.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise04.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise05.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise06.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise07.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise08.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise09.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise10.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise11.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise12.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise13.jpg "T.S. Enterprise")

![T.S. Enterprise](/files/blog/photos-of-the-ts-enterprise/enterprise14.jpg "T.S. Enterprise")
