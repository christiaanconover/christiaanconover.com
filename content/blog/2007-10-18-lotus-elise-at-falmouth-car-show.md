---
title: Lotus Elise at Falmouth Car Show
date: 2007-10-18T19:21:26.000Z
slug: lotus-elise-at-falmouth-car-show

tags:
  - lotus elise
  - falmouth
---

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise01.jpg "Lotus Elise")

I was at the British Car Show in Falmouth, MA a couple of weeks ago, where somebody had brought a Lotus Elise. Being one of my all-time favorite cars, I decided to take some pictures of it.

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise02.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise03.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise04.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise05.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise06.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise07.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise08.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise09.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise10.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise11.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise12.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise13.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise14.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise15.jpg "Lotus Elise")

![Lotus Elise](/files/blog/lotus-elise-at-falmouth-car-show/elise16.jpg "Lotus Elise")
