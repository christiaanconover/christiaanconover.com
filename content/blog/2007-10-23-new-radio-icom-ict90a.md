---
title: 'New Radio!'
date: 2007-10-23T22:46:32.000Z
slug: new-radio-icom-ict90a

tags:
  - amateur radio
---

I just got a new radio, an [Icom IC-T90A](http://icomamerica.com/en/products/amateur/handheld/t90a/default.aspx) handheld. My first impressions are very good, although I need to replace the stock antenna since it doesn't seem very powerful. I'm trying to hit repeaters near Buzzards Bay, MA but I'm not having any luck yet. If you're in the area, I'm listening on the [FARA](http://www.falara.org/) 2 meter repeater most of the time.
