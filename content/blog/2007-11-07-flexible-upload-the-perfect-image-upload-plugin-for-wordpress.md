---
title: 'Flexible Upload: The Perfect Image Upload Plugin for WordPress'
date: 2007-11-07T18:08:50.000Z
slug: flexible-upload-the-perfect-image-upload-plugin-for-wordpress

tags:
  - image resizing
  - photos
  - wordpress plugins
  - wordpress
---

I take a lot of pictures, and I like to upload them to my blog. My camera takes large resolution photos though, so preparing the pictures for upload can be annoying. I used to open up each photo I wanted to upload individually in GIMP, resize it, and once all the pictures were resized I'd upload them one by one. I then decided I would go looking for a batch resizer, and found a very basic one that got the job done. It still wasn't ideal since I was resizing and then uploading, adding (I felt) unnecessary steps to the process. Finally, today I decided I'd see if somebody had been smart enough to write a Wordpress plugin to resize images when you upload them. Lo and behold, somebody was, and the results more than fit the bill.

The plugin is called [Flexible Upload](http://blog.japonophile.com/flexible-upload/), and its name says it all. It will resize the image to whatever size you specify, and allows you to choose which dimension you want to base it off of. It also creates the thumbnail image to whatever size you want. This makes it very convenient for putting images inside posts, since you can have a thumbnail image that's larger than the one Wordpress creates by default, but still small enough that it'll load quickly.

My favorite feature of this plugin is the watermark feature. This allows you to insert your logo or some other identifying image in the image you are uploading, and allows you to choose where in the image to insert it. I have it set up to insert my logo in the bottom right corner of all pictures I upload. That way, all the images on my site will be visually identified with me, whether or not they're viewed on my site or elsewhere.

There are a number of other features for modifying the appearance and placement of images in posts, to streamline that process as well. It covers things like alignment, link target (especially useful if you use something like Lightbox), etc.

This plugin is a much needed addition to Wordpress. I've always thought that the upload system is a little sparse, especially when it comes to images. This fills in the gaps and more. This is a must-have plugin for anybody who uploads pictures to their blog on a regular basis.
