---
title: Visit to New York City
date: 2007-11-12T03:23:41.000Z
slug: nyc-nov2007

tags:
  - new york city
  - times square
  - world trade center
---

I went to New York City this weekend, and took a lot of pictures and video while I was there. I've posted some of them below:

{{< youtube 3dLGEUuUoQ4 >}}

<video controls="">
  <source src="/files/blog/nyc-nov2007/staten-island-ferry-docking.mp4" type="video/mp4">
</video>