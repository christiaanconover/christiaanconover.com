---
title: 'Skribit: The Next Level of User Interactivity in Blogging'
date: 2007-11-12T17:19:50.000Z
slug: skribit-the-next-level-of-user-interactivity-in-blogging

tags:
  - skribit
  - blogging
---

Skribit is a new blog widget created by [Startup Weekend Atlanta](http://atlanta.startupweekend.com/) to allow readers to suggest topics to bloggers to write about. You simply place it on your site, and readers can post ideas for topics. It's currently in closed beta (it was only created this weekend, brainstorm to product), but I'm anxious to be able to try it out.

Skribit was officially launched this morning, and as stated in the FAQ section of their site, it's still in "closed beta" so it's unavailable to the general public. However, you can create an account in preparation for its public release, or simply request to be contacted once it's available. I'm definitely planning on trying it out and most likely keeping it, so I've created an account.

What makes this widget even more appealing beyond its purpose, is the way that it's implemented: it has a management environment to back it up. Suggestions made to you, as well as suggestions you make, are all organized on the Skribit web site, making it easy to go back and see what's been suggested in both directions.

It also provides another way for bloggers to discover and connect with each other. Think of it as pre-commenting: rather than readers/bloggers waiting to see what a particular blogger has written and then commenting on that if they have thoughts about it, readers actually get to take a front seat role and help guide the blogger on what his or her readers really want to know about. Plus, the blogger can then discover other blogs they might like themselves, through the people who read their blog and suggest topics.

I'm personally really glad to see such a thing developed. I've thought this would be useful for a long time, but haven't had the resources to implement it, especially not to the scale that Skribit will (hopefully) reach. It looks like it will be a great product, and has the potential to become as much a part of blogging as commenting itself.
