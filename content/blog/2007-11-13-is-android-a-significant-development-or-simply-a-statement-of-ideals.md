---
title: 'Is Android a Significant Development, or Simply a Statement of Ideals?'
date: 2007-11-13T19:21:08.000Z
slug: is-android-a-significant-development-or-simply-a-statement-of-ideals
tags:
  - android
  - oha
---

In the past few days since the official [Android](http://www.openhandsetalliance.com/android_overview.html) announcement, the Internet has been exploding with speculation, debate, and excitement over the new OS' potential, and its significance in the mobile phone industry. Most people seem to think that it looks cool, and it does: touch screens, 3G support, freedom to create and add whatever apps you want...what's not to love? Plus, with the backing of [more than 30 major technology and telecom companies](http://www.openhandsetalliance.com/oha_members.html), it's sure to get good placement in the market. However, will the vision of a fully open source mobile OS be maintained once the software is put in the hands of the cell phone carriers?

The first looks at Android are appealing. The Android developers released a video demonstrating some of the basic functions of Android:

{{< youtube 1FJHYqE0RDg >}}

It has the desktop-like browsing, the 3G, the hardware acceleration support (in case you want to play Quake on your phone), most of which look similar in function and hardware interface to the iPhone. I know, everybody's comparing Android to the iPhone, so it sounds tired that I'm saying it too. But let's face it: the iPhone has revolutionized the way people think about their phone and the way they interact with it. If Android stands a chance at succeeding, it does to a certain degree have to mimic the iPhone, or create something so revolutionary in both form and function that it makes the iPhone seem obsolete. As much as I'm a fan of Google and their web innovations, and as much as I'm sick of hearing the praises sung about the iPhone and iPod, I just don't see Android busting the iPhone any time soon. All that being said, there is one major thing that Android has above the iPhone: open source, and the freedom to develop and install any apps you want.

The Open Handset Alliance states that Android will be fully open source, free to modify and develop for. However, it does not require carriers to keep the OS unlocked once it's installed on phones for their network. So at first glance it looks like the long-awaited answer to the monopoly cell phone carriers have on phone software, yet as it's written now, it will still allow carriers to keep a strangle hold on the mobile software industry.

The key, I think, will be the handsets it ends up on, to create the full hardware and software package. If Apple had simply created a new mobile OS and contracted with Blackberry to provide the hardware, I doubt the response would have been anything close to what it was. In order for Android to appeal to the general public beyond developers, I think it needs to have a recognizable hardware package as well; something that was developed specifically for Android to act as a "release device".

I'm very curious to see how Android's entry into the mobile phone market plays out, and I'm hopeful that it will create enough competition with the closed source developers that the carriers will realize the need for change. However, with the largest carriers in the U.S. (AT&T and Verizon) missing from the OHA and the iPhone still the epitome of next generation phones, I'm not sure how much of a threat it's going to be able to pose.