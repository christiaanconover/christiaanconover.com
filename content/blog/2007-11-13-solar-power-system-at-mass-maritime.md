---
title: Solar Power System at Mass Maritime
date: 2007-11-13T23:28:30.000Z
slug: solar-power-system-at-mass-maritime
image:
  src: /files/blog/solar-power-system-at-mass-maritime/03.jpg
  alt: Solar panels at Mass Maritime

tags:
  - alternative energy
  - mass maritime
  - solar power
---

![Solar panels on the roof of the MMA dorms](/files/blog/solar-power-system-at-mass-maritime/01.jpg)

The Mass Maritime Green Club took a tour today of the solar power system the school has installed. The system cost about $1 million to purchase and install, and will provide a peak of 81kW to the school. It should complement nicely the 660kW wind turbine that the school put up in 2006.

One of the members of the club put some pictures of the solar installation online, which you can check out below.

![Solar panels and the wind turbine](/files/blog/solar-power-system-at-mass-maritime/02.jpg)

![T.S. Enterprise behind the solar array](/files/blog/solar-power-system-at-mass-maritime/03.jpg)

![Explaining the solar array](/files/blog/solar-power-system-at-mass-maritime/04.jpg) A facilities engineer at MMA explains to cadets how the solar array works

![Solar panels at sunset](/files/blog/solar-power-system-at-mass-maritime/05.jpg)

![Solar panels at sunset](/files/blog/solar-power-system-at-mass-maritime/06.jpg)

![Tug and barge beyond the solar array](/files/blog/solar-power-system-at-mass-maritime/07.jpg)

![Explaining the solar array](/files/blog/solar-power-system-at-mass-maritime/08.jpg)

![Solar panels at sunset](/files/blog/solar-power-system-at-mass-maritime/09.jpg)

![Cadets get a good look at the solar panels](/files/blog/solar-power-system-at-mass-maritime/10.jpg)

![Solar panel grid up close](/files/blog/solar-power-system-at-mass-maritime/11.jpg)

![Solar panels at sunset](/files/blog/solar-power-system-at-mass-maritime/12.jpg)

![Photovoltaic cells up close](/files/blog/solar-power-system-at-mass-maritime/13.jpg)

![MMA's Green Club on the dorm roof with the solar array](/files/blog/solar-power-system-at-mass-maritime/14.jpg) MMA's Green Club on the dorm roof with the solar array
