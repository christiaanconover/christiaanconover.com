---
title: Sea Term Blog
date: 2007-11-28T22:31:05.000Z
slug: sea-term-blog

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - ts enterprise
---

In January and February I will be shipping out on the T.S. Enterprise, Mass Maritime's training ship, for Sea Term. Sea Term is Mass Maritime's hands-on training period, where cadets gain practical experience in working and living on board a commercial ship. Depending on your major, you will do between one and three Sea Terms. For example, a Marine Transportation (deck license) major will do three Sea Terms, but an Emergency Management major will do only one.

This year, we will be traveling to the Caribbean and the South Pacific. We will be going through the Panama Canal, and stopping for a few days in Balboa, Panama. From there we will cross the Equator in the Pacific Ocean and perform the shellbacking ceremony. We will then travel to Costa Rica, where we will spend a few days in port. We will leave from there to go to Aruba, going back through the Panama Canal on our way there. After Aruba, we will make the journey back to Buzzards Bay, MA at which point Sea Term will be over.

I plan to use this blog as both a record of the experiences I'll have while on Sea Term, as well as a way to share my experiences with readers, and keep them updated on our progress. I'll post information prior to departure in January, and plan to post updates at least every other day during Sea Term. Please check back regularly for new information, or subscribe to my RSS feed.
