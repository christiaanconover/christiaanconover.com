---
title: Sea Term Round Table
date: 2007-11-30T00:12:23.000Z
slug: sea-term-round-table

tags:
  - logistics
  - mass maritime
  - sea term
  - sea term 2008
---

Today the freshmen had a meeting with Capt. Rozak, Capt. Bushy and the other staff and cadet officers involved with Sea Term. They addressed the key times we need to know, making sure we bring important documents like a passport and school ID, and the basic rules and regulations. The medical staff talked about how to handle medication and injuries at sea. We also got to talk about some of the details of the sweet trips and excursions we'll be able to take while in port, such as white water rafting in Costa Rica and canopy tours of the rain forest.

As of today we have about 5 weeks left until we ship out, so there's a lot to do in that time. Beyond finishing up our first semester and taking final exams, we have to clean the dorms to an Admirals' inspection-like standard, move the first of our gear onto the ship, get all remaining supplies we need, pack up and report in on January 6th. It's a lot to do, but I'm sure it will all be worth it!
