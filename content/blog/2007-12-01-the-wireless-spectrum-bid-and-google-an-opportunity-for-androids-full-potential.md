---
title: "The Wireless Spectrum Bid and Google – An Opportunity for Android's Full Potential?"
date: 2007-12-01T03:16:46.000Z
slug: the-wireless-spectrum-bid-and-google-an-opportunity-for-androids-full-potential

tags:
  - 700 mhz
  - android
  - fcc
  - google
---

Today Google officially announced that it will be bidding for wireless spectrum in an FCC auction in January. The 700 MHz spectrum is going to be up for auction, and if Google is able to snag a part of it, it could mean a truly open wireless network. It doesn't seem to be any coincidence that this announcement comes right on the heels of [Google's Android announcement](/blog/is-android-a-significant-development-or-simply-a-statement-of-ideals/). With the development of a mobile OS created for open development and communication, the ability to back it with an open wireless network would be a huge boost for Android's success, and make Google even more appealing as a wireless provider.

Google will most likely be vying for spectrum against big names like Verizon Wireless and AT&T, whose experience in the industry puts Google to shame. However, it's not that often that the opportunity for true change in a closed, tightly controlled industry - like the wireless telecom industry - is presented. Google has the financial resources to effectively compete against the existing carriers, and has a reputation among consumers for providing easy to use, convenient, free services that work well. If they are able to gain a portion of the wireless spectrum, they stand to present a legitimate threat to the traditional structure of wireless carriers.

Android is an awesome concept, and I hope that it succeeds. However, as I said when that announcement was made, the major roadblock for Android's success is the wireless carriers, and their ability to lock down the OS once it hits their network. If Google is the network, and allows Android to be used the way they promote it, they could have a strong recipe for success, and - dare I say - revolution in the industry.

I still think, however, that they'll need a "GPhone" to release with Android, as well as if and when their wireless network is open for business. What do you think?
