---
title: "Verizon's Open Network Has Promise, but Initially Doesn’t Seem to Change Much"
date: 2007-12-04T20:01:11.000Z
slug: verizons-open-network-has-promise-but-initially-doesnt-seem-to-change-much

tags:
  - 700 mhz
  - fcc
  - google
  - verizon
---

Verizon's recent announcement to open their network to all CDMA handsets, as well as applications, marks a major transition in the mindset of wireless carriers in the U.S. and presents a great opportunity for innovative development. With the option for consumers to use any handset they like with Verizon's service, manufacturers and developers will have fewer hoops to jump through when it comes to bringing their product into the market mainstream. Plus, people won't be forced to use the programs that Verizon bundles with the phones they sell, or purchase ones only pre-approved by Verizon. In theory, everything should just work together, on one big happy network. In theory.

However, [as reported on TechCrunch](http://www.techcrunch.com/2007/11/28/verizons-open-network-will-really-be-two-tiered/), the open network policy will most likely result in a "two-tiered" network: one for customers who went through all the existing channels to become Verizon subscribers, and one for people who wanted to bring their own equipment and applications to the table. Verizon isn't known for giving their customers much control, so it's not surprising that they're not suddenly giving it away in one fell swoop.

However, the upcoming FCC spectrum auction [may require them to do just that](http://arstechnica.com/news.ars/post/20070731-fcc-sets-700mhz-auction-rules-limited-open-access-no-wholesale-requirement.html). Part of the requirement for being eligible to bid on the 700 MHz spectrum is to allow open devices and open applications. Verizon seems to be trying to show they're willing to comply ahead of time, in an effort to demonstrate that they're really serious about the auction and getting a piece of the newly available spectrum. This auction is a major event in the wireless telecommunications industry, since it will significantly affect the competition in the market. Hopefully, the result of this auction will be a viable alternative to Cable and DSL broadband services. Companies that can buy a piece of it will be able to be competitive, and those that can't won't be able to keep up. Verizon knows this, and following their [failed efforts to change the FCC's policy](http://arstechnica.com/news.ars/post/20071024-let-open-access-reign-verizon-relents-on-legal-challenge-to-fcc.html), they are willing to sacrifice a little to be eligible for the auction.

Regardless of the reason, it's a step in the right direction, and Verizon's all but permanently made the decision to open up, pending the results of the auction in January. Assuming they are able to grab a chunk of the spectrum, they'll have to support open devices and applications as long as they own it. Consumers may finally have a little freedom to operate if they subscribe to the largest wireless carrier in the country. Until that new spectrum becomes available to consumers, however, this announcement seems to be a lot more sound than substance.
