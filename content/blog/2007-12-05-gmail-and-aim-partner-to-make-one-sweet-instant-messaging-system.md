---
title: GMail and AIM Partner to Make One Sweet Instant Messaging System
date: 2007-12-05T22:49:07.000Z
slug: gmail-and-aim-partner-to-make-one-sweet-instant-messaging-system

tags:
  - aim
  - chat
  - gmail
  - google
---

[Google announced yesterday](http://googleblog.blogspot.com/2007/12/gmail-3-aim.html) that they have partnered with AOL to allow GMail users to sign in to their AIM accounts in the GMail Talk interface, and chat with them right from there. I decided to try it out, and I have to say, it's wicked cool.

Once you sign in to your GMail account, and are brought to your inbox, you simply go over to the Google Chat buddy list and click Options, then sign in to your AIM account. It prompts you for your AIM screen name and password, and then you're good to go. It shows status the same way that it does for Google Talk buddies, and displays aliases as well as away messages right on the buddy list. I have everyone on my buddy list aliased for easy reference, so I was pumped to find that out! The chat window pops up the same way, either within the GMail window you have open, or optionally as a popup window.

Google added in features like chat history from the Google Talk system for use with AIM, so it almost makes you want to just use GMail for AIM all the time. Even if you continue to use the regular AIM client, or something like [Pidgin](http://www.pidgin.im/) as your primary instant messaging client, it sure is a huge improvement over AIM Express for web-based AIM access. I, for one, give it two thumbs up...right after I finish using them for my conversation.
