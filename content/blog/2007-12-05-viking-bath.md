---
title: 'VIKING BATH!!!!!!'
date: 2007-12-05T11:18:33.000Z
slug: viking-bath

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - viking bath
---

This morning at 0400 we were woken up Orientation-style for the Viking Bath, a rite of passage for freshmen. Everyone gets into PT gear, and after doing about 30 minutes of intense warm-up in the passageways, we all run down to Cadet Beach and jump into Buttermilk Bay! This is considered a "Recognition" for being able to go on Sea Term, so now we've earned our spot on the ship.

Right now it's about 0615, and the adrenaline has worn off. I'm a little chilly at this point, but really not too bad overall. The water wasn't nearly as cold as I expected (only 41 degrees). Best of all: we got inspections off as a result of doing it! I'm not sure if there were any pictures taken, but if there were I'll be sure to post/link to them here. GO BUCS!!!
