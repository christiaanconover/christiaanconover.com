---
title: First Snow of the Season
date: 2007-12-15T12:47:19.000Z
slug: first-snow-of-the-season

tags:
  - mass maritime
  - snow
---

MMA got its first snow of the season on Thursday, resulting in afternoon classes being cancelled. I took some pictures of the snow-covered campus, which I've posted below.

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/01.jpg)

![Cadets in the snow at Mass Maritime](/files/blog/first-snow-of-the-season/02.jpg)

![T.S. Enterprise in the snow](/files/blog/first-snow-of-the-season/03.jpg)

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/04.jpg)

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/05.jpg)

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/06.jpg)

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/07.jpg)

![Cadets in the snow at Mass Maritime](/files/blog/first-snow-of-the-season/08.jpg)

![Regimental Snowmen](/files/blog/first-snow-of-the-season/09.jpg) The Regimental Staff, as snowmen

![T.S. Enterprise in the snow](/files/blog/first-snow-of-the-season/10.jpg)

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/11.jpg)

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/12.jpg)

![Snow at Mass Maritime](/files/blog/first-snow-of-the-season/13.jpg)
