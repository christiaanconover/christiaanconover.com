---
title: End of the Semester Means Fewer Posts
date: 2007-12-16T06:36:29.000Z
slug: end-of-the-semester-means-fewer-posts

tags:
  - final exams
  - mass maritime
---

This coming week officially starts final exams here at Mass Maritime. However, many teachers have decided to either give their final exam early, to alleviate the stress of having all finals in one week, or to give the last test of the semester. As a result, the past two weeks have found me busy studying for tests and finishing projects. All this has resulted in a lack of updates to my blog, due to a lack of time both to find new topics to write about, and to actually write about them. Once the semester ends, I'm planning on getting back into the updates, and plan to publish a lot of updates before I ship out in January and February. I may be able to make a few updates while at sea, but most likely my efforts will be focused more on my [Sea Term blog](/tags/sea-term/). Stay tuned for new posts after this coming week.