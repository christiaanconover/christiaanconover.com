---
title: Port Change in Costa Rica
date: 2007-12-19T16:00:04.000Z
slug: port-change-in-costa-rica

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

This morning Captain Bushy notified us that we have changed the port we will be visiting in Costa Rica. We will now be at the port of Golfito, and will be at a dock instead of anchored off shore. This should make it much easier for us to go ashore and return to the ship. The schedule will remain the same, it will simply be a different location.
