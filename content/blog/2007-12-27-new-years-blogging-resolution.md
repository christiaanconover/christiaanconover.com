---
title: "New Year's Blogging Resolution"
date: 2007-12-27T06:40:03.000Z
slug: new-years-blogging-resolution

tags:
  - blogging
---

I have decided that in the year 2008, I'm really going to make my blog a priority. I'm resolving to add a new post at least every other day, and will hopefully be able to make it every day. That's why between now and January 1st, I'm going to devote time to coming up with new topics and post ideas to provide fresh content to readers on a regular, frequent basis. I'm planning to make some posts between now and January 1st as well, so don't worry, you won't be left hanging for a week. Not anymore, at least.

What do you plan to do differently with your blog in the coming year?
