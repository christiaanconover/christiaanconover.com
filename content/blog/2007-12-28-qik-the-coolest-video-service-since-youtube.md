---
title: 'Qik: The Coolest Video Service Since YouTube'
date: 2007-12-28T17:54:02.000Z
slug: qik-the-coolest-video-service-since-youtube

tags:
  - android
  - iphone
  - mobile
  - qik
  - startup
  - youtube
---

I just [discovered](http://scobleizer.com/2007/12/27/great-tips-for-startups/) [Qik](http://www.qik.com), and it makes me want a phone with a data plan even more. Ever since I got a camera phone that could record video, I've wanted to find a way to have live video streams, or ultimately have video calling. Well, Qik has finally answered the call for live video streaming.

Qik is a new service that allows you to stream live video from your phone to viewers around the world via the Internet. It uses software installed on your phone to stream the video captured by your phone's camera to the Qik servers, where is provides it to other viewers, as well as recording it for you to view and use later. They currently only support a handful of phones (all Nokia), but they say on their web site that they are "constantly adding support for new phones" so hopefully that list will expand to other manufacturers soon. I smell an awesome app for the iPhone, or for [Android](/blog/is-android-a-significant-development-or-simply-a-statement-of-ideals/)...

This definitely looks like a service to watch, I can definitely see it becoming the Twitter of video.
