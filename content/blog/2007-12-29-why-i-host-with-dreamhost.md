---
title: Why I Host with Dreamhost
date: 2007-12-29T16:00:41.000Z
slug: why-i-host-with-dreamhost

tags:
  - alternative energy
  - dreamhost
  - hosting
---

I switched hosting companies at the beginning of December, because I needed hosting with a little more power and flexibility than I had. I looked at some of the hosts listed on the Wordpress site, which all seemed like good options. Then [a friend of mine](http://www.johncardarelli.com/) recommended [Dreamhost](http://www.dreamhost.com/r.cgi?372907), so looked it up, and was impressed by just the numbers I saw:

- 500 GB disk space
- 5 TB bandwidth
- $5.95/month

I looked a little deeper, and found all the standard stuff that the other big guys offer - unlimited email address, unlimited hosted domains, etc. - along with some other features I didn't see anywhere else like automatically increasing disk space & bandwidth limits, and streaming & chat server support. I found out that the $5.95/month price only applies if you sign up for 10 years of hosting at once, but the 1 year price isn't too bad, and if you use a promo code you can get a discount off the first billing cycle. I've made a promo code for my readers to get $50 off, which is the maximum amount Dreamhost now allows; the code is **cconover50**.

There are a few things about [Dreamhost](http://www.dreamhost.com/r.cgi?372907) as a company that makes me happy to give them my business as well. First off, they are a small business, owned by the employees. Having been self-employed with a small business of my own, I like to support other small businesses when I can. Also, they are a green company which means that they are [carbon-neutral](http://en.wikipedia.org/wiki/Carbon_neutral). I am a big propoent of alternative energy and environmentally friendly practices, so I'm glad to support a company that believes in it too.

I know this sounds like a paid advertisement for Dreamhost, but I am just a really satisfied customer who thought he'd share his experience with other webmasters/bloggers looking for good hosting.

Who do you host with, and what do you think of them?
