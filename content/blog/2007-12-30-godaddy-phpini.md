---
title: Setting Up a Custom PHP.INI File on GoDaddy Hosting
date: 2007-12-30T18:21:47.000Z
slug: godaddy-phpini

tags:
  - godaddy
  - php
  - wordpress
---

**I am [no longer using GoDaddy hosting](/blog/media-temple-service-rocks/) and as such have not maintained this file to account for changes to GoDaddy's hosting platform. I've taken the file down, to avoid any errors resulting from using an out-of-date configuration.**

I've gotten a number of e-mails in response to a comment I made on a post about installing Wordpress on GoDaddy hosting. Given the response I've gotten, I thought it was a good idea to provide my PHP.INI file that I've set up to other people looking for this. You can download it below.

The file is a complete PHP.INI file with all the configuration options you'd find in a full PHP installation. I've changed a few key settings that I found most important, such as max upload size and post size, allowing for high resolution photos to be uploaded.

If you have questions or suggestions about other ways to customize this file further, please leave a comment on this post. Thanks.
