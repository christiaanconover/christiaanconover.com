---
title: 'Sea Term Day 1: Reporting In'
date: 2008-01-07T03:23:47.000Z
slug: sea-term-day-1-reporting-in
image:
  src: /files/blog/sea-term-day-1-reporting-in/01.jpg
  alt: My rack on the T.S. Enterprise

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Today has been a very busy day. The morning started at around 7:00 when people started arriving in Harrington Hall to put their bags down and check in. We had a meeting in Admirals' Hall until about 10:00 reiterating the basic dos and don'ts of the ship. We then broke off into our divisions, and headed over to the ship with our bags. We unpacked throughout the course of the day between other activities, such as lunch, muster, and the evacuation drill we did this afternoon.

Unpacking was a true test of our ability to work together. In a space large enough for two people to somewhat comfortably work, there were six people unpacking their bags and stowing their clothes and other items under their rack, making their rack, all while having to avoid stepping on each other's things - or get stepped on by somebody above you making a top rack.

The Internet is still connected to the ship, so free e-mail is still available. I'll be posting pictures during the course of the week as long as we still have Internet onboard. I've included a few pictures of my hold below.

Tomorrow we will do more drills, and will be loading the ship. Right now, however, I'm going to go to sleep for the first night of many that I'll be spending in my new ~~matchbox~~ rack.

![My rack on the ship](/files/blog/sea-term-day-1-reporting-in/01.jpg) My rack on the ship

![Storage under my rack](/files/blog/sea-term-day-1-reporting-in/02.jpg) Storage under my rack

![My aisle in the hold](/files/blog/sea-term-day-1-reporting-in/03.jpg) My aisle in the hold
