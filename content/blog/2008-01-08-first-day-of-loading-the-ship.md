---
title: First Day of Loading the Ship
date: 2008-01-08T02:48:56.000Z
slug: first-day-of-loading-the-ship

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - ship loading
---

Today we spent the day loading food onto the ship. We need to load enough food for 590 people for our entire time at sea, so needless to say it's an immense and time-consuming task. I was told by a fellow cadet that we loaded 134 palettes of food just today. We will be continuing this task over the next couple of days.

I discovered the benefit of having watch between 0000 and 0800\. If you have watch during that time, you get to go back to bed after morning muster at 0745 and have no responsibilities until the afternoon muster at 1300\. I got to get another solid four hours of sleep this morning, which felt awesome.

There isn't much very interesting happening on board this week, but I can already tell that this is going to be really cool.

![Wrapping a palette to be hoisted](/files/blog/first-day-of-loading-the-ship/01.jpg)

![Palettes waiting to be hoisted](/files/blog/first-day-of-loading-the-ship/02.jpg)

![Craning a palette aboard](/files/blog/first-day-of-loading-the-ship/03.jpg)
