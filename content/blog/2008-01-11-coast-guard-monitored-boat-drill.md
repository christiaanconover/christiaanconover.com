---
title: Coast Guard-Monitored Boat Drill
date: 2008-01-11T17:46:10.000Z
slug: coast-guard-monitored-boat-drill

tags:
  - departure
  - mass maritime
  - sea term
  - sea term 2008
  - us coast guard
---

This morning at 0900 we had the official Coast Guard-inspected fire & emergency and abandon ship drills. It took about 90 minutes, and appeared to run smoothly, so we now have the official seal of approval to ship out! We have less than 24 hours until we depart, though you might not know it from the atmosphere on board. Everyone is continuing to go about business as usual, and I think it's a testament to the planning and execution of pre-departure tasks that nobody seems to be feeling rushed or pressed for time. In fact, the fourth class cadets have another shoreside meeting in Admirals' Hall this afternoon, and then the rest of the afternoon is free!

The weather here is pretty poor today; it rained for most of the boat drills. Right now it's incredibly windy and the rain is still coming down in fits. Luckily, the forecast for tomorrow is good, calling for sun with some breeze. We'll be manning the rails tomorrow morning when we depart, so it's especially nice that we won't be soaked!

I will try to post some more pictures later on tonight while we still have Internet on board the ship. Right now, I have to head to our last meeting on campus until we come back from Sea Term!
