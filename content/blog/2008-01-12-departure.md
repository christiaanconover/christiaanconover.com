---
title: 'Departure!'
date: 2008-01-12T19:20:04.000Z
slug: departure

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Today at 1150 we departed the state pier in Buzzards Bay and started our incredible journey. Family and friends turned out by the hundreds to see us off, carrying banners, blowing horns, and cheering as the ship's whistle blew to announce the beginning of our trip. There was plenty of excitement on deck, as we anxiously awaited the moment when the ship would start moving and we would finally be on our way. The tugs followed us to the mouth of Buzzards Bay, and then we were on our own! After a week of sitting next to the pier in anticipation, we're finally under way! It's neat to feel the ship's motion as I sit in the computer lab writing this entry. I am watching Cape Cod fade out of sight as I look out the window. We were blessed with beautiful weather this morning allowing us to man the rails and watch the entire process of casting off and maneuvering away from the pier. The sun continues to shine this afternoon, but apparently we are expected to encounter inclement weather tonight. That doesn't seem to be concerning the cadets much though, since the Patriots are playing tonight. I have watch again this afternoon at 1530 in the engine room, which should be exciting since the engine room is now fully operational. Then it's Sunday at Sea tomorrow, so I'll have an opportunity to rest after watch and departure procedures. I have many pictures of departure that I'll post once we get to Panama. Right now, however, it's off to maintenance muster.
