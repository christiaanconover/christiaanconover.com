---
title: The Last Night in Buzzards Bay
date: 2008-01-12T04:09:32.000Z
slug: the-last-night-in-buzzards-bay

image:
  src: /files/blog/the-last-night-in-buzzards-bay/dsc00984.jpg
  alt: USCG Auxiliary knot board
tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Tonight marks the last night we'll be in Buzzards Bay for the next six weeks. It's been pretty quiet on board, not much different from any other night we've had so far. The holds were filled with the usual crowd of people and computers - watching movies, playing games, listening to music. There is an air of excitement building among the cadets, especially with the freshmen. In every hold you can hear people discussing their expectations for going out to sea, and experiences they hope to have. A YouTube video posted by a cadet from a previous Sea Term has spurred many conversations over the past week. I'm hoping that we encounter similar weather on our trip! For anyone unfamiliar with the video, here it is:

{{< youtube VKkL2FAAcfQ >}}

The computer lab is pretty full as I'm writing this. Everyone is getting in their last dose of Facebook, AIM, and personal e-mail for the next six weeks (unless, of course, they find an Internet café in port when we arrive). I find it a little funny that one cadet is using his computer time to play online games. There are last minute sign-ups for SeaWave, and advice flying in all directions on ways to save money using it.

First Division takes the watch tonight, which marks the start of at-sea watches. I have watch 0400-0800 and 1600-2000 in the engine room, so luckily I'll be able to be on deck when we shove off.

Tomorrow I'll be writing the day's post from somewhere off the coast on the way to Norfolk. Unfortunately, I won't be able to post any pictures of our departure due to the cost of sending pictures through SeaWave, but I'll be sure to write a detailed description of it, and perhaps add some comments from fellow cadets. Just a little over 12 hours to go!

![4/C Casey](/files/blog/the-last-night-in-buzzards-bay/dsc00994.jpg)

![Watertight door](/files/blog/the-last-night-in-buzzards-bay/dsc00992.jpg)

![Cadet berthing head](/files/blog/the-last-night-in-buzzards-bay/dsc00989.jpg)

![Cadets in a study area](/files/blog/the-last-night-in-buzzards-bay/dsc00987.jpg)

![Knot board](/files/blog/the-last-night-in-buzzards-bay/dsc00984.jpg)