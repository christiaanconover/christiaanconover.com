---
title: The Night Life at Sea; Rougher Weather off the Southern U.S.
date: 2008-01-16T03:00:06.000Z
slug: the-night-life-at-sea-rougher-weather-off-the-southern-us

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Being on board a ship with as many people as we have is similar to being in a big city. There's always something happening, you can always get something to eat, and its never really quiet. The population density inside the holds is greater than some New York subway cars. At night, though, the ship becomes a completely different place. Most people are sleeping, and the only people who are awake are the night shift watch standers. In the mess deck you can usually find a small group who have recently gotten off watch, getting a snack before bed while watching late night television or comparing watch experiences. I've found falling asleep on board the ship to be easier than on shore, not simply because were working a lot harder! The motion of the ship helps with sleep, giving the feeling, as my dad put it, of being in a huge rocking chair. I feel better rested here than I do when Im back at school! We left Norfolk around 0630 this morning. I was sleeping after getting off watch at 0330, so I wasn't on deck to see us depart. I went straight to watch when I did wake up, so I did not have an opportunity to go out on deck until 1530\. When I did get out, the ocean was definitely rougher than it had been the night we left Buzzards Bay. Bigger waves, much higher winds, and lots of spray as the waves crashed against the side of the ship. A few people taking pictures got soaked as they leaned over the side and were met with a large blast of water to the face!

Quick fact: we took on 9500 barrels of fuel in Norfolk, which equates to 522,500 gallons, costing about $890,000!
