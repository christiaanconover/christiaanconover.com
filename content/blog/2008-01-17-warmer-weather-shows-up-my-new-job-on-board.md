---
title: Warmer Weather Shows Up; My New Job on Board
date: 2008-01-17T14:20:06.000Z
slug: warmer-weather-shows-up-my-new-job-on-board

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

This morning we found ourselves in much bluer, warmer water with an air temperature that goes nicely with shorts and a T-shirt. The water has been much calmer today than it was last night, which made working much easier today. Speaking of working, I have taken on a new job on board the ship. I am now Chief Laffan's IT guy, which started immediately as I spent my time before and after watch today working on the ship's network. It will mean a lot more work, but it's something I enjoy doing so it all works out. I have had many of my classmates mention to me that their parents are reading my blog and enjoying it, and I thank you all for being readers. I plan to have a guest writer from time to time, and have already had a number of cadets express interest in doing this. So keep an eye out, it might just be your cadet writing something on here!
