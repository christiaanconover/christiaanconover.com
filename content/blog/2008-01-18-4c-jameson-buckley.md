---
title: 4/C Jameson Buckley
date: 2008-01-18T01:20:06.000Z
slug: 4c-jameson-buckley

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

As I mentioned in yesterday's post, I'll be periodically publishing the thoughts and comments of fellow cadets on board. Tonight 4/C Buckley of 3rd Division is featured.

--------------------------------------------------------------------------------

It's Thursday January 17th 2008, and I just completed my first day of Engineering lab classes. We first went down to the tank top level of the stern of our Ship, which is where the Engineering Lab is. I happened to be in Group A, lucky for me. We had class up until lunch. Our classes included a very in-depth look at the Ship's boiler, like how the fuel runs into boiler to heat the water, it circulates it up as steam into the Dry Pipe where it gets superheated and pushed throughout the main steam cyle of the USTS Enterprise. Next after a quick fifteen minute break the class got together for our last class before lunch and for the day as well. It was a review of the four different cycles throughout the engine room we will have to know for our enginneering test at the end of Sea Term. The first we went over was the lube oil system. It was a complex system that runs lube oil all over the running parts of our engine to keep it from causing friction. Next he reviewed the main steam cycle which we had to know by heart for all of orientation, so this was an easy one for us. Next we review the fuel oil system which carries the fuel to the boiler to ignite and heat the water in the system. There are fourth classmen outside both boilers watching the fire at all times. Last was the Main Circ system, which confused me greatly but we will have time to trace it out with Seniors while we are in the engine room.

After grabbing lunch and a quick nap, we headed back down the Lab space to complete two labs. The first was to create an extension cord and see when we plugged it in if it worked, if not we had to go back and rewire it. After that lab we then went to another group of seniors teaching us the different types of measuring tools we'd use if we became engineers. After some explantion we had a quick exercise of practicing our knowledge. After finishing this we ended for the day, cleaned up the area, and left.

Thanks to Conover for letting me write this and love ya mom.

--------------------------------------------------------------------------------

Thank you Buckley for adding your comments. I'll try to add a different cadet's thoughts each week.
