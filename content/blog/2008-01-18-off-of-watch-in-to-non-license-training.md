---
title: 'Off of Watch, In to Non-License Training'
date: 2008-01-18T01:20:07.000Z
slug: off-of-watch-in-to-non-license-training

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Today was the first day of the new division rotation. On board the ship, each division has a general duty assignment that changes every three days. On any given day there's a division on watch, a division on maintenance, a division in training (license), and an alternate division, which right now means we're doing the training for non-license majors. Division I is currently the alternate division, and my group within the division is doing MSEP training. Today we got an introduction into what MSEP (Marine Safety & Environmental Protection) is, the curriculum, and the kinds of jobs you can get from it. We learned about how to test for various pollutants and conditions in air and water supplies, and about how waste management is done on board the ship. The weather is absolutely amazing now. It's a nice change for the temperature to be warmer outside than it is inside! Today around noon we passed just east (about 15 nm) of San Salvador. We're actually making such good progress that we've slowed down because we're ahead of schedule! If you haven't already, be sure to take a look at the guest post tonight from a classmate of mine. I'm headed back out on deck to enjoy the awesome weather and view of the night sky.
