---
title: Arrival at the Panama Canal
date: 2008-01-22T02:30:05.000Z
slug: arrival-at-the-panama-canal

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - panama canal
---

This afternoon we arrived at the Panama Canal. We have to wait until tomorrow night to go through, so we're going to be hovering outside until then. We're doing anchoring and turning drills right now, so even though we've arrived at our destination, the ship is still moving. Tomorrow maintenance should be easier since the ship won't be rolling as much. I'm planning on being out on deck as much as possible while we're going through the canal, so I can take it all in-and get tons of pictures!
