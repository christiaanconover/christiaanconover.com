---
title: Waiting Outside the Canal
date: 2008-01-23T10:10:06.000Z
slug: waiting-outside-the-canal

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - panama canal
---

We spent today anchored in the waiting area right outside the entrance to the canal. We've been in the company of about 30 ships of all shapes and sizes, from ROROs to small sailboats. I was mistaken in my post yesterday regarding the schedule for going through the canal. We are weighing anchor between 1630 and 1700\. We are expected to get through the first three locks by about 1900\. We should exit the canal into the Pacific around 0130 on Thursday, and be fully docked by about 0300\. I received an e-mail from a parent today wondering about the schedule so she could record our passage through the Gluton locks, so it should be between 1630 and 1900.
