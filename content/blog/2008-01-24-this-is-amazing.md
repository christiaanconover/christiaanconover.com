---
title: 'This Is AMAZING!!!!!'
date: 2008-01-24T08:00:11.000Z
slug: this-is-amazing

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - panama canal
---

It's about 2315 as I'm writing this, and we're currently passing through the part of the Panama Canal known as "The Cut" - the man-made portion after the Gatun Lake. We finished going through the Gatun locks around 2100 and have been steaming along ever since. We couldn't have asked for better weather! The temperature and humidity are perfect, there isn't a cloud in the sky so the stars are amazing and the moon is almost full, and there's no wind. All you hear is the sound of the water gently lapping against the hull as we cruise along at 5 knots, and the quiet conversations of people amazed at what they're seeing and experiencing. I've been on the bow with Chief Mate Taddia and a few fellow cadets talking about the canal, and listening to the radio traffic between the pilots. This is absolutely incredible! Words cannot do this journey justice. I'll post more about it after we arrive in port.
