---
title: 'Farewell Panama!'
date: 2008-01-28T08:00:07.000Z
slug: farewell-panama

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - panama
---

Yesterday was our last day in port in Panama, and what a time it was! The past four days have been incredible. The weather alone has been a nice treat, and we've had tons of fun in and around Panama City. I spent a lot of time in a part of town known as the "Causeway", a more upscale peninsula a few miles long lined with shops and restaurants, and many Maritime cadets. The street is lined with brick sidewalks and palm trees, and there are marinas on both sides filled with beautiful boats and yachts. Everywhere you go you can hear lively music and parties. It was definitely one of the more popular parts of town with the cadets! The highlight of my time in port was meeting up with the Panamanian Maritime Academy cadet who was in my company during Orientation. We happened to come back to the ship in the early afternoon and found him waiting outside the gate to the pier! We spent the rest of the afternoon with him, which was wicked cool since we hadn't seen him since the end of August. On an unrelated note, I received an e-mail through this site a few days ago from a girl named Amanda that read as follows:

> Hey! I hope you're enjoying all your time at sea! I was there the day the boat left...and I was wishing I was on it soooo bad! I'm only a freshman in high school and I have decided to devote my next three and a half years to getting into MMA! I want to get in sooo bad... well hope the rest of the trip goes well.

I thought it was a cool testament to the affect Sea Term has on people both on the ship and off. I have training the next three days. I believe I'll be doing engineer training, so I'll update on that experience later today.
