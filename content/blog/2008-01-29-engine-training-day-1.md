---
title: Engine Training – Day 1
date: 2008-01-29T14:20:17.000Z
slug: engine-training-day-1

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Yesterday was my first day of Engine training. My group spent the morning in the classroom reviewing boiler operations and the systems within the engine room that we need to know for our exam on Febrary 4th. In the afternoon, we were doing labs. We have a list of activities we need to complete during Engine training that are spread out over the three days. Yesterday my group learned how to sweat copper pipe, join lengths of soft copper pipe using flaring and compression fittings, and join PVC pipe using various fittings. One of the projects we'll do is to build a metal picture frame, which we take home with us. So far Engine training has been pretty fun! We're currently steaming towards Costa Rica. It's a three day trip between Panama and Costa Rica, so we'll have liberty again on Thursday. Everyone's pretty excited that we have two ports of call so close together, after having gotten a taste of what being in port is like! That's all for now, I have to go to morning formation, and then head off to another day of Engine training.
