---
title: 'End of Engine Training, Entering Golfito'
date: 2008-01-31T08:40:04.000Z
slug: end-of-engine-training-entering-golfito

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - golfito
  - costa rica
---

Yesterday was my last day of engine training. We learned about various measuring tools, and about basic electrical wiring by building an extension cord. We learned more about electricity in class yesterday afternoon, as well as about various instrumentation. On Tuesday my group was in lab the entire day, working with black iron pipe and making a picture frame out of aluminum stock. The picture frame was my favorite lab because we got to work with so many different tools and have a useable finished product. Today we're coming into Golfito, where we'll be through Sunday. Apparently the port is so small that the pilot is only going to need to be on board for about 15 minutes, and we won't be using any tugs to dock at the pier! Division 1 has watch the first day in port again, so I'll be on board tomorrow. I've realized that being on watch the first day is actually the best day to have it, because everyone else can go out and scope out the port and report back, which saves you a lot of time. That worked out nicely for me in Panama. I'll post more once I find out information about Golfito. Buenos dias!
