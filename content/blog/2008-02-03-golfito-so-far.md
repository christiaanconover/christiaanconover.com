---
title: Golfito So Far
date: 2008-02-03T00:49:03.000Z
slug: golfito-so-far

tags:
  - costa rica
  - golfito
  - maritime bar
  - mass maritime
  - sea term
  - sea term 2008
---

I'm writing this post from a place called the Maritime Bar in Golfito, which provides free Wifi. At the moment it's probably in the 80s with 100% humidity since it's pouring rain. There's a gentle breeze coming through this open-air restaurant, which is also rustling the palm trees and other plants around. Bob Marley is being pumped out of the stereo here, and there's a gorgeous 180 degree view of the bay and the ocean.

Our initial impressions of Golfito were not great, but I think it's grown on us as a group. There are water taxis which will take you to various beautiful beaches around the area. Yesterday I took one of those with a group of other freshman to a beach about 45 minutes away, which was a blast! The beach was awesome, with some of the finest sand I've ever felt in my life. There were fantastic views of the bay and the Pacific, and the mountains around us. After returning from the beach, we went to a restaurant nicknamed the Maritime Bar due to its decor. It's decorated all over with life rings, photos and other memorabilia from various Coast Guard, Navy, and Maritime ships. Of course, Mass Maritime was obligated to leave their mark here as well!

![Diplomacy, Mass Maritime Style](/files/blog/golfito-so-far/diplomacy-mass-maritime-style.jpg) Diplomacy, Mass Maritime Style

I am not signed up for any of the shore excursions here, but I'll try to get some information about them to post here. Perhaps I'll get a guest post from somebody who did one of the excursions.

Unfortunately, making phone calls in Costa Rica is not nearly as cheap as it was in Panama. The phone cards they sell at the pier are $8 for 20 minutes of talk time. I have a [Skype](http://www.skype.com/) account with SkypeOut minutes, so I was able to call home for only $0.02 a minute using my computer with the Maritime Bar's free Wifi. The telecom company in Aruba is pretty good, so making phone calls there should be cheaper. They also have Wifi hotspots throughout the island that you can purchase access to for a nominal fee. I don't know about cell phone service and cost, but I've heard that it should be available for Verizon as well as AT&T.

Since tomorrow is the Super Bowl, the plan is to set up a projector on the helo deck so we can all watch it together as a big group. I'll be sure to take plenty of pictures and post some once we get to Aruba. Right now, though, I'm going go to enjoy the party with the rest of Mass Maritime!

I've provided a video showing the Maritime Bar below.

<video preload="metadata" controls="">
  <source src="/files/blog/golfito-so-far/maritime-bar-golfito.mp4" type="video/mp4">
</video>