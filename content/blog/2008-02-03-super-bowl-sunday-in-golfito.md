---
title: 'Super Bowl Sunday in Golfito!'
slug: super-bowl-sunday-in-golfito
date: 2008-02-03T23:27:06.000Z
image:
  src: /files/blog/super-bowl-sunday-in-golfito/01.jpg
  alt: T.S. Enterprise in Golfito

tags:
  - costa rica
  - golfito
  - maritime bar
  - mass maritime
  - sea term
  - sea term 2008
  - shaft alley
  - us coast guard
---

Today is our last day in Golfito. Everyone's had a good time, but I think we're ready to be on our way. At the moment, however, most people are focused on the Super Bowl just a few hours away. We've been told, as I posted yesterday, that a projector is going to be set up on the helo deck so we can watch the Super Bowl as one big group, which should be a lot of fun! Tonight freshmen have a 2100 curfew, so we'll all be watching it on board. I'm at the Maritime Bar writing this post, and they have the Super Bowl pre-game show on TV but the audio is all in Spanish, so it adds an unusual twist to the Super Bowl.

Last night was a lot of fun. The majority of Mass Maritime cadets on liberty ended up at the Maritime Bar, so we had another night of partying together. The U.S. Coast Guard Cutter Alert is in port with us right now, so the crew was at the bar with us. I hung out with them for a lot of the night, which was a blast!

Tomorrow we'll be under way again, steaming towards the Equator to go through the infamous shellbacking ceremony. There's been a lot of buzz on the ship about it now that we're mere days away!

That's all I have for now. I'm going to go hang out with my shipmates for the last few hours we have in this port.

![T.S. Enterprise in Golfito](/files/blog/super-bowl-sunday-in-golfito/01.jpg) T.S. Enterprise in Golfito

![Golfito, Costa Rica](/files/blog/super-bowl-sunday-in-golfito/02.jpg)

![Golfito, Costa Rica](/files/blog/super-bowl-sunday-in-golfito/03.jpg)
