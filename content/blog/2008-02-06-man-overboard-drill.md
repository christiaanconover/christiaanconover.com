---
title: Man Overboard Drill
date: 2008-02-06T09:00:05.000Z
slug: man-overboard-drill

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - training
  - man overboard
---

Yesterday I had watch again 1200-1600, which actually ended up being sort of fun. About 15 minutes after I took the helm they started a series of man overboard drills, so I got to have plenty of practice with the ship's handling and rudder control. I learned a lot of things about being the helmsman that I most likely would not have otherwise been able to learn and do. It was really exciting and a lot of fun being the one to make the ship maneuver and make the tight turns! Shellbacking is on Friday, and the energy is starting to build. Notices have been posted throughout the ship from Davy Jones warning pollywogs about trespassing. It's pretty wild that we're so close to joining such an exclusive group! That's all for now, I need to get some sleep.
