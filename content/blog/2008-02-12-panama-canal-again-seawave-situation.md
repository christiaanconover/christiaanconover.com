---
title: Panama Canal Again; SeaWave Situation
date: 2008-02-12T03:20:03.000Z
slug: panama-canal-again-seawave-situation

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - panama canal
  - seawave
---

It's about 1915 and we're currently in the Panama Canal, moored in the area between the Miraflores and San Miguel locks. We went through the Miraflores locks around 1100 this morning, and fixed mooring lines around 1230, where we've been since. We should be getting under way again very soon (within the next 30-60 minutes) on our way back to the Atlantic. We've been watching Panamax ships going through the canal all day, from container ships to oil tankers. It's been awesome to be in the canal during the day! Unfortunately, we're transiting the rest of the canal at night again, but it's still cool.

SeaWave has been a little overloaded the past few days, and as a result we're on SeaWave Hours. In an effort to reduce the number of messages being sent, SeaWave is only being made available 1300-1500 and 1800-2000\. Hopefully it will return to normal, but we may be on a limited schedule through the rest of cruise. This is part of why I haven't posted in a few days. I'll try to put up a post each night during the evening time slot. Right now I'm going to head back up on deck to watch us go through the Panama Canal again. I'll post again when we're back in the Atlantic.
