---
title: 'Back at Sea, Two Days to Aruba'
date: 2008-02-13T08:20:03.000Z
slug: back-at-sea-two-days-to-aruba

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - aruba
---

This morning we woke up to find the ship rolling and pitching again. We've been in seas similar to the ones we encountered off the coast of the Carolinas back in January, so a lot of people have been feeling a little sick. Today was the last day of non-license training for Division 1, so we were in the classrooms in the aft part of the ship all day. Naturally, it magnified the movement of the ship by quite a bit. Tonight we had pre-Captain's inspections, in preparation for arrival in Aruba. We're two days away at this point, and everyone's pretty pumped to get off the ship again, and has been looking forward to this port in particular. It should be tons of fun, since there are so many things to do there. The general sentiment aboard, beyond excitement for Aruba, is wanting to go home. We have 11 days until we're back in Buzzards Bay, and it's starting to get to the cadets. There's been plenty of discussion about who's going to do what when we get home, and the things they miss the most. However, everyone's still getting along well, and seem to still be enjoying themselves. That's all I have to report for today. I'm going to go see what's available in the mess deck.
