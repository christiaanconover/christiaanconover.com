---
title: 'Hello from Aruba!'
date: 2008-02-15T23:35:10.000Z
slug: hello-from-aruba

tags:
  - amateur radio
  - aruba
  - kb3mmy
  - mass maritime
  - sea term
  - sea term 2008
---

I'm sitting on top of One Hold on board the Enterprise looking out to sea as the sun sets over the water, enjoying the warm weather and the awesome breeze. The town is busy with tourists looking to have a good time on a Friday night, and the music from the clubs and restaurants downtown can be heard all the way back at the ship. Division 1 has watch today, so I've been on board doing maintenance. We've had the good fortune of having WiFi reception on the ship here, so we can use our down time on our watch day catching up with the rest of the world.

Aruba has so far appeared to be the best port so far in terms of beaches and activities. It's definitely more expensive than Panama and Costa Rica, but I think it's well worth it. I've been here before, so I'm enjoying getting to do and see some of the things I did a few years ago.

I'll be posting pictures from this port over the next two days, so that you can see them before we get home. I am going back out tomorrow so I'll be sure to snap tons of footage (photos and videos) to provide tomorrow evening. Also, if anybody reading this is a ham radio operator, I have my HT with me and am tuned to the repeater operated by P43W available through EchoLink, so feel free to jump on and hail me (my callsign is KB3MMY).

That's all for now, I'll have much more to report tomorrow, I'm sure!
