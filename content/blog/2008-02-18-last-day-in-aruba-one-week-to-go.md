---
title: 'Last Day in Aruba, One Week to Go!'
date: 2008-02-18T02:52:12.000Z
slug: last-day-in-aruba-one-week-to-go

image:
  src: /files/blog/last-day-in-aruba-one-week-to-go/01.jpg
  alt: Lighthouse in Aruba
tags:
  - aruba
  - buzzards bay
  - mass maritime
  - sea term
  - sea term 2008
---

It's our last night in Aruba, and it's been an awesome port! Yesterday we went on a sailing and snorkeling tour with 4/C Buckley's and Hindemith's parents, which was fantastic! We saw tons of fish and and other sea life, and even a WWII-era German freighter, though part of me thought it might be superstitiously wrong to check out a sunken ship as somebody living on one. We've spent a lot of time at the pristine white sand beaches here, and today my group got jet skis, which was wicked fun! Since the water is so warm, it's not a big deal to get completely soaked bouncing over waves and getting hit by the wind-whipped water. This has definitely been the best port for aquatic activities!

One of the other nice aspects of Aruba is that the ship terminals are right downtown. As soon as we walk out the gates (about a 2 minute walk from the ship) we're right in the heart of Oranjestad. We don't have to spend money on taxis to get to things to do, and if we decide to go to other parts of the island, the bus system here is reliable and cheap. For me, the fact that Aruba in general is more expensive than Panama and Costa Rica has been offset by the lack of transportation expense. I think that most people have found this to be the case, which was nice since some people are running low on money.

As much as we've had a blast on Sea Term, everyone is looking forward to getting home. Today marks the one week point from our arrival back in Buzzards Bay, so people are more excited than ever. There have been tons of discussions about seeing family & friends (particularly girlfriends!), using our own showers, and getting a good night's sleep in a bed with more space than a coffin, and that doesn't move! However, we still have a week, and tomorrow it's back to business as usual which for me means maintenance.

I'm currently standing on the boat deck soaking up the warm air as we'll only have a few more days of it. It's going to be a little tough to go back to cold weather after being spoiled with over a month of warmth! Liberty expired at 2200 tonight for 4/C, and expires at 0000 tonight for 1/C. I've been watching the sporadic stream of cadets returning to the ship, tired after a good last day in port.

That's all I have for today. A special thanks to Mr. and Mrs. Buckley and Hindemith - it was great to meet you, I had an awesome time with you guys! Hopefully I'll see you back in Buzzards Bay. To my mom - can't wait to see you in a week, and thanks for the Valentine's message, that was an awesome surprise!

![Lighthouse on Aruba](/files/blog/last-day-in-aruba-one-week-to-go/01.jpg)

![Good times on the beach](/files/blog/last-day-in-aruba-one-week-to-go/02.jpg)

![Dining in style](/files/blog/last-day-in-aruba-one-week-to-go/03.jpg)
