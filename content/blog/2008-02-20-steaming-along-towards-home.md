---
title: Steaming Along Towards Home
date: 2008-02-20T01:10:03.000Z
slug: steaming-along-towards-home

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

We're currently steaming along at about 17 knots due north on our way home. The weather's still pretty warm, and the ship's been doing quite a bit of rolling since we left Aruba yesterday. Today is my last day of maintenance for Sea Term 2008, and tomorrow starts the last rotation on cruise. I'll be starting Deck training, which I'm looking forward to. I have a feeling that everyone will be having a little trouble focusing these last few days. Saturday will be the most difficult, since we'll be anchored in Cape Cod Bay all day, but will also have our last set of exams. We still have a lot to do between now and then though, so we'll be occupied with other things. There's not much else to report right now, I'll post again tomorrow. 5 days to go!
