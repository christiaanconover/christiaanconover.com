---
title: 'Passing Cape Hatteras, 3 Days to Go'
date: 2008-02-22T07:50:03.000Z
slug: passing-cape-hatteras-3-days-to-go

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

We're currently about 200 miles off Cape Hatteras, racing along to Cape Cod. This morning was the first time in nearly 6 weeks that we were a little chilly at morning formation, a refreshing reminder of just how close to home we're getting. The conditions have been a little rough the past couple of days, but everyone's already adjusted so nobody's seeming to notice. When I talked to my dad in port he mentioned a scene in U-571 when the Marine officers are in the officer's mess on the submarine watching the submariners grab plates and cups as they go sliding without even thinking about it, and going about their business. At this point we're starting to become that way, reacting to the effects of rougher seas almost naturally, and going about our business without giving it much thought. As much as I'm looking forward to some time at home, it's certainly cool to watch the transformation in just these two months from total land lubbers to mariners. While we are by no means "experienced" sailors, we are visibly improved from when we left in January. Tomorrow is my last day of deck training, and the end of the last rotation for Sea Term 2008! I have deck watch on Sunday as we are coming into Buzzards Bay, so for once I'm actually a little - dare I say - excited about watch! I don't have anything else tonight, right now I need to go get a haircut in preparation for our last inspection on Saturday.
