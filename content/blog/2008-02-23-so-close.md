---
title: 'So Close!'
date: 2008-02-23T01:30:04.000Z
slug: so-close

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

We're currently about 150 miles from Buzzards Bay, cruising north towards Cape Cod Bay. With about 18 hours until we anchor, everyone's pretty pumped. The last day of the rotations is over, so tomorrow we just have to take our exams and we're done! Once we reach Cape Cod Bay we'll have cell phone service, so I'm sure that most people will be on deck when they have free time making phone calls. The weather's nasty right now; it's been raining all day. We've been told that we're supposed to hit a storm tonight, which is exciting. I hope we do so that we can at least experience one this cruise. That's all for now, next time I post I'll be reporting from Cape Cod Bay!
