---
title: Back to Business
date: 2008-03-03T01:51:17.000Z
slug: back-to-business

tags:
  - dorms
  - mass maritime
  - spring semester
---

Today everyone returns to school for the start of the spring semester. It's moving-in day, and the roads on campus are lined on both sides with cars, and people unloading piles of bags and boxes. Even though we've been off for less than a week, moving back into the dorms makes it feel like forever.

Tomorrow is the academic orientation day for spring semester. We have meetings throughout the day regarding both academic and regimental/campus life issues. Tuesday should start classes, which means resuming the typical routine of inspections and cleaning stations as well. For now though, everyone's focused on unpacking and getting moved in, and catching up with people they haven't talked to in a while.

The mess deck opens tomorrow morning for breakfast, but it wasn't open for dinner tonight. A lot of people went out to eat (including me), so when we walked in to Chili's we found that half the people there were Maritime cadets. It felt a little like being back on Sea Term and going into a restaurant in port!

I'm going to finish unpacking and then go to bed, since I had to be up early this morning to get here in time. I'll update again tomorrow.
