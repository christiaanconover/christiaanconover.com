---
title: Beginning-of-Semester Confusion
date: 2008-03-04T19:53:20.000Z
slug: beginning-of-semester-confusion
image:
  src: /files/blog/first-snow-of-the-season/13.jpg
  alt: Buttermilk Bay

tags:
  - mass maritime
---

Today and tomorrow have been a little hectic as people are easing back in to their routines, and getting various issues sorted out. Schedules, financial aid, business office obligations, books, and a variety of other tasks we have to accomplish. As freshmen, this is the first time we've had to deal with a lot of these things as they were taken care of for us last semester.

Since I had no classes today, and the semester's still getting started, there's not much to report. Once classes and extracurriculars really get going, I'll have a lot more I can post about.
