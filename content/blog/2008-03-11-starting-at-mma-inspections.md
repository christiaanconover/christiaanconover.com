---
title: 'Starting at MMA: Inspections'
date: 2008-03-11T11:50:37.000Z
slug: starting-at-mma-inspections

tags:
  - "admiral's inspection"
  - inspections
  - mass maritime
  - regimental inspections
  - regimental sign-outs
  - starting at mma
---

Inspections are a major part of the life of a fourth class cadet. Every morning at 0650, all fourth class cadets in the regiment stand room inspections, done by the squad leaders in each company. There are very specific requirements for your room to be inspection-ready, the state in which it is supposed to be kept not only for inspections but at all times. When a squad leader walks up to your room, he or she inspects both you and your room mate's uniform. They then inspect your room, checking to make sure all horizontal surfaces have been dusted, your rack (bed) is tight, and that your inspection locker is proper. The inspection locker is the biggest part of the room inspection. Every item in the hanging part of the locker must be in the right order, and all hangers must be two finger widths apart. Everything must be properly ironed, and all buttons and zippers must be closed and fastened. There are certain items that must be placed in the right spot on the floor of the hanging area. The top drawer of the locker must contain specific items, each in a certain place and folded a certain way in the drawer. These inspections are done Monday-Thursday every week.

On Fridays we stand regimental sign-outs. At 0700 we stand in formation on the parade field by company, and each fourth class cadet's uniform is inspected by a member of the regimental staff. In order to earn liberty for the weekend, you must pass these inspections. In the winter we are inspected in our regular long sleeve black uniform, but when we are wearing the short sleeve (summer) uniform, we are inspected in our "salt & peppers" which consist of the regular black pants and a more formal white shirt.

On Sunday nights, fourth class cadets stand regimental inspections. These are room inspections similar to the ones performed each morning, except that your room is inspected by a member of the regimental staff. These inspections serve two purposes: to ensure that all fourth class cadets are back at school after the weekend, and to ensure that our room is clean and orderly to be ready for the coming week.

The only inspection that all cadets in the regiment must stand is Admiral's inspection. Each company stands Admiral's twice each semester, coinciding with each company's weekend watch (I'll discuss this in a later post). The unique thing about Admiral's is that even 1/C officers (including the regimental staff) must stand. All the same requirements for regular inspections apply to Admiral's, but they are much more strict, and additional requirements are added as well. The deck (floor) must be waxed so that it is smooth and shiny, both in your room and in the company's public areas such as the passageways (hallways). The heads (bathrooms) must be spotless, as well as the study lounges. Everything in your room has to be perfect, and your uniform must be pristine. Admiral's are a huge deal, and people usually spend the entire week prior to them cleaning and preparing.

Inspections are the single largest aspect of a fourth class cadet's regimental responsibilities. They are certainly the most time consuming, and while they are no fun, I can appreciate that they make sure my room is always clean!
