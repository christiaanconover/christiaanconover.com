---
title: 'Starting at MMA: Watchstanding'
date: 2008-03-12T13:18:58.000Z
slug: starting-at-mma-watchstanding

tags:
  - mass maritime
  - sea term
  - starting at mma
  - watchstanding
---

All cadets are required to stand watch, license and non-license majors alike. We stand watch in three places on campus - one in the dorms, and two on the Enterprise.

In the dorms, we have watch at the Cadet Information Center (CIC), where we keep track of visitors entering and exiting the dorms, answer the phone during off-business hours, and do rounds throughout the dorms to make sure everything is in proper order. Freshmen are responsible for the rounds, and act as messengers to take information anywhere it needs to be delivered in the dorms or on campus.

On board the ship, there are deck and engine watch posts. Deck watch for freshmen consists of watching the gangway for people boarding and disembarking the ship, signing people in and out, and doing Detex rounds to check for proper order throughout the ship by punching a clock with keys at certain stations. Engine watch is resonsible for ensuring that all equipment in the engine room is performing (or secured) as it should be. Since the engine room is almost entirely secured while at the pier, there aren't many systems to keep track of, which is nice.

Watch is stood 24 hours a day, 7 days a week on the ship, and all hours before and after the academic day at CIC. These watch positions only apply in this manner while at school. During Sea Term, shoreside watch (CIC) is obviously eliminated, and the number of personnel on watch on the ship is increased significantly. However, we use watchstanding while at school to learn proper watchstanding procedures for when we ship out.

All watches are 4 hours long, except holiday watch which is 8 hours long. Watch periods are 0000-0400, 0400-0800, 0800-1200, 1200-1600, 1600-2000, and 2000-0000\. One of the benefits of having an overnight watch is that as a freshman you can sleep during study hours, so you can go to bed before 2200\. During Sea Term, overnight watch is even better because you're not required to be at morning formation the next day, so you can sleep all morning!
