---
title: 'Starting at MMA: Weekends'
date: 2008-03-15T16:07:46.000Z
slug: starting-at-mma-weekends

tags:
  - mass maritime
  - starting at mma
---

Given that it's Saturday, I decided to talk about weekends at MMA.

Since the vast majority of cadets at MMA are Massachusetts residents (many of them living within an hour of campus), most people go home on weekends. It's a nice break from the regiment. For those of us out of state, we have to get a little creative in entertaining ourselves. Some of us go home with friends who live close by, but many of us stay on campus. We quickly get to know the other kids who stay on the weekends, and come up with things to do together. If we have friends at other colleges nearby, we'll sometimes go there to hang out or go to parties. We'll go to the Cape Cod Mall in Hyannis, to see a movie or just hang out. If we're feeling lazy or the weather's bad we'll just hang out in the dorms and watch a movie in the study lounge, play card games, etc.

If you are out of state, playing a sport or joining clubs that have weekend activities is a great way to keep busy and entertained. I am on the sailing team, so I'm at regattas many weekends during the season. It keeps me involved, and gives me an opportunity to do something I like.

Mass Maritime certainly doesn't have the most active weekend life, but if you make an effort you can find plenty of things to do.
