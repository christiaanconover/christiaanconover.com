---
title: "The Mess Deck: MMA's \"Bread and Butter\""
date: 2008-03-27T04:32:52.000Z
slug: the-mess-deck-mmas-bread-and-butter

tags:
  - mass maritime
  - mess deck
---

Hehe, you see what I did there? :-)

Anyway, a parent posted a [comment](/blog/the-latest-updates-from-around-campus#comment-73773481) on a [previous blog entry](/blog/the-latest-updates-from-around-campus) suggesting that I discuss some of the more "mundane" details of life at MMA, such as the mess deck. The mess deck is the hub of cadet life beyond the regiment. Eating, relaxing, and socializing are just a few of the major roles the mess deck plays in cadet life.

On any given day of the week, you'll find a variety of cuisine choices. We have two regular serving lines, each with a different selection. We have a short order sandwiches and wraps line, with different specials each day (I usually get a wrap when they have roast beef) which can be customized to your liking. We also have a pizza line, which usually has anywhere between 6 and 10 different types of pizza to choose from. Next to that is a roasted meat line, where you can get hot sandwiches of the meat of the day. Finally, we have a wraparound salad bar, with a large selection of toppings and dressings. Tuesday is a favorite day among cadets, because we have chicken patties for lunch. On Wednesday nights, Chartwells (the food service company that runs the mess deck) has something special for dinner, or to accompany it. A few weeks ago they brought in some chefs from Peking Palace, a local Chinese restaurant, to make a Chinese buffet for us. We'll also have special buffets the Wednesday before certain holidays, such as Thanksgiving and Christmas.

The mess deck also serves as the social hub for cadets. Here we catch up with kids that we may not otherwise see, and get all the good gossip around school. In this environment the rumor mill is potent, and the mess deck is the best forum. Aside from that, it provides a place for cadets, especially underclassmen, to relax and hang out with friends. The freshmen and sophomores sit on one side of the mess deck, on the side toward the parade field, and the juniors and seniors sit on the other side. This way, we can be at ease without worrying about a squad leader or officer getting on our case about something. We also have two televisions in the mess deck, which gives freshmen a chance to watch TV and get the news.

**On a side note:** I love getting feedback and suggestions for topics to write about. If you have a subject you'd like me to cover, please post it in the **Skribit box on the right**. Thanks!
