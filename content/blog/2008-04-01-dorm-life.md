---
title: Dorm Life
date: 2008-04-01T03:22:43.000Z
slug: dorm-life

tags:
  - cape cod canal
  - dorms
  - inspections
  - mass maritime
  - ts enterprise
---

I'd imagine that one of the big questions that most prospective cadets and parents have, such as [this parent](/blog/the-latest-updates-from-around-campus/#comment-14), is what life in the dorms is like. In general, it's similar to that of most other colleges: there are two people to a room, we share the heads, and we're always in each other's business.

However, we have unique aspects of dorm life, such as cleaning stations and [inspections](/blog/starting-at-mma-inspections/). Cleaning stations are only for freshmen, and are done in the morning at 0550 and at night at 1900\. The assignments for cleaning stations rotate each week, so depending on the size of the company you may only be on the cleaning bill once every 3 weeks or so. There are different specific jobs assigned to each person, so that the entire company gets cleaned twice a day.

Interaction between males and females is also heavily regulated. Men and women are not permitted to be in a room with the door closed. There is also no fraternization permitted, meaning that upper and underclassmen can't date each other. That's not to say that it doesn't happen; it's simply not supposed to happen due to potential abuse of power issues as a result of the regimental structure.

Tonight's events in 1st Company are a perfect example of what life in the dorms is like for freshmen. Due to a series of events related to poor cleanliness in the rooms and the company's public spaces, 1st Co freshmen have morning and evening inspections every day this week (instead of just morning inspections). We also have all-hands cleaning stations from the end of evening inspections until study hours, which will be over an hour's worth of cleaning.

The dorms are, for the most part, pretty quiet. Since we can't throw parties, it keeps the noise level pretty low, except for during study hours (go figure) when everyone on deck has their hatches open and is talking and working together on homework. It's helpful when it comes time to sleep though, since there isn't a lot of disturbance to keep people awake.

One of the things I like most about living in the dorms is the view out my window. I can't think of too many places where I could watch ships & barges going past, and have a 540' ship docked a few hundred feet from my building. (The new version of the blogging software I use is giving me trouble with posting images, so once I get that resolved I'll put up some pictures of my view.)

**On a side note:** I may be at the Welcome Aboard event this Saturday, depending on whether or not I'm sailing this weekend. So, I might have the chance to meet some of the incoming freshmen for next year; I'll keep you posted.
