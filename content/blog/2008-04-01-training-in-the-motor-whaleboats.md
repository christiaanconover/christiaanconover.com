---
title: Training in the Motor Whaleboats
date: 2008-04-01T14:42:13.000Z
slug: training-in-the-motor-whaleboats
image:
  src: /files/blog/training-in-the-motor-whaleboats/motor-whaleboat.jpg
  alt: Motor whaleboat

tags:
  - docking
  - line handling
  - mass maritime
  - motor whaleboat
---

Today in Basic Seamanship Lab we had our first experience training in the motor whaleboats. For those of you unfamiliar with motor whaleboats, here's a picture of one very similar to the ones we have here at the Academy:

Part of Basic Seamanship involves small boat handling, so we use the motor whaleboats to learn line handling and boat maneuvering. Today made it particularly interesting for our first time out, since we were fighting very strong crosswinds while docking, and driving rain.

Class today consisted of the procedures for starting and stopping the diesel engine, proper line handling commands and actions, maneuvering away from the dock, coming back to the dock, and properly securing the boat again. One of the things that everyone took a little while getting used to was using the engine and the forward spring line to properly maneuver the boat against and away from the dock. For those of you that don't know, the forward spring line is a rope that runs from the bow of the boat to a cleat on the dock aligned with the after, or rear, part of the boat. This creates diagonal tension that, by putting the engine in forward and turning the rudder, you can use to push the bow and stern toward and away from the dock in a controlled manner.

Proper operation of the motor lifeboat is one of the qualifications we have to pass in order to pass Basic Seamanship, and eventually get a Coast Guard license. But hey, at least we're messing around in boats!
