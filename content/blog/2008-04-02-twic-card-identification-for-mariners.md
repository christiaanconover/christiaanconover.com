---
title: 'TWIC Card: Identification for Mariners'
date: 2008-04-02T16:43:16.000Z
slug: twic-card-identification-for-mariners

tags:
  - us coast guard
  - homeland security
  - mass maritime
  - port security
  - tsa
  - twic
---

The [Department of Homeland Security](http://www.dhs.gov/) and [Transportation Security Administration](http://www.tsa.gov/) have created a federal identification system for all personnel working in the maritime industry. The ID is called [Transportation Worker Identification Credential](http://www.tsa.gov/what_we_do/layers/twic/index.shtm) with the intention of more effectively regulating and maintaining port security.

All license track cadets here at Mass Maritime are required to enroll in TWIC to be in compliance with maritime regulations. We'll need these cards when we do our commercial shipping co-op in Junior year, as well as once we graduate and are employed in the maritime industry.

Today I had my appointment for enrolling with TWIC. The process requires either a U.S. Passport or two state or federal IDs. They go through all the basic personal information (name, address, phone number, etc.) to make sure they have proper records. They also take fingerprints and a photograph. Unfortunately there is a $132.50 fee associated with registering, and since the card is only good for 5 years the majority of that time it will be unused while we're in school.

Luckily, since there are so many of us doing it, registration is being done right on campus, and cards will be delivered to us on campus as well. That way, we can avoid problems like [Capt. Rodriguez](http://captrichardrodriguez.blogspot.com/) encountered both in enrolling and receiving his card.
