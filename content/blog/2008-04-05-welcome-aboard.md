---
title: 'Welcome Aboard!'
date: 2008-04-05T21:43:00.000Z
slug: welcome-aboard

tags:
  - mass maritime
  - sea term
---

Today was the Welcome Aboard event at the Academy. This is a day specifically for high school seniors who have been accepted into next year's freshman class. It allows them to get a more in-depth sense of some of the things MMA has to offer, such as firefighting, cold water survival, small boat handling, and basic engineering. I was one of the cadets running the Semester at Sea seminar, where future cadets got to see pictures and videos from previous Sea Terms, get information about life as a freshman on the Enterprise, and have a question and answer session.

There were four sessions today, so my group got to talk to about 120 students about our experiences on cruise. We had two juniors and four freshmen (including me) so we were able to offer a wide array of information. We showed a video that one of the juniors had put together after his freshman cruise, the year they went to Europe. Then we showed a slideshow of some [pictures 2/C Wilson took this year](http://picasaweb.google.com/SeaTerm08). While we were showing pictures, we each discussed a specific aspect of Sea Term. We talked about division rotations, the Enterprise itself, deck & engine watches, classes, and port time. We also fielded a lot of questions from the future cadets about life on Sea Term, from watch to life in the holds and time off.

I think today went well overall. If you were there and have any additional questions or comments, or weren't but still have something to share, please post a comment.
