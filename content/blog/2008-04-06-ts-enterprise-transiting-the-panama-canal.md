---
title: T.S. Enterprise Transiting the Panama Canal
date: 2008-04-06T13:39:46.000Z
slug: ts-enterprise-transiting-the-panama-canal

tags:
  - ts enterprise
  - mass maritime
  - panama canal
  - sea term 2008
---

I found this video, I'm guessing put together by a parent, of the Enterprise going through the Miraflores locks in the Panama Canal on our return trip.

{{< youtube mqOU1ATJn6A >}}

I'm pretty sure that I have photographs looking right back at that camera. I'm really glad that somebody recorded these shots, so thank you to whoever posted this!