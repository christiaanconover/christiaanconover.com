---
title: 'Spring Career Fair: An Eye-Opener to Opportunities After Graduation'
date: 2008-04-10T02:36:16.000Z
slug: spring-career-fair-an-eye-opener-to-opportunities-after-graduation

tags:
  - amo
  - career fair
  - mass maritime
  - mcallister towing
  - recruiting
  - us coast guard
---

Today was the annual Spring Career Fair here at MMA, where dozens of companies come to try and recruit this year's graduating class, as well as some of the other cadets for internships and cooperatives. There were close to 100 companies represented, from all over the country and the world.

I only stopped and talked with a few companies, since the main focus of this career fair was to help juniors and seniors line up jobs, so underclassmen were a lower priority. I talked to recruiters from the [United States Coast Guard](http://www.gocoastguard.com/), since I plan to do the Reserves, and will be going to boot camp this summer. I also talked to [McAllister Towing](http://www.mcallistertowing.com/), where I was told that I might be able to get an internship in Baltimore this summer on a tug. I didn't even know that internships were offered to freshmen before tonight! I stopped by the [American Maritime Officers](http://www.amo-union.org/) union booth, and I only talked for a minute with the representative, but I got a sweet Merchant Marine bumper sticker out of it :-)

I had planned to take my camera with me, but in my haste to get out the door after sailing practice to make sure I got there before it closed for underclassmen, I forgot to grab it (I really need to start getting better about bringing it with me more often). Regardless, it was a really good experience, and gave me a sense of what's available after graduation.
