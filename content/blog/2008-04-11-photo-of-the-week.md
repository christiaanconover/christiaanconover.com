---
title: Photo of the Week
date: 2008-04-11T13:26:35.000Z
slug: photo-of-the-week
image:
  src: /files/blog/photo-of-the-week/enterprise-fog.jpg
  alt: T.S. Enterprise in the fog

tags:
  - mass maritime
  - photo of the week
  - photography
---

I'm going to start a new segment called Photo of the Week. Every Friday, I'm going to post a photograph of something MMA-related that either I or another cadet has taken.

If you have a photo you've taken of something at or relating to MMA that you'd like me to post, please e-mail me to let me know.
