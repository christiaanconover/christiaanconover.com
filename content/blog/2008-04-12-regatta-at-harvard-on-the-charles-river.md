---
title: Regatta at Harvard on the Charles River
date: 2008-04-12T16:21:50.000Z
slug: regatta-at-harvard-on-the-charles-river
image:
  src: /files/blog/regatta-at-harvard-on-the-charles-river/regatta.jpg
  alt: Regatta

tags:
  - boston
  - charles river
  - harvard
  - mass maritime
  - sailing
---

Part of the fleet waiting to start at today's regatta.
