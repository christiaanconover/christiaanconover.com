---
title: 'Photo of the Week #2'
date: 2008-04-18T12:57:14.000Z
slug: photo-of-the-week-2
image:
  src: /files/blog/photo-of-the-week-2/aruba-pirate-ship.jpg
  alt: '"Pirate ship" in Aruba'

tags:
  - aruba
  - mass maritime
  - photo of the week
  - sea term 2008
---

This week's [Photo of the Week](/tags/photo-of-the-week/) is out of my archives from [Sea Term 2008](/tags/sea-term-2008/).

I took this photo while on board a similar vessel doing snorkel tours of various points of interest, including a sunken German freighter.