---
title: Whaleboat Quals
date: 2008-04-23T17:00:06.000Z
slug: whaleboat-quals

tags:
  - boat handling
  - line handling
  - mass maritime
  - motor whaleboat
  - quals
---

Today I had my whaleboat qual (short for qualification). We're tested on the [boat and line handling procedures we've been learning](/blog/training-in-the-motor-whaleboats/) for the past couple of weeks, on a points system for various stages of the operation.

The procedure was the same as it's been while we've been practicing - undocking, maneuvering outboard of the Enterprise, making an approach, and docking again. This time, however, we were making a starboard landing (starboard side of the boat against the dock) instead of a port landing, which was how we'd been practicing. At our dock, a starboard landing is much harder because you have to clear the bow of the Enterprise by only about a foot, and then turn hard enough to come in alongside the dock at the right spot. The real kicker is that you're not supposed to put her in reverse to slow down or maneuver, since you should be able to properly manage speed and direction going forward.

I wasn't able to quite hit the mark when making my approach, and ended up about 3/4 of a boat length down from the mark. However, I did everything else properly, so hopefully I passed. We find out next week the results. I'm hoping that doing quals doesn't mean the end of motor whaleboats for this class, because so far that's been my favorite part of deck classes!
