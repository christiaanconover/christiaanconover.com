---
title: 'Helicopter on the Parade Field at Mass Maritime!'
date: 2008-04-24T18:37:03.000Z
slug: helicopter-on-the-parade-field
image:
  src: /files/blog/helicopter-on-the-parade-field/02.jpg
  alt: Helicopter on the parade field

tags:
  - alternative energy
  - mass maritime
  - wind turbine
---

We had a helicopter land on the parade field today, and do some low altitude flying over the campus. It was doing surveying and research for a new experimental low-power RADAR system to integrate with the wind turbine. The system is intended to automatically control the aviation indicator lights on the top of the windmill, so that they can be turned on and off depending on whether aircraft are in the area.

Definitely the excitement for the week. It made it even better that it was wicked nice outside, so I had a good excuse to be outside instead of in my room doing schoolwork :-)

I got some pictures and videos of the helicopter:

<video preload="metadata" controls="">
  <source src="/files/blog/helicopter-on-the-parade-field/helicopter.mp4" type="video/mp4">
</video>

![Helicopter at Mass Maritime](/files/blog/helicopter-on-the-parade-field/01.jpg)

![Helicopter at Mass Maritime](/files/blog/helicopter-on-the-parade-field/03.jpg)

![Helicopter at Mass Maritime](/files/blog/helicopter-on-the-parade-field/04.jpg)

![Helicopter at Mass Maritime](/files/blog/helicopter-on-the-parade-field/05.jpg)

![Helicopter at Mass Maritime](/files/blog/helicopter-on-the-parade-field/06.jpg)

![Helicopter at Mass Maritime](/files/blog/helicopter-on-the-parade-field/07.jpg)

![Helicopter at Mass Maritime](/files/blog/helicopter-on-the-parade-field/08.jpg)