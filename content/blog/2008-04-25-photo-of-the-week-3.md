---
title: 'Photo of the Week #3'
date: 2008-04-25T13:02:48.000Z
slug: photo-of-the-week-3
image:
  src: /files/blog/photo-of-the-week-3/roro.jpg
  alt: RoRo in the Cape Cod Canal

tags:
  - mass maritime
  - photo of the week
  - roro
---

The Photo of the Week for this week was taken from my room last semester.

This is a RoRo (Roll-On Roll-Off) vehicle carrier ship, passing the Mass Maritime campus transiting the Cape Cod Canal. Notice how, even from my elevated angle on the 04 deck, it towers over everything on campus, including the Bresnahan Building (the one with the glass front), with a height of more than 3 stories!
