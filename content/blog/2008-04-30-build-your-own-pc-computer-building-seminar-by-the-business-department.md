---
title: '"Build Your Own PC" Computer Building Seminar by the Business Department'
date: 2008-04-30T16:51:11.000Z
slug: build-your-own-pc-computer-building-seminar-by-the-business-department

tags:
  - build your own pc
  - imbu
  - mass maritime
---

Even though I'm a Deckie, I like to broaden my own field of knowledge by getting involved with things from other departments as well. Currently the International Maritime Business department is offering a 3 part series on how to build your own PC. Being interested in computers, and having built PCs before, I was curious to see what this was all about and what they were using to do it.

I unfortunately was unable to make the first part of the series, in which they covered more of the non-technical aspects of building a PC - where to get parts, the advantages and disadvantages of building it yourself, etc. Last night was the second session, which I luckily was able to attend.

In the second installment, they had gotten all the parts they were going to use, and did the actual assembly of the computer. They purchased parts that were the exact same specs of a Dell XPS (I'm not sure which model, I'll try to get that information and post that here later on), which cost about $2,300 direct from Dell. Using online retailers, they were able to purchase all of the parts (including monitor, mouse and keyboard) for about $1,300\. That's a pretty big benefit of doing it yourself right there!

As they assembled it, they went through what each part does, why it's important to the system, and how to install it. They had a webcam going through a projector so that we could all see the small precision work they were doing, which was definitely helpful. Once it was all assembled, they plugged in the monitor, keyboard, mouse, and the power cord, and pushed the button. The machine whirred to life, but didn't get very far since Windows isn't installed yet. That's what next week's session is for.

Next Tuesday is the final installment in the series. We'll be going over how to install Windows (this machine will be running Vista), as well as Linux. I'll be sure to get plenty of pictures!
