---
title: "Arrr! Speak Like A Scurvy Pirate th' Easy Way!"
date: 2008-05-01T17:45:01.000Z
slug: arrr-speak-like-a-scurvy-pirate-th-easy-way

tags:
  - mass maritime
  - pirate
  - sea-fever
  - twitter
---

[Peter Mello](http://twitter.com/petermello) over at [Sea-Fever](http://sea-fever.org/) found a [service that translates whatever ye type into scurvy pirate shout](http://postlikeapirate.com/). It gunna also message your scurvy pirate message directly to twittArrr, [PirateSpace](http://postlikeapirate.com/myspace.php), or send an e-mail. I can see myself usin' this a lot, fer no reason at all. Avast!
