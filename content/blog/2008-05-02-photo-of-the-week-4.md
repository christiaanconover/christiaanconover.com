---
title: 'Photo of the Week #4'
date: 2008-05-02T11:52:59.000Z
slug: photo-of-the-week-4
image:
  src: /files/blog/photo-of-the-week-4/cutter.jpg
  alt: Coast Guard cutter

tags:
  - cape cod canal
  - mass maritime
  - photo of the week
  - us coast guard
---

This week's Photo of the Week is a picture that I actually took - _gasp_ - this week! It's a photograph of an 87' Coast Guard cutter passing the Mass Maritime campus as it heads east through the Cape Cod Canal.

Since I'm planning on going into the Coast Guard, I get pretty excited when I see Coast Guard vessels and aircraft around campus, and this was no exception.
