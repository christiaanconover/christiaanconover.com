---
title: Thanks to AN UNOFFICIAL COAST GUARD BLOG
date: 2008-05-05T02:22:32.000Z
slug: thanks-to-an-unofficial-coast-guard-blog

tags:
  - cgblog.org
  - military
  - us coast guard
---

On Friday [I was featured as a CG Blog Find](http://www.cgblog.org/2008/05/cg-blog-find-1-christiaanconovercom.html) on [AN UNOFFICIAL COAST GUARD BLOG](http://www.cgblog.org/). I haven't featured this site myself on my blog, but they've been in my blogroll for a while. If you haven't visited there, definitely check it out. It's chock-full of great information about current Coast Guard news, information, and editorials.

Thank you to Joe Coastie and CGBlog.org for featuring me!
