---
title: 'Featured Maritime Photographer: OneEighteen'
date: 2008-05-06T03:44:33.000Z
slug: featured-maritime-photographer-oneeighteen
image:
  src: /files/blog/featured-maritime-photographer-oneeighteen/ebb-tide.jpg
  alt: Ebb Tide
  caption: '"Ebb Tide"'
  link: 'https://secure.flickr.com/photos/72511036@N00/2455264051'

tags:
  - oneeighteen
  - photography
  - us coast guard
---

I've recently been spending a lot of time surfing and exploring on [Flickr](http://www.flickr.com/). I've been uploading my photo archives to Flickr, so while they've been sending I've been checking out other photos on the site. Through some natural stumbling, and some guidance from [other bloggers](http://www.gcaptain.com/), I discovered [OneEighteen](http://www.flickr.com/photos/oneeighteen/).

His real name is Louis Vest, and he's a ship pilot in Houston, Texas. He has a large array of absolutely amazing maritime photographs, covering ships of all shapes and sizes in various lighting and weather conditions. Here are examples of some of my favorites from him:

[![000º True](/files/blog/featured-maritime-photographer-oneeighteen/000-true.jpg)](https://secure.flickr.com/photos/oneeighteen/2455264051/)

[![Cutter "Heron"](/files/blog/featured-maritime-photographer-oneeighteen/cutter-heron.jpg)](https://secure.flickr.com/photos/oneeighteen/1031424094/)

If you're interested in maritime photographs, or photography at all, I'd recommend checking out [Louis' Flickr photos](http://www.flickr.com/photos/oneeighteen/).
