---
title: 'Photo of the Week #5'
date: 2008-05-09T12:12:38.000Z
slug: photo-of-the-week-5
image:
  src: /files/blog/photo-of-the-week-5/nobska.jpg
  alt: Nobska Light

tags:
  - cape cod
  - lighthouses
  - mass maritime
  - nobska light
  - photo of the week
---

The Photo of the Week for this week was taken in Falmouth last Sunday.

Nobska Light in the Fog: Lighthouses are a significant part of the maritime industry, as well as the Coast Guard. Since this one is so close to MMA, it seemed like a logical one to pick.
