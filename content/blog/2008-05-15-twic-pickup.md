---
title: 'TWIC Card Pick-up: Quick, Easy and Generally Painless - For Some'
date: 2008-05-15T18:40:57.000Z
slug: twic-pickup

tags:
  - mass maritime
  - tsa
  - twic
---

A few weeks ago [I posted about TWIC](/blog/twic-card-identification-for-mariners/), the new identification system for mariners from the TSA. I picked up my TWIC card here at school today. The TSA has come to us to do enrollment and delivery, which has been very convenient as it keeps us from having to travel to get it. This week they've been back on campus delivering TWIC cards that have been created and ready for use.

My appointment was at 0945 this morning. I got there right on time, and was able to immediately walk in and sit down with the agent. he pulled out my card from a stack of them being delivered, stuck it in the chip reader, and started the process. He asked for my photo ID, had me enter the 6-8 digit PIN I had selected for my card, and then had me fill out a survey about the TWIC process while we waited for everything to be registered. By 1000 I was out the door and continuing on my way. All in all, my TWIC experience was pretty painless.

A number of other cadets have had experiences not nearly so positive this week, however. I've talked to some kids who were there for over an hour between waiting in line and actually working with the agent activating the card. Apparently the TSA's database system is a tad on the slow, bogged-down side, which means that the transaction between the issuing point and the central database takes a long time. In fact, of the 15 minutes I was in the room, 10 of them were spent waiting for the computer to finish updating the system. One cadet was told that it's better in the morning, but later in the day when the west coast issuing points start coming online it gets bogged down more. This makes me a little concerned, since this system is intended to be used 24/7 around the country once it's fully up and running. Hopefully this slowness is being addressed, and will be resolved before the required registration date in April 2009.

On an interesting side note, I asked the agent about how this was going to work on the end-user side - when we come into port and have to present the card to the authorities. He told me that the person checking your TWIC will have a chip reader similar to the one he was using today. He'll insert your card, which will pull up your profile (including your photo) from the database, so they can verify that your card is not counterfeit. You will then be required to enter the PIN you've chosen, and have your right index finger's print read for identity verification. It certainly sounds like a pretty watertight security measure (excuse the pun). However, it also sounds like a potential source of long lines and big delays. We'll have to wait and see how this plays out.
