---
title: 'Photo of the Week #6'
date: 2008-05-16T12:14:20.000Z
slug: photo-of-the-week-6
image:
  src: /files/blog/photo-of-the-week-6/enterprise-crows-nest.jpg
  alt: "Crow's nest on the T.S. Enterprise"

tags:
  - mass maritime
  - photo of the week
  - sea term 2008
  - ts enterprise
---

This week's photo of the week is of our return to Buzzards Bay after Sea Term 2008\. Some of the officers and 1/C cadets from the Deck department went up by the crow's nest on the Enterprise to watch our transit through the Cape Cod Canal.

I certainly hope that I get to do this my senior year!
