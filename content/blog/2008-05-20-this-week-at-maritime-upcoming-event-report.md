---
title: 'This Week at Maritime: Upcoming Event Report'
date: 2008-05-20T00:55:35.000Z
slug: this-week-at-maritime-upcoming-event-report

tags:
  - change of command
  - coasties
  - emory rice day
  - honor company
  - marching competition
  - mass maritime
  - rotc
---

Usually I try to avoid writing these posts because I generally regard them as filler, but I thought it might be appropriate this week. We have some unique events going on over the next couple of days that I felt would be worthy of some introduction prior to my post-event write-ups that will follow (man, it's just hyphen city back there, isn't it?).

Tomorrow is Emory Rice day, which is our annual field day at the end of the year. The day starts off with a marching competition between the companies. Each freshman platoon in the regiment represents their competition, just as we did for Recognition back in September. This time is a little different however, because we will be marched by the upcoming company commander for our company, providing them with their first real experience in publicly leading the company as an officer. The afternoon is filled with activities aimed at providing us with some fun near the end of the year, such as an obstacle course done by ROTC and a rock wall. All activities are done by company, and are worth points towards Honor Company. I'll explain Honor Company in a later post, so stay tuned for that.

Wednesday afternoon is Change of Command. Now that [Coasties](/blog/coasties-week-all-quiet-above-the-decks/) are over and we're only weeks away from the end of the year, it's time for this year's graduating seniors to step down from their officer positions and hand the reigns over to the upcoming officers for next year. There's a formal ceremony that will be held Wednesday afternoon on the Parade Field. Since I haven't seen a Change of Command at MMA before, I'm not really sure the details of the ceremony. Since I'll most likely have to be in formation for the duration of it I won't be able to take pictures, but I'll try to get some pictures to post from someone here at MMA (if there's anybody reading this who'll be here and would like to share their photos-maybe the Admissions office-I'd appreciate it, shoot me an e-mail.

Not much else to report from Taylor's Point, except that the weather's getting too nice for us to be indoors. I vote that classes start being held outside, who's with me?