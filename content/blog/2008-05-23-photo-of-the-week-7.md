---
title: 'Photo of the Week #7'
date: 2008-05-23T10:48:46.000Z
slug: photo-of-the-week-7
image:
  src: /files/blog/photo-of-the-week-7/survival-suit-training.jpg

tags:
  - seamanship
  - gumby suit
  - mass maritime
  - photo of the week
  - survival suit
---

This week's [Photo of the Week](/tags/photo-of-the-week/) comes from a training lab I had this week.

Survival suits are an integral part of a survival plan after a maritime disaster. In conditions where an unprotected person might have mere minutes to survive, they can provide many hours of survival time, and even keep somebody alive indefinitely. Because they are so effective, we have extensive training in the proper donning and operating procedures associated with survival suits.