---
title: 'Photo of the Week #8'
date: 2008-05-30T13:25:00.000Z
slug: photo-of-the-week-8
image:
  src: /files/blog/photo-of-the-week-8/emory-rice-day.jpg
  alt: Jousting at Emory Rice Day

tags:
  - emory rice day
  - jousting
  - mass maritime
  - photo of the week
---

One of the activities they had at Emory Rice day was the jousting platform. My room mate (on the left) spent plenty of time on it, as he seemed to be the reigning champion. In this round, he was battling our platoon leader, which made it even more entertaining!
