---
title: 'End of the Semester, and Beginning of Finals'
date: 2008-06-06T00:48:46.000Z
slug: end-of-the-semester-and-beginning-of-finals

tags:
  - final exams
  - mass maritime
---

This week is the last full week of classes for the semester, marking the beginning of the end. Starting on Wednesday next week, final exams will be given. We'll be having closed-door study hours on the freshman decks to allow us to study without disturbances, and prepare for finals more effectively.

We're also making preparations to go home for the summer. As part of the procedure for closing out the semester, we have to fully clean the company, as well as our rooms, to an Admiral's-like level. Once the company and all the rooms are cleared by the company officer on the last day, we are free to go. Obviously this is a major priority with cadets!

Since the regular regimental routine for freshmen is all but over for us, I've been giving some thought to what we did this year, and what we'll be doing next year. We dealt with a lot of structure and commitment this year, most of it pretty frustrating (cleaning stations in the morning & evening, musters, study hours, etc.) and seemingly in the way. However, as much as we may have complained about it, we all had that in common, which I've come to realize has really helped bond us as a company, and as shipmates. That's not to say that I enjoy those things any more - I don't, especially cleaning stations - but simply that I can appreciate them for their effect on us. Next year we'll have almost no regimental obligations, short of inspections and morning formation. We'll basically be regular college sophomores with uniforms. It's going to be nice to have more freedom and responsibility, but at the same time it's going to be weird not having somebody looking over our shoulder constantly.

Anyway, enough personal reflection. Countdown to expected departure: 11 days, 15 hours, 12 minutes!
