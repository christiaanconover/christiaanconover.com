---
title: 'Photo of the Week #9'
date: 2008-06-06T16:13:48.000Z
slug: photo-of-the-week-9
image:
  src: /files/blog/photo-of-the-week-9/sheepscot.jpg
  alt: Sheepscot River

tags:
  - boston island
  - photo of the week
---

While it's not a photograph taken on the MMA campus or of a ship, it still has relevance to maritime activities. My family has a summer place in Maine on the Sheepscot River, which is the body of water shown above at sunset. In addition to the river being a large lobstering area, it also happens to be one of the heaviest concentrations of lighthouses in the United States - 4 or 5 of which are all within a few miles of each other!
