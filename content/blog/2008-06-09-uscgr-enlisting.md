---
title: "Coast Guard Reserve: I'm Enlisting"
date: 2008-06-09T14:42:41.000Z
slug: uscgr-enlisting

tags:
  - coast guard reserve
  - us coast guard
---

As I've [mentioned on here before](/blog/photo-of-the-week-4/), along with [some of my readers](http://www.cgblog.org/2008/05/cg-blog-find-1-christiaanconovercom.html), I'm planning on going into the Coast Guard. In fact, I'm making that happen very soon. I've been speaking with a Coast Guard recruiter, and last Thursday I went and met with him. We discussed options, available Reserve positions in the area, and my goals as a Reservist. I am very serious about doing it, so I scheduled an appointment to take the [ASVAB](http://en.wikipedia.org/wiki/ASVAB) and get a medical examination for this coming Thursday.

As it stands, I'll be going to boot camp this summer, reporting some time in early July. While I'd love to be able to make regular blog entries while I'm there to give a glimpse of what it's like in near-real time, I won't be allowed to do so. However, I will make posts periodically between now and then about the enlistment process, and my preparation for boot camp. I'll add a category to the Log Book for Coast Guard Reserve so you can follow that if you'd like.

There are a few interesting facts I thought people who are interested in joining the Coast Guard (Reserve) might like to know:

-   If you are an Eagle Scout, you can enlist directly as an E-3 instead of an E-1. For those not familiar with the miliary rank and pay grade system, that means a higher enlisted rank (Seaman instead of Seaman Recruit) and higher pay right off the bat.
-   If you're in college, you can be eligible for up to $4,500 per year in tuition assistance, plus additional money from the G.I. Bill.
-   Almost all enlisted rates have involvement with law enforcement, regardless of primary mission. So, if you like to cook but still want to have the opportunity to do the cool stuff you see in Coast Guard videos and posters, you can probably do both!

I'll discuss the ASVAB and medical exam more after I get back from it on Thursday.

Semper Paratus!
