---
title: 'Sea Term 2009: Ports of Call'
date: 2008-06-10T12:49:15.000Z
slug: sea-term-2009-ports-of-call

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

"Wait a minute Christiaan, this school year's not even over and you're already talking about next _Sea Term?_ What's up with that?"

Since Sea Term is such a large undertaking, as soon as we return from one the planning begins for the next one. One of the biggest decisions that has to be made is what ports of call we will make. Fortunately, the cadets get to have some input on this decision. A few weeks ago, there was an open meeting for all cadets going on Sea Term 2009 to vote on the choices the Sea Term planners had narrowed down. The decisions then had to be finalized, and we now know which ports we will be stopping in:

1. Nassau, Bahamas
2. Tampa, FL
3. St. Thomas, USVI

Due to the price of fuel, we can't go as far afield as was possible in previous years, which is why we're not too far into the Caribbean, let alone South America or Europe.
