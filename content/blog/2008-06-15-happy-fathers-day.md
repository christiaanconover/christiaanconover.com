---
title: "Happy Father's Day"
date: 2008-06-15T05:42:05.000Z
slug: happy-fathers-day
image:
  src: /files/blog/happy-fathers-day/five-islands.jpg
  alt: Dad and me at Five Islands

tags:
  - "father's day"
---

Happy Father's Day everyone!

My dad is a remarkable man. He is the father of two, and a devoted husband. He is a retired Army officer of 23 years. His encouragement and guidance have helped me determine and soon achieve my goal of becoming a member of the United States Coast Guard, as well as most other achievements in my life. I am proud that he is my father, and can only hope to be as fine a man, officer, and citizen of this country and world as he is. Thank you, dad. Happy Father's Day.
