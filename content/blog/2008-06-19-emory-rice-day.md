---
title: Emory Rice Day
date: 2008-06-19T02:27:25.000Z
slug: emory-rice-day

tags:
  - emory rice day
  - marching competition
  - mass maritime
---

I know this post is really, really long overdue, but I got a Skribit request that I write it, so I thought it was about time I got around to doing it. I've [mentioned Emory Rice day before](/blog/this-week-at-maritime-upcoming-event-report/), where I described the planned itinerary for the day and gave a brief description of what the day is.

First off, the marching competition. First Company was, naturally, the first company to march. I'd like to say that we did well, but I'd be lying. We pretty much fell on our faces. It didn't help that there was a ship going through the canal while we were marching that happened to blow its whistle right as the company commander would give marching commands, but there are no asterisks in these competitions, so we weren't given any leeway in scoring because of it. We messed up a couple of times, but my personal favorite was when we were given the command "to the rear march" (everyone pivots 180 degrees at once, resulting in the entire platoon marching back the way they came) right at the ship blew its whistle, at which point the back half of the company turned and the front half kept marching because they hadn't heard the command. The entire thing was pretty classic. We came in last for that part of the day.

During the day classes continued as usual, with one exception: dress-down tickets had been sold for the day, so the majority of cadets were in civilian clothes. We usually have two or three dress-down days each semester, with the proceeds from the tickets going to a local charity. I'm not sure where the money went for Emory Rice day, but I believe it was a charity.

That afternoon after classes we had a field day. The school had rented a number of field activities, such as a rock climbing wall, a [jousting table](/blog/photo-of-the-week-8/), a dunk tank, and a number of other things. Various clubs and groups at school had their own activities as well. The ROTC program set up an obstacle course on Cadet Beach, which I didn't do but heard that it was pretty good (First Company won this event!). In the mess deck, we had a cookout which was a nice change from the usual routine. Unfortunately, it rained that afternoon or the cookout would have been outdoors. That night they showed a movie in the gym, giving the cadets the choice of what movie to watch. We chose [Boondock Saints](http://www.imdb.com/title/tt0144117/).

Emory Rice day was a lot of fun, and a great way to mark the end of the year. I'm looking forward to it again next year, but hopefully it won't rain again!
