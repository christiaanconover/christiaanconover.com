---
title: 'Off to Boot Camp!'
date: 2008-07-14T05:36:43.000Z
slug: off-to-boot-camp

tags:
  - boot camp
  - cgblog.org
  - coast guard reserve
  - military
  - us coast guard
---

I'm writing this at about 0130, mere hours before I will be hitting the road to go up to Boston and start my journey through boot camp and into the Coast Guard. I'm excited, and a little nervous, but I'm confident that I will be able to handle it.

I may or may not be posting through snail mail, having my mom add my posts for me. I will, however, be posting via snail mail over at [AN UNOFFICIAL COAST GUARD BLOG](http://cgblog.org), as I am the newest writer for that blog. I am excited and honored to join such a great publication, and such fine writers and bloggers. So keep an eye out for my posts over there (and probably here too!) over the next 8 weeks.

Also, if you feel like writing me a letter, whether it be just to say hi, or to ask me a question about something you'd like me to address in a blog post, here is the address:

**SR Conover<br>
USCG Training Center<br>
1 Munro Avenue<br>
Cape May, NJ 08204-5083**

I'll be going almost immediately from boot camp graduation in September up to [MMA](/tags/mass-maritime/), so expect normal blogging to resume after boot camp is over.

Semper Paratus!