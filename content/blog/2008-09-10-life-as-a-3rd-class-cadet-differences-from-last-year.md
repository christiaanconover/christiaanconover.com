---
title: Life As a 3rd Class Cadet - Differences From Last Year
date: 2008-09-10T01:37:13.000Z
slug: life-as-a-3rd-class-cadet-differences-from-last-year

tags:
  - mass maritime
---

Holy smokes, he's back! Yes, after being cut off from the real world for two months, I am finally [finished with boot camp](/blog/off-to-boot-camp/) and starting my sophomore year at MMA. I've been back at Mass Maritime for two days now, having arrived at school yesterday morning. Already I've noticed differences in daily life between being a fourth class and being a third class.

First off, the most obvious differences: We no longer have cleaning stations and study hours. I no longer have to check the cleaning bill to see if I have to be up at 0545 to sweep the decks or scrub toilets, and do it again that night. I don't have to keep my door open for two hours each night pretending to get work done while people file in and out of my room, keeping me from being able to focus. As third class cadets, we pretty much keep to ourselves, and are left alone most of the time, which I like.

We also have fewer inspections, and the ones we do have are at more convenient times. We only have three inspections a week (instead of six as freshmen), and they're all on weekday mornings (instead of having one on Sunday night). As a result, I get to sleep in a little later on mornings that I don't have inspection.

Unfortunately, my academic work load has increased somewhat. Mainly, I've noticed that I have a lot more reading to do than I did last year. While I don't particularly mind reading itself, I have a nasty habit of falling asleep when I am reading from a book for more than about 15 pages, so I'll have to find a way to overcome that.

I've been enjoying watching this year's fourth class cadets around campus, watching them do the things that we had to do - running any time they're outside, squaring corners when they're in the dorms, etc. I've also been greeted as "sir" by at least a dozen fourth class in the past two days, even though I'm only a third class, so I've had to remind them that "blue-taggers" (my class has blue name tags) are not sir or ma'am, we're not much different from them in fact. I actually find it pretty funny.

This year we get liberty on Wednesdays, starting after our last class of the day until we have to sign in between 2300 and 0000. While I'm really looking forward to having time to get off campus in the middle of the week, I'm not really sure what I'm going to do with the time, and foresee it providing a good opportunity to waste money. Still, it's an opportunity to take a break mid-week, so I'll take it.

Not much else seems to be going on around here that's worth reporting on, although tomorrow morning we have a meeting to vote on our ports of call for [Sea Term 2009](/tags/sea-term-2009/), so once I find out the final decision on those I'll post it.
