---
title: My Name
date: 2008-09-16T04:14:30.000Z
slug: my-name
---

I received a suggestion this week that I explain the basis/history of my name, and why it has an unusual spelling.

The spelling of my name is Dutch, having two A's at the end. When my parents were picking names, they considered naming me Christian, but decided it was too common (though I don't even meet too many people with that spelling). My dad was reading a book one day which mentioned a Dutchman with the name Christiaan, and my parents decided it was unusual enough.

I used to be frustrated by my name sometimes, since it is very uncommon, but I've come to realize that it's a gift to have such a unique name. When I bought the web domain for this blog, for example, I was sure that there were no other Christiaan Conovers who may have already bought it.

My name is so uncommon, in fact, that according to an [online check I did a few months ago](/blog/i-dont-exist/), _I_ don't even exist.

That's the story of my name, and I'm stickin' to it!
