---
title: Merchant Mariner Documentation (MMD) - Part 1
date: 2008-09-29T13:34:46.000Z
slug: merchant-mariner-documentation-mmd-part-1

tags:
  - mass maritime
  - merchant mariner documentation
  - us coast guard
---

This year my classmates in license majors and I started the Merchant Mariner Documentation process. Merchant Mariner Documentation, or MMD, is the Coast Guard's merchant marine identification and management system, keeping track of all registered merchant mariners in the United States. It is required to have MMD in order to ship on any commercial vessel in the U.S., so we start the process about 15 months prior to shipping commercially.

We started the process by filling out the initial application, which included the usual pedigree information, as well as questions about personal history (legal issues, etc.) so that the Coast Guard has a record of full disclosure. Each applicant undergoes a background check, so they want to make sure you're honest and forthcoming about any issues they may discover in the process.

We're waiting on the next step right now (I'm not sure when or what that is), but once we reach that I'll make another post about it.
