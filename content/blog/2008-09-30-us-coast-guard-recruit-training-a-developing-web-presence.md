---
title: U.S. Coast Guard Recruit Training – A Developing Web Presence
date: 2008-09-30T14:18:57.000Z
slug: us-coast-guard-recruit-training-a-developing-web-presence

tags:
  - boot camp
  - cgblog.org
  - us coast guard
---

As I'm sure regular readers know, [I went to Coast Guard Basic Training this past summer](/blog/off-to-boot-camp/), having [enlisted in the U.S. Coast Guard Reserve](/blog/uscgr-enlisting/). Since coming back to MMA, I've had quite a few of my shipmates ask me about it, some of whom are interested in doing it themselves. In my Internet travels, I have discovered some resources, some new and some well-established, that may be of interest to those with questions about recruit training.

First off, there is a video series that the Coast Guard has put together called "It's Just Eight Weeks" which highlights portions of recruit training, to give prospective enlistees an idea of what training is like. My understanding is that the videos will soon be obsolete, as they are overhauling the training program and a lot of things are going to change. That said, it's still an interesting series. You can find it here: [It's Just Eight Weeks](http://www.coastguardchannel.com/24_7_vid/EightWeeks.shtml)

The next sites seem to have shown up in time for the Commandant's announcement of expanding social media and Web 2.0 usage in the Coast Guard. First up the TRACEN Cape May's own web site, which seems to still be under construction, but has useful information nonetheless. You can find that by going to [uscgbootcamp.com](http://www.uscgbootcamp.com).

The last site appears to be the newest of all. TRACEN Cape May now has a blog on Blogger, with the very first post dated 23SEP08, which is only a day or two after the Commandant's announcement. The first "from recruit training" post on the blog shows a recruit company doing pugil stick training, so they're obviously wasting no time in getting cool information out on the web. You can find the blog at [uscgbootcamp.blogspot.com](http://uscgbootcamp.blogspot.com).

One thing I find really cool about the last two sites is that they say right up front that they will be posting submissions from recruits during their training. Some of you may be aware that while I was in boot camp, [I posted on CGBlog.org](http://www.cgblog.org/search/label/by%20Christiaan%20Conover) via snail mail about my experiences. I think it's awesome that the Coast Guard is now embracing this idea as official policy.
