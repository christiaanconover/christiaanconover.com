---
title: 'Photo of the Week #11'
date: 2008-10-03T12:50:54.000Z
slug: photo-of-the-week-11
image:
  src: /files/blog/photo-of-the-week-11/sanko-blossom.jpg
  alt: Sanko Blossom

tags:
  - oneeighteen
  - photo of the week
---

This week's Photo of the Week comes from Flickr user [OneEighteen](http://flickr.com/photos/oneeighteen/), a Houston ship pilot. The photo is of a tanker in the Houston ship channel, the Sanko Blossom. I thought it was a cool picture because of the sheer massive size of the ship, and the way that's really emphasized by the angle from which the shot was taken.

I haven't gotten any new photos this year around campus. If you have any photos you'd like to submit for Photo of the Week, please e-mail me. Thanks!