---
title: Training Ship Name Change
date: 2008-10-09T16:42:40.000Z
slug: training-ship-name-change

tags:
  - mass maritime
  - sea term
  - ts enterprise
  - ts kennedy
---

As I'm sure most people are now aware, the Academy trustees have voted to rename the training ship to the _USTS Kennedy_, in honor of Ted Kennedy's service as a U.S. Senator for Massachusetts. I was asked last week via Skribit that I write about cadet reactions to the name change.

There hasn't been much discussion among cadets about the name change, to be honest. We heard a little bit about it at the beginning of the year, and were asked to express our opinions via e-mail about a possible name change. A few weeks later we were informed that the change was going to take place. Since the announcement, there have been some passing comments made, but nobody's discussing it too much since the decision's already been made.

As far as the comments that are being made: certainly there are jokes and such being made about naming a ship after a man who had an accident involving the water, and other jokes related to that. However, there have also been positive comments about the fact that it may help bring the school some more publicity.

I haven't really tried to get involved in such discussions since I really don't know much about Ted Kennedy. However, I do know that he's been a public servant for many, many years and that, despite personal opinions of his mistakes or policies, his time in office is a reflection of patriotism in itself, so I don't see any reason we shouldn't honor that. Just my 2 cents.

What do you think of the name change? Please post your thoughts in the comments area below.
