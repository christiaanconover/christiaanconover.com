---
title: Google Maps Updates MMA
date: 2008-10-14T04:53:20.000Z
slug: google-maps-updates-mma

tags:
  - google maps
  - mass maritime
  - solar power
  - wind turbine
---

On a whim, I happened to look up Mass Maritime on Google Maps tonight, and discovered that they have recently updated their imagery of this area. You can now see the addition built on top of the 1st and 2nd company dorms, as well as the 81kW solar array on top of the addition. The windmill is also clearly visible, along with the new turf field.

**[Check out the new satellite imagery of Mass Maritime](http://maps.google.com/maps?f=q&hl=en&geocode=&q=101+Academy+Dr,+Buzzards+Bay,+MA+02532&sll=37.0625,-95.677068&sspn=38.502405,79.101563&ie=UTF8&ll=41.741186,-70.620568&spn=0.00887,0.019312&t=h&z=16)**
