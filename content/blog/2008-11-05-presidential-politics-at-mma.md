---
title: Presidential Politics at MMA
date: 2008-11-05T00:09:34.000Z
slug: presidential-politics-at-mma

tags:
  - elections
  - mass maritime
---

I received a suggestion a while ago (sorry for the delay to whoever left it) to talk about what opinions are being expressed about the presidential election. To be honest, I've been tiptoeing around the issue on my blog, since this is such a polarized election. However, since today is election day and what's done is done at the time that I'm writing this, I figured it might be a good time to discuss it.

Opinions seem to be quite strong in the regiment, or at least among the people I spend the most time with. Most people appear to be McCain supporters, but people on both sides are not being ambiguous about their politics. It's definitely made for some very interesting and high-powered arguments and discussions among cadets. I'm really glad to see it, since it means that people are taking a strong interest in their political system and in how the government works.

I'm writing this just after 7pm on Election Day, so the first numbers are being reported. No matter which way the election goes, the atmosphere on campus tomorrow is going to be charged.
