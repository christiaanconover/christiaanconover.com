---
title: Drug Testing at MMA
date: 2008-11-12T00:26:24.000Z
slug: drug-testing-at-mma

tags:
  - drug testing
  - mass maritime
---

I received a suggestion a few weeks ago to talk about drug testing. As most people involved with the Academy know, cadets are randomly drug tested to stay in compliance with Coast Guard licensing requirements. Testing has always been conducted regularly at the Academy, but this year it's being done with greater frequency. Starting this year, testing is being conducted twice a month, instead of just once a month as it used to be. This is being done for a couple of reasons. First off, the number of cadets in the regiment has been increasing over the past few years, so they need to test more often to fit more people in. Second, it keeps people "on their toes" about drugs here at the academy, and hopefully deters people who may otherwise have gambled with their chances of getting tested when it was just once a month. The tests are not scheduled, and are spaced unevenly and scheduled at different times and dates each test to make it even more of a deterrent from using drugs.

Not all cadets are tested an equal number of times. Last year I was chosen to be tested for the first testing of the year. I didn't get tested again until the beginning of this year. I have friends, however, who were tested multiple times last year, and some people have been tested more than once already this year. It's luck of the draw, so some people get to experience it more than others. It's not a big deal, but it's annoying to be woken at 0530 to go get tested. Really, drug testing is a very minor aspect of life at the academy.
