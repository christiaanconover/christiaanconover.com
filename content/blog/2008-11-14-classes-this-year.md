---
title: Classes This Year
date: 2008-11-14T02:29:38.000Z
slug: classes-this-year

tags:
  - mass maritime
  - navigation rules
  - sea term 2009
---

I received a suggestion this week that I talk about how my classes are going so far this year. To be honest, classes are much tougher than they were last year. Now that I'm starting to get into the core material for my major, the learning curve is a little steeper.

Rules of the Road is probably my most difficult class, as is the case for many other sophomore Deckies. This is the class where we learn the regulations for ship movement and traffic, as well as how to identify other vessels. It is a lot of memorization, and there are many exceptions and differences depending on geographic location to learn. As if the class itself didn't provide enough of a challenge, passing it is a requirement for being able to go on [3rd class Sea Term](/tags/sea-term-2009/). As such, it's a major part of 3rd class deckie study time each week.

Calculus is giving me some difficulty as well. I have always struggled with math, and calculus is no exception. I'm pushing through it, but it is definitely a challenge. A number of my classmates are struggling with it as well.

Fortunately, the semester is close to being over. We only have another 4 and a half weeks of class time left, including exams. Once the semester ends, all focus shifts to [Sea Term 2009](/tags/sea-term-2009/), which I'm very much looking forward to!