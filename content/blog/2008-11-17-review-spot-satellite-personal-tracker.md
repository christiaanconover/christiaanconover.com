---
title: 'Review: SPOT Satellite Personal Tracker'
date: 2008-11-17T04:44:42.000Z
slug: review-spot-satellite-personal-tracker
image:
  src: /files/blog/review-spot-satellite-personal-tracker/spot00.jpg
  alt: SPOT satellite tracker

tags:
  - gadgets
  - sea term
  - sea term 2009
  - spot
---

I recently received a [SPOT Satellite Personal Tracker](http://www.findmespot.com/en/) as part of a contest run on [gCaptain](http://gcaptain.com). For those of you unfamiliar with the device, it's a GPS antenna and commercial satellite transmitter integrated inside of a handheld case, with a few different functions thrown into the mix. You can use it to track your location throughout the world, send assistance requests, as well as distress and emergency calls. For more detailed information, check out the [SPOT web site](http://www.findmespot.com/en/), and perhaps watch the video they have featured on their home page. This device is an excellent companion for any mariner, and is pretty affordable for what it does.

# First Impressions

The first thing you'll notice about the SPOT is the box it comes in. That may sound a little obvious and stupid, but take a look at it and you'll see what I mean:

![SPOT Box](/files/blog/review-spot-satellite-personal-tracker/box00.jpg)

![SPOT Box](/files/blog/review-spot-satellite-personal-tracker/box01.jpg)

![SPOT Box](/files/blog/review-spot-satellite-personal-tracker/box02.jpg)

![SPOT Box](/files/blog/review-spot-satellite-personal-tracker/box03.jpg)

The box is heavy on bright orange and photographs of dangerous situations. The company's slogan, "Live to tell about it" is emblazoned all over the box. It's certainly eye-catching, and immediately gives the consumer the impression that the company wants: this device is crucial to saving your life.

Once you open the box, it's more of the same: flashy catch phrases and bright colors, all pointing to the same concept. This was my favorite phrase on the box:

![SPOT Box](/files/blog/review-spot-satellite-personal-tracker/box04.jpg)

It's not my favorite because of the unpleasant image it conveys, or because it's sort of cleverly worded, but because of its bluntness: use the SPOT, and you probably won't die. I actually find it a little funny.

Once you finally get inside the box, you find the SPOT. It's about the size of a regular point-and-shoot digital camera, and about twice as thick. When you take it out of the box, the first thing you notice is the rugged case. This this is obviously meant to take a beating. It's made out of hard plastic, with a big rubber surround and grip.

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot01.jpg)

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot02.jpg)

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot03.jpg)

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot04.jpg)

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot06.jpg)

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot07.jpg)

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot08.jpg)

As you can see from the pictures, my unit has already gotten a few bumps and scratches. I've dropped it on concrete, left it out in the rain, and it doesn't even seem to notice. It is definitely a solidly built product, capable of going where it's intended.

The unit sits well in your hand as well (not that it spends much time there, since there isn't a screen). The rubber grips on the sides do a good job of making sure it won't slip out of your hand. If it does slip and fall overboard however, you don't need to worry too much since it floats:

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot09.jpg)

Unfortunately, it seems that it doesn't like to stay upright for very long. So, if you happen to be sending a distress call and it falls in the water, it may not be able to keep sending that signal until you recover it and aim it back toward the sky.

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot10.jpg)

Once I got it assembled and activated it on the SPOT web site, it was time to test it out.

# Using SPOT

The unit is incredibly simple to use. To turn it on, just press the "On/Off" button so that the LED blinks. You have to let it sit for a few seconds after you turn it on before you can use other functions. Once it's ready, just press a function button (there are only 3) based on what you want it to do. The manual explains the specifics on how to do it, so I don't feel like I need to go into them here.

![SPOT Tracker](/files/blog/review-spot-satellite-personal-tracker/spot05.jpg)

Put it in a place where it will have a clear view of the sky, and then just let it go to work. I usually use it in tracking mode. When this feature is on, it sends a GPS position to the SPOT system via satellite every 10 minutes. You can then go back and look at your "breadcrumb trail" later in your account control panel, or if you have Shared pages, other people can see it in real time. I have this feature enabled for mine. You can find my public page by going to [spot.cconover.com](http://spot.cconover.com), or by clicking on the SPOT logo on the right sidebar on my blog.

The GPS appears to be very accurate, often pinpointing you down to the parking space or dock slip you're in. Sometimes the data is a few meters off, which you really only notice when you have it tracking while you're in one spot for an extended period of time as you'll have a number of points in a cluster around the spot where you actually are. That said, for the most part it's right on target.

The SPOT satellite network is very reliable as well. When in tracking mode, the unit will transmit your position every 10 minutes. Even when traveling in urban areas it manages to successfully send the data almost every time. This is particularly handy for me, since I have an 8 hour drive between school and my house, traveling through many urban areas. It's very nice for my parents to be able to hop online and see where I am at any point during my trip instead of having to call me.

The [worldwide coverage of the SPOT network](http://www.findmespot.com/en/index.php?cid=1200) is very good as well. This will be especially nice for [Sea Term](/tags/sea-term-2009/) this year so that friends and family will be able to track us throughout the trip and see exactly where we are. For individual use it's good for the recreational boater who may not have an EPIRB, or can't get a cell phone or VHF signal.

Battery life on the SPOT is very good as well. The manual says that it can send up to 1900 messages on a set of batteries, which if you ran the tracking feature continuously 24/7 would give you about 2 weeks of tracking. The device does require Lithium batteries, and they make sure you know that by writing it everywhere they can find on the back of the device. Lithium batteries are non-rechargeable (unlike their lithium-ion counterparts in cell phones and digital cameras), and are more expensive than regular alkaline batteries. However, they're not that much more expensive, and last significantly longer, as well as being more resistant to damage from harsh climates. I've used mine almost every time I drive anywhere and it's still running strong on the original set of batteries.

So far I've been impressed with the SPOT's performance. I'm eager to see how it does out at sea in a couple of months, and until then I'm going to keep using it in various conditions to see how it does.

Have you used a SPOT, and if so what's been your experience? If not, would you consider getting one?