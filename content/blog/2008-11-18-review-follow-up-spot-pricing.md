---
title: 'Review Follow-Up: SPOT Pricing'
date: 2008-11-18T13:32:34.000Z
slug: review-follow-up-spot-pricing
image:
  src: /files/blog/review-spot-satellite-personal-tracker/spot04.jpg
  alt: SPOT satellite tracker

tags:
  - sea term
  - sea term 2009
  - spot
  - us coast guard
---

I've received a number of e-mails in response to [my SPOT review](/blog/review-spot-satellite-personal-tracker/) asking how much it costs. I decided I'd just make another post to address this, rather than answer all the individual requests. Here's how the pricing breaks down:

# Basic Service: $99.99/year

This provides check-in (send "I'm OK" messages to people you pre-select along with your location), help requests (non emergency) and 911 requests. You get unlimited usage of these services for this fee. In order to use SPOT at all, you have to at least purchase Basic Service.

# Tracking: $49.99/year

This is an additional feature on top of Basic Service. This allows you to keep a "breadcrumb trail" as I discussed in my review. In Tracking mode, the device will send a position update to the SPOT system every 10 minutes, which you can go back later and look at in your account control panel. This also provides public Shared pages, which allow you to share your tracking with other people on publicly viewable pages. Each page has a 500 views-per-day limit as of this writing, but don't worry: you can create multiple public pages, and even password protect a page if you want. I make use of this by having a general public page at [spot.cconover.com](http://spot.cconover.com), but I also have a password-protected page for my family so that they'll always have a page they can see, without having to worry about exceeding the view count.

# Search & Rescue: $7.95/year up front, $150/year each subsequent year

The pricing on this one may seem a little confusing. Basically, you have the option to purchase this service when you first activate your SPOT. If you do this, then you'll only pay $7.95 for this for the first year. Every year after that, it will be $150 per year. So what does this buy you? SPOT has partnered with [GEOS](http://www.geosalliance.com/sar/), who provide private search & rescue services worldwide. For this yearly fee, you'll receive up to $100,000 of search & rescue services when you hit that 911 button. This will cover not only the cost of the private SAR services from GEOS, but fees incurred from any public/official SAR agency for which you may be responsible. It's also nice if you spend a lot of time in places where public SAR agencies may be poor or non-existant. I would think that this wouldn't be as important for mariners who transit the waters of developed nations where public SAR services are very good, such as the Coast Guard. However, in areas that don't have such organizations, this may prove to be vital.

SPOT has a [page on their web site that covers pricing](http://www.findmespot.com/en/index.php?cid=1300) if you want to see it there.

**What services would/do you use if you got/have a SPOT?**
