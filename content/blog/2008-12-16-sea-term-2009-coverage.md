---
title: Sea Term 2009 Coverage
date: 2008-12-16T22:42:12.000Z
slug: sea-term-2009-coverage

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

Well, we're almost halfway through finals week, and only a few weeks away from [Sea Term 2009](/tags/sea-term-2009/). In keeping with last year, I'll be blogging during the voyage about the trip, keeping friends & family of Sea Term cadets up-to-date. This year, however, I have the benefit of knowledge and experience from last year's Sea Term to share with people prior to departure. So, I'll be starting my Sea Term coverage this week and posting throughout the holiday break with tips, information and updates about Sea Term that could be useful.

If you have any questions or suggestions of things you'd like me to address about Sea Term, please leave an entry in the Skribit box on the right side of the page. Thanks!