---
title: 'Sea Term 2009: Communications'
date: 2008-12-17T05:44:56.000Z
slug: sea-term-2009-communications

tags:
  - mass maritime
  - sea term
  - sea term 2009
  - seawave
  - skype
  - ts kennedy
---

One of the biggest issues on cruise last year was communications. While we're at sea, we have very limited contact with the rest of the world. We have an e-mail system that connects a few times a day to send and receive e-mails held in the queue, called SeaWave. And that's it. No Internet, no phone. Just a delayed e-mail system.

It may sound like a problem, but in reality it's all we really need. Most of the time, we're too busy to need much more than that. However, [you may remember](/blog/panama-canal-again-seawave-situation/) that we had some problems with SeaWave last year. This year, we have two new SeaWave units on board the ship, which should prevent us from having the same problems we did last year.

One of the issues that most people seem to be concerned with, and that the school stresses to freshmen, is the cost of SeaWave. It's important to make sure that only text is sent and received over the system, and not pictures or other attachments. SeaWave charges the user based on the number of seconds a message takes to be sent or received, not just on the size of the message. This is an important distinction, because in rare instances of poor satellite reception a simple two or three sentence text-only e-mail can end up costing a lot. I say this not to scare people away from SeaWave or make people worried about their bill. However, if your son or daughter's account suddenly has a $3 or $4 e-mail, don't just assume that they were sending a picture of their time in St. Thomas, it may be a fluke. As I said though, this is very rare and only happened to a few people last year while we were down around the Equator, so it shouldn't be much of a concern this time.

In general, SeaWave is very affordable. I did a lot more e-mailing than most people last year due to my blog, and my bill was still less than $30 for the whole cruise. This included my blog posts, e-mails with friends and family, as well as e-mailing done with people contacting me through my blog. A typical full page printed e-mail usually cost me about 10 cents. A paragraph or two is about 4 or 5 cents.

Of course, there's no requirement to use SeaWave. There are plenty of opportunities to make contact with home when we get into port. This is especially true this year, since one of our ports is in the U.S. so cell phones will work without paying roaming or international charges. In other ports, phone cards are available. I would recommend, however, that you pick up an AT&T or similar international calling card before cruise. Phone cards in port, especially in more touristy ports, tend to be expensive (Costa Rica's phone cards cost $7 for 12 minutes of talk time, 2 of which were charged simply for connecting the call).

Another option to consider is [Skype](http://skype.com). This is free software that allows you to make calls over the Internet. The calls are completely free from anywhere in the world if you call computer-to-computer, and only cost 2 cents a minute to call from the computer to a U.S. phone number. Since the laptops purchased through the school store all have microphones built in (and most other laptops on the market do as well), this would be a very feasible option. My family and I stayed in contact this way last year, and we were able to talk for hours completely free once I got on WiFi.

There are plenty of accessible and affordable ways to stay in touch during cruise. If you know of any other methods I didn't cover, or would like to know more about something I discussed, please feel free to leave a comment below.
