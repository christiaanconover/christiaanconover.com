---
title: 'Sea Term 2009: Laptops'
date: 2008-12-18T02:20:39.000Z
slug: sea-term-2009-laptops

tags:
  - mass maritime
  - sea term
  - sea term 2009
  - seawave
  - ts kennedy
---

I received a Skribit suggestion that I talk about cadets bringing laptops on cruise. Not only _can_ cadets bring their laptops, but I would highly encourage it.

In the evenings on cruise we have plenty of down time, and having a laptop provides plenty of entertainment opportunities. Almost everyone brings DVDs as well as movies stored on their computers directly, so having a computer on board allows you to watch all the movies available (Note: Chartwells also rents movies to cadets very cheaply and they have a large selection).

The ship has a wireless network that covers most areas, which allows access to a number of things, some of which will be new this year. First off, the SeaWave e-mail system [I discussed in my last post](/blog/sea-term-2009-communications/) is accessible over the WiFi. There is also a movie server that allows you to stream movies over the network to your laptop, and has a fairly large selection. This year, we're adding a shipboard portal which will contain the Plan of the Day, general announcements, and other useful information that can be decided on later. Having a laptop will make getting to this information much easier.

There is a computer lab on board, but there are only 14 or 15 computers to serve over 500 people. Plus, this year more than ever before, it's going to be used heavily for classes at sea. Therefore, it's almost a necessity to have a computer as part of your personal equipment.
