---
title: 'Sea Term 2009: SeaWave Sign-Up'
date: 2009-01-03T04:31:01.000Z
slug: sea-term-2009-seawave-sign-up

tags:
  - mass maritime
  - sea term
  - sea term 2009
  - seawave
---

I've been hearing concerns about how to sign up for SeaWave, mainly regarding credit cards. In order to sign up for SeaWave, you do need a credit card so that usage can be billed. This information is necessary during registration to complete the process. If your cadet will be using the credit card of a parent and needs the information for the card, it will need to be provided to them so they can sign up.

During the first week of Sea Term (starting on Monday, 05 January and lasting that full week) we will be at the pier in Buzzards Bay, and will have Internet connectivity provided from a shore wire. You could e-mail the credit card information to your cadet's regular e-mail address, so that it cannot be misplaced and they'll have it readily available to them when they sign up. This is how I did it last year, and it worked quite well. I would not recommend writing it down on a piece of paper for them, as the chances of it falling into the wrong hands are much higher.

I have heard that SeaWave claims you can provide the name, address and phone number of the person holding the credit card, but omit the actual credit card information, and the credit card holder will be contacted by SeaWave directly for this information. I cannot attest to this from experience, but what I can say is that my experience with SeaWave tells me that making it as simple and uncomplicated as possible is the best option. I would suggest having the credit card information at the time of sign up rather than relying on SeaWave to call the card holder, in case they dismiss the sign up request as an incomplete submission and don't follow through, resulting in no SeaWave service.

Hopefully everyone is getting excited for Sea Term, since it's only 2 days away!
