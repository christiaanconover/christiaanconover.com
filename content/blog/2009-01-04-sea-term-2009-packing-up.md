---
title: 'Sea Term 2009: Packing Up'
date: 2009-01-04T05:49:32.000Z
slug: sea-term-2009-packing-up

tags:
  - mass maritime
  - sea term
  - sea bag
  - sea term 2009
  - skype
---

I'm writing this during a quick break I'm taking from packing my bags. I'll be leaving my house early in the morning (0630) to drive from Annapolis, MD up to Buzzards Bay, picking my car up in Connecticut. I'll stay with my grandfather tomorrow night, and then Monday morning I will join the 500 other cadets in checking in and moving on to the ship, to begin Sea Term 2009.

I've decided to post a few last-minute thoughts regarding packing and arrival. First off, if anybody doesn't know yet, cadets arrive in boiler suits on Monday morning. Since we'll be starting work almost immediately upon moving on to the ship, we need to be dressed for the job. Second, try to keep packing down to fitting in one bag, such as your sea bag. It makes it much easier to carry everything and move in if you only have one bag (plus a backpack or other small "carry on"). Last year I had two duffel bags, and it made it very interesting trying to fit through some of the narrow doors on the ship.

My mom informed me that there has been much discussion regarding cell phones in port. As [I discussed in an earlier post](/blog/sea-term-2009-communications/), cell phones are only a logical option in Tampa, since it's within the continental U.S. In our other ports, roaming and international charges will apply, so I would highly suggest not using cell phones at all in these ports. Instead, buy an international phone card before leaving, or find out about the cost of phone cards locally in the ports. I would also suggest using Skype as it's free computer-to-computer, and only 2 cents a minute to call from computer to U.S. phone number, no matter where the computer is in the world. Cell phones are definitely the worst option, even if you have an international plan since those plans are almost sure to be more expensive than the other options I've mentioned.

I'll be posting again tomorrow evening once I'm off the road, so if anybody has any last-minute "what should I bring?" questions they'd like to ask me, feel free to contact me or leave a Skribit suggestion.
