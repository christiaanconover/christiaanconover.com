---
title: 'Sea Term 2009: Less Than 12 Hours To Go!'
date: 2009-01-05T01:30:45.000Z
slug: sea-term-2009-less-than-12-hours-to-go

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

We're now officially less than 12 hours away from the start of [Sea Term 2009](/tags/sea-term-2009/). At this time tomorrow morning, the freshmen will be checking in and starting their first briefing of Sea Term in Admirals Hall. A few hours after that, the ship will be alive with cadets moving in and starting the immense on-load process that will last all week. I'll be posting photos and videos of this process throughout the coming week, so stay tuned!