---
title: The First Day
date: 2009-01-06T04:22:53.000Z
slug: the-first-day

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

I'm laying in my rack as I write this, after a long, exhausting day. I got lucky with my rack assignment, and am at the end of one of the upper holds, in an area with a lot of space in the aisle (a very rare commodity on board), and I'm on the bottom. As a result, I'll have plenty of space to get dressed in the morning, nobody will be walking through my area to get to their rack, and it should be a little quieter. Sea Term is off to a good start for me so far!

This morning I got checked in and on board by about 0930, and was working by 0945\. I'm one of 3 cadets handling IT work on board the ship, and there's a lot to get done this year, particularly this week while we can still get equipment from shore and have Internet access. I worked about 12 hours today, and I'm sure tomorrow will be more of the same. I can't really complain much though, since the other two IT cadets are rates, and have been doing that for the past 5 days.

Being a sophomore this year, arrival day went much more smoothly than last year. In fact, I got to watch the freshman as they came onto campus this morning and a few hours later started boarding the Kennedy. Most of them carry a definite sense of excitement, as well as a little bit of nervousness which is pretty evident just looking at them. Even though they did Mini Cruise as part of Orientation, that was only 3 days and was in calm waters near shore. Living almost literally on top of each other for nearly two months, out at sea, visiting new places, and learning things very different from anything they've likely done before can be daunting, yet thrilling, so mixed emotions are evident, and expected.

Tomorrow will begin the on-load process, which will continue through the end of the week. During the entire day there will be people on the pier and on the ship carrying all manner of food, equipment, office supplies, and other miscellaneous items ad nauseum. Let me put in perspective the amount of food that will be brought aboard: during the next three days, palettes stacked as high as a person will be loaded onto the ship, for eight or nine hours each day, with a palette being hoisted aboard about once every 10 minutes. All of this loading is done almost entirely by the cadets, including managing the process. I will try to get some pictures and perhaps video of this to post this week.

It's time for me to sign off for the night, so I can get some sleep for another busy day tomorrow!
