---
title: About to Head Out
date: 2009-01-11T11:58:02.000Z
slug: about-to-head-out

tags:
  - mass maritime
  - sea term
  - sea term 2009
  - spot
---

I'm in my rack, about to get up to get dressed for watch. I have watch 0800-1200/2000-0000 today, so I'll be on the bridge while we're leaving the pier and heading out to sea! I'm pretty excited about it, but a little nervous since I'll be the first sophomore handling the underway log books this cruise. I'll be turning on the SPOT when I get out on deck, so you can follow us using one of the 5 SPOT pages listed in the SPOT box on the right. Stay tuned for updates while we're underway.
