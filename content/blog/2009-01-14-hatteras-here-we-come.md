---
title: 'Hatteras, Here We Come!'
date: 2009-01-14T06:15:03.000Z
slug: hatteras-here-we-come

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

As I'm writing this, we're passing Cape Hatteras, and the ship is definitely pitching and rolling! We're expecting 9-12 foot seas and winds steady at 35kts with gusts up to 45kts. I have watch 0000-0400/1200-1600 Wednesday and Thursday, so I'll be on the helm as we move south of Hatteras tonight, which I'm pretty excited about. There have been a number of people getting sick, which is a bummer, but completely expected.. Fortunately I haven't been one of them.

The weather started to get warmer throughout the day today. By this evening I was able to be out on deck with short sleeves comfortably, albeit for short periods of time. By tomorrow it should be warm enough to be out on deck nearly indefinitely in short sleeves. Hooray Gulf Stream!

We have a new system of checking on and off the ship this year, which should make accountability much more accurate and reliable, and the process of checking on and off much quicker. Every person on board has a wristband which resembles a Livestrong band, with a small lump where a small computer chip is placed. When put up against the scanner on the pier, it will instantly check the person on or off, and load their ID information on a screen where the watchstander is sitting. Hopefully it will make checking on and off much easier, especially when everyone wants to get off at once in the mornings. The wristbands were distributed this evening, and the IT rates and I will be installing it permanently over the next couple days.

All right, I'm done boring everyone with the details of the new system. Not much else of interest is going on right now, so I think I'll sign off and go get a snack from the mess deck & post a new SPOT position update.
