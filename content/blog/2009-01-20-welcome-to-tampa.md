---
title: Welcome to Tampa
date: 2009-01-20T21:12:27.000Z
slug: welcome-to-tampa

tags:
  - mass maritime
  - sea term
  - rugby
  - san juan
  - sea term 2009
  - seawave
  - tampa
---

I'm writing from the [John F. Germany Public Library](http://bkite.com/03TJK) in Tampa with 2/C Balcunas, checking e-mail and other various Internet items. It's cold here today (54 degrees Fahrenheit) so it's a good day to do indoor things.

Tampa has been a pretty mellow port for me so far. Sunday my grandfather and partner came up from Fort Myers and we spent a few hours together, touring Tampa and going out to lunch, which was really great. After that, I ended up catching up on sleep for the rest of the day. Yesterday I had watch, so between watch shifts I hung out watching movies and sleeping. Today is the first day that I've gone out and done stuff with other Maritimers, and tonight I'm planning on going to the MMA-USF rugby game. A bunch of my friends are going to go too, so it should be a good time.

As you've probably already heard, SeaWave/Globe Wireless went down towards the end of last week, which is why I haven't posted in a few days. A technician is coming from Globe Wireless tomorrow morning to fix it, so we should be back on the air by the time we're leaving Tampa. I look forward to being able to post from sea again.

I've gotten a number of good suggestions that I'm going to act upon once we get underway (thank you to everyone who submitted those). I've gotten quite a few requests for me to feature other cadets, which I plan to do starting on the trip from Tampa to San Juan, so keep an eye out. Who knows, your cadet may be the one quoted on here!

I'm going to sign off for now and head back out into Tampa. Hopefully next time I post I'll be doing so from aboard the T.S. Kennedy.
