---
title: 'Leaving Tampa, E-mail Restored'
date: 2009-01-22T04:15:03.000Z
slug: leaving-tampa-e-mail-restored

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

Our visit to Tampa is officially over. Liberty expired tonight at 2200 for sophomores, and 2100 for freshmen. It's been chilly for Florida the past couple of days, with daytime temperatures in the 50's, and last night the temperature was forecasted to get down to 29 degrees!

We'll be casting off at 0800 tomorrow morning, heading back out to sea. I'm assigned to mooring stations on the stern, which is the first mooring stations I've had so far on cruise. Hopefully the weather will cooperate tomorrow.

SeaWave/Globe Wireless is working again, which you may have guessed by nature of the fact that I'm even able to make this post. This morning a technician from Globe Wireless came to the ship, as well as the IT manager from MARAD for the Gulf region. We got two new servers, with updated software, as well as a lot of good information on how the systems actually work in order for us to be able to fix them ourselves in the future (hopefully). With all of that in place, we should be able to keep e-mail running for the rest of cruise, so everyone cross your fingers! Regardless, you should be able to e-mail with your cadet again.

We have our sailing muster at 0700 tomorrow morning, so I'm going to sign off for now so I can get some sleep for tomorrow. I'll post again tomorrow night. Puerto Rico, here we come!
