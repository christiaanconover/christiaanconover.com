---
title: Headed South and Finally Warming Up
date: 2009-01-23T06:15:02.000Z
slug: headed-south-and-finally-warming-up

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

We got underway this morning around 0800 and headed back out to sea. I was on mooring stations on the stern, and we were definitely feeling the effects of the cold front that came through Tampa at that hour this morning! I had a hat, coat and gloves on and was still cold out on deck. It was pretty cool to watch the tug help maneuver us out of the harbor, since I hadn't seen it help us coming in on account of finishing up the last few touches on our electronic wristband system for checking on and off the ship with less than an hour until needing it. Everything went very smoothly this morning, and by 0900 we were leaving Tampa to our stern.

I spent the entire morning below decks working, since my division is on maintenance rotation today and tomorrow. However, when I came back topside after lunch, I barely needed a sweatshirt, let alone a coat. We've finally reached warm weather, and it should only get warmer from here! Everyone seems excited about that fact, and about arriving in Puerto Rico in a week. However, there's a lot to be done in the next 7 days. We'll run through a couple of rotations, including watch for my division. I'm also pretty sure that we'll have our midterm before arriving in Puerto Rico. We have our work cut out for us, but the reward will be worth it.

In a few days we'll also have our first Sunday at Sea of the cruise. Sunday at Sea is the name for the activities that take place any Sunday that we're underway. Lunch and dinner are barbecue on the helo deck, with music and more. Everyone has the day off, with the exception of those on the watch bill for the day, but between their watch shifts they can enjoy the festivities too. Many people take the opportunity to sleep in, and then get in a few hours of tanning in the afternoon. It's basically getting liberty on the ship while we're underway. I'm certainly looking forward to it!

I'm going to sign off for now and get some sleep. I've got a busy day of maintenance and working on my voyage plan project tomorrow, so I want to be well rested to be able to keep up. More to come tomorrow. REMINDER: I'll continue to update the SPOT a few times a day, so take a look at our progress towards San Juan!
