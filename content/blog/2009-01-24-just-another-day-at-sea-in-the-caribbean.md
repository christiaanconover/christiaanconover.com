---
title: Just Another Day at Sea in the Caribbean
date: 2009-01-24T06:15:02.000Z
slug: just-another-day-at-sea-in-the-caribbean

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

Today was uneventful and routine. I had maintenance again, so I continued work on some of the IT projects I have going on right now. This evening my group worked some more on our voyage plan, which is coming along nicely. Other than that, not much happened.

The only notable event on board today was that we got ice cream in the mess deck. Though it might sound a little goofy to be news, ice cream is a big deal on cruise. With approximately 500 cadets on board, ice cream disappears faster than you can say "liberty" so it is rationed. When it does occasionally show up, the news spreads faster than you can walk from the bow to the stern. Last year it became somewhat of a superstitious double-edged sword though, as it seemed that ice cream was provided for a couple of days leading up to bad news. We'll see if that happens again this year :-)

We're currently rounding the western end of Cuba, getting ready to head east and thread the needle between Cuba and the Cayman Islands. The weather keeps getting warmer, and it feels awesome!

That's all I have for now. Tomorrow the rotation starts over and we go back to training. I'll post about what we do in classes tomorrow.
