---
title: 'So Long, San Juan'
date: 2009-02-03T02:50:56.000Z
slug: so-long-san-juan

tags:
  - mass maritime
  - puerto rico
  - san juan
  - sea term
  - sea term 2009
  - seawave
  - st. thomas
  - virgin islands
---

Today was the last day in San Juan, PR for us. I went on the catamaran and snorkeling exursion arranged by the school today, which was wicked fun. It was a gorgeous day, with plenty of breeze and warm water. This evening we have an early curfew since we'll be sailing tomorrow. Freshmen had to be back at 2100, sophomores at 2200, 1-bar seniors at 2300, and 2-bar & above at 2359\. A few friends and I went out to dinner tonight and then came back, making it an early night in preparation for getting underway again tomorrow.

The past few days have been a blast. As I mentioned in my last post, I had watch the first day so I didn't get off the ship. The second day I went out with a few friends to an area called Isla Verde, and hung out at the beach all afternoon. We went out in town that evening and had a good time going from place to place.

Tomorrow we leave, after taking on bunkers most of the day. We're scheduled to leave San Juan around 1500\. We arrive in St. Thomas on February 12, so we'll be underway for 9 days between here and the Virgin Islands. Hopefully my SeaWave account will be working properly again so that I can continue blogging during that leg of the voyage.

I don't have any pictures to post right now, as I forgot to charge up my camera the entire time we were here (I know, stupid move, but it happens). I'll get some from my friends to post once we arrive in St. Thomas, and I'll make sure I have plenty. I may even try to throw in a few videos.

I've also been looking into adding a few interviews with other cadets on board, so keep an eye out for that.

I'm going to sign off for now, and head to bed to get ready for resuming normal activities tomorrow. I'll try to post again tomorrow, SeaWave willing.
