---
title: Back To Blogging
date: 2009-02-08T06:15:08.000Z
slug: back-to-blogging

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

I'm back on the blog, after a brief hiatus due to a mix-up with SeaWave and my e-mail not working. It's back to normal now, so I'll be picking up where I left off. Also, I apologize if anyone e-mailed me and it bounced back due to my e-mail being down. If that's the case, please resend it.

Tomorrow is Sunday at Sea, so I'll be writing a nice, long post to catch up on what's happened since we left San Juan when I last posted. We're currently anchored in the harbor of Mayaguez, PR (it should show up on the SPOT), and will be here for the duration of Sunday at Sea tomorrow.

I need to go work on my voyage plan for now, so I'll sign off. More to follow tomorrow!
