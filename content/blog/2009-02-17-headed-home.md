---
title: Headed Home
date: 2009-02-17T05:50:49.000Z
slug: headed-home

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

This morning around 0900 we departed St. Thomas, turned north and started heading back to Massachusetts. My division is on the Alternate rotation today and tomorrow, though I'm not on the support bill today, which means that I was able to watch us leave St. Thomas. I then indulged in a morning nap, a first for me this cruise.

Now that our next port of call is the State Pier in Buzzards Bay, everyone is expressing the same sentiment: "Are we there yet?" along with discussions about when people think it will start getting cold again, when we will get cell service again, and the first thing they're are going to do when they get off the ship.

There are other signs that cruise is drawing to a close in day-to-day life on board the ship. Ice cream is more regular in the cadet mess cooler as the foodservice personnel try to use some of it up, which everyone is enjoying (I'm not eating it though, as I have a superstition about ice cream from the cadet mess cooler).

The current rotation is the second to last one of cruise. Wednesday will start the very last rotation, for which I will be on maintenance. After that, we have final exams, and then we're done with cruise!

Unlike last year, we won't be anchoring the day before transiting the Cape Cod Canal en route to Mass Maritime. New regulations prohibit discharging gray water in Cape Cod Bay, which we have to do prior to arriving at the pier. As a result, we'll have to stay outside Cape Cod Bay until we are preparing to transit the canal. I'm not sure how good the cell coverage will be for us that day, but I'm sure that we'll have it enough that the weather decks will be lined with people on the phone.

I'm going to sign off for now and head to bed, as I have laundry watch 0400-0800/1600-2000 tomorrow. I have a few guest posts lined up to post this week, now that things are dying down a little around here. Keep an eye out for those in the next few days.
