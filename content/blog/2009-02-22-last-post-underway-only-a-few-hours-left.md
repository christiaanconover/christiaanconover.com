---
title: 'Last Post Underway, Only a Few Hours Left!'
date: 2009-02-22T01:56:07.000Z
slug: last-post-underway-only-a-few-hours-left

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

I'm writing this with less than 12 hours until we arrive at the State Pier, and prepare to disembark the T.S. Kennedy for the last time on Sea Term 2009\. Captain's Inspection is finished, and all that remains is packing personal gear, doing a final cleaning of the holds, and manning the rails. A general feeling of excitement, relief, and accomplishment is very noticeable throughout the ship.

Our current plan keeps us on track for arrival at the pier at 0743\. We'll be transiting the canal twice on Sunday morning, once going Eastbound, and once going Westbound. We pick up the pilot on the Buzzards Bay end of the canal around 0400, head East through the canal to Cape Cod Bay, turn around in the Bay, and then head West to the State Pier. I'll be on watch 0000-0400 tonight, so I won't be on the bridge during either canal transit, but I may find a place to put the SPOT for the trip. Otherwise, you can track us on www.marinetraffic.com either by searching for the vessel name, or by monitoring the map in the canal area.

Based on our trackline on the ECDIS, we won't have cell service much tonight, so I wouldn't expect to be able to talk on the phone with your cadet until tomorrow morning. As a reminder: SeaWave accounts should be cancelled after cruise is over to avoid paying the $5 monthly minimum fee.. This can easily be done either onboard the ship while we're still underway, or at home by going to [my.seawave.net](http://my.seawave.net) and logging in with the username and password from the ship. If your cadet has been using SeaWave this trip, I'd recommend checking with them to see if they cancelled their account while on board.

On a personal, and somewhat bragging (at least I admit it) note, I am the recipient of the Master's Award for the second year in a row. My room mate, 3/C Joseph Connor, is also a second year recipient, which is pretty cool. Congratulations to Joe and all the other Master's Award cadets.

I'm going to sign off for now (for the last time on cruise), and go pack. I'll do a final "summary of cruise" post once I get home and can add some pictures and other extras. I've enjoyed maintaining the blog again this year (however less frequent than last year the posts were), and hope that you've found it informative and entertaining. See you all in a few short hours!
