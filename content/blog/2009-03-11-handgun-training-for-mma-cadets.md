---
title: Handgun Training for MMA Cadets
date: 2009-03-11T21:59:17.000Z
slug: handgun-training-for-mma-cadets

tags:
  - bourne
  - cape cod times
  - firearm
  - handgun
  - mass maritime
  - piracy
  - police
---

As most people are fully aware, piracy has become a major issue recently, particularly off the coast of Africa. Numerous merchant vessels have been attacked and hijacked, left vulnerable to these attacks by crews that are at a disadvantage from lack of effective defenses.

Mass Maritime has recognized this problem, and yesterday gave training to 6 cadets, with the help of the Bourne Police Department, in handgun firing. The [Cape Cod Times](http://www.capecodonline.com/) has the full story, which you can [find at their web site](http://www.capecodonline.com/apps/pbcs.dll/article?AID=/20090311/NEWS/903110317).

**Voice your opinion:** should merchant vessels carry firearms on board to defend against pirates? Should this become a required part of licensing? How does this fit into merchant mariner training? Leave your comments below.
