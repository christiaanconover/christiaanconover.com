---
title: TWIC Credentials Required For MMC
date: 2009-03-23T17:11:11.000Z
slug: twic-credentials-required-for-mmc

tags:
  - mass maritime
  - mmc
  - mmd
  - tsa
  - twic
  - us coast guard
---

The Coast Guard has put new regulations into effect regarding TWIC credentials and Merchant Mariner licensing. As previously stated by the Coast Guard, all mariners currently holding an MMC (Merchant Mariner Credential) MUST hold a valid TWIC by April 15 2009\. However, TWIC is now going to be required for any new MMC applicants in order to be issued an MMC.

**[Read more at MariTalk »](http://www.maritalk.com/2009/03/23/twic-credentials-required-for-mmc/)**
