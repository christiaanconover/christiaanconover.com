---
title: Life at Mass Maritime During the Maersk Alabama Hijacking
date: 2009-04-09T17:02:57.000Z
slug: life-at-mass-maritime-during-the-maersk-alabama-hijacking

tags:
  - capt joseph murphy
  - capt richard phillips
  - maersk alabama
  - mass maritime
  - piracy
  - shane murphy
  - somalia
---

The [recent turn of events](/blog/maersk-alabama-hijacked-with-american-crew-mass-maritime-graduates-on-board/) for the [Maersk Alabama](http://en.wikipedia.org/wiki/MV_Maersk_Alabama) has had an interesting and prominent connection to Mass Maritime. As I reported in my last post, both the captain and the Chief Mate on board that ship are Academy graduates, as well as the mate being the son of one of our professors, Capt. Joseph Murphy.

Since Capt. Murphy is both closely tied to the situation and an experienced and well-known mariner himself, the news media has gravitated to him for answers like flies on butter at a picnic. Since early yesterday afternoon, they have been interviewing him continuously as new information was received about the situation on board the Alabama. During my last class yesterday around 2 PM, there were about 6 or 7 TV trucks parked on campus broadcasting, as well as reporters from radio stations and newspapers around the area. When I came back to campus after being out last night, there were still a few news trucks at 11 PM. This morning, there were news crews here already setting up and filming during morning formation. As the day has gone on, crews have been filing back on to campus and setting up to resume their coverage with Capt. Murphy right here from MMA.

After lunch today I walked down towards Blinn Hall and the canal, where most of the trucks were parked, to take a look at the commotion. I managed to snap a few pictures of the scene.

![News Trucks at Mass Maritime](/files/blog/life-at-mass-maritime-during-the-maersk-alabama-hijacking/news-trucks-mma.jpg)<br>
These are a few of the news trucks on campus today. These particular ones arrived shortly before lunch, apparently to get footage in time for the noon news broadcasts.

![Capt. Murphy Doing Interviews](/files/blog/life-at-mass-maritime-during-the-maersk-alabama-hijacking/capt-murphy-interview.jpg) Shown here is Capt. Murphy speaking with news crews before doing an on-camera interview. Click on the picture to get more details.

Everyone here at MMA has been concerned for the crew of the Alabama & their families, and are hopeful that this will end peacefully. It was a relief to hear that the ship was underway again with a sizable security force on board, headed for Kenya. Still, seeing familiar names and images of places around campus on national news networks has been an odd & exciting sensation. There was a lot of pride and positive energy around campus when we heard that Mass Maritime graduates successfully retook their ship from pirates. This has certainly been an unusual couple of days.
