---
title: 'Maersk Alabama Hijacked with American Crew, Mass Maritime Graduates On Board'
date: 2009-04-09T03:56:43.000Z
slug: maersk-alabama-hijacked-with-american-crew-mass-maritime-graduates-on-board

tags:
  - maersk alabama
  - mass maritime
  - piracy
  - somalia
---

For those who may not have heard yet, or do not have full details, the American-flagged ship [Maersk Alabama](http://en.wikipedia.org/wiki/MV_Maersk_Alabama) was hijacked early this morning off the coast of Somalia by pirates. The ship carries a crew of approximately 20 American citizens, of which are graduates of Massachusetts Maritime Academy. The Chief Mate aboard the ship, Shane Murphy, is a 2001 graduate as well as the son of Capt. Joseph Murphy, a professor here at the academy and very prominent figure in the maritime industry for his contributions to education and safety at sea.

Here are the highlights of the situation, up to this point:

- The ship was attacked by pirates early this morning, which the ship's crew tried to fight off for about 5 hours
- Once unsuccessful at repelling the pirates, they locked themselves in the steering gear room of the vessel, effectively hiding from the pirates
- The crew managed to overpower one of the four pirates aboard, taking him as their own prisoner.
- The remaining pirates took the master of the vessel, Capt. Richard Phillips (also a graduate of MMA), as prisoner and retreated from the vessel using a lifeboat from the ship, and taking Capt. Phillips with them
- The crew of the Alabama negotiated a release agreement, to hand over the pirate they captured in exchange for their captain. The crew provided the captured pirate, but the other pirates reneged on the deal and kept Capt. Phillips hostage.
- The crew has been attempting to negotiate the release of the captain using any other means aboard the vessel, such as food, with no success.
- The U.S. Navy was alerted of the incident when it happened, and has dispatched a destroyer, the USS Bainbridge, to aid the Maersk Alabama. They were more than 200 miles from the Alabama when they received the distress call. They have since arrived at the Somalian coast, where the captain is being held.

There is no word yet on the status of the captain, but earlier today the Second Mate on board the Alabama said that the captain had a ship's radio with him and was maintaining contact with his crew. Shane Murphy is currently commanding the vessel.

Capt. John Konrad over at [gCaptain](http://gcaptain.com/) has been maintaining an excellent live blog post with updates to the situation. You can find that post here.

As a result of Capt. Murphy's son being on board the ship, Mass Maritime's campus has been flooded with media crews today, interviewing Capt. Murphy and doing live reports from campus. A number of cadets were either interviewed or shown in footage aired on TV, which is said to be airing on Good Morning America (and possibly other shows) tomorrow. Keep an eye out for that, as well as new developments to this story.

Please keep the crew and their families in your thoughts as they go through this ordeal.
