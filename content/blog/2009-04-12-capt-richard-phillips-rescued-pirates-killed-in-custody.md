---
title: 'Capt. Richard Phillips Rescued, Pirates Killed & In Custody'
date: 2009-04-12T19:18:58.000Z
slug: capt-richard-phillips-rescued-pirates-killed-in-custody

tags:
  - capt joseph murphy
  - capt richard phillips
  - maersk alabama
  - mass maritime
  - piracy
  - shane murphy
  - us navy
---

Capt. Richard Phillips, master of the Maersk Alabama who's been held hostage since Wednesday, was rescued late this morning Eastern time. It's been reported that when the pirates were sleeping or otherwise unaware, he made another attempt to escape and jumped overboard, at which point the Navy was able to neutralize the pirates. Of the four pirates holding him hostage, 3 were shot and killed by Navy snipers, and 1 is currently in custody on board a U.S. Navy vessel.

Everyone in the maritime community is thrilled with the positive outcome. The Academy will be having public celebrations of his release and of the safe return of the crew. I'll be posting about updates to this as it happens.
