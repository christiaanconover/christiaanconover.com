---
title: 'Interview: Adm. Allen, Commandant of the Coast Guard, on Social Media – Part 1'
date: 2009-05-11T04:00:53.000Z
slug: adm-allen-interview-01
image:
  src: /files/blog/adm-allen-interview-01/adm-allen.jpg
  alt: 'ADM Thad Allen, Commandant of the Coast Guard'

tags:
  - interview
  - us coast guard
---

I did an interview in April with Adm. Thad Allen, the 23rd Commandant of the U.S. Coast Guard, regarding social media & the Coast Guard. The Coast Guard has been spearheading a move into the world of Web 2.0 and interaction with the public through online outlets, and Adm. Allen has been a prominent part of that transition. Here is the text of that interview.

**Can you describe your personal social media journey?**

It would take more room than is available on a single blog. The work that really got me interested was a book. "In The Age of the Smart Machine" was written by Harvard Professor Shoshanna Zuboff. It was one of the pivotal points in my life. I was leaving my assignment as Budget Officer at Maintenance and Logistic Command, Atlantic and headed to the Sloan School at MIT. This book deals with the transition of the work environment and the nature of our work from a physical and material world to one where our work is virtual and invisible to the eye. A couple of other books that have influenced me have been Chaos, Linked, and Nexus. Social media is the merging of social networks with information technology. I have followed both for many years so this is pretty natural.

**We are focusing on leadership this month in the Coast Guard. Which USCG leadership competencies relate most directly to social media? In which leadership competencies do the internal and external social media tools hold the most promise?**

Depending on how you are using it, social media could relate to just about all of them, but to answer your question I'll choose one from each category:

-   Leading self: Self-awareness and learning – Social media is all about transparency and feedback and this makes us more aware as leaders, better able to understand complex issues and respect differing opinions, and more able to sense and adapt to changing conditions.
-   Leading others: Team building – Social media tools can empower individual team members to more actively provide input to and influence the outcome of a project or decision. This improves collaboration and information exchange among team members and ultimately results in a better final product.
-   Leading performance and change: Customer focus – The "social" aspect of this new information environment facilitates two-way communications. This allows Coast Guard leaders to better understand the needs, perspectives and opinions of our customers and to help them better understand the reasoning behind a certain decision or course of action we may take that effects them.
-   Leading the Coast Guard: Partnering – Social media facilitates greater collaboration and provides practical ways to engage the numerous internal and external stakeholders involved in or impacted by our broad world of work.

**Has thought been given to having a deployable team (perhaps as part of the DOG) of social media specialists to respond to major events and incidents? While general social media competency for all members and high level competency for PA should be a goal it seems it would be useful to have a deployable team in the interim and perhaps as an ongoing resource for high profile events.**

Keeping in mind that this question relates to the external aspects of social media, I think that our Public Affairs specialist are the right people to orchestrate our social media efforts during a critical incident. Much of what takes place in the social media realm already falls in their world of work and we have seen them interact in that environment with great effect, including during Hurricane Ike, the Miracle on the Hudson, and most recently with the floods in North Dakota. The external component of social media is an extension of our existing public affairs policies and practices and the public affairs program is taking a strategic look at the competencies and tools required for the future in terms of how it trains and equips its people.

**One of the great features of social media is accessibility from almost anywhere at any time. How are we addressing the tension between security and access? This seems to go two ways - access to the public social media tools from inside the CGDN and access to the internal tools for those temporarily, such as being off duty or not on reserve service, or permanently, like most Auxiliarists, outside the CGDN/Portal. Our members engaged in social media activities as part of their duties appear to utilize the public tools through their own resources, largely on their own time. At the same time, for the Portal/Quickr platform to be fully effective it seems problematic for access to be limited to the duty period for active, reserve and civilian members and inaccessible to most of the Auxiliary.**

All great points and we have addressed this on numerous occasions on my blog and during different interviews. There will always be a tension between security and access. Our ability to exist on both the .gov and .mil domains brings with it certain security responsibilities that we cannot overlook in order to maintain the integrity of those critical networks. That being said, we recognize the strategic and operational value of social media and have directed our IT staff to find ways for us to do both. The new portal, which we are gradually phasing in, is already enhancing our ability to use social media for internal purposes. We are also working on the off-duty access issues.

**For the Guardian new to social media where would you suggest they start? Internal tools or external? Building competence as privately as an individual or jump right in as part of their duties?**

The strength of social media is that it is flexible and adaptable to your specific needs. The first step is to develop awareness of what the different tools are and how they may be used and then consider the potential benefit they may bring to your job or unit. Ultimately, everything we do is assessed on its contribution to mission execution or mission support.

**Along the same lines, what sites/blogs/books would you suggest to build social media competence, the Commandant's Social Media Reading List?**

I just [published something like that as my 200th blog](http://www.uscg.mil/comdt/blog/2009/04/200th-blog.asp#links).

**Do you have examples of best practices use of social media within the Coast Guard?**

First we have to acknowledge that our formal foray into social media is still in its infancy. So far, the most visible activities have related to external communication. This was done deliberately, as this was the low-hanging fruit that we could use to gain some organizational inertia. These specific efforts have significantly enhanced our presence in the blogosphere, helping us to inform the Coast Guard narrative and we have seen very positive results in terms of our customer interaction, particularly with the maritime community through maritime focused blogs.

Some of this was already being done by Coast Guard employees on their own initiative, like [JD Cavo from the National Maritime Center](http://www.uscg.mil/comdt/blog/2009/01/coast-guards-james-cavo-gcaptains-top.asp#links) or this example by Jorge Arroyo, correcting some [critical misinformation on a sensitive rule-making issue](http://www.navagear.com/2008/12/new-ais-rules-navagear-gets-it-wrong/). Internally we are trying to increasingly use wikis to improve the efficiency and quality of the rule making and policy development processes. We expect these activities to accelerate as the new portal is brought on-line and more employees begin to champion these tools.

* * *

This interview is coming on the heels of a report published by the National Defense University titled "Social Software and National Security: An Intial Net Assessment" which discusses the use of social media in government agencies to share information both internally & with the public. I think this indicates an important shift in the institutional mindset of government in relation to the Internet & interaction with the American people, and I'm proud that the Coast Guard is at the forefront of this shift.

Thanks to Adm. Allen for taking the time to do this interview. I'll be publishing part 2 in a couple of days.

**Update**: [part 2 is available](/blog/adm-allen-interview-02/).

_Photograph by [Tidewater Muse](http://www.flickr.com/photos/tidewatermuse/169814954/in/photostream)_
