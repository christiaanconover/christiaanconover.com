---
title: 'Interview: Adm. Allen, Commandant of the Coast Guard, on Social Media – Part 2'
date: 2009-05-13T04:00:38.000Z
slug: adm-allen-interview-02
image:
  src: /files/blog/adm-allen-interview-01/adm-allen.jpg
  alt: 'ADM Thad Allen, Commandant of the Coast Guard'

tags:
  - interview
  - us coast guard
---

A couple of days ago, I posted [part 1 of my interview](/blog/adm-allen-interview-01/) with Adm. Allen, Commandant of the Coast Guard, regarding social media. Below is part 2.

**There are a number of units that have created Twitter accounts to release official news and connect with the public. Is this something we will be seeing from more units, possibly as an added aspect of the PA rates assigned to the various units around the country?**

We don't require field units to adopt or employ any particular tool. We have provided some overarching policy and guidance that allows them to make their own assessment, based on their mission requirements, of how social media tools might be leveraged to improve their performance.

**What is the best way for members of the Coast Guard interested in contributing to the social media outlets to get involved?**

As I stated in my "Way Ahead" message, "Unfortunately, it is impossible to ensure that information passed via the social media is complete and accurate, thus, the reader has to assume responsibility for judging the validity of the information." Simultaneously, the social media environment, and the information within it, is becoming increasingly influential. Thus, we need to be aware of what is being said on Coast Guard related topics and, when appropriate, contribute to the dialogue.

As part of our social media initiative we have released two interim policy ALCOASTS (458/08 & 548/08). These give clear guidance on how members of Team Coast Guard can influence the information environment in both official and unofficial capacities. Consistent with our long-standing public affairs philosophy, "If you do it or are responsible for it, you can talk about it."

One of the benefits and challenges to us with social media is the speed in which it moves. The Coast Guard cannot expect to continue operating strictly in the hierarchical, top-down fashion, but must also adapt to be more horizontal and collaborative or face organizational obsolescence. This is a significant cultural change for us, but I am confident that our outstanding people possess the knowledge and judgment to be able to more efficiently monitor and evaluate the information environment they operate in and effectively and deliberately engage in the dialogue to further Coast Guard strategic objectives and benefit mission execution and support. This has to be done with appropriate consideration of information release guidelines that are designed to protect the Coast Guard and its members from any harms associated with unauthorized release of protected or non-public information, but it has to be done.

**Are there plans to make use of the pervasiveness of social media for disseminating information during emergency situations?**

We have begun doing this in an ad hoc fashion. More formally, our Public Affairs program is looking both internally within the service and also working with DHS Office of Public Affairs and sister components to incorporate social media into the official Emergency Support Function (ESF) 15 practices.

**With the Coast Guard's transition to FORCECOM/OPCOM and with respect to these particular commands, is the Coast Guard looking to invoke a permanent presence on the Internet with regards to continual social media updating or monitoring? I know this is currently being done by District/Area External Affiars, however, there seems an advantage of having full time (24/7) monitoring which could be done by the new Command Center structure.**

We are looking at including a 24/7 social media/public affairs watch in the future.

**At the headquarters level, or even your direct staff, what kind of an element is monitoring the health of the Coast Guard on the internet?**

Just as we have always done press clippings to assess attitudes and opinions being communicated on the Coast Guard and its roles and responsibilities, we do the same assessment when it comes to social media. This is consistent with our goal of a more nimble and adaptable organization that actively senses the environment, recognizes changes and trends, and responds accordingly in the interest of mission execution, mission support and public stewardship.

**As Commandant have you felt push-back from the commands around the country to not be so involved on the Internet?**

There has not been push-back, but there has been thoughtful discussion about the risks/benefits of this new information environment.

The fact is, the environment has changed and we have no control over that, so the choice is: either ignore the change, which subjects our organization to all of the risks with none of the benefits; or adapt to the environment, where we can mitigate the risks and leverage the benefits.

We have chosen the latter and we have been pleased with the early results. That being said, adapting to this environment is a significant cultural change and some people are more comfortable with it than others, but the more successes we achieve and share, the more adopters we are going to have and eventually it will be a natural part of how we operate.

* * *

Great insight into the social media movement in the Coast Guard. As I said in part 1, I'm thrilled that the Coast Guard is heading in this direction. On a side note, Adm. Allen has [apparently created](http://ucgblog.blogspot.com/2009/05/follow-commandant-on-twitter-learn-of.html) an [official Twitter account for the Commandant](http://twitter.com/iCommandantUSCG). What convenient timing!

Once again, thank you to Adm. Allen for taking the time to provide us with this interview and inside perspective on the Coast Guard's social media initiative. Semper Paratus!

_Photograph by [Tidewater Muse](http://www.flickr.com/photos/tidewatermuse/169814954/in/photostream)_ ovide us with this interview and inside perspective on the Coast Guard's social media initiative. Semper Paratus!

_Photograph by [Tidewater Muse](http://www.flickr.com/photos/tidewatermuse/169814954/in/photostream)_
