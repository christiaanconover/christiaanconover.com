---
title: Hunt for a 1995 M3
date: 2009-10-16T03:20:54.000Z
slug: hunt-for-a-1995-m3

tags:
  - bmw
  - bmw m3
  - my cars
---

Since my accident and subsequent totaling of my car, I'm looking to make an upgrade to replace it. I'm in the market for a 1995 BMW M3 5spd. My ideal color would be either Brilliantrot or Hellrot, followed by Cosmos black. I've found a few potential vehicles, but I'll update here on how the search is going.
