---
title: The BMW of my Dreams
date: 2009-10-25T16:41:45.000Z
slug: the-bmw-of-my-dreams
image:
  src: /files/blog/the-bmw-of-my-dreams/bmw-concept.jpg
  alt: BMW concept car

tags:
  - bmw
  - concept car
---

BMW of my "dreams" is right...it's actually an artist's rendering of their idea of a BMW supercar. It takes cues from the M1 Hommage, and the Vision EfficientDynamics concept. When it all comes together, you get one completely badass car. If BMW ever makes this (or anything close to it) I would do crazy things to get my hands on one.

The source article for this can be [found here](http://jalopnik.com/5382557/why-cant-real-bmws-look-this-good).
