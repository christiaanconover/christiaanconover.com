---
title: New Twitter Account for BMW
date: 2010-02-01T23:25:25.000Z
slug: new-twitter-account-for-bmw

tags:
  - bmw
  - twitter
---

I've created a new Twitter account dedicated to all things BMW relating to me. You can find it at [@conoverbmw](https://twitter.com/conoverbmw). I'll be using this Twitter account in conjunction with this blog, and to post anything I find or do involving BMW, including finding and propagating other BMW-related Twitter accounts.

**Update:** My BMW Twitter account is no longer active. I decided that it was counter productive to compartmentalize my various interests on Twitter, so I've consolidated back to my original, primary Twitter account. Please follow me **[@cconover](http://twitter.com/cconover)**.
