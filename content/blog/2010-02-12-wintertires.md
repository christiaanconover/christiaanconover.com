---
title: 'Winter Tires, and Why You Need Them'
date: 2010-02-12T18:15:55.000Z
slug: wintertires

tags:
  - bmwblog
  - winter tires
---

The fine folks at [BMWBlog](http://bmwblog.com/) published an excellent article last week on winter tires, and why they're a near necessity for anybody who lives outside of the tropics. It dispells some common misconceptions about winter tires, like the idea that you only need them for snow. It presents the science behind winter tires in an understandable way, and makes me understand why I love my winter tires so much!

Take a look at the article over on their site. Also, just because it's on a BMW site doesn't mean it's less applicable to any other car. Winter doesn't care about the badge on your hood.

[To Buy or Not to Buy? Winter Tires are the Question »](http://www.bmwblog.com/2010/02/05/to-buy-or-not-to-buy-winter-tires-are-the-question/)
