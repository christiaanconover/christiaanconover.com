---
title: 'New BMW Podcast: RoundelTable'
date: 2010-03-18T06:08:34.000Z
slug: roundeltable

tags:
  - bmw
  - podcast
  - roundeltable
---

I'm excited to announce that I'm starting a BMW podcast, called [RoundelTable](http://roundeltable.com). RoundelTable will be a weekly show, which will cover a wide variety of topics related to BMW including news & events, tech talk, etc. We'll regularly have guests to provide in-depth insight into various aspects of the world of BMW. You can find the podcast at [roundeltable.com](http://roundeltable.com), or on Twitter [@RoundelTable](http://twitter.com/RoundelTable). Stay tuned!

**Update:** RoundelTable has moved to the [Wheelspin Network](http://wheelspin.tv). The show's new page is at [wheelspin.tv/rt](http://wheelspin.tv/rt) and is on Twitter [@WheelspinTV](http://twitter.com/WheelspinTV).
