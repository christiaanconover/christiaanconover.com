---
title: 'My New Podcast Venture, and Potential New Business'
date: 2010-03-25T17:45:24.000Z
slug: my-new-podcast-venture-and-potential-new-business

tags:
  - bmw
  - podcast
  - roundeltable
---

At this point I don't think it's any secret I'm a total gearhead, and an even bigger BMW nut. I read [blogs](http://bmwblog.com/), I spend an excessive amount of time on Twitter, and hang out on [forum](http://forums.bimmerforums.com/forum/index.php) after [forum](http://forum.e46fanatics.com/) all dedicated to cars & BMW. I'm also a tech nerd (I'll admit it) and love trying & talking about new technology. One thing I haven't really tried yet but want to is podcasting. My primary hurdle to doing it has been the lack of a topic to talk about. However, I've decided that my BMW fanaticism provides me with plenty of stuff to talk about with ease, so my first podcast is all about BMW. It's called [RoundelTable](http://roundeltable.com/), and will be dedicated to all things BMW, from company news & product announcements to racing to enthusiast community happenings. Basically, if it involves BMW we're gonna cover it. The podcast is still in the early stages of development, but it should be a good time and fill a niche.

This brings me to the second part of this post. Since deciding to start this podcast the wheels of creativity & inspiration have been turning. Anybody familiar with [Leo Laporte](http://leoville.com/) and his [TWiT network](http://twit.tv/) are aware that he has a large lineup of high-quality talk shows regarding various aspects of the tech world, with engaging guests who do weekly programs on their topics. Many of his programs are niche topics, such as "This Week in Google" or "Macbreak Weekly" and go in-depth on their respective subjects. I've been thinking that an automotive netcast network with a similar model would be really cool, so I'm very seriously considering getting this going. I don't have many details figured out yet, but it's got real potential and I'm excited about it so I'm going to pursue it.
