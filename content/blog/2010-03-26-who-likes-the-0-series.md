---
title: Who Likes the 0 Series?
date: 2010-03-26T13:25:57.000Z
slug: who-likes-the-0-series

tags:
  - 0 series
  - 318ti
  - bmw
  - front wheel drive
  - hatchback
  - hofmeister kink
  - kidney grills
  - rear wheel drive
---

Now that we know a [front-wheel drive BMW](http://www.bmwblog.com/2010/03/25/bmw-ceo-ready-for-front-drive/) is all but official and [we've seen a photo](http://www.autoexpress.co.uk/news/autoexpressnews/244420/bmws_new_baby_zeroes_in_on_fiesta.html) of what appears to be a concept, it's time to put the cards on the table. The glaring issue for every BMW enthusiast is the fact that BMW **doesn't build front-wheel drive cars**. They're known for building well-balanced, powerful, well-handling **rear-wheel drive** cars. This is tantamount to heresy for many enthusiasts, and likely will keep many who are BMW fans from considering buying one. I'll admit that it actually looks pretty good in the picture, at least for a compact hatchback, but **it just doesn't say "I'm a BMW"** when you look at it. The front end looks sloped and elevated more like a SAV than a compact car, and it just looks like it'd be better suited for your 16 year old needing to get to school or for the Honda tuner looking to make the jump to Europe than as a proper BMW driver's car.

Of course, according to [this article](http://www.autoexpress.co.uk/news/autoexpressnews/244420/bmws_new_baby_zeroes_in_on_fiesta.html) that's sort of what BMW has in mind. The author states that the car is taking aim at the new Ford Fiesta, with a pricetag "likely to start from around £16,000" (about $24,000 USD). I certainly can't fault BMW for wanting to expand into new markets (and really, this is a market they've been near in Europe for years with cars like the 120d as a recent example), but **I don't think this is the way to do it**.

![BMW 1 Series](/files/blog/who-likes-the-0-series/1-series.jpg)

Certainly the report that **80% of 1 series drivers either think their car is FWD** or couldn't tell you either way is disappointing, but when you look deeper into that statistic and realize that it's based on polls taken of drivers of 3 and 5 door 1 series hatchbacks, it **starts to make some more sense**. The people driving coupes and convertibles, for the most part, know what their car is about and how it's made. They're the ones buying cars for handling, power, balance, with some comfort & luxury added in. The hatchback drivers are likely buying the car for economy, comfort, perhaps styling, and the ability to still have the Roundel on the hood. Obviously speculation and generalization, but looking at the statistic cited above I can't help but think **that's partly what's at play here**.

So capitalizing on that, BMW wants to build a sporty little hatchback on the MINI platform for an entry-level price. However, they did that once before, back in the 90s during the days of the E36 - the 318ti. A 3 door hatchback with an economical yet still moderately powerful 4 cylinder engine with good handling & **rear wheel drive** like any other 3 series or **proper BMW**. Why not build off of that model with the new 0 series? Judging by the proportions in the photo, it's fairly similar in size to the old 318ti so it's not a stretch to suggest it. Sure, there are plenty in the BMW community who laugh off the 318ti as a "baby Bimmer" and make jokes about its trunk having been stolen, but when you put all the kidding aside it was (& many still are) a good little car that was true to the BMW heritage.

If you want a BMW-built front-wheel drive 2 door hatchback, buy a MINI. If a car is carrying the BMW emblem however, there are certain elements of the car that are non-negotiable: kidney grills, the Hofmeister kink, good handling & power, and **rear-wheel drive**. Without those things, **you might as well just drive a Civic or a Golf**.
