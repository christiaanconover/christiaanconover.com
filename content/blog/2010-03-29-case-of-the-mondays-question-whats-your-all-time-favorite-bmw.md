---
title: "Case-of-the-Mondays Question: What's Your All-Time Favorite BMW?"
date: 2010-03-29T13:00:22.000Z
slug: case-of-the-mondays-question-whats-your-all-time-favorite-bmw
image:
  src: /files/blog/case-of-the-mondays-question-whats-your-all-time-favorite-bmw/case-of-the-mondays.jpg
  alt: 'Case of the "Mondays"'

tags:
  - '#BMWBunch'
  - '#BMWMonday'
  - bmw
  - case of the mondays
  - hofmeister kink
---

In the spirit of [Monday-related BMW memes](http://bit.ly/aalGAB), I'm going to try out something new on my blog. Each Monday I'm going to post a question related to BMW, and everyone can leave their response in the comments. Questions will draw from literally any and every corner of the Bimmer map, so it should get interesting!

**This week's question: What's Your All-Time Favorite BMW?** I understand that this question can (and probably should) be all but impossible to narrow down to one answer, but give it a shot. If you absolutely can't get to just one, post a pair & we'll vote in the comments.

![BMW 3.0 CSL](/files/blog/case-of-the-mondays-question-whats-your-all-time-favorite-bmw/bmw-30-csl.jpg)

I'll start: I was torn between a few choices, but finally decided on one: the **BMW 3.0 CSL**. I've had a love affair with this car for a long time, for a whole host of reasons. First off, the obvious: it's absolutely gorgeous. This car embodies the best of BMW design & car design in general from the late 60s and early 70s, and has timeless beauty & appeal. It's a car you'd absolutely love to cruise around in or take on a road trip, but at the same time would be great to take roaring through the twisties. From the long sweeping hood to the C pillar roundels next to the Hofmeister kink, all the way back to the sculpted tail, it's the perfect example of a classic BMW coupe.

The engine in this car is an extension of this image of the perfect BMW. A 3.0 liter inline 6 making close to 200bhp, it's the grandfather to a long line of incredible, award-winning inline 6 engine in the 2.5-3.5 liter range. The heart of this car beats pure Bimmer in every respect.

Finally, the history & heritage this car represents. On the commercial production side, it marks the first project for the BMW M Division (OK, [I got that from Wikipedia](http://bit.ly/bx04JB) & couldn't find a source for it so it may or may not be 100% true, but still...you get my point) which has heritage I don't even have to explain. As the ancestor to the ///M cars it has a proud place in history. On the racing side of the house it was very competitive & successful in the [European Touring Car Championship](http://bit.ly/a5cxkU) and other circuits.

Despite all of these amazing qualities (many of which, to be fair, apply to any of the 3.0 coupes) I had an incredibly tough time coming to this decision given how many excellent contenders there are. Enough from me, though...

**Which one is your favorite?**
