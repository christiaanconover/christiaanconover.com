---
title: The 5 Series Touring is Still Better than the GT
date: 2010-03-29T23:04:12.000Z
slug: the-5-series-touring-is-still-better-than-the-gt
image:
  src: /files/blog/the-5-series-touring-is-still-better-than-the-gt/bmw-530d-touring.jpg
  alt: BMW 530d Touring
  caption: BMW 530d Touring

tags:
  - 5 series gt
  - bmw
  - sport activity vehicle
  - touring
---

From the first announcement and unveiling of the [5 Series Gran Turismo](http://bit.ly/aAFa9A), I haven't liked it at all. It looks like a cross between a Porsche Panamera and an X6 - in a bad way. It takes the worst elements of a bunch of different cars, and blends them into one big rolling box of parts that look like they were put together as an afterthought. The front end takes cues from the F10 5 series with the large grills, and the sharknose-type front with aggressive look that would be ok - if it didn't look pinched and strangely sloped in relation to the car as a whole. The rest of the car looks like a crushed and lowered X6 that resulted in some odd, stretched version that doesn't really understand what it is, and nobody seems to be able help it figure that out.

It isn't just the car itself that bothers me though, but what it represents. For the past 20 years BMW has been building the 5 series touring bodies, and they've been highly acclaimed by owners and reviewers alike. Cargo & passenger space to spare without sacrificing that quintessential BMW look & feel you find in the sedan version of the 5 series. Sure, they may not have stood out to the average person as a performance vehicle, but they fit right in with their fellow Roundel-bearing models. They were intended more for family use - carting kids & pets around, running errands, family road trips - than for use as an executive saloon, but they were unmistakably a car first, and a cargo hauler second. They were true to what BMW does best & is known for.

What BMW wasn't known for until recently was building SUVs (or SAVs as BMW likes to call them). With the advent of the X series they've been trying to change that, which is disappointing. At the time that the X series were introduced SUVs were all the rage, so it made good business sense to enter that market. However, SUV sales and popularity have been steadily declining over the past few years across the board, to the point that a number of SUV models have been discontinued. Yet what has BMW done? They've not only continued building theirs, but they've introduced new ones (even M-tuned ones, but that's a discussion for a whole other post) and now they've built a "car" that looks like, for all intents and purposes, an SAV.

Now, I'm sure there's data to explain this decision, but frankly I don't care. What I see in the 5 GT is an identity crisis in the brand. A car maker who has a nearly cult-like following for its amazing performance cars - both ///M and standard series vehicles - and has a long history of successful car racing is dedicating an increasing amount of money and resources to vehicles that don't jive with this heritage. The 5 GT looks like the kind of car you'd drive if you were really looking for a small SUV that didn't quite look like an SUV. It's not a 5 series, and shouldn't be considered a replacement for the Touring because it's a totally different car.

The 5 Series GT might be a fine vehicle in its own right, but it looks weird & unappealing, and doesn't make sense as an addition to the 5 series line. The classic styling of a Touring body make perfect sense, because they're just a long car. The 5 GT doesn't because it's a bizarre blend of SAV and sort-of car with none of the good parts of either. Basically, it doesn't deserve a 5 series badge.
