---
title: 'Today is "Celebrate My Car" Day!'
date: 2010-03-31T03:30:48.000Z
slug: today-is-celebrate-my-car-day

tags:
  - '#cardate'
  - 330ci
  - bmw
  - celebrate your car
---

So it took me until 11:30 this evening to notice that today's date is the same as the model number of my car - 3/30 ("330"ci). So I'm declaring today "Celebrate My Car" day, and anyone else who has a 330 of any style for that matter.

**For anybody whose car model has a date that matches** (for example, anyone with a 528i would be associated with May 28), **I'll make a celebratory post on that day**. Then, if you have a car for that date, post a link to a picture of it in the comments.

Let's all give our cars honorary birthdays, and celebrate them! I mean, why not, right?

**If you've got a 330 of any variation, post it up in the comments.**
