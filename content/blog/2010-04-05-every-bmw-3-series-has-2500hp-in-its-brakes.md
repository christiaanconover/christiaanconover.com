---
title: 'Every BMW 3 Series has 2,500HP…In Its Brakes'
date: 2010-04-05T17:05:56.000Z
slug: every-bmw-3-series-has-2500hp-in-its-brakes
image:
  src: /files/blog/every-bmw-3-series-has-2500hp-in-its-brakes/bmw-brakes.jpg
  alt: BMW brakes

tags:
  - 3 series
  - bmw
  - brakes
---

This was [published on Jalopnik](http://bit.ly/9xeUYG), and was such a cool statistic that I just had to reblog it. Apparently in response to the recent Toyota recall situation, BMW engineers decided to calculate the braking power of the 3 series. According to Jalopnik:

> The engineers arrived at the 2,500 HP number after measuring the car's 60-0 MPH deceleration time (2.5 seconds), then calculating how much horsepower would be required to achieved equivalent acceleration (0-60 MPH in 2.5 seconds). This is obviously a number that exists merely in the calculations on an engineer's note pad - the rear-wheel drive 3-series would need extensive modifications to transfer that decelerative power into accelerative power then send it to the ground - but it does demonstrate exactly how much more powerful the brakes are over the engine, which, in the case of this Bimmer, makes between 200 and 300 HP.

So obviously this information doesn't translate into any sort of usable data, but anyone who's gone hard in the brakes in a 3 series can understand this. I did the 60-0 brake test in my '01 330ci to my buddy who drives a RAM 1500, and afterward he told me the incredible rate of deceleration made him feel a little nauseous from the forces felt on his face.

But really, who cares about this theoretical number? Jalopnik goes on to explain why this is relevant.

> What's this mean for unintended acceleration? Even if you couldn't shift the car into neutral, even if the throttle was stuck wide open, even if you couldn't turn the engine off, you could still stop the car by using the brakes, which can easily overpower the engine.

This is good information to have, regardless of whether your car is apparently prone to throttle/acceleration problems. Of course, the best solution to not being able to "shift into neutral" is to drive a manual :-)

So besides this being valuable information and testament to awesome brakes, chalk up another awesome statistic for BMW fans to have in their arsenal.
