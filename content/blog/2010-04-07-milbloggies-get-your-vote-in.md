---
title: 'Milbloggies: Get Your Vote In!'
date: 2010-04-07T18:39:46.000Z
slug: milbloggies-get-your-vote-in

tags:
  - milblogging
  - military
  - us coast guard
  - us navy
---

![Milblogging](/files/blog/milbloggies-get-your-vote-in/milblog.gif)

The annual Milblogging conference is coming up, and as part of the event they have awards for military bloggers in various categories, one for each of the armed services and support categories as well. Here's the best part: they're reader's choice awards, so you get to vote for your favorite in each category!

I'm not nominated so this isn't me trying to source votes for myself. However, I have some good friends who are and keep great blogs that deserve to win, so I'm trying to help them gain some well-earned recognition.

U.S. Navy: [Naval Institute Blog](http://bit.ly/9xkpn7)<br>
U.S. Coast Guard: [An Unofficial Coast Guard Blog](http://bit.ly/bJmq04)<br>
U.S. Military Supporter: [BostonMaggie](http://bit.ly/98E1PZ)

These blogs do great work in connecting the military with civilians, as well as other military members. Please show them you support their efforts by voting for them. [**You can vote on the Milblogging web site**](http://bit.ly/9OXsSG).

*Image taken from Milblogging.com, a subsidiary of [Military.com](http://military.com/).*