---
title: The New 1M is BMW Doing it Right
date: 2010-04-13T13:50:24.000Z
slug: the-new-1m-is-bmw-doing-it-right
image:
  src: /files/blog/the-new-1m-is-bmw-doing-it-right/1m-prototype.jpg
  alt: BMW 1M prototype

tags:
  - bmw 1m
  - bmw
  - bmw m1
---

[BMWBlog](http://bmwblog.com/) posted today some [spy photos of the upcoming BMW 1M](http://bit.ly/avTuv7) in typical prototype camouflage. Even with the disguise the car looks good and evident of some ///M tuning. It's also evidence of BMW acknowledging the enthusiast community and the complaints about cars getting too big. It's an important piece in BMW moving into the future without neglecting the wants & needs of enthusiasts with a more nostalgic sense of the brand.

Now let me be clear: I don't buy into BMW's statement that the 1 series is a harkening back to the 2002\. The 2002 was revolutionary, and a classic. It was BMW's true entry into the automotive mainstream in the U.S. and there's something majestic and sporty about it that would be nearly impossible to match again. Perhaps 30 years from now I'll look at the 1 series differently, but for now I can't draw the parallel. I see the 1 series as a modern E36, with the smaller, more nimble platform and slightly boxier shape than the E92 3 series. To be fair I'm a little biased toward the E36 since my first BMW was a '92 325is, but the E36 is a fun and exciting driving platform. It's a good size for a modern sports coupe or sedan, and it handles well & provides good driver feedback. The 1 series is an updated extension of this concept, and I like it.

![BMW 1M Rendering](/files/blog/the-new-1m-is-bmw-doing-it-right/bmw135icoupe.png)

The upcoming M version of the 1 series is BMW staying true to itself. It's evidence of the company returning to what made it so popular in the first place: smaller, sporty cars with good looks, excellent handling, and some luxury comfort on the inside. Over the years the cars have gotten bigger, to the point that the E90 M3 has a wheelbase and length just a tick smaller than the E23 7 series, with around the same curb weight. There's been a lot of complaining over this continual growth, and BMW responded with 1 series. It's not remarkably smaller than the 3 series, but it's just small enough. It's gotten mixed reviews, but most of the negative comments have been directed at the cosmetics of the car while the mechanics & handling of it got a lot of positive feedback. The M version will have cosmetic improvements in keeping with typical styling from the ///M Division, and will be powered by an upgraded version of the already excellent N55 3.0 liter twin-turbo inline 6\. There's no doubt in my mind that these changes are going to make the car very appealing to enthusiasts, and may very well be a competitor to the M3.

I'm all for the return to smaller, more agile cars. It won't hurt that they should get better fuel mileage as well, but the main issue is a reduction in bloat. The 1 series strikes a good balance between size & comfort for the modern automotive market. I'm excited to see an M version of it come to market.

What are your thoughts?
