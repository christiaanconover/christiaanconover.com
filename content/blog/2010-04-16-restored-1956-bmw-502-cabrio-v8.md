---
title: Restored 1956 BMW 502 Cabrio V8
date: 2010-04-16T17:21:26.000Z
slug: restored-1956-bmw-502-cabrio-v8
image:
  src: /files/blog/restored-1956-bmw-502-cabrio-v8/cabrio01.jpg
  alt: 1956 BMW 502 Cabrio V8

tags:
  - bmw
  - bmw 502
  - ncc bmwcca
---

Last night's [BMWCCA National Capital Chapter](http://nccbmwcca.org) social turned out another beautiful restoration: a [1956 BMW 502 Cabrio](http://bit.ly/95NLvL) with a 2.6L V8\. The owner actually has his own BMW shop and is very experienced so the end result is pretty much perfect.

The 502 was first launched in 1954, and took the title of Germany's first post-war V8 car. It achieved a top speed of 160km/h (about 100 mph) and was reported to be Germany's fastest sedan in production.

![1956 BMW 502 Cabrio V8](/files/blog/restored-1956-bmw-502-cabrio-v8/cabrio02.jpg)

![1956 BMW 502 Cabrio V8](/files/blog/restored-1956-bmw-502-cabrio-v8/cabrio03.jpg)

![1956 BMW 502 Cabrio V8](/files/blog/restored-1956-bmw-502-cabrio-v8/cabrio04.jpg)

![1956 BMW 502 Cabrio V8](/files/blog/restored-1956-bmw-502-cabrio-v8/cabrio05.jpg)

![1956 BMW 502 Cabrio V8](/files/blog/restored-1956-bmw-502-cabrio-v8/cabrio06.jpg)
