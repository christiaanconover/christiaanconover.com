---
title: 'Case-of-the-Mondays Question: Feature Your Car'
date: 2010-04-19T14:29:14.000Z
slug: case-of-the-mondays-question-feature-your-car
image:
  src: /files/blog/case-of-the-mondays-question-whats-your-all-time-favorite-bmw/case-of-the-mondays.jpg
  alt: 'Case of the "Mondays"'
  link: '/tags/case-of-the-mondays/'

tags:
  - case of the mondays
---

I missed last week for a Case of the Mondays question, sorry about that. I'm back this week though, so let's get to it! This week I've decided to make the question more personal. **What's your favorite picture of your own BMW?**

Everyone loves to show off their cars, let's be honest. We put a lot of time, money & love into them and we want people to appreciate that. Well, what better group of people than fellow Bimmer enthusiasts? Post up a link to your favorite picture of your very own BMW in the comments. Let's see those beauties!

Here's mine:

![Natalia in the Field](/files/blog/case-of-the-mondays-question-feature-your-car/natalia.jpg)

Ok so it's not a great picture, but I've only had the car for a few months and haven't gotten a chance to get some good photos of it. However, of the ones I have it's my favorite.

**Let's see what you've got!**