---
title: 'Blue Angels: Live Streaming This Afternoon'
date: 2010-05-26T13:06:15.000Z
slug: blue-angels-live-streaming-this-afternoon
image:
  src: /files/blog/blue-angels-live-streaming-this-afternoon/blue-angels.jpg
  alt: Blue Angels in formation

tags:
  - annapolis
  - blue angels
  - livestream
  - military
  - us naval academy
  - us navy
---

This week is [Commissioning Week](http://www.usna.edu/SpecialEvents/CommWeek.htm) at the [Naval Academy](http://www.usna.edu/) here in [Annapolis](http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Annapolis,+MD&sll=37.09024,-96.503906&sspn=47.033113,77.431641&ie=UTF8&hq=&hnear=Annapolis,+Anne+Arundel,+Maryland&ll=38.978494,-76.524925&spn=0.090744,0.216637&t=h&z=13&iwloc=A). As a result, the town is flooded with friends & family of the graduates from out of town, and parts of the city are closed off to traffic at certain hours. One of the major reasons for this is a favorite annual tradition: the [Blue Angels](http://en.wikipedia.org/wiki/Blue_Angels) show on Wednesday afternoon.

Every year, the Blue Angels put on a demonstration during Commissioning Week, over the Severn River right next to the Academy grounds. Not only does it draw the guests of the graduates, but thousands of area locals descend on the banks of the Severn to watch the Blue Angels perform their incredible aerobatics. I've gone a few of the 9 years I've lived here, and it's always exciting.

This year I happen to have the ability to share it with others out of town. I can broadcast live using the [UStream](http://ustream.tv/) app on my [Motorola Droid](http://en.wikipedia.org/wiki/Motorola_Droid). **The Blue Angels demonstration starts at 2PM ET**, and I'll be broadcasting from the [U.S. Naval Institute](http://www.usni.org/) building on the Naval Academy grounds starting around 1:30PM, which you can watch right here.