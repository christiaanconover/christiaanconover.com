---
title: Google Voice Goes Public - Right Before the New iPhone
date: 2010-06-22T18:07:42.000Z
slug: google-voice-goes-public-right-before-the-new-iphone

tags:
  - android
  - apple
  - google
  - google voice
  - iphone
---

Google has just announced that starting today, [Google Voice](http://google.com/voice) will be open to the general public & will no longer require an invite. For those unfamiliar, Google Voice is a service from Google that gives you a phone number which you in turn can have forward to any combination of phones, use as a voicemail service that automatically transcribes them, and much more.

I've been using Google Voice since I got my invite in June of last year & absolutely love it. Everyone I talk to has that as my number, and now I can text from my phone & computer, and everything is always synced. It's totally integral to my daily life now. I'm very excited that other friends can now make use of this great service.

That is, except for my friends with iPhones. Sure, they can use the service, but Apple has denied them the option & choice to use the service to its full potential. In arguably the most notable rejection of an app from Apple's App Store, the company decided that the Google Voice app duplicated core features already available on the iPhone and wouldn't put the native Google Voice iPhone app on the store. As we all know this is the only way to get apps for iPhone, so those who really want this app are SOL.

As most people also know by now, Apple is coming out with the next generation of iPhone, which may start landing in people's hands as early as tomorrow. Pre-order sales have overwhelmed Apple & AT&T, and the device is obviously going to be a big hit.

Does the timing of Google Voice going public strike anyone else as a little more than coincidence? Google and Apple are directly (some would say aggressively) competing for market share in the mobile space with their respective mobile device operating systems/devices. With Apple about to roll out a major new product, it certainly seems like a great opportunity for Google to strike a blow to Apple by pointing out a few major complaints & missing features of iPhone by making a major product launch of their own. All those new iPhone users who unbox their shiny new device over the next few days & want to add support for Google Voice to complete the package will suddenly be met with frustration & disappointment when they discover that Apple has decided not to give them the freedom to get to use this service they've heard great things about from their Android-using friends. They may start to have a little resentment and feel belittled by Apple's decision. If I were in marketing at Google, this is how I'd be thinking & actively working to create. It certainly can only help Android's already rapid adoption & market penetration.

I may be reading way too far into this, but it's the first thought I had right after I heard the announcement (well OK, maybe the second thought - my first one was "Yippee!!!!!!!"). It just strikes me as a little too perfect to be an accident.

**What do you think?**
