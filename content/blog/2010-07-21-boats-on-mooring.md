---
title: Boats on Mooring
date: 2010-07-21T00:06:06.000Z
slug: boats-on-mooring
image:
  src: /files/blog/boats-on-mooring/01.jpg
  alt: Boats on mooring

tags:
  - boats
  - boothbay
  - boston island
  - maine
---

I took some photos of boats on moorings as I was heading out to the island this evening.

![Boats on mooring](/files/blog/boats-on-mooring/02.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/03.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/04.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/05.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/06.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/07.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/08.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/09.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/10.jpg)

![Boats on mooring](/files/blog/boats-on-mooring/11.jpg)
