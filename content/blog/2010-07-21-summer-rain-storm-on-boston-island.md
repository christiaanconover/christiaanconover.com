---
title: Summer Rain Storm on Boston Island
date: 2010-07-21T22:23:22.000Z
slug: summer-rain-storm-on-boston-island

tags:
  - boston island
  - maine
---

We're having the beginnings of a large rain storm that's on its way through tonight. Dinner is being made, the boat is tied up securely, everything is safe and the porch is cool & dry - the perfect way to enjoy summer rain. It's getting harder as I write this, but still very pleasant. Enjoy the video, complete with thunder.

<video preload="metadata" controls="">
  <source src="/files/blog/summer-rain-storm-on-boston-island/storm.mp4" type="video/mp4">
</video>