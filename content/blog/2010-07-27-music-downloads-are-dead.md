---
title: Music Downloads Are Dead
date: 2010-07-27T02:46:58.000Z
slug: music-downloads-are-dead
image:
  src: /files/blog/music-downloads-are-dead/itunes-trashcan.png
  alt: iTunes in the trash

tags:
  - grooveshark
  - itunes
  - music
  - streaming
---

...or at least they are for me.

For the past two weeks I've been using [Grooveshark](http://bit.ly/bXxoWo) exclusively for music. I initially learned of & heard good things about the service on [an episode of This Week in Google](http://bit.ly/c0lnim) and figured I'd give it a try. After all, it's only $3 a month for unlimited streaming so what did I have to lose? Well, after using it for 14 days not only did I not lose anything, but I'm totally sold & hooked on streaming vs. downloading. Why? Well, for a number of reasons.

First, convenience. I don't like spending time cultivating my music library, or copying songs to my phone, or fixing file tags. I love listening to music, but I still consider it to be a secondary activity that I don't want to dedicate a lot of time to. By extension, I don't like spending time downloading new music. So to have millions of songs available, already curated & managed and waiting for me to decide on a whim that I want to listen to them is ideal for me. Pretty much anything I can think of to listen to, I can find. I also don't enjoy using iTunes as it seems to be increasingly bloated & resource intensive. Plus, I don't have to store all those songs on my computer & phone, which brings me to my next point:

Peace of mind. Anyone with a sizable music collection on their computer is aware of two things: all those songs take up a lot of space, and it takes a lot of time to acquire & maintain. I have about 25GB worth of music on my laptop, which I've accumulated over the course of 6 or 7 years. It's nice to have all that music in that I have a lot of variety, but about 70% of that music I probably haven't listened to in at least a year. So why bother taking up that much space on my computer? Plus, with that much time & money invested I have to spend additional money & effort to ensure that it's all backed up, or risk losing all of it if my computer fails. None of this is very appealing. By using a streaming solution, none of this is an issue. I don't have to store anything - just a little bit of software to access the service, and there's nothing to back up because it's all kept on the streaming servers, which is much cheaper - which leads me to my final point: price.

The cost of a single song from iTunes in 99 cents. The cost of a VIP subscription for Grooveshark is $3 a month, and even cheaper if you opt to be billed annually (I didn't only because I wanted to have some time to decide whether it's really worth it before committing for a year). So for the cost of 3 individual songs, I can listen to unlimited songs. There are other services out there that do streaming such as Rhapsody or Napster, and they're a little more expensive but still much cheaper than buying all that music. Music is expensive & at more than a few songs starts to become a luxury expense that isn't important enough to me to pay. Streaming is very affordable, and fully satisfactory to me. Since I have a Droid & unlimited data over 3G (and I'm very rarely ever outside the coverage of either 3G or WiFi) it's very practical for me to rely on an Internet-based solution for my music. Between Pandora & Grooveshark, I have no need to store any music on my phone or computer at all.

Now admittedly, there are some disadvantages to streaming. You do need good bandwidth to enjoy smooth playback. You are kind of SOL if you're in a place with no connection available (though Grooveshark does allow you to store songs for offline use on your phone, as long as your subscription is active), such as a plane or the subway. Also, your music is at the mercy of the streaming provider. However, with the increasing proliferation of broadband & Internet connected mobile devices the drawbacks are becoming less & less noticeable. It also seems like a much more appealing distribution method for record labels, since it reduced the convenience & appeal of pirating, and still ultimately gives them some control over the content. It's pretty much a win-win.

So I'm totally done with downloaded music. I'm planning to copy my stored music onto my server at home in an archive, and then delete it all from my laptop. I've already removed it all from my phone (freeing up a sizable chunk of memory), and have replaced the native music player widget on the home screen with the Grooveshark widget. I've totally switched over to streaming - and couldn't be happier.

**Would you (or have you already) stopped downloading in favor of streaming, or are you unable to bear separating from your iTunes library?**

*The iTunes logo is a registered trademark of Apple, Inc.*