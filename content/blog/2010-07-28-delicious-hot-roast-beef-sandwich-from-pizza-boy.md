---
title: Delicious Hot Roast Beef Sandwich from Pizza Boy
date: 2010-07-28T18:55:54.000Z
slug: delicious-hot-roast-beef-sandwich-from-pizza-boy
image:
  src: /files/blog/delicious-hot-roast-beef-sandwich-from-pizza-boy/01.jpg
  alt: Roast beef sandwich

tags:
  - food
  - pizza boy
  - roast beef
---

My sister and I went up to Wareham, MA this afternoon to donate a carload of stuff from my grandfather's house to Salvation Army as we help him clean stuff out. Realizing that one of my favorite food joints was right up the street, I decided that lunch was at [Pizza Boy](http://maps.google.com/maps/place?um=1&ie=UTF-8&q=pizza+boy+wareham&fb=1&gl=us&hq=pizza+boy&hnear=Wareham,+MA&cid=8601032669886984800) today. Take a look at this amazing hot roast beef sandwich.

![Roast beef sandwich](/files/blog/delicious-hot-roast-beef-sandwich-from-pizza-boy/02.jpg)
