---
title: 'Boat Ride to Boston Island [Video]'
date: 2010-07-29T05:05:56.000Z
slug: bi-boat-video

tags:
  - boats
  - boston island
  - maine
---

<video preload="metadata" controls="">
  <source src="/files/blog/bi-boat-video/boat-ride-to-boston-island.mp4" type="video/mp4">
</video>

I took this video right after I took [these pictures](/blog/boats-on-mooring/), but haven't been able to post it until now on account of a 10MB video limit on my phone when uploading over 3G instead of WiFi.

Anyway, the video begins at the end of the mooring row at [Boothbay Region Boatbard](http://www.brby.com/) and ends at our dock on [Boston Island](http://boston-island.com/). Pretty much perfect weather.
