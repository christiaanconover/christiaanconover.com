---
title: 'OhLife: The Cool & Convenient Way to Keep a Journal (Really)'
date: 2010-07-29T13:23:11.000Z
slug: ohlife

tags:
  - ohlife
---

Let's face it: journals aren't really considered "cool" or are difficult to maintain. You have to make an effort to remember to update it every day, and if you forget then it can be overwhelming to catch up. However, it is nice to have a record of what you did written in your own words. Enter a new service that aims to make keeping a journal easier & more enjoyable.

It's called [OhLife](http://ohlife.com/), and it's incredibly simple. Its primary component is email, so there's pretty much nothing new to learn. Every night at 8PM it sends you an email asking you how your day was, and includes a random entry you've made previously. All you have to do is reply to the email with your entry for the day, and OhLife takes care of filing & formatting it.

The web site is equally simple. Once you log in, there are only a few options: view the most recent entry, view a page of past entries, change settings (of which there are very few), and view individual entries or a random entry. The interface is designed to look like you're reading from a page in a real, physical journal. It's enjoyable to read entries, even if you've just written them & know what they say.

They make a point of telling you that the service is built around privacy. They don't have any way to connect to social networks, or publicly share your entries. The only way to see what you've written is to log in to your account, and the only way to contribute or communicate with OhLife is through the email address you register with them.

So there's not much to the service, which is actually appealing. There are a few flaws to it though. The most noticeable problem is that there's no way to export your entries if you decide you no longer want to use OhLife. Hopefully that feature is in development, but for a service that's intended entirely for personal information it seems like something that should have been available at launch. Also, it would be nice to have the ability to share data as an opt-in feature. It's not likely that I'd want to share my entries, but having the choice would be nice.

I've only made 3 entries so far, but I'm enjoying using OhLife. I also discovered that it's designed to properly file your entries by date regardless of when you send them. Each daily reminder email that OhLife sends you comes from a unique, identifiable email address so that when you reply, it knows what date the entry is for. I didn't submit my second entry until the next day, so I was happy to learn this.

It's a pretty neat idea, and is the first solution I've found to keep me diligent about documenting my activities. It's easy to use, and enjoyable. A few missing features, but by no means a deal breaker. Definitely worth giving a try!

**Update (7/29/2010)**: OhLife added the export feature the day after this post was originally published. You can find it on the "Past" page, and it creates a simple text file that you can download with all of your entries.
