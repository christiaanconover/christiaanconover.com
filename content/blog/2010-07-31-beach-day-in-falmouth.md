---
title: Beach Day in Falmouth
date: 2010-07-31T19:11:20.000Z
slug: beach-day-in-falmouth
image:
  src: /files/blog/beach-day-in-falmouth/falmouth-beach.jpg
  alt: Falmouth beach

tags:
  - beach
  - cape cod
  - "martha's vineyard"
---

My sister & I took some time to go to the beach this morning/afternoon. This was my view while laying on a towel overlooking the water toward Martha's Vineyard, watching boats go by. Very pleasant.
