---
title: 'Happy 220th Birthday to the Coast Guard!'
date: 2010-08-04T17:48:31.000Z
slug: uscg-220-birthday
image:
  src: /files/blog/uscg-220-birthday/cgbirthday.png
  alt: Coast Guard birthday

tags:
  - alexander hamilton
  - coast guard day
  - revenue cutter service
  - us coast guard
---

Today marks the 220th anniversary of the [United States Coast Guard](http://www.uscg.mil/). It was on this day in 1790 that [Secretary of the Treasury](http://en.wikipedia.org/wiki/United_States_Secretary_of_the_Treasury) [Alexander Hamilton](http://en.wikipedia.org/wiki/Alexander_Hamilton) established the [Revenue Cutter Service](http://en.wikipedia.org/wiki/United_States_Revenue_Cutter_Service) to intercept smugglers. Thus began the [origins of the Coast Guard we know today](http://en.wikipedia.org/wiki/United_States_Coast_Guard#History), and the law enforcement mission of the U.S. Coast Guard.

So to all members of the Coast Guard past & present, thank you for your service & continued dedication to the mission and values of the United States Coast Guard. Semper Paratus!

As a side note, [Ryan Erickson](http://ryanerickson.com/) has a cool Google Logo for Coast Guard day on his site, though unfortunately Google didn't use it.
