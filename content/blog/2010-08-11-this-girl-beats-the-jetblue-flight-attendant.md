---
title: This Girl Beats the JetBlue Flight Attendant
date: 2010-08-11T16:43:03.000Z
slug: this-girl-beats-the-jetblue-flight-attendant
image:
  src: /files/blog/this-girl-beats-the-jetblue-flight-attendant/jenny.jpg
  alt: Jenny
  link: 'http://thechive.com/2010/08/10/girl-quits-her-job-on-dry-erase-board-emails-entire-office-33-photos/'

tags:
  - elyse porterfield
  - the chive
---

At this point, pretty much everyone's heard about [Steven Slater's dramatic exit from his job](http://www.nytimes.com/2010/08/10/nyregion/10attendant.html?_r=1&ref=steven_slater) this week. He's quickly become something of a cultural icon, and his story is pretty hilarious & creative.

[This one is equally (perhaps even more) funny & creative](http://thechive.com/2010/08/10/girl-quits-her-job-on-dry-erase-board-emails-entire-office-33-photos/).

Her name was initially thought to be Jenny, but she was in fact [revealed as an actress pulling a prank](http://thechive.com/2010/08/11/a-word-from-jenny-16-photos/). The story was that she quit her job using a series of photos of herself holding up a whiteboard with an amusing string of statements & awesome facial expressions. She was the assistant to some broker who apparently was a total douche, and she did a little digging to find out more dirt about him to use to embarrass him.
