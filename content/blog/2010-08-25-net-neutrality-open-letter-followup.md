---
title: 'Net Neutrality Open Letter: A Follow-Up'
date: 2010-08-25T01:38:58.000Z
slug: net-neutrality-open-letter-followup

tags:
  - ben cardin
  - fcc
  - internet freedom preservation act
  - net neutrality
---

Yesterday I received a reply to an email I'd sent to [Sen. Ben Cardin](http://cardin.senate.gov/) that contained a link to [my open letter on Net Neutrality](/blog/net-neutrality-open-letter/). Here is what he said:

> Dear Christiaan:
>
> Thank you for contacting me in opposition to network neutrality.
>
> Due to the widespread growth of Internet broadband use, the issue that Congress needs to consider is whether legislation is needed to regulate access to broadband networks and services. The main concern is that further regulation would hurt deployment and progression of broadband facilities. Many Internet access providers previously stated that they will abide by the rules that the Federal Communications Commission (FCC) established in 2005.
>
> On January 9, 2007, Senator Byron Dorgan (D-ND) introduced S. 215, the Internet Freedom Preservation Act. The bill would establish Internet neutrality duties for service providers, prohibit user discrimination, and allow providers to expand their business practices. S. 215 was referred to the Senate Committee on Commerce, Science, and Transportation, and awaits further action there. Please be assured that I will follow the progress of this bill closely.
>
> I appreciate hearing from you about this issue, and hope you will not hesitate to contact again about this or any other matter of importance to you.

I appreciated the Senator replying to my email (to date, he is the only elected official of the 5 or 6 I contacted that has given any response). However, there were a few things in his message that concerned me.

First off, **I am NOT in opposition to Net Neutrality**. In fact, I couldn't be more in favor of it. Any limits placed on the Internet & the concept of Net Neutrality will serve no benefit to the consumer, or anyone for that matter (except the greedy financial interests of telcos). Net Neutrality is crucial to the continued success of the Internet as a platform for education, communication, commerce, and countless other things that are dependent upon its continued open access system.

Second, the issue should not be whether regulating the Internet is necessary. The issue should be how to put regulations in place to protect it as it is. Further regulation that would put restrictions on its use or allow service providers to filter access & content would not only hurt deployment, but it would take away some of the value of the Internet to consumers - including those yet to receive a broadband connection. Rather than spending time, money and effort determining whether we should regulate the Internet, let's spend that time & money on ensuring that all Americans get the Internet as it's supposed to be provided - open, unfettered & free from censorship. If telcos are balking at the cost of running lines, they'll still be balking even if they get their way to regulate the networks however they choose. Of course the service providers will abide by the FCC's rules - until they see that [the FCC doesn't exactly have the authority to tell them how to run their networks](http://www.nytimes.com/2010/04/07/technology/07net.html). They're not going to violate the government's requirements, but if those "rules" suddenly don't carry as much weight as everyone thought, they're not going to pay attention to them. We are talking about businesses after all, and ones that have already expressed interest in locking down their networks & have shown that they don't have the consumers' interests in mind.

Now, I did read over the Internet Freedom Preservation Act that Senator Cardin talked about. For the most part, I think it's a good solution. It covers most of my grievances about regulation of the Internet, and I urge Sen. Cardin and all other elected officials to push for this bill to be passed. It's certainly much, much better than the proposal that Google & Verizon have offered up.
