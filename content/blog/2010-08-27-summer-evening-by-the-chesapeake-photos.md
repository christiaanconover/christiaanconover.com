---
title: Summer Evening by the Chesapeake
date: 2010-08-27T14:31:22.000Z
slug: summer-evening-by-the-chesapeake-photos
image:
  src: /files/blog/summer-evening-by-the-chesapeake-photos/01.jpg
  alt: Chesapeake Bay

tags:
  - chesapeake bay
  - sunset
---

Happy Friday everyone! I took these photos earlier in the week while I was out walking the dog, and I figured I'd post them up as a pleasant start to the weekend. Enjoy!

![Sunset on the Chesapeake](/files/blog/summer-evening-by-the-chesapeake-photos/02.jpg)

![Sunset on the Chesapeake](/files/blog/summer-evening-by-the-chesapeake-photos/03.jpg)

![Sunset on the Chesapeake](/files/blog/summer-evening-by-the-chesapeake-photos/04.jpg)

![Sunset on the Chesapeake](/files/blog/summer-evening-by-the-chesapeake-photos/05.jpg)
