---
title: 'Formula 1 Austin Track Design Unveiled, Looks Pretty Good'
date: 2010-09-01T05:12:55.000Z
slug: formula-1-austin-track-design-unveiled-looks-pretty-good
image:
  src: /files/blog/formula-1-austin-track-design-unveiled-looks-pretty-good/f1-austin.jpg
  alt: F1 Austin track

tags:
  - austin grand prix
  - formula 1
---

The track design for the Formula 1 Austin Grand Prix has been unveiled, and it looks very promising. According to the [American Statesman](http://www.statesman.com/sports/formula1/promoter-unveils-f1-track-layout-890582.html), the designer says it takes inspiration from some of the world's best Grand Prix circuits and will offer good visibility for fans throughout the track, due to elevation changes & turn arrangements.

The track will be 3.4 miles long, consisting of 20 turns and a total elevation change of 133 feet. It features a back straight that is 3/4 of a mile long, where the designer expects the cars will be able to achieve a 200MPH top speed. The track is designed to create more exciting races by enabling more overtaking than usually takes place at other tracks in the Formula 1 schedule. He also predicts that cars will be able to achieve 180MPH as they pass the grandstand as they head for Turn 1, which due to its tight uphill direction & potential for overtaking could make it one of the track's signature turns.

2012 is when the first U.S. Grand Prix is scheduled to occur. The track will be built on a [900 acre area south of Austin](http://www.autoblog.com/2010/07/27/austin-f1-track-location-revealed/), and is expected to hold between 130,000 and 140,000 spectators.

**So, who's down for a meetup at the U.S. Grand Prix 2012?**
