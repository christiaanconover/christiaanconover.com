---
title: Who Else is Bored by the Apple TV?
date: 2010-09-03T13:32:16.000Z
slug: who-else-is-bored-by-the-apple-tv
image:
  src: /files/blog/who-else-is-bored-by-the-apple-tv/apple-tv.jpg
  alt: Apple TV

link: /blog/who-else-is-bored-by-the-apple-tv
tags:
  - amazon
  - amazon instant video
  - apple
  - apple tv
  - boxee
  - flickr
  - gadgets
  - netflix
  - pandora
  - roku
  - twit
  - vimeo
---

So, Apple had [one of their now-famous events today](http://www.engadget.com/2010/09/01/live-from-apples-fall-2010-event/). They unveiled a bunch of things, including a touch screen [iPod Nano](http://www.engadget.com/2010/09/01/ipod-nano-first-hands-on/), a [new iPod Touch](http://www.engadget.com/2010/09/01/ipod-touch-2010-first-hands-on/) with front-facing camera & FaceTime, and a [new iPod Shuffle](http://www.engadget.com/2010/09/01/ipod-shuffle-first-hands-on/) (which doesn't really look any different from the old one). The thing that everyone was waiting for leading up to the event though, and the thing that's gotten the most attention, is the [new Apple TV](http://www.engadget.com/2010/09/01/apple-tv-2010-hands-on/).

Let's lather then shave here, and give it a fair shake on its strong points. First, form factor. There's no denying that the device itself it beautifully crafted and appeals from a purely aesthetic standpoint. It's small, polished & has that funny little piece of fruit on the top that woos so many people.

Second, interface. The [screenshots I've seen](http://www.engadget.com/photos/apple-announces-new-apple-tv-interface/) look like they've stayed true to Apple's reputation of clean, simple UIs. Straightforward yet elegant. Predictable though.

Third, simplicity. Beyond the interface, the hardware itself is simple. Plug in a power cord (not even an external power adapter, just a cord), run the cable from the Apple TV to your television, an Ethernet cable if you're not using WiFi, and you're good to go. The remote is only a couple of buttons. There's nothing here to confuse even the most technologically challenged folks.

It has a decent content offering as well. Apple is going to be offering content directly from their own platform, featuring $0.99 TV show rentals from ABC & Fox, HD movie rentals for $3.99 to $4.99, Netflix, YouTube, and of course iTunes. They'll also enable you to stream content from your computer, or to play content from your iOS-powered device on your TV wirelessly. All cool features, and the content offering is decent.

That's actually a pretty good offering for a $99 device, so why am I bored? Because when it's [compared to what else is available in this segment](http://www.engadget.com/2010/09/01/appletv-vs-the-competition-how-does-it-stack-up/), it's underwhelming.

Now before I go any further, I'll fully disclose that I haven't used any of the products that are mentioned in this post [except for one](http://www.boxee.tv/) which I'll get to later. Based on published specs & outside reviews though, there's enough information available to glean some useful data about Apple TV's competition. Its features & content offerings can't compete with what's available on [Roku](http://www.roku.com/), for the same price or cheaper for both [the content](http://www.roku.com/roku-channel-store) & the [device itself](http://www.roku.com/roku-products#3), and by all accounts is as simple to set up & use as the Apple TV. Roku has over 50 "channels" of content, including one for news & other information programming from the major networks like NBC & CBS - for free. It of course offers a Netflix interface [which looks excellent](http://www.crunchgear.com/2010/06/03/new-netflix-interface-for-roku-is-now-live/), along with content from some of my favorites like [TWiT](http://twit.tv/), [Pandora](http://www.pandora.com/), [Vimeo](http://vimeo.com/) & [Flickr](http://www.flickr.com/) - services for which the Apple TV has no offering. Fun fact: [Amazon Video on Demand](http://www.amazon.com/gp/video/ontv/start), one of the "channels" on Roku, just announced on Wednesday that [they're now offering $0.99 TV show](http://www.crunchgear.com/2010/09/01/amazon-unveils-99-fox-and-abc-tv-show-rentals-apple-fanboys-say-wha/) **[purchases](http://www.crunchgear.com/2010/09/01/amazon-unveils-99-fox-and-abc-tv-show-rentals-apple-fanboys-say-wha/)** from Fox & ABC - the same price Apple TV will be charging for 24 hour rentals. Oh, and by the way: the Roku box that's priced the same as the Apple TV ($99) is capable of playing 1080p video, while the Apple TV tops out at 720p. For most people that's plenty, but if you're watching sports or streaming a high definition video over your network where bandwidth is plentiful, why not go for the best picture possible?

Roku is an example of a device that's already available, and has been for years. Let's talk about one whose device isn't available yet, but whose software has been available for a few years now & is excellent: [Boxee](http://www.boxee.tv/). I mentioned having used one of the services in this segment, and this is the one. Boxee is an excellent piece of software, and offers more content than you can hope to consume - only it's a la carte, so you take only what you want & need. Oh & by the way, the software & all the apps inside are free (thought some do require a subscription to the service they interface, like Netflix). It's also an open platform, which means you can modify it if you have the ability, but moreover it means you can develop an app for it if you'd like. There's great content available through Boxee. Plus, they already have a hardware device on the way, which is shaping up to look like an awesome product. Unfortunately it's [a little delayed](http://www.engadget.com/2010/06/11/boxee-box-delayed-until-november/) coming to market, but nonetheless it's gonna be awesome when it does show up. It has the same kinds of features the others do, with one significant exception: the remote control has a QWERTY keyboard on the back so you can easily search for content. It's expected to be twice as expensive as the Apple TV, but to me it looks twice as good.

I'm not even going to get into all the [Blu-Ray players](http://www.lg.com/us/tv-audio-video/video/LG-blu-ray-dvd-player-BD550.jsp) & [other devices](http://www.wdc.com/en/products/products.asp?driveid=735) that are on the market. Let's just say that this market has been established, and has had some fantastic offerings already available before the new Apple TV even showed up. Ones that don't lock you in to only a few services & content platforms, only work to their full potential if combined with a bunch of other products from the same manufacturer, and have capabilities below what the competition offers for the same cost.

To me it seems that Apple TV seriously missed the boat on taking over this market: allow apps. Apple is arguably the leader in device-specific app marketing & distribution, so why not start allowing developers to build apps for a whole new interface & experience? This is what Apple has become known for with their small devices. It just doesn't make any sense why they'd pass up this opportunity. If they'd done that, I wouldn't even have written this post. I hate to admit it because I don't like the app model, but it's that significant.

There is one thing that the Apple TV has going for it, despite all its shortcomings: Apple. Let's be honest, Apple has built up their standing & reputation in our culture as the go-to company for simple, chic & fun tech products. It seems that anything they put their fruit silhouette on turns to gold. So why should the Apple TV be any different? It's gotten a lot of hype, it'll be on display in every Apple Store in every major mall across the country, and people will flock to it because it's cheap, exciting & new. That's the sad truth. It makes me upset, because it's going to reinforce that Apple doesn't have to make a revolutionary product at this point for people to treat it like one. They just have to make it good enough & put it on sale.

So, I'm bored by it. I'm annoyed by it, but mostly I'm bored. It's nothing new. It had all the potential in the world, and it blew it. Meanwhile, [competitors are working hard](http://www.google.com/tv/) getting their products out, or have sweetened the deal on already available good products, which means the window of time that Apple has to make this thing really revolutionary & stick out is closing fast. Consumers are somewhat going to win from this competition - but not with the Apple TV, or at least not right now. I'm excited about what else is available & what they're doing to get known, but I'm bored & disappointed by a product I wanted to like.
