---
title: A Real Mini Cooper Diesel in the U.S.?
date: 2010-09-10T15:41:22.000Z
slug: a-real-mini-cooper-diesel-in-the-u-s
image:
  src: /files/blog/a-real-mini-cooper-diesel-in-the-u-s/mini-diesel.jpg
  alt: M/T DIESEL

tags:
  - mini cooper s
  - diesel
---

This was in the parking lot this morning as I was walking into work, and I couldn't help but notice that one special word on the trunk lid: "DIESEL". Is this really a MINI Cooper S convertible with a Diesel engine, here in the U.S.? The only Diesel MINI I know of that's slated to come to the U.S. is the Countryman Diesel, which isn't supposed to show up until early next year.

Does anyone know anything about this? Is this a legitimate Diesel MINI, or just somebody's personalized paint job that coincidentally rings some bells?

**Update:** turns out it's just a reference to car being a "tender" for the owner's boat and is NOT a diesel, as per [the comment he left](http://disq.us/mbl6o). Case solved.
