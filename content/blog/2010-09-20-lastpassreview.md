---
title: 'Review: LastPass Password Manager'
date: 2010-09-20T21:54:20.000Z
slug: lastpassreview
image:
  src: /files/blog/lastpassreview/lastpass-homepage.png
  alt: LastPass

tags:
  - cyber security
  - lastpass
  - password manager
  - yubikey
---

About two months ago I started using a service called [LastPass](http://lastpass.com/), based in large part on the [review and recommendation](http://twit.tv/sn256) of [Steve Gibson](http://www.grc.com/) on [Security Now](http://twit.tv/sn). He explained in-depth why LastPass is safe, effective and a much better solution than maintaining passwords yourself. Intrigued by this product that Steve seemed so enthusiastic about (and given that I trust Mr. Gibson's opinions when it comes to computer security) I created an account & tried it out. In short, LastPass lived up to all the expectations I had for it, and works like a charm for all of my needs. Let me explain how & why.

**Accessible Anywhere** For me, the most important aspect of LastPass is that it's based on the cloud. All the other features of Lastpass are excellent & very important to any good password manager, but what good are they if I can't access them wherever I am? I'm frequently away from my computer, instead using my phone or other computers where my computer can't come. Being able to always have access to the most up-to-date login data for the sites I use at any Internet-connected device I might encounter ensures that I'll be willing to take full advantage of the rest of LastPass' capabilities. Plus, I know that my data is always backed up & secure, no matter what happens to my own computers.

**Secure** Security is an integral part of how LastPass works. When you create a new entry, the data is encrypted on your local client. This encrypted data is then sent over a [SSL](http://en.wikipedia.org/wiki/Transport_Layer_Security) connection to the LastPass servers, where it's securely stored. The only way to decrypt the data is using your own password; there's no backend or method for LastPass or anyone else to view your data without your credentials. Plus, they support additional methods of authentication, such as [Yubikey](http://www.yubico.com/products/yubikey/), to make your account even more secure.

**It Just Works** These are great features, but they mean nothing if the service doesn't make it easy to use. The LastPass plugin is really good at detecting new sites & prompting you to save them, recognizing existing sites even if the URL doesn't exactly match what you have saved, and detecting when you've changed the password in an existing site. It also includes a feature to generate a secure password, and saves it in the database until you've added it to a site entry.

**Ok, this stuff sounds great, but how is it to live with?** For years I've been skeptical of password managers because I thought they'd be annoying & a hassle. I was also worried about losing the data. I figured there would be cumbersome steps to go through, I'd spend twice as much time logging in as if I'd just done it myself, and I'd end up being frustrated. It also concerned me to put all my passwords in a list. To be honest I never would have tried LastPass in the first place unless I'd heard Steve Gibson explain the benefits of it & give it his seal of approval. Since setting it up & building out my list of sites, not only has it cut down significantly on the time it takes me to log in to all my various services, but it's made my own habits (and passwords) much more secure.

For a long time I'd been using a few different passwords across all the different sites I visited. They were all randomly generated "strong" passwords, but they were used in a lot of places so it left me vulnerable. If somebody had compromised one of those passwords they would have had access to a large percentage of the sites I use. There's a good chance that this is resonating with you, as many people have this same practice, which makes them equally vulnerable. Now, every single site I use has a different strong password that's been generated and is stored by LastPass. My standard password scheme is: 32 characters long and consisting of upper and lower case letters, numbers, and special characters - and every password is completely different. Now, all I have to remember is one really strong password for my LastPass account, and all these other randomly generated passwords are taken care of for me.

Now, there is one major inconvenience to this system, which is having to log in to LastPass each time I open my browser. If I set it up to keep me logged in between sessions, it would leave a hole in the security that LastPass offers, so I have it set to require me to enter my password each time I log in. It can get annoying, but it only takes a moment. However, I've found a solution for that too: [Yubikey](/blog/yubikeyreview/). I mentioned this a few paragraphs up, and I'll explain in a later post fully how this works, but I'll give a brief synopsis now. Basically, it adds another layer of authentication using a little USB key. When I open my browser, LastPass prompts me for my Yubikey instead of my password. Rather than typing in a long string, I simply insert the Yubikey & press the button, and LastPass authenticates me & logs me in. It also makes the authentication process more secure than just a username & password, so my web experience is both simpler and safer.

Since using LastPass my impression of password managers has changed completely. I now rely completely on LastPass & am very happy with its performance. I'd highly recommend using it to anybody, and have even set up my grandfather to use it for his sites, complete with a Yubikey. No more need for little notebooks of passwords, or only having 2 or 3 passwords for everything. Set it up & let it do the rest.

_Take a look at [my review of the Yubikey](/blog/yubikeyreview/), and how it integrates with Lastpass._
