---
title: 'BUMP: The Social Network for License Plates'
date: 2010-09-23T06:39:26.000Z
slug: bump-the-social-network-for-license-plates
image:
  src: /files/blog/bump-the-social-network-for-license-plates/bump.jpg
  alt: Bump logo

tags:
  - cars
  - bump
  - license plate
  - social media
---

A new service has shown up online geared entirely towards license plates - or rather, the cars & drivers to whom they belong. The [basic premise](http://www.bump.com/about) is that people ought to be able to connect with fellow drivers/human beings through something unique & universally understood: the license plate on our car. [BUMP](http://www.bump.com/) is trying to capitalize on the ubiquity of license plates & cell phones to create a social network & communication platform.

I heard about the service in passing earlier today on a recent episode of [Tech News Today](http://twit.tv/tnt), but didn't pay it much mind. Then I stumbled across [a Mashable post](http://mashable.com/2010/09/23/bump-license-plate-messaging/) on my [Twitter feed](https://twitter.com/cconover) that mentioned the service & I figured I'd check it out. Naturally I signed up for it because it involves cars and it's a new service, so I couldn't resist. It only took a few minutes to claim [my license plate](http://www.bump.com/car/MDBMW0691) & set up [my user account](http://www.bump.com/user/cconover). So far it seems pretty cool, though I haven't gotten to really use it yet since nobody else I know is on there.

Speaking of which, BUMP it still in invite-only beta, but guess what? Apparently I can send out invites, so if you want one shoot me an email & I'll hook you up (for as many as BUMP lets me send out). Once you're set up, stop by & "bump" me and perhaps throw your user URL in the comments below. You can find my BUMP account in the "Find Me Online" menu at the top of this page, which I just recently added.