---
title: Follow the San Francisco World Series Riots Live on the Internet
date: 2010-11-02T06:03:07.000Z
slug: san-francisco-riots

tags:
  - '#SFRiot'
  - '#sfscanner'
  - san francisco
  - world series
---

Isn't technology great? The [San Francisco Giants](http://en.wikipedia.org/wiki/San_Francisco_Giants) won the World Series tonight, [sparking riots all over San Francisco](http://cnvr.cc/bPtf1Y). Obviously I'm nowhere near there, but because of the Internet I can see & hear all the excitement in real time. Here's how you can too!

# Police Scanner Feed

Listen to the live San Francisco Police Department scanner feed right here via UStream (**audio feed provided by [Soma FM](http://soma.fm/)**)

[Download PLS file for use with your own media player](http://cnvr.cc/bdvvq1).

# News Feed

KRON is broadcasting live footage from the streets of San Francisco on their official UStream channel, which you can see here.

# Twitter

Follow the following hashtags: [#SFRiot](http://cnvr.cc/bmqONc), [#sfscanner](http://cnvr.cc/9ahmkZ)

[@Stammy](http://twitter.com/stammy) is tweeting highlights from the police radio traffic.
