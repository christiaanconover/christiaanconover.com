---
title: Trying Out Blackbird Pie for WordPress
date: 2010-11-06T05:31:33.000Z
slug: trying-out-blackbird-pie-for-wordpress

tags:
  - blackbird pie
  - social media
  - twitter
---

I [recently learned](http://cnvr.cc/9Wi1N0) of a clever plugin for WordPress that embeds tweets in an attractive, fully functional manner into a Wordpress post with just tweet URL. It's called Blackbird Pie, and it appears to work really well. All you have to do is install the plugin & it adds a button in the native editor in WordPress. Click it, paste the URL to the tweet you want to include, and it does the rest.

As you'll notice below, any links that were included in the tweet are working, and it pulls in the design of the user's Twitter background. It's like having a screenshot, but much easier & more useful. Expect to see a lot more of these on here in the future. Mainly because I can.

{{< tweet 780190191976448 >}}

**Update 03JAN12**: I've switched to [Twitter Embed](http://wordpress.org/extend/plugins/twitter-embed/), since Blackbird Pie was causing issues with my current site theme and Twitter Embed is even simpler to use.

**Update 14JUN12**: With the release of WordPress 3.4 these plugins are no longer required, as the ability to embed tweets via URL is supported natively in WordPress.