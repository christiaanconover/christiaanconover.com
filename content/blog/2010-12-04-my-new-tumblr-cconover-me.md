---
title: 'My New Tumblr: cconover.me'
date: 2010-12-04T06:01:34.000Z
slug: my-new-tumblr-cconover-me

tags:
  - cconover.me
  - social media
  - tumblr
---

I've resurrected my use of Tumblr & will be using it from now on to post items of a more random & informal nature. I'm using it with a new domain: cconover.me. I'll post things that I think are cool, weird, fascinating, exciting, or anything else I feel like sharing, as well as stuff I'm doing & have encountered.

How is this different from the blog on this site? Think of it like the difference between writing a letter & sending a text. The blog on this site is more of the polished, curated side of my personal content publishing. My Tumblr will be the text message-like side of my stuff, where I post up short, quick things I want to share.

In addition to being able to follow it directly through Tumblr, you'll also see my Tumblr posts show up on my [Twitter feed](http://twitter.com/cconover), [Facebook fan page](http://www.facebook.com/pages/Christiaan-Conover/157273287627300) & the RSS feed associated with cconover.me. So if you're already following me in any of those places then you'll be seeing my Tumblr stuff automatically.

**Update**: The address cconover.me now points to ~~my Google+ profile~~ this site.
