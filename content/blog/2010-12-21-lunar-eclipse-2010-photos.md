---
title: 'Lunar Eclipse 2010 [Photos]'
date: 2010-12-21T18:26:49.000Z
slug: lunar-eclipse-2010-photos

tags:
  - lunar eclipse
  - moon
  - photography
---

Last night (I guess I should say early this morning) a full lunar eclipse took place. I stayed up to watch most of it, and took a lot of photos & video. Here are some of the better photos that came out of it.

![Lunar eclipse](/files/blog/lunar-eclipse-2010-photos/01.jpg)

![Lunar eclipse](/files/blog/lunar-eclipse-2010-photos/02.jpg)

![Lunar eclipse](/files/blog/lunar-eclipse-2010-photos/03.jpg)
