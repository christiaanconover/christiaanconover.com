---
title: 'Lunar Eclipse 2010 [Video]'
date: 2010-12-26T16:44:51.000Z
slug: lunar-eclipse-2010-video
tags:
  - lunar eclipse
  - moon
---

{{< youtube TOhbAacmg60 >}}

In addition to the [photos of the lunar eclipse](/blog/lunar-eclipse-2010-photos/) I posted earlier this week, I also took video of the event. It's rather long, but it shows the eclipse from just as the moon is fully covered through to when it starts to reappear on the other side.
