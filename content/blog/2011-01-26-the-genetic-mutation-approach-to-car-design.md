---
title: The Genetic Mutation Approach to Car Design
date: 2011-01-26T19:05:42.000Z
slug: the-genetic-mutation-approach-to-car-design
image:
  src: /files/blog/the-genetic-mutation-approach-to-car-design/mutation-car.png
  alt: Genetic mutation car

tags:
  - car design
  - genetic mutation
---

My friend sent me a link to a Flash-based scenario simulator that designs a "car" based on genetic mutation.

If that sentence just made your head spin, allow me to explain. Using an algorithm and some graphics to show the results, the Flash application builds a car using a series of geometric shapes & a couple of wheels. The size and dimension of every piece can be altered by the algorithm. It then tries to drive the car down what looks like an offroad trail, with a number of bumps and hills. If/when the car gets stuck or flips, the program records where and how, and the specs of the car, and uses this data to evolve the design of the car.

It's basically a cool demonstration of evolutionary algorithmic design using vehicles & cool graphics.

**[Check it out!](http://megaswf.com/serve/102223/)**

**Update:** there's [a web site](http://boxcar2d.com/) for the project, where they appear to continue updating it.
