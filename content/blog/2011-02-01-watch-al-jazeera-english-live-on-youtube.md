---
title: Watch Al Jazeera English Live on YouTube
date: 2011-02-01T20:30:38.000Z
slug: watch-al-jazeera-english-live-on-youtube
image:
  src: /files/blog/watch-al-jazeera-english-live-on-youtube/tahrir.jpg
  alt: Tahrir Square

tags:
  - al jazeera english
  - egypt
  - hosni mubarak
  - youtube
---

The news from Egypt is exciting & fascinating: the most populous country in the Middle East is working for democracy through peaceful protest & the call for [Hosni Mubarak](http://en.wikipedia.org/wiki/Hosni_Mubarak), dictator for 30 years, to step down & the entire government to be restructured. Mubarak's regime has shut down the Internet in Egypt, and cell phone service has been interrupted on and off. [Al Jazeera English](http://en.wikipedia.org/wiki/Al_Jazeera_English) has managed to continue coverage throughout the demonstrations, and is making their broadcast available online. YouTube has enabled AJE to use their beta live streaming service, which I've been watching today during the massive protest in [Tahrir Square](http://en.wikipedia.org/wiki/Tahrir_Square).

The stream is freely available, and you can watch it on the [Al Jazeera English YouTube channel](https://www.youtube.com/user/AlJazeeraEnglish). It's lower on the page, to the right of the channel info block.
