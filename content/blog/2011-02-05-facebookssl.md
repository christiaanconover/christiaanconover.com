---
title: Protect Your Facebook Account from Hijacking
date: 2011-02-05T20:00:35.000Z
slug: facebookssl
image:
  src: /files/blog/facebookssl/facebook-ssl.png
  alt: Facebook SSL

tags:
  - cyber security
  - facebook
  - firesheep
  - social media
  - ssl
---

I realize that this is actually kind of old news (a week or so), but I still felt it was important enough to share with my readers that may not be aware of this feature, and the reason for needing it.

Facebook [recently enabled the ability to use a persistent secure connection](https://blog.facebook.com/blog.php?post=486790652130) to their servers when accessing the Facebook site. What this means is that, once you turn it on in your account settings, all data sent between your computer and Facebook will be fully encrypted. This helps keep your data private, but more importantly it prevents possible snoopers & hijackers from gaining access to your Facebook account without you knowing it, and without even needing your password. Up until recently this was a complicated, cumbersome procedure. Thanks to a little piece of software called Firesheep, it's now shockingly simple.

[Firesheep](http://en.wikipedia.org/wiki/Firesheep) is a Firefox extension that allows anyone with enough knowledge to install the plugin & use a web browser the ability to hijack other people's insecure connections when using unprotected Wifi (like Starbucks or most other public Wifi hotspots). It shows a list of all the connections being made through the wireless access point that Firesheep can access, and with one click the Firesheep user is able to take over that connection & be that person, without the victim knowing it or providing any data whatsoever. If a connection is secured however, then Firesheep won't be able to do a thing.

I highly recommend everyone turn on full time encryption for their Facebook account. There's absolutely no reason not to, and every reason you should. Plus, it takes all of about 15 seconds to set up. I'll even save you the most time-consuming part & [send you right to the settings page to do it](https://www.facebook.com/editaccount.php). Once you're there, click the "change" link next to Account Security, check the box next to "Browse Facebook on a secure connection (https) whenever possible" and click Save, and you're done! You've now protected yourself from Firesheep on Facebook.
