---
title: 'Unboxing: Zoom H4n Portable Recorder'
date: 2011-02-05T18:30:37.000Z
slug: zoomh4nunboxing

tags:
  - recorder
  - unboxing
  - zoom h4n
---

<video preload="metadata" controls="">
  <source src="/files/blog/zoomh4nunboxing/zoom-h4n-unboxing.mp4" type="video/mp4">
</video>

Yesterday I received a new piece of equipment for doing shows & field recordings: the [Zoom H4n](http://www.zoom.co.jp/english/products/h4n/). This is a portable stereo recorder which can support up to 4 channel simultaneous recording, using a combination of the built-in stereo condenser mics & the XLR & 1/4" combination inputs.

This is just an unboxing video; the demo & review video will follow shortly after.