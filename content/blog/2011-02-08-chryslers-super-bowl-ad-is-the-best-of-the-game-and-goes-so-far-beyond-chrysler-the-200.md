---
title: "Chrysler's Super Bowl Ad is the Best of the Game, and Goes So Far Beyond Chrysler & the 200"
date: 2011-02-08T07:59:17.000Z
slug: chryslers-super-bowl-ad-is-the-best-of-the-game-and-goes-so-far-beyond-chrysler-the-200
image:
  src: /files/blog/chryslers-super-bowl-ad-is-the-best-of-the-game-and-goes-so-far-beyond-chrysler-the-200/eminem-chrysler-detroit.jpg
  alt: Eminem in the Chrysler Super Bowl ad

tags:
  - chrysler
  - chrysler 200
  - eminem
  - super bowl
  - super bowl ads
---

Like 111 million other Americans, I watched the [45th Super Bowl](http://www.nfl.com/superbowl/45), though [my team](http://www.patriots.com/) unfortunately didn't make it. Unlike a number of years past, I actually watched for the football, but of course I watched the ads as well. There were a number of [funny](https://www.youtube.com/watch?v=3lWb1V87x3o), [amusing](https://www.youtube.com/watch?v=3snyXTNmFm8), [clever](https://www.youtube.com/watch?v=OTzk3ibvQeo) & [cute](https://www.youtube.com/watch?v=R55e-uHQna0) ads, and even some that were [downright bizarre](https://www.youtube.com/watch?v=qRMMBXx3kqk) or [straight up hot](https://www.youtube.com/watch?v=sOgAYnxVcww). Some of the car commercials [avoided the humor](https://www.youtube.com/watch?v=K89y4uZp9eQ) route and went [straight to the point](https://www.youtube.com/watch?v=c6nHFM19pWc). None, however, were as effective in grabbing attention & resonating on so many levels as the spot Chrysler put out. All I can say is: well done, Chrysler. Well done.

[Read more at Wheelspin »](http://wheelspin.tv/2011/02/08/op-ed-chryslers-super-bowl-ad-is-the-best-of-the-game-and-goes-so-far-beyond-chrysler-the-200/)
