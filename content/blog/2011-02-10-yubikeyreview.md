---
title: 'Review: Yubikey Authentication Device'
date: 2011-02-10T14:30:55.000Z
slug: yubikeyreview
image:
  src: /files/blog/yubikeyreview/01.jpg
  alt: Yubikey
tags:
  - cyber security
  - gadgets
  - lastpass
  - reviews
  - yubikey
---

As anyone who knows me well will tell you, I'm ~~kind of~~ a security geek. I'm fascinated by encryption, data protection, strong passwords, and generally locking things down just because I can. For a long time there was a challenge with some of my friends to see if any of them could get into my computer, or at the very least figure out one of the 4 passwords required to boot it up to a desktop. So when I found out about [Lastpass](/blog/lastpassreview/) & started using it, I was pretty excited. A service that fully encrypts all my passwords, allow me to generate strong, unique passwords for every site I use, and syncing to the cloud (my other tech passion) - what could be better? Well imagine my excitement when I found out that there was a device that could make not only Lastpass exponentially more secure & at the same time easier to use, but anything I did requiring a password! Enter the [Yubikey](http://www.yubico.com/yubikey).

> **"Christiaan, we're not all geeks like you. Why would I ever want this thing?"**

Great question, and I'll tell you why. If you're like a lot of people these days, a large majority of your life is stored electronically. Some of it is online with Google, Facebook, Flickr, etc. and some of it is on your own computer. It makes life very convenient, and in many ways a lot of fun. However, without some good strong passwords to protect all that information, **you're leaving yourself very vulnerable** to people who may be ill-intentioned.

If you're someone who keeps a little book, cheat sheet or a bunch of Post-Its with your passwords written down (or you use the same password for your bank as you do for Facebook) you're also leaving yourself incredibly vulnerable & you should definitely [take a look at my review of LastPass](/blog/lastpassreview/).

Now we've all heard it before: "make sure you use passwords with random letters, numbers & symbols and never use a word somebody could guess!" That's all well & good, but let's be honest: passwords are hard enough to remember as it is without having to add in gibberish. This is where the Yubikey comes in. It generates very long (mine is set to 54 characters), randomly generated passwords that you can use in conjunction with something you can actually remember. When combined, you finally have that password the security people tell you to have. The best part: all you have to do is push a button! It also features One Time Password technology (the primary feature of the device, actually) which I'll explain later.

**So the bottom line** on why you should keep reading this post, and why I think you ought to have one: it's as simple to use as it is incredibly secure.

## The Device Itself

First off, let's look at the actual hardware. This thing is small. I mean, it's noticeably smaller & lighter than your average flash drive. I keep it on my keychain (which is notoriously sparse) and don't notice it at all.

As you can see, quite small and also quite thin.

![Yubikey Size Comparison](/files/blog/yubikeyreview/quarter01.jpg)

![Yubikey Size Comparison](/files/blog/yubikeyreview/quarter02.jpg)

Don't think that it's fragile though. It's made from strong plastic, and is **sealed to be water resistant**. Notice how the USB tab doesn't look like the one on your flash drive, with the normal rectangular metal plug? That's because those are breakable, and leave the drive open to water damage. On the Yubikey the USB contact strips, as well as the touch-sensitive activation button, are integrated right into the plastic body. Mine has been through the laundry a couple of times, had drinks spilled on it, and even been dropped in a puddle - none of which even began to phase it. As you can see in the pictures mine has managed to accumulate a few scratches & dents along the way, but they don't affect it one iota. This thing is tough.

## "So if I plug it into my computer that means drivers and a headache, right?"

More good news! When you plug in the Yubikey, your computer will recognize it as a USB keyboard. This is awesome, because I'm not exaggerating when I say that USB keyboards are probably the most universally supported computer accessory in the world. Everything supports them, with no software or outside drivers needed. **Just plug this thing in & go**. Like I said earlier, it's simple.

The other cool thing about this is, when you press the button to use it, the Yubikey "types" the entry into the password field, and you'll see a whole bunch of dots suddenly fly across the screen. It's like watching the world's fastest typist or something.

## Setting It Up

Some of this continues down the path of [K.I.S.S.](http://en.wikipedia.org/wiki/KISS_principle) and some of it is not quite so straightforward - but before you get dismayed, don't worry! I've [written a post that explains step by step](/blog/yubikeyconfig/) & in-depth exactly how to configure your Yubikey for optimum security & simplicity. Now, if you're only planning to use it with Lastpass or other One Time Password compliant services, then it'll be ready to go the second you take it out of the box, no device configuration required. However, if you want to maximize your use of it & really secure your passwords all around, that's what I'll be explaining. Check back for that post, I'll put a link right here once it's up.

What I will say at this juncture is the more complicated aspect is a one-time process. You do it as soon as you get your Yubikey, and you NEVER have to do it again (unless, of course, you decide you want to for whatever reason). In any other place where you'd be setting up the Yubikey for use with various services (such as Lastpass), that service will walk you through exactly what to do, which usually only consists of pushing the button. Simple.

## Using It

Let's all say it together: Siiiiiiiiiiimmmmple. Just stick it into the USB port on your computer, and when a service you've set it up with prompts you for your Yubikey **you just hold your finger on the button for one second, release, and you're done**.

You'll know when you've got it inserted the right way & that it's ready to be used when the LED in the center of the button lights up green, like this:

![Yubikey in the Cr-48](/files/blog/yubikeyreview/cr48.jpg)

Only one end of it fits in the computer, and if it doesn't light up the first time flip it over. It really is no more difficult than that.

The way I have mine set up is with a static password (one that never changes), which I use in combination with a password I remember & type in to form a very secure Windows password. I also have it set up to do One Time Password for logging into Lastpass, which I mentioned at the beginning.

One Time Password (OTP) is pretty much what it sounds like: each password is only used one time, ever. Basically, when you push the button on the Yubikey, it generates an instance of OTP. The first 12 characters are always the same, as a way for it to identify itself. The rest of the password (very long & ugly) is totally random, and never repeated - ever. Once it generates the OTP, it sends it to whatever service requested it, such as Lastpass. That service then checks that password against its own records, to see if it makes sense for that device. If it does, it then sends it along to a central authentication server, which also checks to make sure that password makes sense for your device, as well as verifying that the sending service (i.e. Lastpass) is authorized to authenticate with this device. If all this stuff checks out, you're logged in. If it sounds complicated, that's because it is, and it's actually far more complex than I just explained. However, you don't need to worry about ANY of that in order to use it, I just wanted to demonstrate how secure One Time Password is.

In addition to all the verification procedures & checks that One Time Password offers, there's another major benefit. If you're using a computer on which some unscrupulous person has set up a keylogger to gather people's usernames & passwords, OTP foils their attempts. Since they'll log a password that can never be repeated, it makes their data totally useless & keeps you 100% protected. Take that, criminals!

Plus, don't forget: all of this happens when you just push one button, and let it do the rest.

## Final Thoughts

At this point I've had my Yubikey for about 6 months, and I can't imagine living without it. It's made me so much more confident in & comfortable with my personal cyber security, and at the same time made being secure easier than it ever was before. I honestly can't think of a single thing I'd change about it (OK, maybe the initial configuration - though that's truly not hard) and recommend it to everyone, period. I have my 85 year-old grandfather set up with one to simplify his computer using experience & keep him safe on the computer, and I couldn't have kept him this secure & still allow him to go about his business any other way.

**I give the Yubikey a 9 out 10**. If you want to get one, you can order it at the Yubikey web site.

[**Purchase a Yubikey »**](https://store.yubico.com/store/catalog/index.php?ref=212)

[Configure Your Yubikey](/blog/yubikeyconfig/)

_Full disclosure: the link contains my affiliate code, which gives me a few cents of any Yubikey purchased from it. It's not much & the price is the same whether you use that link or go directly to the store, but it helps offset the cost of running this site a little bit. I'd appreciate it if you use the link above, but most importantly I just really think it's a great product & think everyone should get one._
