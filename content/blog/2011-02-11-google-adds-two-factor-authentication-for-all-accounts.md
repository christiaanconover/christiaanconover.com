---
title: 'Google Adds Two Factor Authentication for All Accounts, Which You Should Use'
date: 2011-02-11T20:29:00.000Z
slug: google-adds-two-factor-authentication-for-all-accounts

tags:
  - 2 factor authentication
  - cyber security
  - gmail
  - google
---

It must be [security](/tags/cyber-security/) month here on my blog, since I've already written a [couple](/blog/facebookssl/) of [posts](/blog/yubikeyreview/) on that subject, with one or two more on the way. Regardless, I felt this was important enough to share with readers.

Google announced yesterday that [they're enabling two part authentication on all Google accounts](http://googleblog.blogspot.com/2011/02/advanced-sign-in-security-for-your.html). Originally offered [back in September 2010](http://googleenterprise.blogspot.com/2010/09/more-secure-cloud-for-millions-of.html) to Google Apps customers, this security enhancement feature is now rolling out to users of standard Google/Gmail accounts.

Why does this matter? Well, if you're like millions of people who use Google services including [Search](http://google.com/) and especially if you use [Gmail](http://gmail.com/), [Calendar](http://calendar.google.com/), [Docs](http://docs.google.com/), etc. then Google has a lot of information about you stored on their servers. Google's got a pretty good track record for strong security & data protection, and have [received recognition for it](http://googleenterprise.blogspot.com/2010/07/google-apps-for-government.html), but none of this really matters if users aren't protecting themselves. Chances are pretty good that you're using a password that isn't considered strong (like "password", "12345" a pet's name, etc.), or may even be reusing that password across multiple sites. If this sounds like you, you're definitely going to want to check out Google's two part authentication. I'd also recommend taking a look at [Lastpass](/blog/lastpassreview/) & [Yubikey](/blog/yubikeyreview/) to lock down your online presence with ease for all the sites you use.

Actually, even if you're using a strong password you should still enable two part security. See, strong passwords are excellent & I implore everyone to use them, but if somebody is able to log or record your password without your knowledge then it doesn't make a difference. The risk of this is fairly low on your own personal computer where you control everything, but if you're using someone else's machine (or a public shared terminal) then you have no idea what could be going on behind the scenes.

The way Google's two part authentication works is this: you log into your account as you normally would, with your email address & password. Rather than taking you directly to the service you want to use, it prompts you for a verification code. This code is a randomly generated number that changes every time you log in, making it impossible to replicate. So where do you get this number? From your cell phone. Google has developed an app for all the major smartphone platforms (Android, iOS, Blackberry) that will give you the code for that session. If you don't have a phone that can run this app, Google will send you a text message with the code. You can also opt to receive a phone call which will read the code to you. This way, anyone trying to break into your account will have to not only know your email & password, but also have physical access to your mobile device to receive the code. Without all of this, they won't get a thing.

Of course, this all seems like kind of a hassle to go through every time you access your email. Luckily, you can set it to remember computers that you trust, such as your personal computer at home. If you log on at a computer where you've set it to remember verification, you won't be prompted for the verification code & can just log in normally. Anywhere else you go, such as a public computer at a coffee shop, you (or anyone trying to pretend they're you) will have to enter the code.

So now that we're all on the same page about why this is important, how do you enable? Great question, and I'll show you.

First, [go to Google & sign in](https://www.google.com/accounts/Login) to your account. Once you're in, click on Settings -> Google Account settings.

![Google Account Settings](/files/blog/google-adds-two-factor-authentication-for-all-accounts/01.jpg)

Once you're there, locate the link that says "Using 2-step verification" in the Personal Settings section.

![Using 2-step Verification](/files/blog/google-adds-two-factor-authentication-for-all-accounts/02.jpg)

From there, follow the instructions on screen.

If you have a smartphone & would like to use the app for it, Google has [provided instructions for each major platform](http://www.google.com/support/accounts/bin/answer.py?hl=en&answer=1066447).

One of the things that's really awesome about 2 step verification is application-specific passwords. If you use software with your Google account that doesn't support 2 step verification (such as mobile Gmail, your smartphone, a desktop Google Talk client, etc.) the system will give you application-specific passwords. You simply type in the application you need it for, and it generates a 16 character random password that you enter into that application in lieu of your normal account password. You then have a control panel of all the application-specific passwords you're created & when they were set up. The best part: you can revoke access for any application at any time, and it will immediately invalidate that unique password. That way, if your phone is lost/stolen & you don't want people accessing your email on it, you can simply deny it access to your account & they can't do a thing. No need to change your primary Google password, just remove the one for your phone. Very smart.

Now not all accounts have had this enabled yet, so you may receive a message that this feature will be available soon for your account. If that's the case, be sure to check back & turn this on once you're able to. This is a simple & effective way to prevent people mucking about in your business, and it's exciting to see large companies taking such an active interest in helping their users stay secure.

_For more of my posts about how to protect your data, [look at my security archives](/tags/cyber-security/)._
