---
title: An Update on the Ultimate Blogger Contest
date: 2011-03-02T21:00:35.000Z
slug: ubupdate-1monthtogo

tags:
  - bmw
  - ultimate blogger
---

For the past couple of weeks [I've been an entrant in a competition called the Ultimate Blogger](/blog/ub/). When I submitted my entry & started promoting it, I figured I'd get a respectable number of votes, but it would be a ludicrous understatement to say that about what's actually happened. Within a couple of days I'd managed to grab 2nd place and pass 1000 votes. At this point I'm a little more than 2 weeks in & have almost 4200 votes, putting me in 3rd out of nearly 500 entrants!

I've been in second place up until this morning, when an entrant suddenly surged to the front of the pack at a rather astounding rate (somewhere along the lines of 5000-6000 votes in less than 12 hours). Regardless, I'm still doing very well with a significant lead ahead of the folks behind me, and my votes continue to climb steadily.

**Don't forget, you can vote once a day per IP address, so please do so**! You can vote by going to [cconover.com/ubvote](http://cconover.com/ubvote) and clicking the "+1" icon on the right.

Since this post is being written on March 2, that means we're officially less than a month away from the end of this stage of the contest! Despite feeling like forever at first, the time is actually going by pretty quickly, which is exciting.

I'm also doing a [daily video update](http://cnvr.cc/ubvideo) on my [YouTube channel](https://www.youtube.com/christiaanconover) about the contest, so be sure to check that out.

Thank you to everyone who's been voting & spreading the word, I'm very grateful for all the support!
