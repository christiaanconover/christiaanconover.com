---
title: How To Change Your Wordpress Permalink Settings Without Breaking Anything
date: 2011-03-03T14:50:03.000Z
slug: change-wordpress-permalinks
image:
  src: /files/blog/change-wordpress-permalinks/301-redirect.jpg

tags:
  - permalink
  - 301 redirect
  - wordpress
---

For years I've been using the same link structure on every Wordpress site I operate: example.com/year/month/day/post-title. I opted for this method because I liked that it gave time context to the content right inside the URL. Plus, my links looked fancier & more impressive! Recently I've decided I don't like this so much anymore, mainly for one reason: **it's not easy to tell somebody a link like that in conversation**. Since doing more audio & video content where I can't simply post a text link for people to click on, I've decided that big, complicated links are a pain. This is [partially solved by link shorteners](/blog/3-reasons-you-should-be-using-a-custom-url-shortener/), but these aren't good for conversation either. If, for example, I wanted to tell somebody about my post on Lastpass it would be awkward to say "go to C-N-V-R dot C-C slash lastpassreview" since they won't remember it & it sounds like a pain to do. Instead, if I could just say "go to C Conover dot com slash lastpassreview" they'll be more likely to do it. So I decided to change my Wordpress settings to enable this method.

There's just one problem: I've been using the old system for years, and those links are all over the place. I don't want them to break & people to get "Page not found" errors all day long, I want them to be routed to what they are interested in seeing. Luckily **that's easily accomplished** no matter how much you want to change.

### Global Redirects

The first thing to do is install a plugin called Dean's Permalinks Migration. This plugin is incredibly simple to set up, but very powerful. All you do is provide to it the old permalink structure (the one you're phasing out) which is simply the syntax code in the box on the "Permalinks" settings page in your Wordpress installation. Once you've saved that, the plugin will look for any request to your site using the old link structure. If it gets one, it redirects it to that same location under the new permalink structure. If the requested page doesn't exist (or at least not at that slug) then the user will still get a 404 error, but otherwise **it will work immediately across the whole site**.

![Permalinks Migration Settings Page](/files/blog/change-wordpress-permalinks/permalinks-migration.png)

Of course, the most important part of this is actually deciding on what you want the new permalink structure to be. The Permalinks Migration plugin only supports one entry for the old permalink structure, so you only want to change it once. Hopefully the plugin developer will add support for multiple entries, but for now be sure you've chosen the structure you really want.

The way this plugin handles redirects is important. Any time somebody goes to a page at the old location, Permalinks Migration returns a 301 code with the redirect command. "Christiaan, what's a 301 code & why do I care?" Good question, I have an answer. See your visitors won't really care or notice, they'll simply click or type a page they want, and after a moment they'll get there. Search engines will notice and care, because a 301 code indicated to a search engine that it's a permanent redirect. When a search engine is told that, it makes a note to update this page in its listings to reflect the new location to which it's being redirected. It may take a while for this change to show up in Google results & the like, but after a while you'll see the new URLs showing up in search results.

### Special Case Pages

Of course, not all pages will end up staying in the same place. You may decide you want to change a page's URL to make it simpler, more descriptive, or just because you think it looks better. I actually did this with all the show episodes on [Wheelspin Network](http://wheelspin.tv), to make it easier to quickly pull up an episode. Before I was using the structure I mentioned at the top of this post, but I've switched to just the show abbreviation & episode number. For example, the link to episode 40 of RoundelTable used to be: [wheelspin.tv/2011/02/01/roundeltable-40-bill-auberlens-daily-driver-has-3200-horsepower/](http://wheelspin.tv/2011/02/01/roundeltable-40-bill-auberlens-daily-driver-has-3200-horsepower/) instead, it now reads: [wheelspin.tv/rt40](http://wheelspin.tv/rt40).

Doing it that way makes it much easier for someone to quickly pull up an episode if I reference it during a show, and it looks a lot cleaner. However, since the location of the page itself has changed the Permalinks Migration plugin won't be sufficient to get a user from the old location to the new one. For this situation, we need to create a custom redirect. That's where the plugin [Quick Page/Post Redirect](http://wordpress.org/extend/plugins/quick-pagepost-redirect-plugin/) comes in.

This plugin does the same basic thing as Permalinks Migration, but on a page-by-page basis. All you have to do is go to the plugin's Settings page, and in a blank line enter the location the user requests on the left, and the location to redirect them to on the right. It's that simple.

![Quick Redirects Settings Page](/files/blog/change-wordpress-permalinks/quick-redirects.png)

As a side note you're not limited to redirecting to pages just on your site, but to any URL you want. You can see in the picture above that I have some links going to completely different places, but I can still give people a link through my domain to get there. Very handy.

This is obviously the more time-consuming stage of the process, but it's just as critical to a successful migration. The best way to ensure you don't miss any custom redirects is to add one to the list every time you change a page's location. That way you don't forget about any & end up sending users to a dead end.

### You're All Set!

Properly configuring these two plugins should ensure a totally seamless transition to your new permalink structure. I personally would like to see Wordpress support some sort of page tracking & redirect for permalink modification by default, but in lieu of that the solution I've outlined works like a champ.

Do you have a different solution for doing this? Let me know in the comments, I'd love to have some additional ways in my toolbox.
