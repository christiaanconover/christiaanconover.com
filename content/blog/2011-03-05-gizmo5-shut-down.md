---
title: "Gizmo5 is Shutting Down - C'mon, Really Google?"
date: 2011-03-05T06:15:04.000Z
slug: gizmo5-shut-down

tags:
  - gizmo5
  - google
---

So I got an email today from the folks at Gizmo5, the SIP voice over IP service provider [bought by Google back in November 2009](http://techcrunch.com/2009/11/09/exclusive-google-has-acquired-gizmo5/). Here's what it said:

> Hello,

> Gizmo5 is writing to let you know that we will no longer be providing service starting on April 3, 2011\. A week from today, March 11, 2011, you will no longer be able to add credit to your account.

> Although the standalone Gizmo5 client will no longer be available, we have since launched the ability to [call phones from within Gmail](http://www.google.com/chat/voice/) at even more affordable rates.

> If you purchased calling credit and have a balance remaining in your account, you can request a refund by logging in to <http://my.gizmo5.com>. If you are in the United States, you can instead choose to transfer your credit to a Google Voice account, so it can be used for calling from Google Voice or Gmail. If you don't have a Google Voice account, please [create one](https://www.google.com/voice) so that we can transfer your credit.

> Please request a call credit transfer or refund by **April 3, 2011**. If you don't request a call credit transfer or refund by this date, we will automatically refund your remaining call credit via the payment method you originally used to purchase the credit. Note that if you paid via Moneybookers or if the credit card on file has expired, we will not be able to automatically refund your unused credit, so please log in to initiate the refund process.

> Thank you,

> The Gizmo5 Team

Seriously Google? With all the effort you've been putting into voice communication & giving people access to [Google Voice](https://www.google.com/voice) in as many ways as possible, you're going to discontinue the best & cheapest way for people to use landlines with their GV account? The only reason I even signed up for a Gizmo5 account in the first place was because it could integrate right into Google Voice. It would allow me to use a landline at home where (at the time) I got terrible cell reception, while paying next to nothing and not have to use access numbers or do anything different to go through Google Voice. It was easy for me, and easy money for you.

So why shut it down? I can't imagine it was for a lack of users, since you guys closed the ability to create new accounts after you acquired the company. Perhaps existing users were reducing their usage, but that could be due in part to the fact that it seemed like Gizmo5 had been all but abandoned after the acquisition. I understand that I can make Google Voice calls right inside Gmail, which is a great feature, but it doesn't replace all the things Gizmo5 offered - the most important feature being the ability to use a normal desk phone with my GV number. I'm really not a fan of phone calls in general, but sometimes it's nice to be able to just pick up the phone & take care of things.

I really hope Google relaunches the Gizmo5 service in some form, even if it's simply integrated directly as a piece of Google Voice. In the meantime, my SIP adapter I have just for Gizmo5 will sit idle.
