---
title: Ecclestone is Determined to Destroy Formula 1 Credibility
date: 2011-03-07T22:37:17.000Z
slug: f1-artificial-rain

tags:
  - bernie ecclestone
  - formula 1
---

[Bernie Ecclestone](http://en.wikipedia.org/wiki/Bernie_Ecclestone) proposed in an [interview last week](http://www.formula1.com/news/interviews/2011/3/11783.html) that Formula One should add "artificial rain" to Grand Prix races to make them more exciting. He claimed that this would encourage overtaking because it would force drivers to deviate from the standard fastest dry track line, and said that the most exciting racing takes place in the wet. Those assertions may be based in fact, but he's out of his mind.

The addition of fake rain onto the track would turn F1 races into [American Gladiator](http://en.wikipedia.org/wiki/American_Gladiators) events. Instead of legitimate racing testing the skill of the drivers and ingenuity of car designs & teams, they're going to throw a wrench into all of that by throwing some water on the track every once in a while. It sounds like a page out of [Death Race](http://en.wikipedia.org/wiki/Death_Race_(film)). "Webber's got quite a lead built up, but let's see what happens when throw the sprinklers on right after these messages!" It's crap racing, and a cheap rating stunt.

I've heard the idea tossed around that Ecclestone is going to use this outrageous proposition as a way to barter for something less extreme that he actually wants, and depending on what that is I could understand. Regardless, let's hope this attempt to take a big bite out of F1's credibility as a motorsport circuit is shot down.
