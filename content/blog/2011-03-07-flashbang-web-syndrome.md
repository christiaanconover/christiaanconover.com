---
title: Hashbang is the New Flash
date: 2011-03-07T05:35:53.000Z
slug: flashbang-web-syndrome
image:
  src: /files/blog/flashbang-web-syndrome/hashbang.png
  alt: Hashbang

tags:
  - flashbang web syndrome
  - hashbang
  - flash
---

I've been doing a [bunch](http://isolani.co.uk/blog/javascript/BreakingTheWebWithHashBangs) of [reading](http://adactio.com/journal/4346/) lately on [hashbangs](http://hashbang.org/) and [why](http://www.webmonkey.com/2011/02/gawker-learns-the-hard-way-why-hash-bang-urls-are-evil/) they're [terrible](http://blog.benward.me/post/3231388630). For those not familiar, a hashbang is the "#!" you may have seen in URLs on sites like [Twitter](http://twitter.com/#!/cconover) or the [Gawker](http://gawker.com/#!5753733/welcome-to-the-new-gawker) blogs. It looks fairly innocuous, if a little ugly and code-y, but in reality it's a mess. See, what these two little characters represent is the destruction and destandardization of how web addresses should work.

I'm not going to [go into the technical details](http://www.tbray.org/ongoing/When/201x/2011/02/09/Hash-Blecch) about how a hashbang URL differs from a standard URL in terms of what happens behind the scenes, but suffice it to say there's a lot more complication & things that can - and do - go wrong when those two symbols show up.

The thing that strikes me is the similarity between the arguments against hashbangs & the arguments against Flash. In both cases you have a technology for providing content to users that makes unreasonable assumptions about what their browser supports. Flash obviously requires a third party plugin which [comes with its own set of problems](http://www.antezeta.com/blog/flash-problems), to include major security & performance flaws. Hashbangs aren't in and of themselves the technology, but represent the heavy use of Javascript & AJAX which are not web standards, and have varying levels of support between browsers. **It's fundamentally bad practice** for your site to require features your users may not have, and shouldn't be required to.

Hashbangs go one step further in their damage-doing. Unlike Flash, which if it fails you can always fall back to HTML content, hashbangs prevent any ability to have a backup plan if the user can't run the Javascript. Again, I refer you to the technical details on why this is, but the bottom line is **it makes it impossible to do anything** beyond scripting to load content. I have no problem with Javascript as a tool to create rich web applications & interfaces, but don't make it such an integral part of the site's structure that it can't survive without the script. Again, it's fundamentally bad practice.

Despite the fact that this is a horrible way to design a site to operate, we're seeing some major companies implement it as a "feature" of their site. At least Twitter gives you to option to either use the site with hashbangs, or revert to the older version which uses real URLs. I'll at least give them credit for not requiring it 100%. Gawker on the other hand doesn't have any ability to browse their site in a non-hashbang method so far as I can tell, which really irks me. To compound the issue, they had a [problem with their script](http://www.webmonkey.com/2011/02/gawker-learns-the-hard-way-why-hash-bang-urls-are-evil/) which demonstrated exactly why this is such a flawed technique.

So, both Flash & hashbangs are not standards-compliant. Both have major weaknesses & make assumptions about the user's capabilities. Both have serious stability & reliability problems. Both are also used in the name of a richer web experience which, with the advent of HTML5 and other technologies & best practices, can be accomplished in a more efficient & dependable fashion. I'm going to lump this issue into one term:

**Flashbang Web Syndrome**, the practice of a developer getting over zealous in their use of non-standard practices in the name of user experience, at the expense of long-term dependability and usability.

So, the next time you're building a web product, make sure you build it on a solid foundation of standards & best practices, and don't start throwing flashbangs at your users.
