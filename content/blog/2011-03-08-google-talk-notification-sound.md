---
title: Google Talk Notification Sound
date: 2011-03-08T18:47:31.000Z
slug: google-talk-notification-sound

tags:
  - google
  - google talk
  - notifications
---

I posted this on Android Forums a while back, and recently stumbled across it again so I thought I'd post it here. I wanted the notification sound for Google Talk on my Android phone for - surprise! - Google Talk messages. Unfortunately Google decided not to include it by default, so I managed to get it & post it online.

I figured there are probably other people who want it as well, so I've decided to put it here as well.

<audio controls="">
  <source src="/files/blog/google-talk-notification-sound/gtalk-notify.mp3">
</audio>

**[Download MP3](/files/blog/google-talk-notification-sound/gtalk-notify.mp3)**
