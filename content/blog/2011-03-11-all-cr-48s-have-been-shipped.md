---
title: All Cr-48s Have Been Shipped
date: 2011-03-11T17:32:57.000Z
slug: all-cr-48s-have-been-shipped
image:
  src: /files/blog/cr48/boxart.jpg
  alt: Cr-48 box
tags:
  - cr-48
  - gadgets
  - google
---

There have been no definite numbers from Google regarding how many [Cr-48s](/blog/cr48/) they planned to ship, or when the shipments would end. A tweet from [Sundar Pichai](http://en.wikipedia.org/wiki/Sundar_Pichai) a couple of days ago has cleared all of that up, and if you've yet to receive one of the test notebooks then you probably won't like the news.

{{< tweet 45181003521728512 >}}

Still no word on exactly how many Cr-48s were shipped in total, but estimates are around 60,000.

As mentioned in Sundar's tweet, Chrome OS notebooks will be commercially available this summer. As a recipient of a Cr-48, I can say that I'd definitely be interested in getting another Chrome OS computer. If you're a student looking for a small laptop to take to class then Chrome OS may be a very viable option.

If you received a Cr-48, let me know your thoughts on it.
