---
title: "I've Found My New Best Friend in Hosting, and Their Name is Media Temple"
date: 2011-03-12T04:58:06.000Z
slug: media-temple-service-rocks

tags:
  - hosting
  - media temple
---

About 6 weeks ago I decided to try out [(mt) Media Temple](http://mediatemple.net/) to host my sites. I'd heard good things about them from [friends](http://drawne.com/) of mine & various [web](http://paulstamatiou.com/) [folks](http://jessicagottlieb.com/) with high traffic sites, and decided it was worth a try. After years of hopping host to host, I've finally found a home to settle down & raise a web family.

I'm using Media Temple's [(gs) Grid Service](http://mediatemple.net/webhosting/gs/), which is their answer to shared hosting. Part of the reason I was intrigued is that it's not like most shared hosting, putting your account on a single server with hundreds of other sites to share the resources. Grid Service puts your site on a cluster of servers, so that the system can scale to meet demand. This means faster load times for end users, as well as better reliability & uptime.

I plan to go into all the details about what made me switch in a later post, but right now I want to highlight the reason I'm now a convert: their customer service. I've never encountered support reps as helpful & dedicated as these guys in just about any company I've worked with, let alone hosting. Let me demonstrate with an example.

Before I go any further, I want to clarify (as the guy from Media Temple did) that the case I'm going to spell out is not how every single case is handled, but he's just into this particular topic & there was a low number of tickets that gave him the time to dive into my issue as much as he did. That said, take this as an indication of what these guys are willing to do for their customers.

I've been trying for a while to figure out some redirect issues I've been having related to [RoundelTable](http://roundeltable.com) & [Wheelspin](http://wheelspin.tv). When I first started RoundelTable everything was located at roundeltable.com but has since been moved to wheelspin.tv. All of the links to episodes are exactly the same as they were originally, just at the new domain. The landing page for RoundelTable as a show, however, is a page within the hierarchy of Wheelspin. I wanted people going to roundeltable.com to be sent to this page, but any location within roundeltable.com to be taken to that same location within wheelspin.tv. Hopefully that all makes sense.

I [posed this question to the folks over at Stack Overflow](http://stackoverflow.com/questions/5228787/apache-rewrite-redirect-different-for-tld-vs-directory) and got some helpful advice, but the solution I got wasn't quite working. It was then suggested I use rewrite logs to debug it. I opened a ticket with Media Temple support on how to go about this, and this is when **things started getting awesome**. Here's the first reply I got from the support guy:

> The RewriteLog directive can not be enabled on the (gs) Grid-Service, as it requires changes to apache configuration files that are controlled at the system level - which is not possible on the shared system.

> I took a look at the rewrite rules you were trying to set up in your `.htaccess` and it appears that the failure to rewrite may have been a simple typo. In your rule, you had the uri regex defined at `^(.*)` rather than `^(.*)$` - it needs both the ^ and $ bookmarks for apache to specify the beginning and end of that portion of the rule.

> I commented out both incorrect rewrite condition/rule sets and added a third to your `.htaccess`. Hopefully, if that isn't exactly what you are looking for, it can provide you a template to get started on your own rewrites. If you have any additional questions or concerns regarding this information please just respond to this support request, I'd be happy to try and get you pointed in the right direction.

How cool is that? He actually looked at my specific file & made some suggestions on how to fix my exact problem. He didn't just refer me to a wiki or knowledge base article, he actually addressed my specific problem. Then he went a step further & added what he thought would fix the problem to the file, but commented out so I could decide whether to use it. I was blown away.

However, it didn't fix my issue. To be fair, I hadn't explained yet exactly what my situation was, but simply inquired about accessing the rewrite logs because my setup wasn't doing what I wanted it to. So I posted this reply:

> Thanks, I took a look at your changes & it does help some. However, the main reason I was trying to get some log data is because I'm trying to set [this](https://stackoverflow.com/questions/5228787/apache-rewrite-redirect-different-for-tld-vs-directory) up and I wanted to see why if there was an indication as to what was happening when the rules were parsed.

I figured that by showing him my Stack Overflow question he'd get a sense for what I was trying to accomplish & might be able to point me to a resource on how to accomplish this, or at the very least tell me whether this could be done. Apparently I underestimated this guy's knowledge on mod_rewrite and interest in helping me. Here's what I got in response (I hope you're sitting down):

> What you're looking for is absolutely possible. The answer they provided you on SO is close, but not quite what you're looking for. This should be all you need in your `.htaccess`:

> ```apache
> RewriteEngine On
> RewriteRule ^$ [http://wheelspin.tv/rt/ ](http://wheelspin.tv/rt/%C2%A0)[L]
> RewriteRule ^(.*)$ [http://wheelspin.tv/](http://wheelspin.tv/)$1
> ```

> -Why no `RewriteCond`?<br>
> The conditional you had set up was checking if the `HTTP_HOST` was equal to your domain name. Since this check exists inside the `.htaccess` for that domain, it is redundant because the only way for a request to have made it there is to be for your domain.

> -What are the rules doing?<br>
> After the rule declaration, you have regex which defines what is coming after the `RewriteBase` (which, by default is the trailing `/` after the `HTTP_HOST`). So the first rule says that there must be nothing there (which note, is technically and subtly different from `^/$`) and it will redirect to your desired subdirectory. If there is "something" there, it will skip that rule and go to the next which takes that "something" `(.*)` and send it over to the other site as the trailing `REQUEST_URI`.

> -What is the `[L]` and `[L,R=301]` about?<br>
> The `[L]` can essentially be translated as a stop sign. If there are multiple rules strung together, without a last flag, apache will try to apply all of them simultaneously. In the above, without it, the domain name by itself would be redirected to `http://http://wheelspin.tv/http://wheelspin.tv/rt`

> The `[R=301]` says to store the redirect as a `301`. This means browsers, search engines, etc which cache this information will save it as a permanent redirect. Since it's technically not necessary, I didn't include it in the example above, but you might want to add this flag to each rule - I'm not sure what you're ultimately looking to do with the rewrites.

> If there is anything else I can help clear up, please let me know and I'll be happy to help.

He just gave me a crash course in `mod_rewrite`, tailored to my scenario! He basically wrote a how-to blog post in the help ticket. I was blown away. Excited at the prospect of finally having a solution to this long-standing question, I headed on over to my `.htaccess` file to do some editing. This is what I found:

```apache
Options +FollowSymLinks
RewriteEngine On
#RewriteCond %{HTTP_HOST} ^roundeltable.com$ [NC]
#RewriteRule ^.*$ http://wheelspin.tv/rt [R=301,L]

#RewriteCond %{HTTP_HOST} ^roundeltable.com [NC]
#RewriteRule ^(.*) http://wheelspin.tv/$1 [R=301,L]

RewriteCond %{HTTP_HOST} ^roundeltable.com [NC]
RewriteRule ^/$ http://wheelspin.tv/rt/ [R=301,L]
RewriteRule ^(.*)$ http://wheelspin.tv/$1 [R=301,L]

######## this is probably what you're looking for =) -nat ########
# RewriteEngine On
# RewriteRule ^$ http://wheelspin.tv/rt/ [L,R=301]
# RewriteRule ^(.*)$ http://wheelspin.tv/$1 [R=301]
#
```

**HE EFFING PUT IT IN THERE FOR ME!** He left the rest of the file exactly as I'd left it with all my experimenting, then added his suggestion at the bottom commented out. I removed everything above his suggestion, removed the comments & it worked! I finally had an answer to my problem. I was ecstatic. I wrote back a very excited & positive response which I won't include here because I've basically already said it all. I then tweeted this:

{{< tweet 46381680214294528 >}}

Nat McLean, I nominate you for the award of Silver Server for going above & beyond the call of duty. Media Temple folks, if you're reading this then give this man a raise, or a bonus, or at least some extra time during his lunch break. You guys rock, beyond your excellent hosting service, for providing better support than anyone I've used before.

I'm going to stop waxing poetic about this before it gets any cheesier, but suffice it to say I'm totally impressed & blown away by Media Temple's customer service. If you're not hosting with them, I'd recommend seriously considering changing that.