---
title: Look for My Ultimate Blogger Info in the Paper
date: 2011-03-15T17:02:09.000Z
slug: ub-newspaper-interview

tags:
  - bmw
  - natalia
  - newspaper
  - ultimate blogger
---

I did an interview this morning with [a reporter](https://www.facebook.com/pages/Wendi-Winters/26907964103?sk=wall) from our [local paper here in Annapolis](http://www.hometownannapolis.com/) about the Ultimate Blogger contest & [my entry](http://cconover.com/ubvote). She asked about how the contest works, why I'm competing, what makes me qualified and a whole bunch of other details. It's expected to be published this coming Monday, and obviously I'll be posting up the info as soon as it's available. She also took some pictures of [Natalia](/tags/natalia/) & I, so hopefully one of those will make it into the article as well!

Please continue [voting every day](http://cconover.com/ubvote), and thanks to everyone who's been voting & getting the word out.