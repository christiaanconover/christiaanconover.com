---
title: My Ultimate Blogger Entry in the Newspaper
date: 2011-03-21T19:32:09.000Z
slug: ubpaper

tags:
  - bmw
  - capital gazette
  - ultimate blogger
---

As I [mentioned last week](/blog/ub-newspaper-interview/), my [Ultimate Blogger entry](http://cconover.com/ubvote) is getting some coverage in the [local paper](http://hometownannapolis.com/). The article got published today! You can [see it online](http://www.hometownannapolis.com/news/cbn/2011/03/21-12/Around-Broadneck-BHS-grad-revved-up-over-dealerships-blogger-contest.html), or just read it here as I've included it below.

Here's the text of the article:

> Not every man remembers the first time he fell in love. Cape St. Claire resident Christiaan Conover, 22, does. His passion was roiled "the first time the thunderous shout and titillating lines of my neighbor's Corvette called to me like Jessica Alba in a swimsuit."
>
> Christiaan, a '07 Broadneck High graduate, loves classy cars. In recent years, the University of Maryland, College Park Computer Studies major and Coast Guard Reservist has shifted his affections to BMWs.
>
> "I fell in love with BMW after one ride behind the wheel," he said.
>
> Christiaan has a web show devoted to all things BMW. Broad-casting for nearly a year, it live-streams from 8 to 9 p.m. Tuesdays at [www.RoundelTable.com](http://www.roundeltable.com/) with co-host Josh Lewis. In addition to the show, Christiaan blogs and Tweets regularly.
>
> Endras BMW, a large dealership in Ontario, Canada, is running The Ultimate Blogger Contest. It's seeking to hire someone to blog about its products, services and promotions. The winner has a job for a year, a $65,000 salary, a new BMW every six months, a year's free lease on a downtown Toronto condo, and will be dispatched to blog about BMWs at events, drives and races all over North America.
>
> No surprise, Christiaan wants that job. The winners in the first round are selected through an online voting process. There are 500 contestants worldwide. Currently, Christiaan is in third place. Voting ends April 1. You can vote for him once a day per device and/or IP address at [www.cconover.com/ubvote](http://www.cconover.com/ubvote).
>
> When the voting ends, the top 20 vote-getters have 48 hours to post a video about themselves. From that pool, Endras BMW officials will select and fly 10 candidates to Toronto for a job interview.

Hopefully we'll see a nice bump in votes from this!

If you want to check out all the details about the contest, check out [my post with all the information](/blog/ub/).
