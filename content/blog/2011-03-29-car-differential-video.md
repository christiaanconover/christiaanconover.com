---
title: "Ever Wondered How a Car's Differential Works? Here's Your Answer"
date: 2011-03-29T13:24:56.000Z
slug: car-differential-video

tags:
  - cars
  - differential
---

{{< youtube F40ZBDAG8-o >}}

Even for gearheads, some systems in a car are a little hard to wrap your head around. Things like the transmission & differential are good examples of this. My friend sent me a video today that does a great job of explaining the principles of a differential in a simple, easy to understand way. The best part: it's from the 1930s, so it's got that great old film feel to it.

It's actually pretty interesting whether you're a car person or not. Take a look.