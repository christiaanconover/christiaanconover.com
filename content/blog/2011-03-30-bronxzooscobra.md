---
title: Looking For Some Comic Relief? Check Out @BronxZoosCobra
date: 2011-03-30T17:59:18.000Z
slug: bronxzooscobra

tags:
  - '@BronxZoosCobra'
  - humor
  - twitter
---

As you may or may not have heard, a cobra in the Reptile House at the Bronx Zoo [escaped from its habitat](http://www.msnbc.msn.com/id/42290811/ns/us_news-weird_news/) over the weekend. The Bronx Zoo has [taken precautions](http://bronxzoo.com/multimedia/headlines/reptile-house-closed.aspx) to avoid the possibility of visitors encountering the snake, and they believe it's still in the Reptile House, so it's not a huge deal. Still, there's a lot of comedy to be found in the situation, and somebody has apparently taken to starting the [latest Twitter sensation](http://twitter.com/BronxZoosCobra) around it.

Whether you're a Twitter-er or not, it's worth a read & a follow because some of the posts are truly hilarious. Here are some of my favorites so far:

{{< tweet 53054880021102593 >}}

{{< tweet 52769121443659776 >}}

{{< tweet 52944396001345536 >}}

It took me 5 minutes to decide which 3 to include here. Definitely check it out, it's comic gold.

**[@BronxZoosCobra](http://twitter.com/BronxZoosCobra)**

*Thanks to my friend [Roxy](https://www.facebook.com/harajukuroxy) for sharing this with me.*