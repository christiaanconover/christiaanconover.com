---
title: 'Last Day In This Round of Voting!'
date: 2011-04-01T13:32:03.000Z
slug: last-day-in-this-round-of-voting

tags:
  - bmw
  - ultimate blogger
---

Today is the **last day** in this first round of voting for the Ultimate Blogger contest! It's been many weeks since I [first submitted my entry](/blog/ub/) and started asking for your support in getting votes, and it's been quite the journey. From ranking changes and rallies to a newspaper article, it's been exciting to be part of it and now we're almost there.

# But it's not over yet!

There's another round of voting coming up, and everyone starts back at zero! At this point it seems fairly safe to say that I'll be in the top 20, which is the requirement to move on. This weekend I'll be putting together a video to demonstrate why I'm the Ultimate Blogger. By the end of Sunday that video will be posted, and the race will be on. I'll be asking once again for everyone's votes to **put me in the top 10 video entrants**.

Keep an eye out for information from me over the next few days on how the video voting stage will work and when it's time to start getting those votes in & spreading the word!

Thank you so much to everyone who's been helping and supporting me, let's keep it going!

**[Vote for me!](http://cconover.com/ubvote)**

_You can [check the leaderboard here](http://cnvr.cc/ubleaderboard)._
