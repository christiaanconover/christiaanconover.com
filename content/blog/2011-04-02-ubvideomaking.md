---
title: 'Ultimate Blogger Stage Two: Video'
date: 2011-04-02T04:22:47.000Z
slug: ubvideomaking

tags:
  - '#teamconover'
  - bmw
  - endras bmw
  - ultimate blogger
---

Just mere minutes ago the voting for the first round has ended, and I wrapped up in 4th. Since the top 20 move on to Round 2, it's a pretty excellent spot to be. Now comes the scramble: I have 48 hours from now to produce and post a video demonstrating why I'm the right man for the job.

Once the video is posted I'll need to get votes, just like we've been doing for the past few weeks, to get into the top 10 for round 2\. The top 10 candidates will then go to Toronto to interview.

I'll use this page to update my progress throughout the weekend, so bookmark this to see how things are going!

**Note:** if you happen to be in the Annapolis area this weekend and would like to help out with the video production at any point, feel free to contact me.

# Saturday, April 2 12:19AM

Here's the email I got from Endras BMW with the rules & restrictions for the video:

> Dear Ultimate Blogger Contestant,

> Congratulations! You've made it to Phase II and you are one step closer to becoming the Ultimate Blogger.

> You now have exactly 48 hours to submit your video for Phase II. As soon as voting for this round begins (April 4 at 10:00 am ET) your votes will be reset to zero and the voting period for your video will commence.

> Please read the following guidelines for Ultimate Blogger – Phase II **very carefully** and adhere to each. Failure to comply with these guidelines will result in the removal of your video from the site until the issue is rectified.

> - Video must be submitted by **April 3 at 11:59 pm ET**.
> - Failure to submit your video by **April 3 at 11:59 pm ET** (without proof of technical difficulty or proof that failure to post was at no fault of your own) will result in **immediate disqualification**.
> - Video must be a **minimum of 60 seconds** and a **maximum of 90 seconds** long.
> - **Videos must comply with the rules and regulations of the contest** (please see the site for a reminder if necessary), and cannot contain profane or inappropriate material. The definition of profane and inappropriate material is at the discretion of Endras BMW. Any videos that are deemed profane or inappropriate will be immediately removed by administration.
> - Videos **MUST** prove why you are **the best candidate for the Ultimate Blogger position**, **your obsession with and knowledge for BMW**, and **your obsession with performance**.
> - All contestants must confirm receipt of this email with a quick reply.

> To submit your video, upload it to **YouTube** and email the YouTube link to xxxxxxxx@endrasbmw.com by **11:59pm ET on April 3**.

> Good luck with your videos. We can't wait to see them!

> Ultimate Blogger Administration

Let's do this, see everyone in the morning!

# Saturday, April 2 12:42AM

I just received the following email from Endras:

> Hello Contestants!

> We have noticed a slight mistype in our Phase II guidelines email.

> **The previous email contained the text:**

> `Video must be a **minimum of 60 seconds** and a **maximum of 90 seconds** long.`

> **While it should have read:**

> `Video must be a **minimum of 60 seconds** and a **maximum of 120 seconds** long.`

> Thank you and good luck!

Interesting.

# Saturday, April 2 8:05AM

I'm up & moving, let the brainstorming begin!

# Sunday, April 3 1:40PM

All footage has been shot, editing has started and we're making good progress. The video should be posted in a couple of hours!

# Monday, April 4 10:00AM

The video is posted, and voting has begun! Go to [cconover.com/ubvote](http://cconover.com/ubvote) to watch it & vote for my entry!