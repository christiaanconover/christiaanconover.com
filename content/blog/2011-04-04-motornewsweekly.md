---
title: 'My Newest Endeavour: News Correspondent for Motor News Weekly'
date: 2011-04-04T23:47:47.000Z
slug: motornewsweekly

tags:
  - bob long
  - motor news weekly
  - radio
  - talk radio network
---

As most readers are probably already aware I've been doing a [weekly BMW program](http://roundeltable.com/) for almost a year now. What you may not realize is that we're currently working out the details of a syndication deal with automotive radio legend [Bob Long](http://www.boblongradio.com/)'s [newest venture](http://automotiveradionetwork.com/). As a result of this process I've gotten a fantastic opportunity.

Starting this weekend, April 9-10, I'll be a news correspondent on Bob's show [Motor News Weekly](http://www.trn1.com/motornews-about). The show is part of the [Talk Radio Network](http://www.trn1.com/) and airs on hundreds of stations nationwide. My spots will be 2-3 minutes long and will cover the week's automotive headlines. I'm very excited about this new adventure & all that it has to offer!

The show airs at 11AM Eastern on Saturdays & Sundays on your local Talk Radio Network affiliate.
