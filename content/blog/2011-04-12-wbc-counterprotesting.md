---
title: "I'll Be Counter-Protesting the Westboro Baptist Church on Thursday"
date: 2011-04-12T20:43:14.000Z
slug: wbc-counterprotesting

tags:
  - counter protest
  - fort meade
  - westboro baptist church
---

The [Westboro Baptist Church](http://en.wikipedia.org/wiki/Westboro_Baptist_Church) is going to be [protesting at Meade High School](http://www.eyeonannapolis.net/2011/04/11/westboro-baptist-church-to-target-meade-high-school/) on Thursday morning. In a [press release](/files/blog/wbc-counterprotesting/wbc-ft-meade-14apr11-press-release.pdf) they stated that they're targeting Meade because of a large number of homosexual students. The school is part of the [Anne Arundel County Public School](http://www.aacps.org/) system, though it primarily serves the military community of [Fort Meade](http://en.wikipedia.org/wiki/Fort_George_G._Meade).

I'll be attending a [counter-protest on Thursday morning](https://www.facebook.com/event.php?eid=209691095726011) in support of the students and tolerance. It starts at 6:30am and lasts for an hour. I'll be taking photos & video while I'm there. If you're around and available Thursday morning, come join us. Let's provide a buffer between kids trying to get an education and the hateful rhetoric of the Westboro Baptist Church.

**Update**: The [Anne Arundel County Police Department](http://www.aacounty.org/Police/index.cfm) has [released details about road closures](http://www.eyeonannapolis.net/2011/04/13/update-westboro-baptist-church-protest/) during the protest.

[See pictures, videos and information from the counter protest](/blog/wbc-counterprotest-meade/)
