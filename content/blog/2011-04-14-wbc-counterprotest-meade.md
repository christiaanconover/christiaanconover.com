---
title: Counter Protest of Westboro Baptist Church at Meade High School
date: 2011-04-14T14:33:53.000Z
slug: wbc-counterprotest-meade
image:
  src: /files/blog/wbc-counterprotest-meade/01.jpg
  alt: Counter protesters

tags:
  - counter protest
  - fort meade
  - westboro baptist church
---

The [Westboro Baptist Church](http://en.wikipedia.org/wiki/Westboro_Baptist_Church) picketed this morning at [Meade High School](http://www.meadesenior.org/). As I discussed [in a post the other day](/blog/wbc-counterprotesting/), they based today's demonstration on claims that the school has a large number of homosexual students. When they announced their intentions, a surge of counter protest movements fired up, and the result was impressive.

We got there around 6:30am and already there was quite a crowd. The police closed off the road where the protest was being held, and kept the WBC & counter protesters on separate sides of the road. There were 5 or 6 people from Westboro Baptist Church:

![Westboro Baptist Church Demonstrators](/files/blog/wbc-counterprotest-meade/02.jpg)

Which was dwarfed by the crowd on the other side, which must have been at least 500 people.

![Counter Protesters Against the Westboro Baptist Church](/files/blog/wbc-counterprotest-meade/03.jpg)

![The Crowd of Counter Protesters](/files/blog/wbc-counterprotest-meade/04.jpg)

There was plenty of press coverage, both of the counter protest:

![Journalists Photographing the Counter Protest](/files/blog/wbc-counterprotest-meade/05.jpg)

And of the Westboro demonstrators.

![Journalists interview Shirley Phelps of the Westboro Baptist Church.](/files/blog/wbc-counterprotest-meade/06.jpg) Journalists interview Shirley Phelps of the Westboro Baptist Church.

While the signs held by demonstrators from Westboro Baptist Church were offensive and inflammatory on their own, the thing that perhaps made people the most infuriated was their dancing on the American flag, often smiling while doing so.

![A Westboro Baptist Church Demonstrator Standing on an American Flag](/files/blog/wbc-counterprotest-meade/07.jpg)

My mom made large signs which got quite a lot of attention, and I thought conveyed a great message of peace and tolerance,

![My Mom's Signs for the Counter Protest](/files/blog/wbc-counterprotest-meade/08.jpg)

![Mom and Her Signs for the Counter Protest](/files/blog/wbc-counterprotest-meade/09.jpg)

But perhaps the license plate on the car next to ours described the entire situation best.

![Awesome License Plate in the Parking Lot at the Counter Protest](/files/blog/wbc-counterprotest-meade/10.jpg)

One thing's for certain: as frustrating and bothersome as the WBC's message may be, they managed to bring together a community in a peaceful way around a common cause of respect and affirmation of positive feelings toward people of all races, genders, creeds and sexual orientations - all of which were well represented this morning.

**You can [watch all my videos from the protest in the playlist](https://www.youtube.com/user/ChristiaanConover?feature=mhum#p/c/37756B7E3A6B33B2) on my [YouTube channel](https://www.youtube.com/user/ChristiaanConover).**

Also [check out the video](https://www.youtube.com/watch?v=yUCwx-foGFg) taken by [John Frenaye](http://twitter.com/jfrenaye) of [Eye on Annapolis](http://www.eyeonannapolis.net/).
