---
title: 'LastPass May Have Been Breached, but Not Really - Regardless, Your Data is Safe'
date: 2011-05-05T18:03:42.000Z
slug: lastpass-data-still-safe

tags:
  - cloud computing
  - cyber security
  - data breach
  - lastpass
  - yubikey
---

Lastpass, my [favorite password management solution](/blog/lastpassreview/), noticed a "network traffic anomaly" on their servers yesterday which they couldn't account for. They [immediately notified users](http://blog.lastpass.com/2011/05/lastpass-security-notification.html) that they were investigating if any data was breached, and said this:

> Because we can't account for this anomaly either, we're going to be paranoid and assume the worst: that the data we stored in the database was somehow accessed. We know roughly the amount of data transferred and that it's big enough to have transferred people's email addresses, the server salt and their salted password hashes from the database. We also know that the amount of data taken isn't remotely enough to have pulled many users encrypted data blobs.

So basically, they don't know for sure but better to be safe than sorry. Good on them.

**Here's the good news**: if you use a [Yubikey to authenticate with Lastpass](/blog/yubikeyreview/), you can ignore this entire situation. Because Yubikey authenticates with a third party beyond Lastpass, any data that may have been gained is completely useless without your physical Yubikey. The only way an attacker could gain access to your account is if they a) had managed to figure out your password (unlikely, but possible) and b) if they are holding your exact Yubikey. If you're holding it right now, then the laws of physics tell us that's impossible. The moral of the story is, [buy a Yubikey immediately](https://store.yubico.com/store/catalog/product_info.php?ref=212&products_id=2&affiliate_banner_id=1) and get Lastpass Premium if you haven't already ([Yubikey offers a package deal for this](https://store.yubico.com/store/catalog/product_info.php?products_id=25&osCsid=488f8c5cc7558015b4e6c9b9dccf3132)).

The threat here is not one of a hacker actually having your passwords - that's protected by the full end-to-end encryption that Lastpass implements, and this is exactly why they do. The concern is that whoever may have grabbed data is now going to try a brute force attack on users' passwords in an effort to gain access to their account. Only if they successfully figure out the password can they see any of your data.

For those unfamiliar, a brute force attack is when a person trying to gain access to data tries a whole bunch of different passwords, often using a list of dictionary words as part of it. It's basically a game of chance: sooner or later they're bound to hit on somebody's account which is using a simple dictionary word for the password, so they just keep trying every word on everyone until they do.

Since Lastpass has identified this risk, they've taken steps to mitigate it. They're tracking IPs from where users log in, and if it's not a recent IP they block authentication. Mobile devices are also blocked, and they're requiring these users to change their master password. [Steve Gibson](http://grc.com), the security guru and my (unbeknownst to him) mentor, [explained this all very well in a tweet earlier today](http://www.twitlonger.com/show/a9bdm4).

Unfortunately, due to the volume of password reset requests from this situation they're a bit overloaded. They're asking users not to change their password unless prompted by the system, or as least not for a couple of days. Whether you're comfortable with that is up to you. As Steve explained though, they've also completely changed how passwords are handled going forward to make even existing passwords incredibly difficult to brute force. I would say it's fairly safe to hold off changing your password unless Lastpass explicitly asks you to.

Here's the bottom line: unlike the [recent breach of the Sony Playstation Network](http://technologizer.com/2011/04/26/playstation-network-breach-data-stolen/), all your data is fully encrypted and completely inaccessible without figuring out your credentials. Since Lastpass has already responded very appropriately and is taking all the steps they can to prevent any actual data breach, you shouldn't be worried. As I mentioned above, you should also be using a Yubikey.
