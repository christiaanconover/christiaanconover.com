---
title: 'THX "Deep Note" Sound Downloads'
date: 2011-05-05T13:41:35.000Z
slug: thx-sound-download
image:
  src: /files/blog/thx-sound-download/thx.jpg
  alt: THX logo

tags:
  - deep note
  - thx
---

Let's face it: who doesn't think the [THX](http://en.wikipedia.org/wiki/THX) sound at the beginning of movies (called "Deep Note") is totally awesome? Being the audio geek that I am, I'll sometimes skip back a few seconds on a DVD just to hear it a couple of times - yes, I'll admit it.

I was browsing around YouTube and found some HD copies of various THX intros, and got to thinking: wouldn't it be cool if I could have these sounds on my phone to test my car stereo, or use when testing an audio system when I'm setting it up? So I fired up the ol' studio and captured some sounds.

The next logical step, of course, is to share them with people. So for your listening enjoyment here are 4 variations of the infamous THX sound.

**MP3**<br>
[THX DVD](/files/blog/thx-sound-download/thx-dvd.mp3)<br>
[THX Rainforest](/files/blog/thx-sound-download/thx-rainforest.mp3)<br>
[THX Thunderstorm](/files/blog/thx-sound-download/thx-thunderstorm.mp3)<br>
[THX Transformers](/files/blog/thx-sound-download/thx-transformers.mp3)

**WAV**<br>
[THX DVD](/files/blog/thx-sound-download/thx-dvd.wav)<br>
[THX Rainforest](/files/blog/thx-sound-download/thx-rainforest.wav)<br>
[THX Thunderstorm](/files/blog/thx-sound-download/thx-thunderstorm.wav)<br>
[THX Transformers](/files/blog/thx-sound-download/thx-transformers.wav)
