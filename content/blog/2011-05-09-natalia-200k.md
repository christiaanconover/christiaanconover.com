---
title: Natalia Reaches a Milestone
date: 2011-05-09T01:26:08.000Z
slug: natalia-200k
image:
  src: /files/blog/natalia-200k/200k.jpg
  alt: 200k miles

tags:
  - bmw
  - bmw 330ci
  - natalia
  - my cars
---

Last night [Natalia](/tags/natalia/) hit a major milestone: 200,000 miles! This makes the 3rd car I've had to hit this point. Not everyone is a fan of seeing these sorts of things on their cars, but I consider it a point of pride that my car has gone this far and is still running like a champ.

Who else has had their car reach 200k miles (or even higher)?
