---
title: "Ultimate Blogger: I Wasn't Selected"
date: 2011-05-16T19:55:47.000Z
slug: ubfinale

tags:
  - bmw
  - ultimate blogger
---

So here we are at the conclusion of a months-long journey in pursuit of the Ultimate Blogger position, and despite lots of effort and tons of support, it didn't work out. While I'm certainly disappointed (who wouldn't be?) it's not the end of the world, and I have other things to pursue and will continue building my own products and brands.

I continue to be grateful to and flattered by the massive outpouring of support and voting during the course of the contest, and I hope that everyone who helped me try to achieve this goal understands how much I appreciate their efforts. It's been truly amazing to see such a huge group of people helping me. Thank you.

Congratulations to all the entrants who made it through to the last stage, it's been a pleasure competing with you, and talking BMWs with you! Congratulations to [the winner](http://alexotics.blogspot.com/), and good luck!

I'm looking forward to seeing what things I do next, and am glad to have such a large group of supporters!

**Update:** For those that are curious, here's the email:

> Dear Ultimate Blogger Contestants,

> Thank you so much for your interest in the competition and your incredible enthusiasm for the Endras BMW brand and the role of our Ultimate Blogger. It was a pleasure to meet you during your interview and see your passion for BMW. We had so many worthy candidates for the role of our Ultimate Blogger - all of you were obsessed with BMW, had a true passion for the brand and many of you had excellent ideas on how you would take hold of the position and promote Endras BMW. After interviewing all of you, we truly realized that any one of you could have filled the role and therefore it was a really difficult decision to choose just one of you.

> We wish we could have hired all of you but as you know, we can only hire one Ultimate Blogger so I am pleased to announce that the Endras BMW Blogger is Alexander De Clerck (username Alexotics) from Vancouver, British Columbia. Congratulations, Alex!

> Mark and I would like to thank all of you for participating in our competition and we do hope to stay in touch with all of you. BMW fans are a true community of enthusiasts and we sincerely hope you continue to fuel your passion for the brand as we do every day.

> Thank you so much.
