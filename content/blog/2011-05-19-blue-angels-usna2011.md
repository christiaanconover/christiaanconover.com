---
title: "Blue Angels at the U.S. Naval Academy: I'll be Live Streaming [Updated]"
date: 2011-05-19T14:54:36.000Z
slug: blue-angels-usna2011
image:
  src: /files/blog/blue-angels-live-streaming-this-afternoon/blue-angels.jpg
  alt: Blue Angels flying in formation

tags:
  - annapolis
  - blue angels
  - us naval academy
  - us navy
---

It's that time of the year again: [Blue Angels](http://en.wikipedia.org/wiki/Blue_Angels) at the [Naval Academy](http://www.usna.edu/homepage.php). Every year the team of stunt aviators comes to Annapolis during [Commissioning Week](http://www.usna.edu/SpecialEvents/CommWeek.htm). It's a highlight of the season for Annapolitans, and massive crowds line the banks of the Severn River to watch the show.

You may remember that [I streamed the event last year](/blog/blue-angels-live-streaming-this-afternoon/). It went so well I decided to do it again this year too! You'll be able to view the stream & chat at my live page.

The Blue Angels start at 2PM ET on Wednesday, May 25 2011. I'll start the live stream at 1:45PM. Mark your calendar and enjoy the show! Also, be sure to check my [Facebook](https://www.facebook.com/cconover) & [Twitter](https://twitter.com/cconover) pages for any updates.

**Update (May 23)**: The Blue Angels have cancelled this week's show due to safety concerns after a show in Virginia this weekend. You can [read about all the details](http://www.hometownannapolis.com/news/nav/2011/05/23-46/Blue-Angels-cancel-Naval-Academy-air-show.html).
