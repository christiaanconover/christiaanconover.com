---
title: Packing for Maine
date: 2011-05-25T23:28:00.000Z
slug: packing-for-maine
image:
  src: /files/blog/packing-for-maine/01.jpg
  alt: Packing

tags:
  - boston island
  - memorial day
---

We're headed to Maine this weekend to open up our summer place. We'll be leaving within the hour and driving overnight. More pictures from the trip to come as we go.
