---
title: Crossing Into Maine
date: 2011-05-26T09:40:00.000Z
slug: crossing-into-maine
image:
  src: /files/blog/crossing-into-maine/01.jpg
  alt: Bridge into Maine

tags:
  - boston island
  - memorial day
---

Last border left to cross. Next stop: Boston Island!
