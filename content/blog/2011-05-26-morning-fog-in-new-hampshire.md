---
title: Morning Fog in New Hampshire
date: 2011-05-26T09:23:00.000Z
slug: morning-fog-in-new-hampshire
image:
  src: /files/blog/morning-fog-in-new-hampshire/01.jpg
  alt: Morning fog

tags:
  - boston island
  - memorial day
---

Fog hovers low over the land on a cool New England morning in May.

As of the time of this picture, we're about 3 hours from our boatyard.
