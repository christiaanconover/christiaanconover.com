---
title: 'New Boat Engine!'
date: 2011-05-26T13:33:00.000Z
slug: new-boat-engine
image:
  src: /files/blog/new-boat-engine/01.jpg
  alt: Yamaha 75hp outboard

tags:
  - boats
  - boston island
  - memorial day
---

Behold: the brand new Yamaha 75hp 4-stroke! So excited to see what this baby's got.
