---
title: Pit Stop at L. L. Bean
date: 2011-05-26T13:03:00.000Z
slug: pit-stop-at-l-l-bean
image:
  src: /files/blog/pit-stop-at-l-l-bean/bean-boot.jpg
  alt: Bean Boot

tags:
  - boston island
  - ll bean
  - maine
  - memorial day
---

<video preload="metadata" controls="">
  <source src="/files/blog/pit-stop-at-l-l-bean/ll-bean-fish-tank.mp4" type="video/mp4">
</video>

We decided to make a quick stop at the L. L. Bean Flagship Store in Freeport, ME. It's always a good place to take an early morning stretch break.

The fish tank is new since the last time I was here, and is wicked cool. There's a bubble that lets you sit "inside" the tank!