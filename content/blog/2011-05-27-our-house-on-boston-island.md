---
title: Our House on Boston Island
date: 2011-05-27T18:22:00.000Z
slug: our-house-on-boston-island
image:
  src: /files/blog/our-house-on-boston-island/house.jpg
  alt: House on Boston Island

tags:
  - boston island
  - memorial day
---

Since it's sunny and pleasant today (albeit a bit foggy on the water) I figured it'd be a good time to post some pictures of the house.

It's actually a kit house, bought from the Sears catalog in the 1890s. It was brought out to the island unassembled by rowboat, and built on the site where it stands today.

The island has no electricity or running water, but we have propane which is hauled from the mainland and a rain water collection system. Being out here is basically living like a middle class family in the late 19th century.
