---
title: Fog at the Boatyard
date: 2011-05-28T23:05:57.000Z
slug: fog-at-the-boatyard
image:
  src: /files/blog/fog-at-the-boatyard/fog-boatyard.jpg
  alt: Fog at the boatyard

tags:
  - boston island
  - memorial day
---

After a couple of days of hard work we decided to come into the boatyard and get cleaned up. I snapped this while waiting for a shower to become available.
