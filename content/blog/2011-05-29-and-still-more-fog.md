---
title: And Still More Fog
date: 2011-05-29T13:38:38.000Z
slug: and-still-more-fog
image:
  src: /files/blog/and-still-more-fog/fog.jpg
  alt: Fog on Boston Island

tags:
  - boston island
  - memorial day
---

The view out my window this morning when I woke up...of more fog. Not that it's surprising or unexpected.

We're headed into town today for brunch and a local reggae cover band. Pictures and videos to come.
