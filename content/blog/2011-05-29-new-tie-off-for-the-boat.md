---
title: New Tie-Off for the Boat
date: 2011-05-29T20:48:00.000Z
slug: new-tie-off-for-the-boat
image:
  src: /files/blog/new-tie-off-for-the-boat/01.jpg
  alt: Boat on the tie-off

tags:
  - boston island
  - memorial day
---

One of the projects I had on my list for this trip was to upgrade the tie-off for our boat. Since there are three houses that share one float, we need a method to keep them accessible but off the loading area. The tie-offs are simply lines with weights on the end strung out to a piling that a boat is clipped on to.

I decided to change the way ours connects to the dock by splicing an eye in the line and installing galvanized hardware. This makes it stronger (and simpler) than a clove hitch.

I also added a dedicated cleat for our boat, so we won't have to hitch it to the railing every time, again making the process simpler.

![Tie-off](/files/blog/new-tie-off-for-the-boat/02.jpg)

![Tie-off](/files/blog/new-tie-off-for-the-boat/03.jpg)

![Tie-off](/files/blog/new-tie-off-for-the-boat/04.jpg)

![Tie-off tools](/files/blog/new-tie-off-for-the-boat/05.jpg)

![Tie-off](/files/blog/new-tie-off-for-the-boat/06.jpg)
