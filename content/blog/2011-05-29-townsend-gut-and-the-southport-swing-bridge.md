---
title: Townsend Gut and the Southport Swing Bridge
date: 2011-05-29T19:38:02.000Z
slug: townsend-gut-and-the-southport-swing-bridge
image:
  src: /files/blog/townsend-gut-and-the-southport-swing-bridge/01.jpg
  alt: Southport swing bridge

tags:
  - Boston Island
  - Memorial Day
---

We went in to town this afternoon for brunch. I snapped some photos of Townsend Gut, and the bridge that connects Southport to Boothbay.

![Southport swing bridge](/files/blog/townsend-gut-and-the-southport-swing-bridge/02.jpg)

![Southport swing bridge](/files/blog/townsend-gut-and-the-southport-swing-bridge/03.jpg)

![Southport swing bridge](/files/blog/townsend-gut-and-the-southport-swing-bridge/04.jpg)

![Townsend Gut](/files/blog/townsend-gut-and-the-southport-swing-bridge/05.jpg)

![Townsend Gut](/files/blog/townsend-gut-and-the-southport-swing-bridge/06.jpg)

![Townsend Gut](/files/blog/townsend-gut-and-the-southport-swing-bridge/07.jpg)

![Townsend Gut](/files/blog/townsend-gut-and-the-southport-swing-bridge/08.jpg)

![Townsend Gut](/files/blog/townsend-gut-and-the-southport-swing-bridge/09.jpg)

![Townsend Gut](/files/blog/townsend-gut-and-the-southport-swing-bridge/10.jpg)
