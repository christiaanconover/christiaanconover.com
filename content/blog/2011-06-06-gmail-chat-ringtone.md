---
title: Gmail Chat Ringtone
date: 2011-06-06T17:06:47.000Z
slug: gmail-chat-ringtone

tags:
  - google talk
  - ringtone
---

I use [Gmail Chat](http://www.google.com/talk/) all the time, both for instant messaging and calling. Since I use [Google Voice](http://google.com/voice) for all phone calls and text messages, it's very convenient to make calls through Chat since they integrate with each other. I like the ringtone that Google has for incoming calls in Chat, so I decided to put it on my phone as well.

<audio controls="">
  <source src="/files/blog/gmail-chat-ringtone/gtalk-ringtone.mp3">
</audio>

If you'd like to use it as your ringtone, you can download the MP3 below.

**[Download Ringtone](/files/blog/gmail-chat-ringtone/gtalk-ringtone.mp3)**
