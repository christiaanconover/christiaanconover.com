---
title: Getting Back to Ham Radio
date: 2011-09-15T19:57:57.000Z
slug: getting-back-to-ham-radio

tags:
  - amateur radio
  - arrl
  - hurricane
  - hurricane irene
  - w3vpr
---

After a rather long hiatus, I've decided to get back into ham radio. For the past couple of years I've been absent from the hobby, but [Hurricane Irene](/blog/irene/) and the value of my HT in getting information both from amateur and commercial sources reminded me how much I enjoy it.

So I've renewed my ARRL membership, reconnected with [my local club](http://w3vpr.org/), and tonight I'm headed to a club meeting. It's good to be back.
