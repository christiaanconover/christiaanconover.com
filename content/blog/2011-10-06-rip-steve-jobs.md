---
title: RIP Steve Jobs
date: 2011-10-06T03:46:31.000Z
slug: rip-steve-jobs
image:
  src: /files/blog/rip-steve-jobs/rip-steve-jobs.png
  alt: RIP Steve Jobs

tags:
  - steve jobs
---

News broke earlier tonight that [Apple founder Steve Jobs died today at age 56](http://www.washingtonpost.com/local/obituaries/steve-jobs-apple-computer-co-founder-dies/2010/09/21/gIQAc14aOL_story.html). Jobs battled pancreatic cancer for many years, and his disease ultimately took him.

While I wouldn't consider myself an Apple fanboy, I have immense respect for what Steve Jobs accomplished. His vision changed the world, and the way we think about technology. He will be missed.
