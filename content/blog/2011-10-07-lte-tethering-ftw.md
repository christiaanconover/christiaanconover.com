---
title: LTE Tethering FTW
date: 2011-10-07T16:01:02.000Z
slug: lte-tethering-ftw

tags:
  - android
  - htc thunderbolt
  - imac
  - lte
---

I recently rooted my [HTC Thunderbolt](http://en.wikipedia.org/wiki/HTC_ThunderBolt) and put [Cyanogen Mod 7](http://www.cyanogenmod.com/) on it, which includes tethering capability. This morning I connected my iMac at work to my phone, and ran a speed test. The results are impressive.

Download: 15.48 Mb/s<br>
Upload: 4.86 Mb/s<br>
Ping: 104ms

The test was run with the following signal status:

![LTE Tether Speed Test Phone Status](/files/blog/lte-tethering-ftw/signal.png)

So in case there was still any doubt, LTE is awesome.
