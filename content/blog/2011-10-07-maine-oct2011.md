---
title: Maine for the Weekend
date: 2011-10-07T20:19:47.000Z
slug: maine-oct2011
image:
  src: /files/blog/our-house-on-boston-island/house.jpg
  alt: House on Boston Island

tags:
  - boston island
  - liveblog
---

My father and I are headed up to Maine this weekend to close up our summer house. October is a great time of year to be there, since the leaves are changing and the weather is cool, but not so cold that it isn't fun. I'll be posting pictures on this page while we're there, so check back for that.

# 11 October 2011, 0950 EDT

So obviously the live blogging thing from Maine didn't work out so well. Part of that is because there's no good way to live blog from a mobile device with the plugin I'm using (hopefully that will be changed). The other reason is because I was really busy during the day and exhausted at night, so I didn't bother.

Anyway, here are some pictures from the rest of the weekend.

Saturday night's sunset, as seen from the top of the island overlooking the Sheepscot river.

![Boston Island Sunset October 2011](/files/blog/maine-oct2011/sunset01.jpg)

![Boston Island Sunset October 2011](/files/blog/maine-oct2011/sunset02.jpg)

![Boston Island Sunset October 2011](/files/blog/maine-oct2011/sunset03.jpg)

The results of our tree-cutting efforts.

![View After Cutting Down Trees](/files/blog/maine-oct2011/trees01.jpg)

## 08 October 2011, 1936 EDT

Took a nap once we got out to the island around 0615, slept until about noon.

Here are some pictures from the work we got done today.

![Cut-down trees](/files/blog/maine-oct2011/trees02.jpg)

![Cut-down trees](/files/blog/maine-oct2011/trees03.jpg)

![Cut-down trees](/files/blog/maine-oct2011/trees04.jpg)

![Cut-down trees](/files/blog/maine-oct2011/trees05.jpg)

![Cut-down trees](/files/blog/maine-oct2011/trees06.jpg)

![Cut-down trees](/files/blog/maine-oct2011/trees07.jpg)

# 08 October 2011, 0642 EDT

...and we've arrived.

# 07 October 2011, 1921 EDT

On the road headed north, just went across the Bay Bridge.

# 07 October 2011, 1611 EDT

Finished with work, headed home to pack and hit the road!

# 07 October 2011, 1100 EDT

It looks like the weather is going to be pretty nice in Maine this weekend.

![Weather Underground Southport, ME 07OCT11](/files/blog/maine-oct2011/weather01.png)
