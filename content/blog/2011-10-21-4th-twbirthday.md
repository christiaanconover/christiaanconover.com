---
title: My 4th TwBirthday
date: 2011-10-21T15:07:12.000Z
slug: 4th-twbirthday

tags:
  - social media
  - twbirthday
  - twitter
---

I got a tweet this morning from a Twitter account I didn't even know existed called TwBirthday. The [service](http://twbirthday.com/) apparently tracks when you signed up for Twitter, and messages you on your Twitter "birthday" - kind of a neat idea.

{{< tweet 127293536679899136 >}}

Does this give me free reign to be a precocious and finicky toddler on the Internet?