---
title: 'Google+ for Apps Accounts is Here!'
date: 2011-10-27T19:09:37.000Z
slug: gplus-gapps

tags:
  - google
  - google apps
  - social media
---

It's finally here! [Google+ for Google Apps](http://googleblog.blogspot.com/2011/10/google-popular-posts-eye-catching.html) accounts is a reality!

I've been a Google+ user [since it first launched](http://googleblog.blogspot.com/2011/06/introducing-google-project-real-life.html), and I really like it. The only issue I had with it was that I couldn't use my Apps account (which is the only Google account I use on a regular basis), and had to use a standard Gmail account instead. Big bummer, and it ended up keeping me from being very active on the service.

Today, Google has enabled the ability for Apps users to Google+ it up like everyone else. In order to use it, you or your domain's administrator must enable the Google+ service on your domain. Once that's done, you can set up your account and you're good to go!

I've already set mine up, which you can find ~~at cconover.me~~ [here](https://plus.google.com/107178809279250518773).
