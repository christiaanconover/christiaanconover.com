---
title: Nationwide Emergency Alert System Test November 9th
date: 2011-11-03T22:13:03.000Z
slug: eas-09nov11
image:
  src: /files/blog/eas-09nov11/eas.jpg
  alt: EAS

tags:
  - eas
  - fcc
  - fema
---

In case you haven't heard yet, the [Federal Emergency Management Agency](http://www.fema.gov/) (FEMA) and the [Federal Communications Commission](http://www.fcc.gov/) (FCC) are conducting a nationwide test of the [Emergency Alert System](http://en.wikipedia.org/wiki/Emergency_Alert_System) on November 9th, 2011.

Why does this matter? It's the first time it's being done across the entire country, all at once, initiated by a federal agency. Up until now this system has been activated by local and state emergency management agencies, and if the federal government wanted to have a message disseminated it would have to request each state to activate it. The goal with this test is to determine whether it's feasible to activate an emergency alert nationwide in case something like a terrorist attack occurs.

Due to my involvement with a number of emergency-related organizations, I've received a number of emails reminding me about this test and asking me to help disseminate information about it in advance. In that spirit, here are the details:

- **Wednesday, November 9th, 2011**
- **2:00PM Eastern/11:00AM Pacific**
- **Duration: 20-30 seconds**

Most people will likely not even notice it since it's in the middle of the work day, which is sort of the point. For those that do happen to see or hear it, it will sound like any other EAS test you've encountered before.

All in all this isn't all that big a deal, but because it's being done nationwide the government is doing their best to warn everyone in advance that it is, in fact, just a test.
