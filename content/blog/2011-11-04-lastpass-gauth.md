---
title: LastPass Now Supports Google Authenticator
date: 2011-11-04T19:34:01.000Z
slug: lastpass-gauth

tags:
  - cyber security
  - google authenticator
  - lastpass
---

[LastPass](http://lastpass.com), my [password manager of choice](/blog/lastpassreview/), has added support for [Google Authenticator](https://market.android.com/details?id=com.google.android.apps.authenticator&hl=en) as a method of two-factor authentication. For those unfamiliar, [Google added two factor authentication support](/blog/google-adds-two-factor-authentication-for-all-accounts/) earlier this year, a component of which is a mobile app that generates a random 6 digit string that refreshes every 30 seconds. The app is free, and you simply scan a QR code to configure it.

While I use Google Authenticator with my Google accounts, I haven't yet tried it with LastPass since [I use a Yubikey](/blog/yubikeyreview/). I still highly recommend Yubikey, but it does cost money and require a LastPass Premium subscription. Using Google Authenticator, however, is totally free and **if you're not using a Yubikey, you should use Authenticator**.

Lastpass has a how-to on [setting up Google Authenticator with LastPass](http://helpdesk.lastpass.com/security-options/google-authenticator/) so I'm not going to go into that here. I reached out to LastPass to find out whether there was a way to use Yubikey and Google Authenticator together in some sort of prioritized fashion ("if no Yubikey, request Authenticator code"). They said that's not available right now but it's in the works.

Bottom line: this is an awesome addition, and you should use it if you don't want a Yubikey. If you're not using LastPass, you should be doing that too.
