---
title: "Natalia's New Exterior Lights"
date: 2011-12-02T22:17:27.000Z
slug: natalia-new-lights

tags:
  - bmw 330ci
  - bmw
  - natalia
  - my cars
---

After the bulb sockets in my car's corner lights and the turn signal socket in one of my tail lights went bad, I decided it was time to replace a number of the exterior lights on her. I replaced everything except the headlights, which are the OEM units from the factory (I really like the OEM HIDs, so I see no need to swap them out).

The first thing I replaced was my corner lights. They had been the OEM ambers, but they clash with the black paint so I got smoked clear corners. I opted for LED amber lights instead of the chrome/amber incandescent because I like the clean flash characteristic and color of LEDs.

![Natalia's New Corner Lights](/files/blog/natalia-new-lights/corner-lights.jpg)

Next I replaced the stock amber side markers with dark smoked clear side markers. I chose the dark smoked ones because on a black car, when they're not illuminated they all but disappear into the paint job so you get the best of both worlds - a clean, unbroken look when they're off and extra signaling when they're on. Again, I opted for LED amber lights instead of incandescent bulbs.

![Smoked Corner Light and Dark Smoked Side Marker](/files/blog/natalia-new-lights/corner-side-marker.jpg)

Finally I replaced the tail lights. I chose smoked red/clear LED units, for all the reasons I've already listed. On a black car especially, the smoked tails give a much cleaner look.

![Smoked LED Tail Lights](/files/blog/natalia-new-lights/tail-lights.jpg)

I'm really happy with the look these lights have given my car. I'm amazed at what a difference the tail lights make. Plus, the LEDs look awesome when they're lit up :-)

Now that everything on the exterior is LED (minus the headlights), I need to replace the license plate bulbs with LEDs as well. I don't know if I'm going to replace the interior lights with LEDs, only because I like the off-white glow of the stock bulbs. In time I may change my mind though.
