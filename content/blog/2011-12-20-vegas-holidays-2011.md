---
title: 'Vegas, Baby!'
date: 2011-12-20T03:04:03.000Z
slug: vegas-holidays-2011

tags:
  - vegas 2011
---

This year for Christmas and New Year's the whole family is going to Las Vegas. My parents have been once before, but for the rest of us it will be the first time.

In addition to the normal Vegas Strip activities we'll be checking out things outside the city. I plan to post pictures and updates while we're out there.

If you have any tips on things we shouldn't miss, please leave a comment.

[**Posts from Vegas 2011**](/tags/vegas-2011/)