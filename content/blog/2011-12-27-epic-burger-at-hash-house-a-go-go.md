---
title: Epic Burger at Hash House A Go Go
date: 2011-12-27T02:05:37.000Z
slug: epic-burger-at-hash-house-a-go-go
image:
  src: /files/blog/epic-burger-at-hash-house-a-go-go/burger.jpg
  alt: Burger

tags:
  - hash house a go go
  - vegas 2011
---

Possibly the most epic and amazing burger I've ever eaten.

Two 8oz beef patties stuffed with bacon strips and bacon-filled mashed potatoes, topped with cheese, lettuce, onions and more bacon.

Incredible. Hooray for [Hash House a Go Go](http://www.hashhouseagogo.com/)!
