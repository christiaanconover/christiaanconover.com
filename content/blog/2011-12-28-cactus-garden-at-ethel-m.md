---
title: Cactus Garden at Ethel M
date: 2011-12-28T02:25:05.000Z
slug: cactus-garden-at-ethel-m
image:
  src: /files/blog/cactus-garden-at-ethel-m/cactus-garden.jpg
  alt: Cactus garden

tags:
  - ethel m
  - vegas 2011
---

The Ethel M Chocolate company has a cactus garden that they decorate for the holidays.

I have to admit that it's pretty strange to be walking around in short sleeves among cactus during Christmas time.
