---
title: 'Seriously?! An ATM for Gold?!'
date: 2011-12-30T02:14:30.000Z
slug: gold-atm-vegas
image:
  src: /files/blog/gold-atm-vegas/atm.jpg
  alt: Gold ATM

tags:
  - vegas 2011
---

High on the list of "only in Vegas" moments - an ATM to purchase gold. You can buy it in bars, coins, and a variety of other forms, all in different sizes. Wow.
