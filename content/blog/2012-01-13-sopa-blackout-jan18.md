---
title: Fight Censorship on Anti-SOPA/PIPA Blackout Day
date: 2012-01-13T16:57:32.000Z
slug: sopa-blackout-jan18

tags:
  - censorship
  - pipa
  - sopa
---

On January 18, the House Oversight and Government Reform Committee will [hold a hearing](http://oversight.house.gov/index.php?view=article&id=1553:issa-announces-oversight-hearing-on-dns-a-search-engine-blocking) to hear testimony from technology experts and job creators about why the [PROTECT-IP Act](http://en.wikipedia.org/wiki/PROTECT_IP_Act) and [Stop Online Piracy Act](http://en.wikipedia.org/wiki/Stop_Online_Piracy_Act) are bad. In conjunction, some of the Internet's largest sites will be protesting by shutting themselves down for the day.

On January 24, 2012 the U.S. Senate will vote on PIPA, a bill that would enable government censorship of the Internet. **This cannot be allowed to pass**.

[PROTECT-IP](http://en.wikipedia.org/wiki/PROTECT_IP_Act), and a similar house bill called the [Stop Online Piracy Act](http://en.wikipedia.org/wiki/Stop_Online_Piracy_Act), would permit federal law enforcement to block the [domain name](http://en.wikipedia.org/wiki/Domain_name) of any site deemed by the Department of Justice to be infringing on copyright. This includes not only sites that are directly providing pirated material such as [The Pirate Bay](http://thepiratebay.org/), but any site that links to another site containing copyrighted material. This includes search engines and social networks. Imagine if Google was taken offline because it indexed a site with a pirated copy of a movie (which it almost certainly has since Google indexes the entire internet, good or bad) and returned that site in a search result. Imagine if all of Facebook was taken down because one person of the over 800 million members posted a link to a site where you could download Lady Gaga's newest album for free (that kind of thing happens all the time). Under these laws, both of those scenarios would be possible and legal. Furthermore, the laws circumvent due process. In order for a site to be blocked, the DoJ simply has to find that it contains or links to piracy, and that's it. There's no trial or hearing, and minimal investigation. All these bills would accomplish is breaking the Domain Name System and opening up security holes in the Internet, penalizing law-abiding citizens in the process - all while doing next to nothing to actually stop piracy.

Numerous companies and legislators have voiced their opposition to these bills including [Google](http://www.mattcutts.com/blog/internet-censorship-sopa/), [Reddit](http://blog.reddit.com/2012/01/stopped-they-must-be-on-this-all.html), [Facebook, Twitter, Wikipedia](http://news.cnet.com/8301-31921_3-57342914-281/silicon-valley-execs-blast-sopa-in-open-letter/) and [countless others](http://en.wikipedia.org/wiki/Stop_Online_Piracy_Act#Opposition). As part of the effort to raise awareness and protest SOPA/PIPA, [Reddit](http://blog.reddit.com/2012/01/stopped-they-must-be-on-this-all.html) and a [growing list of other prominent](http://www.webpronews.com/more-sites-join-upcoming-anti-sopa-blackout-2012-01) sites will be blacking out their sites on January 18 8am-8pm EST. [Wikipedia is even considering joining the party](http://www.forbes.com/sites/insertcoin/2012/01/13/wikipedia-mulls-sopa-blackout-as-other-sites-join-in/). As some of the highest traffic sites on the Internet this action would be seen by tens of millions of people.

**I'll be taking part in the blackout** as well, by shutting down my site that day and displaying an anti-SOPA/PIPA message in its place. If you have a web site, I highly encourage you to do the same. There is already a [WordPress plugin](http://wordpress.org/extend/plugins/sopa-strike/) to do this very easily. The more people actively show their opposition, the better.

Efforts taken against these bills have already have an effect. Constituent emails, calls and letters have forced Congress and the Senate to revise the bills, which is a start. We need to do everything we can to kill these bills for good.

Don't let the lazy, greedy entertainment industry executives hamstring the entire Internet for the sake of their dying business models. Contact your congressman or senator in opposition of SOPA and PIPA.

# Do Your Part

[**Stop American Censorship**](http://americancensorship.org/)<br>
[**Electronic Frontier Foundation**](https://www.eff.org/)
