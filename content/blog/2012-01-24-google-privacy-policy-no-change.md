---
title: "Google's Privacy Policy Changes: Not Much Has Changed"
date: 2012-01-24T22:55:55.000Z
slug: google-privacy-policy-no-change
image:
  src: /files/blog/google-privacy-policy-no-change/01.png
  alt: Google privacy policy

tags:
  - google
  - privacy
---

Google [announced today](http://googleblog.blogspot.com/2012/01/updating-our-privacy-policies-and-terms.html) that they're making changes to their privacy policy to make it simpler, and unify all the disparate policies for their various products under one canonical document. One of the things that surfaced out of the trimming down of language was that users are unable to opt out of Google using certain data in your profile to customize its other services to your interests. While it may sound like a Big Brother shift in Google's mindset, [the reality is far more mundane](http://thenextweb.com/google/2012/01/24/google-is-changing-its-privacy-policies-with-one-document-to-rule-them-all/).

First off, users have never been able to opt out of this type of tailoring. Google has primarily used data about your searches and the contents of messages in your Gmail account to target relevant ads. It's not done by a group of spooky Google employees in a dark room taking notes about what kind of ice cream you like - it's done by a software algorithm looking for keywords. It's always been done that way, and if you have a Google account or use Google for searching, you've already agreed to this. Google makes money on ads, so the more interesting the ads are to users, the more money they make.

Second, this data stays inside Google's ecosystem. This goes back to the algorithm: advertisers buy ads on Google by specifying keywords for which their ads are to be displayed. Beyond this, the software does the rest. Your data is never displayed to advertisers because it doesn't have to be.

I disagree with a lot of the comments I've seen about this move being "evil" when there really hasn't been much of a "move" to begin with. The only thing that's really happened is Google has stripped away a lot of the confusing language that has obscured these policies and kept most users ignorant, which to me seems like the opposite of evil. Whether you believe the underlying practice - shared by many such companies - of using your data to customize the experience to your tastes is evil (including, albeit, the ads being displayed to you) is your prerogative. However, it's naive to think that any Internet service these days doesn't use your data (which, lest we forget, you voluntarily gave to them) one way or another to make changes to the service or aid in their business model - especially if the service is offered for free. You're not forced to use any of these services, and if you choose to make use of their products and provide them with your data then you accept how they use it. If you don't like it, delete your account and use something else.

Google hasn't changed the way they do business, and they haven't started giving away your data to others. They're simply clarifying what actually happens when your data lands on their servers. That's not evil, and that doesn't change anything.

{{< youtube KGghlPmebCY >}}

Feel free to tell me I'm wrong in the comments - I look forward to a debate.

**Update (January 25, 2012):** Google now has a page where you can [view what information Google has gathered for use with customizing ads](http://google.com/ads/preferences). You can see what Google thinks are relevant categories and demographics for you based on search habits. You can also **opt out of customized ads**. This is available for ads displayed with search results, in Gmail, and across the web.