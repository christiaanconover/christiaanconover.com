---
title: "GPS-Based Insurance Pricing: Why It's Misguided and Why Speed (By Itself) Isn't Relevant"
date: 2012-02-10T16:45:40.000Z
slug: no-gps-insurance
image:
  src: /files/blog/no-gps-insurance/insurance-gps.png
  alt: Insurance GPS

tags:
  - auto insurance
  - gadgets
  - gps
---

Marketplace Tech [reported today](http://www.marketplace.org/topics/tech/gps-unit-meet-car-insurance) that a UK insurance company called Motaquote is rolling out the ability to record your driving habits using a TomTom GPS unit, and use that data to set your car insurance rate. The idea is that the policy and pricing can be tailored to better reflect your specific driving, rather than base it off of actuarial tables. If you're a "safe" driver you could be spending less, and if you're an "unsafe" driver you could be spending more. On face value it seems pretty reasonable. I disagree.

For many, the alarm bells will likely start going off around the privacy concerns - that by using GPS data your insurance company will then have a complete log of every single place you drive, when you drive there, how often, etc. which seems "creepy." That's not the part that bugs me, especially since anyone with a cell phone is already providing all this data to their cell carrier whether they like it or not, who in turn make it trivial for law enforcement to access that data, sometimes [without even requiring a warrant](http://arstechnica.com/telecom/news/2009/12/sprint-fed-customer-gps-data-to-leos-over-8-million-times.ars?utm_source=rss&utm_medium=rss&utm_campaign=rss). The data's already out there in less regulated hands than insurance companies, so privacy isn't the problem.

The problem lies in algorithmically determining what makes a "safe" driver. A large portion of this data will no doubt be driven by speed, since that's an inherent feature of GPS. **Speed doesn't cause accidents, stupidity causes accidents**. A driver doing 80 MPH while maintaining a safe distance from other vehicles and paying attention to surroundings so he or she can make decisions rather than reactions is a safer driver than the one doing 55 MPH talking on the phone and tailgating. Anyone who's been in both situations can attest to that. However, a GPS doesn't know whether you're too close to the car in front of you or talking on your phone. It simply knows the speed at which you're traveling. That information by itself is nearly useless in determining actual safety.

Let's not just focus on what's going on inside the vehicle - what about traffic conditions? Where I live there are highways that, at certain times of the day, if you're not doing at least 70 you're likely to cause an accident. How will the computer know that? It won't. It can't see that you're contributing to others' road rage by going too slow, or nearly having collisions by going too fast in high traffic, or recklessly changing lanes. None of these other factors are known. If I'm a fast driver with no accidents, or at the very least no accidents that were my fault or can be attributed to vehicle speed, am I a better driver than somebody who obeys all speed limits but has had multiple at-fault accidents? I would say yes, and I'd hope you agree.

**"Christiaan, it's not that simple. There are always mitigating circumstances."** THAT'S THE POINT. Driving is much more complicated than how fast you're going. If insurance companies wanted to have behavior-based discounts, they ought to reward drivers for taking advanced driving courses that teach them to be better, safer drivers. Encouraging people to be better drivers through education and practice will have a greater long-term effect on safe driving than providing everyone with a digital nanny telling them to "SLOW DOWN!" from the windshield.

The Marketplace report says that the GPS system offered in the UK can track things like traffic conditions, braking, and cornering technique. I might buy into braking, since I suppose it can calculate the rate at which you decelerate and make a judgement based on that. The same goes for cornering. Traffic conditions, however, I call BS. I've never encountered a single automated traffic reporting system that could report with anything more than about 80% accuracy what actual conditions were. Furthermore, it can't tell you what reckless behavior other drivers around you are doing that could result in a collision, and that your avoidance maneuvers may appear to the sensors as unsafe (rapid braking, swerving to avoid someone or something).

Progressive's Snapshot device gathers [many of the same factors](http://www.progressive.com/auto/snapshot-common-questions.aspx) as the UK system such as braking, mileage and time of day when driving. The key to Progressive's system is that your rates won't _increase_ beyond your original quote, they'll only decrease if your driving habits warrant it. That way you know you'll never pay more than a certain amount, but could be paying less. It's also a voluntary program, and you can opt out any time, which is crucial. I'd probably be willing to do it if it's structured like that.

I'm not categorically opposed to gathering data about driving habits to help in setting insurance rates more accurately. I simply think we should be designing better systems to gather the data that actually matters, and set rules & standards for how that data is used and what insurance companies are permitted to do with it in deciding rates. Most importantly: it should be optional, with no penalty for saying no.
