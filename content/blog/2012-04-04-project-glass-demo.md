---
title: "Google's 'Project Glass' Augmented Reality: Yes Please"
date: 2012-04-04T19:48:01.000Z
slug: project-glass-demo
image:
  src: /files/blog/project-glass-demo/project-glass.jpg
  alt: Project Glass

tags:
  - augmented reality
  - gadgets
  - google
  - google glass
---

Google has just released the first video showing off what their long-rumored wearable augmented reality tech might look like. Simply put, it looks amazing. The idea that news, weather, communications, geographic-specific overlays, etc. can all appear seamlessly in front of your eyes as you go about your business is just about the ultimate in personal computing.

I will admit, it also seems super nerdy. However, so did the portable computer, the pager, the PDA, the cell phone, and numerous other technologies when they first came along. Okay, the pager was always nerdy. Regardless, all these things became common and even expected over a short period of time.

I really can't wait for this to become available to consumers, and will get my hands on this as soon as I can.

Google has created a [Google+ page about Project Glass](http://g.co/projectglass) where they provide additional details, and will likely post updates about the project in the future.

{{< youtube 9c6W4CCU9M4 >}}