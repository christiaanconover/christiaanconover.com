---
title: 'Zagato Coupe: I Must Have This Car'
date: 2012-05-27T01:45:02.000Z
slug: bmw-zagato-coupe
image:
  src: /files/blog/bmw-zagato-coupe/bmw-zagato-coupe.jpg
  alt: BMW Zagato Coupe

tags:
  - bmw
  - bmw z4m coupe
  - bmw zagato coupe
---

It's [no secret that I'm a big fan of the Z4 coupe](/blog/celebrate-your-car-day-calling-all-z4-drivers/), specifically the [Z4M Coupe](http://en.wikipedia.org/wiki/BMW_M_Coupe#Second_generation_E86_.282006.E2.80.932008.29). It's one of my all-time favorite cars, both in performance and - perhaps most notably - styling. So when BMW unveiled the [Zagato Coupe](http://www.autoblog.com/2012/05/25/bmw-reveals-stunning-zagato-coupe-at-villa-deste/) yesterday at [Concorso d'Eleganza Villa d'Este](http://en.wikipedia.org/wiki/Concorso_d'Eleganza_Villa_d'Este), it was love at first sight. They've managed to take all of my favorite design elements of the Z4M Coupe, combine them with some modern BMW design language, and add a little retro flare to create one of the most stunning sports coupes I've ever seen. From every single angle this car is like Sofia Vergara on wheels. Seductive, spicy, aggressive yet inviting. Drop-dead gorgeous.

Autoblog has a [great photo gallery](http://www.autoblog.com/photos/bmw-zagato-coupe-0/full/) of the Zagato Coupe, which I encourage you to check out. I have a feeling you'll be just as sucked in as I am.
