---
title: This is How You Cook Chickens
date: 2012-05-27T16:01:10.000Z
slug: this-is-how-you-cook-chickens

tags:
  - memorial day
---

<video width="1280" height="720" controls="">
  <source src="/files/blog/this-is-how-you-cook-chickens/cooking-chickens.mp4" type="video/mp4">
</video>

My friend has a big Memorial Day party every year. This is a recent addition.
