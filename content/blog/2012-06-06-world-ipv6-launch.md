---
title: Today is World IPv6 Launch Day
date: 2012-06-06T14:33:15.000Z
slug: world-ipv6-launch
image:
  src: /files/blog/world-ipv6-launch/world-ipv6-day.png
  alt: World IPv6 Day

tags:
  - ipv6
---

Today marks a major milestone in the evolution of the Internet - one that most people probably won't even notice, though it has an impact on every single device on the net. Today is the [worldwide launch of version 6 of the Internet Protocol](http://www.worldipv6launch.org/), or [IPv6](http://en.wikipedia.org/wiki/IPv6).

Every device on the Internet needs an Internet Protocol, or IP, address in order to communicate. Think of it like a street address or a phone number for your computer or mobile device. Your IP address is what tells the rest of the Internet where to send things when you ask for them in your browser. Without an IP address, you wouldn't be able to get online at all. Furthermore, every IP address has to be unique or else there would be mix-ups and nothing would ever get to where it's supposed to go.

Our current structure, [IPv4](http://en.wikipedia.org/wiki/IPv4), has space for about 4.3 billion addresses, which sounds like a lot until you consider that there are 11 billion devices on the Internet today. We've completely exhausted the available addresses, and without being able to add more the Internet won't be able to grow.

Enter IPv6\. It has a very different structure for addresses, which results in a total of **340 trillion trillion trillion** possible addresses. That's enough for each of the 7 billion people on this planet to have 48,571,428,600,000,008,096,784,712,080 addresses all to themselves.

Unfortunately, IPv4 and IPv6 aren't compatible with each other. In order to access IPv4 from IPv6 and vice versa, a converter is needed somewhere along the line, which creates bottlenecks. The solution is to get everyone migrated to the new standard, and get rid of IPv4 altogether.

This brings us to today. A number of the largest Internet companies and Internet Service Providers around the world have all committed to turning on IPv6 on their networks and servers. Getting companies with such large web presence migrated over will do a great deal in moving the rest of the Internet forward.

Google has a good video explaining the importance of this transition:

{{< youtube -Uwjt32NvVA >}}

How does all this affect you? Well, if you're simply an Internet user, you shouldn't notice much of anything, except perhaps a little bit slower of a connection to sites on IPv6 if you're stuck on IPv4 and having to go through a converter.

If you run a web site, you have a responsibility to support IPv6 as soon as possible. The easiest way is to use [CloudFlare](https://www.cloudflare.com) to handle your DNS, since [they provide IPv6 support automatically](https://www.cloudflare.com/ipv6-challenge). Otherwise, you can contact your hosting provider about their plans for IPv6.

I'll admit that this is pretty seriously down in the weeds as far as technology goes, but it affects everyone who uses the Internet, and is important to the future prosperity and expansion of the online world.

**Update**: Vint Cerf did a Hangout with key people in getting IPv6 launched on major networks. If you're interested, check out the video.

{{< youtube lMcf6LxMgYI >}}