---
title: Thomas Point Light in the Morning
date: 2012-06-20T12:32:01.000Z
slug: thomas-point-light-in-the-morning
image:
  src: /files/blog/thomas-point-light-in-the-morning/tpl.jpg
  alt: Thomas Point Light

tags:
  - annapolis
  - chesapeake bay
  - thomas point light
---

Good morning Annapolis.
