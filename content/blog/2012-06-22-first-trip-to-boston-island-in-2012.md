---
title: First Trip to Boston Island in 2012
date: 2012-06-22T22:20:32.000Z
slug: first-trip-to-boston-island-in-2012
image:
  src: /files/blog/first-trip-to-boston-island-in-2012/house.jpg
  alt: Our house on Boston Island

tags:
  - boston island
  - maine
---

This weekend I'll make my first trip of the season to [Boston Island](http://boston-island.com). It will mostly consist of cutting down trees and other maintenance, but any trip to the Island is well worth it. As I usually do, I'll make an effort to post pictures and updates while I'm there. I'll also be [posting stuff via Twitter](https://twitter.com/cconover).

I'll also post some stuff on the [island's web site](http://boston-island.com), so be sure to check that out as well.

# 26 June 2012, 1038 EDT

Obviously I wasn't able to publish any updates during the weekend, so I decided to post some stuff after the fact. We did some tree clearing, general maintenance and cleanup, and also did a fair bit of just hanging out. Here are some pictures I took while I was there.

![Sheepscot River fog](/files/blog/first-trip-to-boston-island-in-2012/sheepscot-fog.jpg)

![Sunset](/files/blog/first-trip-to-boston-island-in-2012/sunset00.jpg)

![Sunset](/files/blog/first-trip-to-boston-island-in-2012/sunset01.jpg)

![Sunset](/files/blog/first-trip-to-boston-island-in-2012/sunset02.jpg)

![Ebenecook Harbor](/files/blog/first-trip-to-boston-island-in-2012/ebenecook.jpg)

![Flag pole at sunset](/files/blog/first-trip-to-boston-island-in-2012/flagpole.jpg)
