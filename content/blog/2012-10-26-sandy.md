---
title: 'Hurricane Sandy: Live Blog'
date: 2012-10-26T16:58:06.000Z
slug: sandy
image:
  src: /files/blog/sandy/radar00.gif
  alt: Storm projection for Hurricane Sandy

tags:
  - annapolis
  - hurricane
  - hurricane sandy
  - liveblog
---

The Mid Atlantic region is getting ready for this season's hurricane: [Sandy](http://en.wikipedia.org/wiki/Hurricane_Sandy). As of Friday morning (26 October, 2012) the storm is projected to make landfall on the east coast of the United States somewhere between the Chesapeake Bay and Long Island. Meteorologists are also suggesting that Sandy may combine with a nor'easter currently developing to create a much more powerful hybrid storm. Extensive damage, power outages and flooding are expected, with high winds, hurricane-force gusts and torrential rainfall that could last as much as a couple of days. Some areas could also see feet of snow. Sandy is expected to lose its tropical characteristics once it makes landfall, but its strength and power will likely remain.

Weather will start to worsen Sunday night in the Annapolis area, and the stormy weather may stay with us as late as Wednesday. I'm planning on live blogging during the storm (the weather geek in me can't help it), so keep an eye on this page for updates.

If you're in an area that will be impacted by this storm, make sure you're prepared. Stock up on non-perishable food, bottled drinking water, and batteries. Make sure you have flashlights and a battery-powered radio on hand. If your house is in an area at risk for flooding or damage that could pose a threat to your safety, contact your local Emergency Management office for shelter locations.

# Liveblog

## <time timedate="2012-10-31T14:18:00+00:00">October 31, 2012 10:18am</time>

Not much in the way of damage or anything picture-worthy in my neighborhood. Looks like it's pretty much over for us in Annapolis at this point.

## <time timedate="2012-10-30T18:20:00+00:00">October 30, 2012 2:20pm</time>

Power is still on, and appears to have never gone out. Friends & family that live in an area east of Annapolis that lost power last night report that it came back on around 1300 today, which is much faster than usual. I'm headed out in a bit to take some pictures, which I'll post here.

## <time timedate="2012-10-30T04:52:00+00:00">October 30, 2012 12:52am</time>

Amazingly the power is still on at my apartment. The radar is showing another line of strong weather headed our way though, so we may lose it in the early hours of the morning. I'm headed to bed, so we'll see where things stand when I wake up.

## <time timedate="2012-10-29T20:57:00+00:00">October 29, 2012 4:57pm</time>

Latest radar picture as of 1655 EDT.

![Sandy radar](/files/blog/sandy/radar01.png)

## <time timedate="2012-10-29T20:51:00+00:00">October 29, 2012 4:51pm</time>

Wind and rain have increased significantly. Annapolis Mayor Josh Cohen has requested that all city residents "shelter in place" for the duration of the storm.

## <time timedate="2012-10-29T13:38:00+00:00">October 29, 2012 9:38am</time>

Steady rain when I woke up this morning. Some wind but not a lot. Pictures to follow.

<video preload="metadata" controls="">
  <source src="/files/blog/sandy/sandy00.mp4" type="video/mp4">
</video>

## <time timedate="2012-10-29T03:30:00+00:00">October 28, 2012 11:30pm</time>

Rain is intensifying. Power is still on, but the wind is picking up.

## <time timedate="2012-10-29T01:39:00+00:00">October 28, 2012 9:39pm</time>

First rain of the storm started coming down a couple of hours ago. It's gone from drizzle to actual rain in the past half hour or so.

## <time timedate="2012-10-28T04:33:00+00:00">October 28, 2012 12:33am</time>

The City of Annapolis will be opening parking garages to city residents to shelter their vehicles for the duration of the storm, starting at 3pm Sunday.

## <time timedate="2012-10-26T20:34:00+00:00">October 26, 2012 4:34pm</time>

Meteorologists at the National Hurricane Center say that in some of their models they're seeing "[a storm at an intensity that we have not seen in this part of the country in the past century](http://www.npr.org/sections/thetwo-way/2012/10/26/163690889/if-sandy-becomes-frankenstorm-it-could-be-worst-in-a-century)."

## <time timedate="2012-10-26T20:14:00+00:00">October 26, 2012 4:14pm</time>

Provisions are purchased. Plenty of bottled water, non-perishable foods, and batteries. Good to go.

## <time timedate="2012-10-26T17:02:00+00:00">October 26, 2012 1:02pm</time>

[Anne Arundel County OEM](http://www.aacounty.org/departments/office-of-emergency-management/) has sent out the following message to residents:

> 10/26/2012 12:43:12 PM EST – Anne Arundel County residents living in low-lying areas should prepare for flooding associated with Hurricane Sandy. Please be prepared to evacuate if flooding occurs in your area. This storm is predicted to produce heavy rains and strong winds for an extended duration. Expect widespread power outages. You can monitor the County Cable stations for live updates beginning Monday morning at 9AM on Verizon Channel 38 and Comcast Channel 98.