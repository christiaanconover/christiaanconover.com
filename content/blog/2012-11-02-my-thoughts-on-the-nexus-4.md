---
title: My Thoughts on the Nexus 4
description: 'The Nexus 4 has a gorgeous design. There are some features (or lack thereof) that overshadow its looks, and make me hesitant.'
date: 2012-11-02T19:12:25.000Z
slug: my-thoughts-on-the-nexus-4
image:
  src: /files/blog/my-thoughts-on-the-nexus-4/nexus4.jpg
  alt: Nexus 4

tags:
  - android
  - cdma
  - galaxy nexus
  - google
  - gsm
  - hspa+
  - lte
  - nexus
  - nexus 4
---

This is a first for me on this blog: a post about a phone. I've written posts about [specific devices](/blog/cr48/) (even ones that [don't yet exist](/blog/project-glass-demo/)) before, but I haven't gone down the smartphone rabbit trail. I'm breaking that pattern.

On Monday Google announced [their latest additions](http://googleblog.blogspot.com/2012/10/nexus-best-of-google-now-in-three-sizes.html) to the [Nexus](http://en.wikipedia.org/wiki/Google_Nexus) line. I already have a Nexus 7, so their update to that product wasn't very interesting to me. Nor was the Nexus 10, despite its impressive specs, since I'm just not looking for a tablet that big (part of the reason I've not bothered with an iPad). The [Nexus 4](http://www.google.com/nexus/4/) was a different story. My current phone is a [Galaxy Nexus](http://en.wikipedia.org/wiki/Galaxy_Nexus), the predecessor to the Nexus 4, and I love it. It's fast, has LTE, and runs the latest version of Android, with no messy add-ons from the manufacturer or carrier. It's a great phone, and it has sold me on sticking with the Nexus line of devices. When I saw the Nexus 4, I immediately had gadget lust. It looks great, it's has an even newer release of Android, it's got wireless charging (something I've wanted for a while), has a nice camera, [and on and on](https://play.google.com/store/devices/details?id=nexus_4_8gb&feature=microsite#hardware-tech-specs).

As I dug deeper though, some problems arose. The most glaring issue is the lack of LTE. I've been using LTE for nearly two years, and there's no way I'm going back to a non-LTE device. The speeds are just too darn fast to give up. Where I live Verizon LTE is available everywhere, so I have no incentive to revert. I was supremely bummed that the Nexus 4 isn't going to include it. To be fair, it also appears that they're making no plans to offer a CDMA version (the technology that Verizon's network uses) so unless I switched to AT&T or T-Mobile I wouldn't be able to use it, but it's still a disappointment.

They also decided to make the entire back of the phone from a piece of glass, to create a neat holographic effect with the design. It's a really cool idea, but I would have thought after all the [issues with the iPhone 4S back glass](https://www.google.com/search?q=iphone+4s+broken+glass) that Google would have shied away from that idea. I'm certainly reluctant to own a phone with glass on the back (even if it is Gorilla Glass as used in the Nexus 4), even more so when review units have been [cracked by accidental knocks off tables](http://www.theverge.com/2012/11/2/3589280/google-nexus-4-review). It pretty much guarantees that the phone will have to be kept in a case, which I'd guess might interfere with the totally awesome wireless charging. Grrrr.

What I've realized is that almost all the features of the Nexus 4 that I really like, such as the new Camera app, are software features. Since my Galaxy Nexus is still a very capable and competitive device, and a Nexus, I'd bet I'll be seeing the update to Android 4.2 headed to my phone in the near future, which will bring much of the cool stuff I want from the Nexus 4.

If I was on AT&T or T-Mobile those issues may be muted for me since I'd actually be able to use the phone, and at [$300 for a fully unlocked device](https://play.google.com/store/devices/details?id=nexus_4_8gb&feature=microsite) I could pretty easily convince myself that it's worth owning. I still really want the Nexus 4, and [like others have written](http://www.zdnet.com/google-nexus-4-why-im-saying-goodbye-to-verizon-7000006636/), it's almost enough to make me think of jumping to a GSM carrier. For now though, I think I'm perfectly happy sticking with my Galaxy Nexus with the next release of Android, and seeing what the successor to the Nexus 4 has to offer.

Still...I want that phone.
