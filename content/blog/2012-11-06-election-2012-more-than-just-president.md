---
title: Voting Today is For More Than Just the President
date: 2012-11-06T15:46:17.000Z
slug: election-2012-more-than-just-president

tags:
  - election
  - election 2012
---

I doubt anyone thinks that this is a throwaway election with nothing at stake. No matter where you fall on the political spectrum, the stakes are high and the arguments heated. Yet as with every election, there's a segment of the electorate who isn't going to vote, for a variety of reasons. What seems - especially recently - to be the most cited reason is that individual votes don't matter (unless you're in a swing state, perhaps). That may be the case, but there's so much more to voting than that.

The fallacy with that argument lies in the notion that the only question on the ballot that matters is for President of the United States. Yet every state has their own questions covering additional elected positions, bills put up for referendum, state/county/city/town constitutional amendments, and so on. Regardless of whether your vote for President has any real impact is immaterial. No matter which state you're in there are questions for which your vote absolutely matters, and are likely to affect your life in some way or another.

I live in Maryland, which this year has a number of controversial and impactful ballot questions. This year we're deciding most notably, and among other issues: [whether to offer reduced tuition rates for public higher education to children of illegal immigrants](http://www.elections.state.md.us/elections/2012/ballot_question_language.html#state4), [the right for homosexual couples to legally marry](http://www.elections.state.md.us/elections/2012/ballot_question_language.html#state6) and be granted the legal rights of married people, and [increases in gambling, to include the legalization of table games](http://www.elections.state.md.us/elections/2012/ballot_question_language.html#state7). These are significant, contentious issues that will affect the lives and livelihoods of a large percentage of the population, with second and third order effects. Every vote does in fact count for those questions, so the aforementioned argument holds no water here.

As American citizens we have the right to vote, which is an incredible and under appreciated reality. The flip side is that we also have the right not to vote. However, as citizens of a free and civil society we have the responsibility to take action in moving our nation in a direction we believe will benefit the People. That responsibility encompasses all questions, not just President.
