---
title: "Let's Encourage Voter Turnout with a Tax Incentive"
date: 2012-11-14T15:51:21.000Z
slug: voting-tax-incentive

tags:
  - election
  - election 2012
  - voting
---

Last week I wrote a post about [my issue with the notion that there's no point in voting because votes don't count](/blog/election-2012-more-than-just-president/). The right to vote as a citizen of the United States is one of the most incredible aspects of our system of government. Not only do we get to have a voice in our governance, but the collective decisions made by the People with said voice is acted upon, without prejudice, by our elected officials. The civility and power of this process cannot be overstated.

Yet there are people who don't bother to cast their vote, for any number of reasons - not enough time, votes don't matter, nothing will change no matter who they vote for, and the list goes on. Regardless of how you feel, the fact is your vote does matter. I'm of the opinion that not only is it the right of the People to vote, it is also their responsibility. So how to encourage people to take the few minutes to have a say in how their government runs? How do we get them to see the value of their vote at the individual level?

**Offer a tax incentive for voting**. If you voted this year, take a deduction from your income tax. Didn't vote? No problem, you just won't get the tax break. In an era when taxes, economic issues and voter suppression are all prominent in the political discourse, a voting tax incentive would give people on both sides of the aisle brownie points with voters. It doesn't make voting mandatory (as it is in some countries), and it doesn't create a penalty for not participating. It's the carrot instead of the stick.

Not all of the electorate sees the value of voting, financially or otherwise. If they can save some money simply by showing up and pushing a few buttons, I'm betting we'll see a noticeably higher turnout.
