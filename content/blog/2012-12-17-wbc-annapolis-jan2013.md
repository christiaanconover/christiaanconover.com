---
title: 'Westboro Bapist Church Coming to Annapolis, Counter-Protest Organized'
date: 2012-12-17T15:26:23.000Z
slug: wbc-annapolis-jan2013

tags:
  - counter protest
  - same-sex marriage
  - westboro baptist church
---

The Westboro Baptist Church has announced that [they will be protesting in Annapolis](http://annapolis.patch.com/articles/westboro-baptist-church-to-picket-in-annapolis-jan-2) on January 2, 2013 from 8-9am:

![The Westboro Baptist Church's entry on their schedule for Annapolis on January 2, 2013](/files/blog/wbc-annapolis-jan2013/wbc-press-release.png)<br>
The Westboro Baptist Church's entry on their schedule for Annapolis on January 2, 2013. I have provided a screenshot instead of a link to their site as I've been informed that they get money from donors for every hit their site receives.

Their protest is in response to the homosexual couples who will be at the court house to get married. January 2 is the first day this will be legal following [the referendum during this year's election](http://www.nytimes.com/2012/11/07/us/politics/same-sex-marriage-voting-election.html?_r=0).

In April of last year local residents [staged a counter-protest](/blog/wbc-counterprotest-meade/) to drown out WBC's hateful message and show support for those being targeted. Let's organize another counter-protest for January 2.

[**RSVP on Facebook if you're planning to attend »**](https://www.facebook.com/events/134205716736270/)
