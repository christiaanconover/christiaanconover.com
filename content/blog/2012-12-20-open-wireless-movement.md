---
title: 'Open Wireless Movement: Why You Should Join'
date: 2012-12-20T18:10:09.000Z
slug: open-wireless-movement

tags:
  - cyber security
  - eff
  - open wireless movement
  - openwireless.org
---

Let's face it: these days it's pretty tough to get things done without the Internet. Most people have at least one computer, and an increasing number of people have smartphones and tablets. Most of these devices become pretty useless without Internet connectivity, yet despite the massive proliferation of Wifi and 3G/4G, it can be difficult to find wireless access outside of your usual stomping grounds - especially if you don't have a data plan with a cell carrier.

Wouldn't it be great if there was widespread, freely available Wifi that you could jump on whenever you wanted or needed it? No per-location costs, no subscription to a Wifi service, no hoops to jump through. Just connect and go. Better yet, hop from one Wifi hotspot to the next seamlessly, with no extra effort required.

**Welcome to the [Open Wireless Movement](https://openwireless.org/)**. The [Electronic Frontier Foundation](https://www.eff.org/), along with a number of other organizations, have created a framework for individuals, businesses, ISPs - pretty much anyone who wants to participate - to be able to provide open Wifi that uses a recognizable name, already has [Terms of Service](https://openwireless.org/important-information) drafted and vetted by lawyers, and allows for some individual modifications or restrictions the participant may want to include.

The idea is pretty simple: set up an open, unprotected wireless network with the name "openwireless.org" and allow people to get online without any hassle. By using the name openwireless.org for the network, you then fall under the terms and protection of Open Wireless Movement, and users will know exactly where to go for information about the network they just joined. Oh, and once a device has connected to and remembered a network with the name "openwireless.org" it'll automatically connect to any network with that same name in the future.

Why should you bother giving the Internet you've paid for away for free? For starters, most people don't use anywhere close to their connection's full capacity. Most of the time (even when you're web browsing) your Internet connection is essentially sitting idle. You're paying for something that much of the time isn't doing anything. Why not get your money's worth, and help out fellow citizens in the process? If it catches on, maybe you'll be helped out by using somebody else's openwireless.org connection one day. Think of it like digital karma, or the Golden Rule.

Not sold on the cyber "peace, love and granola" idea? Here's another reason it's good for you: security. You may be asking, "hold the phone - how is having an unprotected network good for security?" and I'll agree, on face value it doesn't make any sense. The answer is that it's not your only network. Chances are good that right now your home Wifi has a password to access it, which is supposed to keep out unwanted users and keep your connection secure. It does one of these things really well, and it isn't the part about keeping others out. A motivated intruder with just a bit of off-the-shelf software will get into your network if he or she wants to, regardless of your password. Once they do, they're inside your private network and have free reign.

Now let's present a new scenario: you still have your password-protected network that you and your family members use, but you also have an unprotected openwireless.org network available to people just looking for a few free bits to check email. Rather than break into your private network, they'll take the path of least resistance and just use the open network. Your data is still private, and they got what they were looking for. Everybody wins. The best part: many modern Wifi routers have [support for a guest wireless network](https://openwireless.org/routers), and keeps the private & public traffic completely separate. So yes, it is in fact good for security.

I encourage you to check out the Open Wireless Movement and consider setting up a network. Even if you don't take the plunge, keep an eye out for openwireless.org networks - at least now you know what they're all about.
