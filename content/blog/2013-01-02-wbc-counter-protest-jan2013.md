---
title: 'Counter-Protest of Westboro Baptist Church in Annapolis [Photos & Video]'
date: 2013-01-02T16:55:22.000Z
slug: wbc-counter-protest-jan2013
image:
  src: /files/blog/wbc-counter-protest-jan2013/00.jpg
  alt: Counter-protesters

tags:
  - annapolis
  - anne arundel county
  - counter protest
  - same-sex marriage
  - westboro baptist church
---

This morning the Westboro Baptist Church [staged a protest outside the Anne Arundel County Courthouse in Annapolis, MD](/blog/wbc-annapolis-jan2013/). They came because today was the first day same sex marriages were officially legal, and could be conducted at the courthouse. Citizens from all over the area came out to counter-protest, and show support for the couples exercising their newly granted right.

![Members of Westboro Baptist Church demonstrating outside Anne Arundel County Courthouse in Annapolis, MD.](/files/blog/wbc-counter-protest-jan2013/01.jpg)<br>
Members of Westboro Baptist Church demonstrating outside Anne Arundel County Courthouse in Annapolis, MD.

The counter protest was [organized on Facebook](https://www.facebook.com/events/134205716736270/), and at the peak I would estimate there were 250-300 people in attendance. Signs, music & singing, outfits and chants were all part of the atmosphere of a community coming together around their own in defiance of a message of hatred and intolerance. The crowd was as diverse as it was united.

![A portion of the counter-protest gathering shortly before 8am. The crowd roughly doubled at the peak.](/files/blog/wbc-counter-protest-jan2013/02.jpg)<br>
A portion of the counter-protest gathering shortly before 8am. The crowd roughly doubled at the peak.

![A counter protester holds a sign parodying the message of the Westboro Baptist Church.](/files/blog/wbc-counter-protest-jan2013/03.jpg)<br>
A counter protester holds a sign parodying the message of the Westboro Baptist Church.

The counter protest was on the sidewalk outside St. Anne's Parish, who organized their own demonstration by singing hymns accompanied by amplified piano music.

![People gathered outside St. Anne's Parish in Annapolis. St. Anne's set up an amplified piano outside its doors, and members of the church & public sang hymns as part of the counter protest.](/files/blog/wbc-counter-protest-jan2013/04.jpg)<br>
People gathered outside St. Anne's Parish in Annapolis. St. Anne's set up an amplified piano outside its doors, and members of the church & public sang hymns as part of the counter protest.

The outfits ran the gamut, from almost nothing on:

![A counter protester naked except for his underwear holds a furry, rainbow-colored pole in front of his groin.](/files/blog/wbc-counter-protest-jan2013/05.jpg)<br>
A counter protester naked except for his underwear holds a furry, rainbow-colored pole in front of his groin.

To over the top:

![Dale Crites posed and danced near the WBC protesters, drawing lots of cheers from the counter-protest crowd.](/files/blog/wbc-counter-protest-jan2013/06.jpg)<br>
Dale Crites posed and danced near the WBC protesters, drawing lots of cheers from the counter-protest crowd.

The crowd was excited and friendly, and we peacefully shared our message of support. I'm proud that my fellow citizens came together to push back against bigotry.

<video preload="metadata" controls="">
  <source src="/files/blog/wbc-counter-protest-jan2013/counterprotest.mp4" type="video/mp4">
</video>

To see additional coverage from local media outlets, check out the posts on [Eye on Annapolis](http://www.eyeonannapolis.net/2013/01/02/westboro-baptist-church-comes-to-annapolis/) and [Annapolis Patch](http://annapolis.patch.com/articles/protests-mark-start-of-same-sex-marriage-in-annapolis).

_You are welcome to share & modify any of the imagery on this page, in accordance with my [content license](/license/) (it's pretty liberal)._
