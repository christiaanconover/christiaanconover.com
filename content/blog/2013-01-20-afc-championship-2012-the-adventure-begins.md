---
title: 'AFC Championship 2012: The Adventure Begins'
date: 2013-01-20T16:44:44.000Z
slug: afc-championship-2012-the-adventure-begins

tags:
  - 2012 afc championship
  - afc
  - afc championship
  - baltimore ravens
  - football
  - new england patriots
  - nfl
---

I'm in Providence getting ready to head up to Gillette Stadium for the Patriots-Ravens showdown. By a stroke of luck I managed to get a ticket Friday and drove up with some friends Saturday afternoon. The trip is off to a good start already, including some night club adventures in [Federal Hill](https://en.wikipedia.org/wiki/Federal_Hill,_Providence,_Rhode_Island) and the craziest cab ride I've ever taken. As my friend [Michael pointed out](http://www.michaelspinosa.com/2013/01/20/day-1-january-19th-2013-calm-afc-storm/), Providence definitely has the worst drivers in America. I have to agree with him as well that it's very satisfying to watch a Ravens fan in New England experience what Pats fans deal with in Baltimore.

This is the first NFL game I've ever been to - might as well start big! I'll post some pictures from Foxborough later in the day.
