---
title: View from the stands
date: 2013-01-20T22:45:06.000Z
slug: view-from-the-stands
image:
  src: /files/blog/view-from-the-stands/view.jpg
  alt: View from the stands at Gillette Stadium

tags:
  - 2012 afc championship
  - afc championship
  - football
  - new england patriots
  - nfl
---

Mid field. Let's go!
