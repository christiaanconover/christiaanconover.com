---
title: President Obama Comes to Annapolis
date: 2013-02-06T19:45:45.000Z
slug: potus-annapolis-06feb13
image:
  src: /files/blog/potus-annapolis-06feb13/arrive00.jpg
  alt: President Obama steps off Marine One at the Naval Academy
  caption: President Obama steps off Marine One at the Naval Academy

tags:
  - annapolis
  - barack obama
  - potus
  - us naval academy
  - us naval institute
---

President Obama came to Annapolis today to meet with Democratic senators. He came on Marine One and landed at the Naval Academy, right outside my office. One of my coworkers had a DSLR with them and was able to get some good pictures of the President's arrival.

![Marine One lands at the Naval Academy](/files/blog/potus-annapolis-06feb13/arrive01.jpg)<br>
Marine One lands at the Naval Academy

![President Obama walks to his limousine from Marine One](/files/blog/potus-annapolis-06feb13/arrive02.jpg)<br>
President Obama walks to his limousine from Marine One

After he had finished his business, he came back to the Naval Academy, boarded Marine One, and headed back to Washington. We went across the Severn River to get some pictures & video of his departure.

![Marine One takes off from the Naval Academy](/files/blog/potus-annapolis-06feb13/depart00.jpg)<br>
Marine One takes off from the Naval Academy

<video preload="metadata" controls="">
  <source src="/files/blog/potus-annapolis-06feb13/potus.mp4" type="video/mp4">
</video>

[**Full gallery »**](http://www.flickr.com/photos/usnavalinstitute/sets/72157632701037817/)

*Photos courtesy of [U.S. Naval Institute](http://www.usni.org) [[Flickr set](http://www.flickr.com/photos/usnavalinstitute/sets/72157632701037817/)]*