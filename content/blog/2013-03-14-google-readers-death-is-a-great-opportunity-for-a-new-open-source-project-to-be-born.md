---
title: "Google Reader's Death is a Great Opportunity for a New Open Source Project to be Born"
date: 2013-03-14T00:46:03.000Z
slug: google-readers-death-is-a-great-opportunity-for-a-new-open-source-project-to-be-born
image:
  src: /files/blog/google-readers-death-is-a-great-opportunity-for-a-new-open-source-project-to-be-born/google-reader-dead.png
  alt: Google Reader shutdown message

tags:
  - google
  - google reader
  - open source
  - rss
---

It's official: Google Reader is dying. Citing declining usage, Google announced this afternoon that their RSS aggregator tool [will be shut down on July 1st](http://googleblog.blogspot.com/2013/03/a-second-spring-of-cleaning.html). It's had a good run since its launch in 2005, but it's time to shutter the proverbial doors.

Google also says Reader has a loyal following, which makes sense: it's the best web-based RSS reader available, and everyone I know who uses RSS to keep up-to-date uses it. Its demise leaves a hole in the market - not an exciting, sexy segment of the market, but crucial for those who rely upon it.

This seems like the perfect opportunity for an open source project to step in. No startup will take this on, because in the age of Twitter, Facebook, Flipboard, etc. the business case isn't strong enough. No existing company will build this product for the same reason. This is a problem that can only be solved by the open source community.

Here are the key features this new project should have, from my perspective:

- **Web-based**: There are plenty of existing local apps for RSS aggregation, but those don't solve the main problem Google Reader solved - accessibility everywhere. I have two computers, a smartphone, a tablet, and a work computer. Only a web app can provide access to my feeds easily across all those devices. Ideally this web app will be designed for self-hosting, with a hosted option available down the line (a la WordPress).
- **Mobile-Friendly**: It's fine for this app to be browser-only. That browser experience needs to be great on all devices, from phone to desktop. Perhaps mobile apps will be developed later, or the community can develop their own apps to hook into it, which brings me to my next point.
- **Full-Featured API**: RSS can be used for a lot of things beyond simply reading blog updates. Even if all you want to do is read those updates, the app grabbing all those feeds should be accessible in a variety of ways. Being that it's an open source project, that seems especially likely. A strong API is critical to the growth of the project into other spaces (something Google Reader didn't even accomplish to the level it could have). I can easily see an opportunity to create one (or two) click subscription buttons on web sites, neat mobile apps made by the community, accessing feed analytics using tools like [ThinkUp](http://thinkup.com), and the list goes on.
- **Import from Google Reader**: Since most people who will likely use this software will be coming from Reader, making the transition as seamless as possible will go a long way towards adding users.

I haven't started this project yet because I don't have time to dedicate to this, but I'd be happy to contribute if somebody else takes this on. If there's an existing project that's doing this but has heretofore gone unnoticed in the massive shadow of Reader, I'd love to know about it and help out. One thing that's become clear since Google's announcement is how dedicated Reader's users are, and how strong a demand there is within that niche.
