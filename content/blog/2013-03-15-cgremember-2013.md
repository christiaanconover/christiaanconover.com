---
title: 'CG Remember: Remembering our Fallen Coast Guard Shipmates and their Families'
date: 2013-03-15T20:45:44.000Z
slug: cgremember-2013
image:
  src: /files/blog/cgremember-2013/cgremember.jpg
  alt: CG Remember logo

tags:
  - cg remember
  - lucy love
  - military
  - run2remember
  - ryan erickson
  - scholarship
  - sean lawler
  - us coast guard
---

It's been a while since I've posted anything Coast Guard on here, but friend and fellow Coastie [Ryan Erickson](http://www.ryanerickson.com/) sent me a piece about [CG Remember](http://cgremember.com/), a program dedicated to remembering those Coast Guardsmen and women who have fallen in service to their country, which I felt should be shared with my readers. I won't go into any further detail about it since Ryan covers that in his words below.

*Note: I have a policy against guest posts on this site and I don't consider this to be one. I feel this is an important cause deserving of as much attention as possible about which I would have written, but Ryan is able to write about it with more authority than I could have.*

--------------------------------------------------------------------------------

I still remember the first time ME1 (formerly MK1) Sean Lawler called me up to tell me about this guy who decided he was going to run the Keys 100 (in Key West, FL) in remembrance of Coast Guard members who'd fallen in the line of duty. To put it bluntly I thought it was a crazy idea- ambitious- but crazy. However, as I started to work with ME1 on getting the word out, the more I realized that I actually knew little about those who've died in the line of duty. Short of Douglas Munro, and the smattering of Shipmates lost during the 2011 timeframe, I was ill equipped to know who they were.

LT Brian Bruns, that ambitious individual, had a goal of not only bringing awareness to those who've fallen in the line of duty but was also looking for a way to bring awareness to the Coast Guard Foundation's [Fallen Heroes Scholarship Fund](http://www.coastguardfoundation.org/how/scholarships/directory). Bruns' and Lawler's plan worked. Enter the 2011 Coast Guard Run2Remember; in the end LT Bruns ran the 100 mile ultra marathon in memory of 90+ Shipmates who had fallen since September 11th. Donated funds came in at around $2,000 all of which were donated to the Fallen Heroes Scholarship Fund. Not too bad for a single runner and a few followers.

Last year's second annual Coast Guard Run2Remember (2012) got a whole lot bigger, and LT Lucy Love entered into the coordinator's seat. After seeing the impact the first event had on the families of the fallen, LT Love stepped up and took the initiative to ensure a 2nd event took place. With LT Bruns deployed, she assumed the reins and transformed a one-man event into a movement. She continued to work with ME1 Lawler and together they renewed a campaign to bring further awareness to the _Foundation's_ scholarship fund and the Coasties we've lost.

Their hard work paid off. LT Love involved not only some 93 people to run the Keys 100 but also individual events at units throughout the Coast Guard. Units from Virginia to Washington, Alaska, Hawaii, and even Kuwait were holding their own Run2Remeber events. While Love worked to coordinate the actual running in Key West (yes, another crazy one!) and help the individual event holders at units around the globe, Lawler was (and still is) hard at work getting the word out via their [Facebook page](http://www.facebook.com/cgr2r), [unit event pages](http://cgremember.com/2013-event-locations/), [Twitter](http://www.twitter.com/cgremember), and in general online social interactions. Lawler is also the designer, three years running, of their logos and images. It's a lot of work on top of their day jobs.

As a member of the Coast Guard, I was part of our local Run2Remember here in Juneau, AK last year. We had a turnout of about 50 or so people all wearing the t-shirts with the names of the fallen on the back. I still wear mine knowing someone is reading the back. At the end of the 2012 campaign, LT Love and ME1 Lawler's work enabled them to donate $12,000 dollar to the Fallen Heroes Scholarship Fund. Awesome!

The event has kind of taken a life of its own without a doubt. I admit I never saw it getting as big as it has. With that in mind I wondered if the Coast Guard as an organization would have taken notice- they have. Though the event isn't sponsored by the Coast Guard it definitely is supported. Which is good enough for me.

The 2013 campaign has changed a little. After listening to both participants and wishful participants of last year's event the duo set in motion a change of name and a change of participation. Starting this year the formerly named Run2Remember has officially been changed to [**CG Remember**](http://cgremember.com).

Why the change to such a successful event? Well, in short, not everyone runs or can run. So the name change opens up the event to not only runners but also bikers, rowers, rollers skaters, house sitters, and your backyard Bar-B-Q. "Virtually any event can be used as a remembrance event. It's not about the exercise or running, it never was, it's about remembering." says ME1 Lawler.

The goal hasn't changed though. The event is still here to help the Fallen Heroes Scholarship Fund. What has changed, along with the name, are the t- shirts. Over the past two years the shirts have listed the names of the fallen since September 11, 2001; 90+. This year that number has risen to 126 fallen Shipmates going back to 1982\. It will also include the most fallen Shipmate, Chief Petty Officer Terrell Horne III, who died in 2012\. ME1 Sean Lawler said it well in a letter to 2012 participants writing, "To the families of the fallen members, we know that nothing will ever ease the pain you have from losing a loved one, but you need to know that every member of the Coast Guard is with you, thanks you, and will always remember your sacrifice. Our uniforms have our names on the right side, but bear the words [U.S.] Coast Guard on the left...and that makes us all family. We will always remember our family." I look at the work that is being done here as Coasties helping Coasties. We're known as an organization that is always there to help the public; however, we're also just as capable with helping each other. Now the easy part: getting involved. If you're in the Coast Guard it's likely that your unit is already planning something. Check the list of participating units at [event page](http://cgremember.com/2013-event- locations/) which will have your local point of contact. If you're not in the Coast Guard, or you'd rather not participate in an event but still want to help Coast Guard Foundation Fallen Hero Scholarship fund, you can purchase your own t-shirt for $20\. The best part is **100% of the proceeds go to the scholarship fund**. If you'd rather just donate without anything in return you'll also find the address to mail donation on that same page.

Either way it's a great cause.

Thank you LT Love and ME1 Lawler for keeping this annual remembrance in motion.

*LT Lucy Love was the Coast Guard's Shipmate of the Week on 15 March 2013*