---
title: The Internet is Fragile and We Need a Plan B
date: 2013-03-19T15:22:29.000Z
slug: danny-hillis-tedtalk-internet

tags:
  - danny hillis
  - net neutrality
  - ted
---

<video poster="/files/blog/danny-hillis-tedtalk-internet/hillis-ted.png" preload="metadata" controls="">
  <source src="/files/blog/danny-hillis-tedtalk-internet/hillis-ted.mp4" type="video/mp4">
</video>

[Danny Hillis](http://en.wikipedia.org/wiki/W._Daniel_Hillis) gave a TED talk in February discussing how the notion of trust is ingrained in the protocols that form the Internet, and why that leaves the Internet vulnerable. He says that because of the network's trusting nature and the increasing number of critical services and systems that rely on the Internet, the network itself - not just the serivces using it - is fragile. His argument is that, in addition to putting greater effort into protecting the network, we need to develop a completely separate "plan B" in case the Internet were to crash.

There are some important points to consider about what he's saying. There's nothing wrong with the architecture of the Internet as it stands today. The protocols are sound - they've been proven to work as they were designed at incredible scale. We don't need to redesign or replace the Internet. We have things using the Internet that are pushing the limits of what it was designed for, which is okay - except there's nothing in place to keep things running if it fails. We don't need a full backup, just a redundant network for critical services like emergency responders.

This isn't fear mongering, and it isn't saying the Internet is doomed. It's practical, long-term risk planning that deserves consideration. It's an issue that resonates with me both as [a proponent of the free and open Internet](/blog/net-neutrality-open-letter/), and as a security geek.

Danny does a great job of explaining the issues facing the Internet in plain terms, and why his proposed solution isn't as daunting as it may seem.

[Danny Hillis TED Talk on TED.com »](http://www.ted.com/talks/danny_hillis_the_internet_could_crash_we_need_a_plan_b.html)
