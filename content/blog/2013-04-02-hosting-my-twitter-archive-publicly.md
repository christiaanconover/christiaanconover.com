---
title: Hosting my Twitter Archive Publicly
date: 2013-04-02T17:05:20.000Z
slug: hosting-my-twitter-archive-publicly

tags:
  - data liberation
  - social media
  - thinkup
  - twitter
---

A few months ago Twitter finally turned on the ability for users to [download their entire archive of tweets](http://blog.twitter.com/2012/12/your-twitter-archive.html). It's been a feature many power users have been longing for, and Twitter did a nice job with implementation. Rather than simply giving the user a JSON or XML dump, they build what amounts to be a self-contained web app of your entire tweet history. It actually doesn't even need a server - it's just a bunch of HTML, CSS and JavaScript files that your browser can handle all on its own. It's data portability for real people.

My archive has sat buried in a folder on my computer for a couple of months, unused and ignored. Then Kevin Marks posted a [guide to hosting your archive on Google Drive](http://epeus.blogspot.com/2013/04/hosting-and-impermanence.html) today and I figured I might as well put my archive online too. Since I have a web server already, my process was even simpler than his. [My archive now lives online in all its splendor](https://twarchive.cconover.com/).

This is, of course, just a temporary measure until [ThinkUp](https://thinkup.cconover.com) adds the ability to import your archive into its database. Hopefully that'll be a feature with the upcoming 2.0 release.
