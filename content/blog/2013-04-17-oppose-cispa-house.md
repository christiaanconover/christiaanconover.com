---
title: Do Your Part to Oppose CISPA
date: 2013-04-17T18:58:34.000Z
slug: oppose-cispa-house
image:
  src: /files/blog/cispa-passed-the-house-now-we-need-to-fight-it-in-the-senate/cispa.jpg
  alt: CISPA

tags:
  - cispa
  - cyber security
  - house of representatives
  - pipa
  - sopa
---

The [Cyber Intelligence Sharing and Protection Act](http://en.wikipedia.org/wiki/Cyber_Intelligence_Sharing_and_Protection_Act), more commonly known as CISPA, is [scheduled to go to the House floor for a vote as early as this afternoon](https://www.eff.org/deeplinks/2013/04/cispa-goes-floor-vote-privacy-amendments-blocked). CISPA would broaden and streamline the sharing of internet traffic information between the federal government and technology providers and manufacturers, without safeguards for personal privacy protection.

It endangers the free and open Internet, and if passed could have significant negative impacts on free speech, innovation and even the security of the very infrastructure it's supposed to protect. CISPA is a poorly written bill that would very likely do more harm than good - just like [SOPA, its predecessor](/blog/sopa-blackout-jan18/).

Like we did when SOPA and PIPA were up for vote, it's time to make it clear to our representatives that we're against CISPA. Here's how you can help.

# Call Your Representative

The Electronic Frontier Foundation has done a great job of making it easy to voice your opposition to CISPA. One of the best things they've done is set up a page that gives you the phone number for your specific representative, and an easy script to follow when you call. Calling is perhaps the most powerful way to express your opinion, since it's direct one-to-one contact, and requires a few minutes of time to actually make the call.

**[Call your representative »](https://action.eff.org/o/9042/p/dia/action/public/?action_KEY=9202)**

# Email Your Representative

If you can't call for whatever reason, it's not a problem - emailing works great too. The EFF has also made this an easy process, using a simple pre-filled form. Just provide your zip code, and they'll handle the rest.

[**Email your representative »**](https://action.eff.org/o/9042/p/dia/action/public/?action_KEY=9048)

# Tweet Your Representative

Yet another great tool EFF has created is a one-click way to tweet your representative. They've got a ton of pre-written tweets that @reply representatives and senators all over the country. This is a great thing to do not only because you get the attention of the elected officials, but also because you help spread the word to others to get involved.

I've embedded the EFF's tweet tool right on this page, so start tweeting!
