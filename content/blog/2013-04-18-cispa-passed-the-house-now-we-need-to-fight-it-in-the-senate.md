---
title: 'CISPA Passed the House, Now We Need to Fight it in The Senate'
date: 2013-04-18T19:53:46.000Z
slug: cispa-passed-the-house-now-we-need-to-fight-it-in-the-senate
image:
  src: /files/blog/cispa-passed-the-house-now-we-need-to-fight-it-in-the-senate/cispa.jpg
  alt: CISPA
tags:
  - cispa
  - cyber security
  - house of representatives
  - online privacy
  - pipa
  - senate
  - sopa
---

Earlier today the [House of Representatives voted to pass CISPA](https://www.eff.org/deeplinks/2013/04/us-house-representatives-shamefully-passes-cispa-internet-freedom-advocates). While this is certainly an unfortunate and disappointing outcome, [CISPA](http://en.wikipedia.org/wiki/Cyber_Intelligence_Sharing_and_Protection_Act) is far from becoming law. The bill now heads to the Senate, so once again we'll need to rally together to defeat this onerous legislation.

The [Electronic Frontier Foundation](https://www.eff.org/) set up a bunch of great tools for fighting CISPA with the House, and I'm sure it's only a matter of time before they do the same for the Senate. In the mean time please call, email and tweet your state's senators in opposition of CISPA.

[**Find your senators »**](http://www.senate.gov/)

We also need to put pressure on major players in the tech space whose products and customers will be affected to come out against the bill. Alexis Ohanian has already started doing this with Google, Facebook and Twitter.

{{< youtube IkuH5ZjEdBw >}}

If these large companies can put their weight behind opposing CISPA it might help get a lot more attention aimed at the issue, and get a much broader base of people fired up about it.

The fight isn't over yet, and it's regular people that will make the difference - [we've already seen proof of that](/blog/sopa-blackout-jan18/).

![Keep calm and stop CISPA](/files/blog/cispa-passed-the-house-now-we-need-to-fight-it-in-the-senate/keep-calm-fight-cispa.jpg)
