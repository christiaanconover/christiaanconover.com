---
title: Thoughts Following the Boston Marathon Bombing Ordeal
date: 2013-04-20T02:51:42.000Z
slug: thoughts-following-the-boston-marathon-bombing-ordeal
image:
  src: /files/blog/thoughts-following-the-boston-marathon-bombing-ordeal/news-coverage.jpg
  alt: News coverage of the Boston Marathon bombing
  caption: News coverage of the capture of one of the Boston Marathon bombers.

tags:
  - boston
  - boston marathon bombing
  - terrorism
---

This has indisputably been one of the wildest weeks in recent memory. In the afternoon of Monday, April 15 [two bombs went off near the finish line of the Boston Marathon](http://en.wikipedia.org/wiki/2013_Boston_Marathon_bombings), killing 3 and injuring over 180 people. In the days that followed a whirlwind of events transpired that reads like an episode of 24, culminating in the killing of one suspect and the apprehension of another. Now that the ordeal is over and the long, arduous process of investigation and prosecution really kicks off, it's amazing to think back on how this whole thing was wrapped up.

The bombings and subsequent manhunt have demonstrated one thing: we as a nation are much more capable to respond to domestic threats than ever before. The investigation progressed at an incredible pace, citizens worked with responding agencies to provide as much assistance as they could, and the time from the first blast to the final arrest didn't even fill a work week. The sad but perversely comforting truth is that, as a people, we're getting pretty good at this.

## First Responders Crushed It

The biggest part of bringing the situation to a close was the incredible effort put forth by the police, fire and EMS personnel that were involved. Earlier today it was reported by NBC News that there were more than 9,000 law enforcement officers in Boston from all over the region - making the police coverage per-capita more than double that of any other major city in the nation. Because of the coordinated efforts of multiple local, state and federal agencies this horrendous event was ended in less than a week. Even more amazingly, one of the two suspects was apprehended alive - a critical step in learning what motivated the crime, and how it was executed.

Perhaps one of the most amazing parts of the process was the investigation that took place away from TV cameras. I can't even imagine the amount of imagery investigators sifted through and analyzed, and the number of man hours expended, over the past couple of days that allowed them to narrow down the suspects in less than 3 days. A gargantuan and truly heroic effort on the part of the agencies involved.

## Twitter Won the Day for Coverage

When I was looking for up-to-the-minute information about the situation in Boston, I didn't turn to TV or radio. I turned to Twitter. Between the coverage provided by media outlets and [journalists who've embraced the Internet age](https://twitter.com/acarvin), witnesses posting pictures and observations on-scene, and multi-way conversation with friends, I was able to get every angle I was looking for in one place, faster than traditional media was able to pump it out. Plus, unlike other outlets who still haven't figured out how to make their coverage available everywhere, I can get Twitter on every single screen I use.

## Interesting Precedents Have Been Set

One of the largest cities in the United States was in lockdown for an entire day while police searched for a single 19 year-old terrorist. That's a wild and, to my knowledge, unheard-of occurrence. Boston is a city of 625,000 people, and if you include the greater metropolitan area that number swells to as much as 4.4 million. It makes sense why this was done both to protect the public, and to quickly locate and apprehend a dangerous suspect, but I wonder if this sends an unintended message: commit an act of terrorism that, while anything but insignificant, isn't that big in itself, and if you get away and keep stirring up a little mayhem you'll bring an entire city to its knees in a way that no single home-built bomb could. In truth it's the rawest and most insidious form of terrorism: the mental attack. This situation has clearly demonstrated this, and we'll have to wait and see the effects this has on our lives going forward, because you can rest assured there will be changes.

The captured suspect is not only alive and therefore able to be questioned, but he's being charged as a civilian criminal rather than an enemy combatant. When was the last time somebody was arrested on terrorism charges and tried in civilian criminal court? Because this case is being handled by the FBI and will be trial by jury, the public will be able to learn about the motivation behind the bombing through the trial. It also ensures due process will be followed to the letter of the law, since nobody wants to risk any chance of acquittal or mistrial through police or investigator impropriety. It's important to demonstrate that despite a horrific attack perpetrated against American citizens, we are a nation that adheres to its laws and treats all suspects as innocent until proven guilty, regardless of the crime.

## What's Next?

In a perfect world things go back to normal and we let the police and courts do their job, and we continue on with our lives. As trite and cliche as it is to say, if we change our behavior and cower then the terrorists win.

However, we don't live in a perfect world. After the initial fervor over the arrest and media digging into the suspect's history down to what he had for breakfast three years ago last Tuesday - and covering it ad nauseum, this story is going to quiet down until the trial starts. Despite this, we won't be able to escape the constant reminders. For the foreseeable future public events will be more complicated to organize and attend because of security. Some people will be wary to attend events like this at all. Hopefully we learned our lesson after 9/11 not to vilify brown people because the terrorists happened to be followers of Islam, but only time will tell. All those security warnings we've seen and heard continuously about reporting suspicious or unattended bags and packages, but most of us (if we're honest) didn't pay a whole lot of mind, suddenly have new significance. So be extra careful not to leave your bag sitting at airport baggage claim for too long :-P

Switching gears from the pessimism, we should all take stock in what went right in a bad situation. Runners who finished the marathon continued past the finish line for an additional two miles to the hospital to give blood. Race volunteers and first responders alike ran toward the blasts to help anyone and everyone they could, as fast as possible. Let's hope that going forward this is what people remember.

Like everyone else, I'm mainly celebrating the fact that nobody else will die at the hands of the perpetrators. Tonight the people of Boston can sleep well knowing that the nightmare is over.
