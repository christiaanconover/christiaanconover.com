---
title: All The Pixels
date: 2013-05-01T21:02:00.000Z
slug: all-the-pixels
image:
  src: /files/blog/all-the-pixels/monitors.jpg
  alt: Two monitors

tags:
  - imac
  - photography
---

I was given a 24" second monitor for my 21" iMac at work. That's a total of 45 diagonal inches of screen.
