---
title: My First Time-Lapse
date: 2013-05-03T20:57:15.000Z
slug: my-first-time-lapse

tags:
  - photography
  - time-lapse
---

<video preload="metadata" controls="">
  <source src="/files/blog/my-first-time-lapse/time-lapse.mp4" type="video/mp4">
</video>

The camera in Android 4.0 introduced a very cool feature: time-lapse video. Despite how awesome I think it is, I've never actually used it - until today. I shot this video out of the 4th story window of a hotel near BWI airport. I think I'm going to love using time-lapse, so brace yourselves for a whole slew of these types of videos from me.