---
title: We Need a Viewport Header
date: 2013-05-07T16:23:34.000Z
slug: viewport-header

tags:
  - browser
  - header
  - http
  - responsive design
---

Responsive design techniques are the future for the web, and for good reason. The ever-increasing number and variety of devices and their screen sizes makes the practice of dedicated desktop and mobile sites not only outdated, but unscalable. I'm a huge proponent of responsive & adaptive design and implement these technologies in every site I build. Despite all the amazing benefits they provide, there's still something that bugs me as a developer: lack of server-side responsiveness.

For all the amazing solutions the community has created to address the issues of feature support and resource optimization, they're almost exclusively client-side (read: JavaScript) utilities. This creates two familiar but ever-important problems: browser support for JavaScript, and after-delivery processing. The former is diminishing with every passing day, but the latter is one that JavaScript can never solve. Scripting can't even begin to work its magic until after the page has already started loading, meaning assets are already being downloaded by the browser, meaning the server has already done the work it's going to do in processing that page. None of that processing involves dynamically optimizing for the client's viewport, and that's not anyone's fault - there isn't a standard for providing this data to the server. The solution is to **send that information in the form of a browser header**.

Using a header allows the viewport to be delivered to the server at the earliest possible stage of the page request; before any processing has gotten a chance to start. It's readable not only by dynamic languages, as would be the case using things like cookies, but by the actual web server software. It would be a reliable, consistent format for passing information that thus far has only been able to come from hacks and workarounds. It would also allow us to have the optimization of a dedicated mobile site, with the flexibility of a responsive design.

I propose the header taking the following format:

```text
Viewport: [WIDTH]
```

Where [WIDTH] is the width in pixels of the viewport. As an example, a browser with a viewport of 1280 pixels wide would pass the following header:

```text
Viewport: 1280
```

I want to address some of the arguments likely to be made against this. I don't think this is a silver bullet solution to rectify all possible concerns with the responsive design approach. I think it has the potential to streamline common bottlenecks such as unnecessarily large images being delivered to small viewports, and heavy conditional JavaScript and liberal application of `display: none` being used to overwrite or hide code after it's already been sent to the browser. In other words, it could fix the most glaring performance problems. It also addresses the unscalable practice of User-Agent detection for delivering different versions of a site to different devices. I also foresee this taking away the need for many of the hacks people have come up with for things like responsive images that result in non-semantic HTML or dependence on scripting. It's not a solution to issues like a change in viewport due to device rotation; that still needs to be handled using existing methods and is up to the developer to effectively implement.

The main purpose here is to create a universal, scalable metric for server-side responsiveness that builds on existing technology, and doesn't need scripting.
