---
title: The Google Doodle for Saul Bass is Awesome
date: 2013-05-08T15:07:36.000Z
slug: google-doodle-saul-bass
tags:
  - google
  - google doodle
  - saul bass
---

{{< youtube 64lDaAmpvSo >}}

Google is celebrating graphic designer [Saul Bass](http://en.wikipedia.org/wiki/Saul_Bass)' 93rd birthday today with a really neat Doodle - this time a video, as a tribute to his work in films. One of the things I love about Google Doodles is that, because they like to pick less well-known subjects, I often get introduced to new people and subjects with which I wasn't familiar. Saul Bass is no exception. I recognize his work for sure, but I didn't know him by name. The Doodle is worth a watch, and if you're anything like me you'll spend the next 20 minutes reading about Saul Bass.
