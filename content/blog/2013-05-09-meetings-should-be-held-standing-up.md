---
title: Meetings Should Be Held Standing Up
date: 2013-05-09T19:23:33.000Z
slug: meetings-should-be-held-standing-up
image:
  src: /files/blog/meetings-should-be-held-standing-up/meeting-standing-up.jpg
  alt: Stand-up meeting

tags:
  - business
  - meetings
---

Meetings are a huge time-waster. Unless your organization has broken through to a state of enlightened gathering where meetings stay on topic, are brief, and are used sparingly in order to maximize effectiveness and minimize productivity destruction, you've probably ~~suffered~~ sat through countless meetings that could just have easily been handled with a couple of quick emails.

Part of the problem, I think, is that people sit down for meetings. When somebody sits down they subconsciously have the expectation of being there for a while. You sit down, put your computer, tablet or pad of paper on the table, and you settle in. Everyone in the room is doing this, so it grants a silent permission for people to speak at length. By contrast, if everyone in the room is standing for the duration of the meeting there's an expectation of brevity since people are in an action position, ready to move forward with their tasks. Nobody wants to talk (or listen) for a long time, because everybody is ready to get right to whatever it is they're discussing. Besides, standing is good for you!

If you're a data-driven person like I am, measure meetings in terms of man hours. If you have an hour-long meeting with 8 people, that's 8 man hours you're spending sitting around a table talking. In some cases that really may be the best use of those hours, but often it's probably not. I've had the pleasure of having a few standing meetings, and they've typically been at least 50% shorter than they would have been sitting down in a conference room. I left those meetings feeling more productive and eager to tackle the discussed project.

So let's all stop wasting so much time sitting around talking, and spend more time working on stuff that actually matters.
