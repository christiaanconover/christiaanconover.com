---
title: Good Morning from Virginia Beach
date: 2013-05-13T13:03:38.000Z
slug: virginia-beach-sunrise-13may13
image:
  src: /files/blog/virginia-beach-sunrise-13may13/va-beach-sunrise.jpg
  alt: Virginia Beach sunrise

tags:
  - joint warfighting conference
  - jwc 2013
  - us naval institute
  - virginia beach
---

I'm in Virginia Beach this week for [EAST: Joint Warfighting Conference 2013](http://www.usni.org/events/east-joint-warfighting-2013), which is being hosted by my office. My hotel is on the 7th floor of a beachfront hotel, so this was my view this morning when I woke up. I can live with this.
