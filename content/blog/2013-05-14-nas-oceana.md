---
title: Touring Naval Air Station Oceana
date: 2013-05-14T14:45:46.000Z
slug: nas-oceana
image:
  src: /files/blog/nas-oceana/fa-18-nose.jpg
  alt: Nose of an F/A-18

tags:
  - f/a-18
  - nas oceana
  - naval aviation
---

"I won't be offended if you turn away to watch the planes flying. I do it myself all the time," [NAS Oceana](http://en.wikipedia.org/wiki/Naval_Air_Station_Oceana) commanding officer CAPT Bob "Goose" Geis tells our group as he starts his brief on the facility's history and operations. It's an appropriate introduction to a meeting being held in the control tower conference room, a space seven stories above the tarmac with floor-to-ceiling glass on three sides, giving a 270 degree view of everything happening on the airfield. It's an impressive sight, and you can't fully appreciate the scale of NAS Oceana's aircraft operations until you see it from above.

Oceana is huge. It's the Navy's east coast [Master Jet Base](http://en.wikipedia.org/wiki/Master_Jet_Base), and as of 2012 it has 19 squadrons and 337 total aircraft. Standing in the tower, it's row after row of F/A-18 jets so far into the distance that it becomes hard to discern what type of plane you're looking at. That doesn't include the various other cargo and support planes, and the smattering of helicopters dotting the tarmac. Among all these aircraft is a constant stream of ground vehicles running around carrying fuel, parts, and who knows what else. At any given moment there must be at least a dozen jets taxiing to or from one of NAS Oceana's four runways, including the area's only 12,000 foot runway (the other three are 8,000 feet a piece). CAPT Geis tells us that when NASA was still operating the Space Shuttle program, Oceana was on the list of possible divert points for landing because they have a runway long enough.

![NAS Oceana F/A-18s on the Tarmac.](/files/blog/nas-oceana/oceana-tarmac.jpg)
<small>NAS Oceana F/A-18s on the Tarmac.</small>

Off of those runways 200,000 flights a year take off, and watching the traffic it's not hard to believe. Every few minutes another pair of fighters take off with a thunderous roar that, even sitting behind the thick glass of the tower conference room, resonates through your entire body. Their rate at which planes are coming and going seems to outpace even the busiest commercial airports.

![CAPT Bob "Goose" Geis, NAS Oceana commanding officer, answers questions during our time in the tower conference room.](/files/blog/nas-oceana/geis.jpg)
<small>CAPT Bob "Goose" Geis, NAS Oceana commanding officer, answers questions during our time in the tower conference room.</small>

CAPT Geis completes his brief and invites us to watch the planes and take pictures from the conference room, and takes our questions. He points out the differences between the F/A-18 Hornet and the newer Super Hornet. After talking with him, I'll bet you a beer I can pick out one from the other at night just by the lights alone. He points out a line of F/A-18s parked a few hundred yards away with strange paint schemes. "Those are part of an agressor squadron. Their job is to play the part of enemy aircraft during training missions. Those particular planes are painted to look like Chinese fighters."

Leaving the tower, we take a quick van ride down the tarmac to [VFA-15](http://www.vfa15.navy.mil/)'s hangar and squadron offices. We're given a briefing on the history of VFA-15, its recent deployments, and a day in the life of a naval aviator. The Lieutenant Commander giving the brief paints a clear picture of the incredible amount of resources required to keep a fighter squadron flying. VFA-15 has 200 personnel assigned to it, only 25 of whom are officers. It takes 175 enlisted sailors to maintain the squadron's aircraft, and while they're out at sea maintenance is conducted 24/7, broken into twelve hour shifts (on shore they do maintenance Sunday night through Friday afternoon). Putting planes in the air is incredibly expensive, so they make the most of every minute they fly. NAS Oceana has a massive 94,000 square miles of airspace off the Atlantic Coast that they use to fly training missions of all varieties. They have telemetry systems on each aircraft that sends data about every single thing happening on board back to ground stations, and is used during their extensive, in-depth debriefs to fine-tune every aspect of a pilot's skills.

Down in the hangar bays are a number of fighters in various states of disassembly while maintenance is performed. One of the planes has one of its engines completely removed, and you can see all the way through from the tail to the air intake opening. Walking around the hangar around the planes, you start to realize how much bigger they are than they appear to be in pictures, or when they're flying overhead.

One of the squadron pilots is answering questions about the planes and their weaponry, but to be honest I'm only half listening because I'm too enthralled with checking out these amazing machines up close. It's small details that stand out. The end of the arresting hook, used to catch one of the wires on the deck of an aircraft carrier to stop the plane when it lands, is only about the size of a fist. It's incredible to think that a little chunk of metal on the end of a pole is solely responsible for keeping the plane from careening off the end of the ship. For an aircraft that costs tens of millions of dollars you'd expect every tool to be high-grade, specialized equipment - and much of it is, but there are also what appear to be $5 oil drain pans from an auto supply store on the floor, and foam packing blocks taped to the edges of the control surfaces to protect them from damage. Of course these items make sense logically, but they seem so incongruous in conjunction with the rest of the items in the hangar.

Above all else, I keep thinking how amazingly cool it must be to fly one. It's one thing to watch Top Gun and want to buzz the tower, but it's a whole different thing to watch them in person; to stand next to one looking at the humongous engines that hurtle these planes through the air at supersonic speeds.

![F/A-18s Flying](/files/blog/nas-oceana/fa18s-flying.jpg)

My main takeaway from the tour is that NAS Oceana is all about big. Big runways, big numbers of aircraft, big speeds, big missions. It's a wild experience to stand on the tarmac of one of the busiest airports in the country, feet away from planes that are capable of seemingly physics-defying feats of speed and agility, and remember that you're a ten minute drive from the beach. All I kept thinking while I was there was, "this is so freakin' cool!"

_This post was [originally published on USNI Blog](http://blog.usni.org/2013/05/14/tour-nas-oceana). I have posted it here for my archives._
