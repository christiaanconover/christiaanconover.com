---
title: Shipping Up to Boston Island for the Weekend
date: 2013-05-23T20:11:39.000Z
slug: boston-island-memorial-day-2013
image:
  src: /files/blog/boston-island-memorial-day-2013/boston-island-sheepscot.jpg
  alt: Sheepscot River from Boston Island

tags:
  - boston island
  - maine
  - memorial day
---

It's Memorial Day weekend, which means my family and I are headed to [our summer place in Maine](https://www.boston-island.com) to open it for the season. It's become an annual tradition, and for me it marks the start of summer.

As I've done during all my past trips to the Island, I'll post pictures while I'm there. Check out the [Boston Island blog](https://www.boston-island.com) as well, as we'll be posting stuff from our trip there too.
