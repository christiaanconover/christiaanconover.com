---
title: "We're Missing the Bigger Picture with Edward Snowden"
date: 2013-06-25T22:12:45.000Z
slug: were-missing-the-bigger-picture-with-edward-snowden
image:
  src: /files/blog/were-missing-the-bigger-picture-with-edward-snowden/snowden.jpg
  alt: Edward Snowden

tags:
  - edward snowden
  - nsa
  - prism
  - whistleblower
---

The reaction to Edward Snowden's leaking of classified documents was exactly what everyone expected: the government went crazy and leveled accusations of treason and espionage (subsequently [filing charges against him under the Espionage Act](http://www.latimes.com/news/politics/la-pn-espionage-charges-edward-snowden-20130621,0,4999142.story)), the media had a field day scraping up details about Snowden and interviewing countless people about implications, ad nauseum. The public were naturally divided between those who agree with the government's characterization of the man and his actions, and those who support what he did as patriotic exposure of government overreach.

What's interesting is the number of comparisons I've seen to Bradley Manning, Julian Assange, WikiLeaks and any other recent leakers of secrets. The comparisons themselves aren't notable; it would be strange not to see those lines being drawn. The fact that there are so many similar groups & individuals in recent history is telling, and something that should be paid more mind by those in charge. This is the manifestation of a growing mistrust in our government - not just by political extremists, but by average citizens who are seeing and hearing things they know are wrong and feel powerless to correct them using conventional means. That mistrust is only reinforced when programs such as PRISM are exposed to sunlight; just because you're paranoid doesn't mean the world isn't out to get you.

I'm not one for conspiracy theories, and I'm not in the camp demanding smaller government to the point that nothing but defense is left intact. We need government, and truth be told our lives as Americans are the better for it. Yet with the growing number of examples where surveillance of American citizens and loose interpretation of rights occur, with increasing levels of secrecy not only about the actual surveillance but the legal process for allowing these activities, it's easier to understand why people like Snowden are taking matters into their own hands.

Even more significant to me is the anecdotal evidence I've seen that opinions of Snowden are highly generational. Most of his supporters that I've seen are my generation or younger - a segment of the population that has lived the majority of their lives post-9/11 under the Patriot Act. A segment of the population that's grown up with the Internet - where there really is no such thing as a secret and information flows freely, which is ingrained in our collective consciousness. This is the generation that is entering the workforce and voting, and for the most part we don't seem to have a whole lot of issue with what Snowden has done.

Charging people like Edward Snowden and Bradley Manning without taking a hard look at the policies that led them to do what they did is treating the symptoms while ignoring the disease. We need to stop talking about Snowden, and focus on things like [FISA Court](http://en.wikipedia.org/wiki/United_States_Foreign_Intelligence_Surveillance_Court), [National Security Letters](http://en.wikipedia.org/wiki/National_security_letter) and the [Patriot Act](http://en.wikipedia.org/wiki/Patriot_Act). We need to stop pretending that those leaking information are the only ones at fault, which given the current tone of the conversation seems to be the prevailing opinion. The truth is, the American people are somewhat to blame by not demanding more transparency and narrower parameters on information gathering. Our leaders are at fault for pushing the boundaries of the law and then doubling down by insisting they did nothing wrong once the shit hits the fan. Legally Snowden is in the wrong, but morally I'm not so sure.

It's become cliché in modern times, but Benjamin Franklin got it right when he said "Those who would give up Essential Liberty to purchase a little Temporary Safety, deserve neither Liberty nor Safety." That's the issue at hand right now, and its consequences are coming home to roost.
