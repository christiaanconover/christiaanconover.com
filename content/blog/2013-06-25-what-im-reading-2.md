---
title: "What I'm Reading"
date: 2013-06-25T18:40:56.000Z
slug: what-im-reading-2
image:
  src: /files/blog/what-im-reading-2/anustrt.jpg
  alt: ANUSTRT

tags:
  - arrested development
  - bmw
  - edward snowden
  - instagram
  - prism
  - "what i'm reading"
---

[**Instagram Video and the Death of Fantasy**](http://bits.blogs.nytimes.com/2013/06/24/digital-diary-instagram-video-and-death-of-fantasy/)<br>
Despite the claims made by Mark Zuckerberg and Instagram CEO Kevin Systrom, Instagram isn't capturing the memories of your life - it's a fantasy highlight reel.

[**Demonizing Edward Snowden: Which Side Are You On?**](http://www.newyorker.com/online/blogs/johncassidy/2013/06/demonizing-edward-snowden-which-side-are-you-on.html)<br>
This author asserts that coverage of the Edward Snowden situation isn't journalism but sensationalism and bandwagon jumping. Something tells me [Jeff Jarvis](http://buzzmachine.com/) would agree.

[**This Virginia Driver Has The New Greatest License Plate Of All Time**](http://jalopnik.com/this-virginia-driver-has-the-new-greatest-license-plate-573821044)<br>
A BMW with an awesome Arrested Development vanity plate? Huzzah!

[**Keeping Your Data Private Denies You Access to the Latest Tech**](http://www.wired.com/business/2013/06/personal-encryption-is-dying/?cid=co9161274)<br>
In the wake of PRISM's ousting, some have moved to protect their data from all prying eyes. The problem: many of the features in the most popular tools & services couldn't exist if everything is encrypted.
