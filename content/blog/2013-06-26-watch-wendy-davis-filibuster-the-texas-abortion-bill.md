---
title: Watch Wendy Davis Filibuster the Texas Abortion Bill
date: 2013-06-26T02:51:12.000Z
slug: watch-wendy-davis-filibuster-the-texas-abortion-bill

tags:
  - abortion
  - texas
  - wendy davis
---

Texas State Senator Wendy Davis is [currently filibustering an abortion bill](http://www.cbsnews.com/8301-201_162-57590966/texas-senator-wendy-davis-filibusters-against-abortion-bill/) that, if passed, would effectively close every abortion clinic in the state. To defeat the bill she has to stay on her feet until midnight, at which point the 30 day special session ends.

Senator Davis started speaking at 11:18am CDT and as I'm writing this at 9:44pm CDT she is still going strong.

**Update**: the full video of the filibuster (many, many hours) is not available, so I've replaced that with a clip of one of the highlights.

{{< youtube NlEHJNTQeRs >}}