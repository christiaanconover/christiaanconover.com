---
title: "What I'm Reading"
date: 2013-06-26T19:35:57.000Z
slug: what-im-reading-3
image:
  src: /files/blog/what-im-reading-3/wendy-davis-shoes.jpg
  alt: "Wendy Davis' Shoes"

tags:
  - doma
  - google fiber
  - same-sex marriage
  - scotus
  - wendy davis
  - "what i'm reading"
---

[**Hopes for a non skim-milk marriage**](https://medium.com/better-humans/92ec465b1209)<br>
A gay woman in Tennessee explains what DOMA being struck down, and marriage in general, means to her.

[**Gigabit Seattle priced at $80 per month, just over what Google Fiber costs**](http://arstechnica.com/business/2013/06/gigabit-seattle-priced-at-80-per-month-just-over-what-google-fiber-costs/)<br>
I'm so happy to see others following Google's lead to start bringing gigabit fiber to the masses. Now I'm just waiting for somebody to come give me a viable alternative to Crapcast.

[**Messing with Texas**](https://medium.com/ladybits-on-medium/cb900fa4f14b)<br>
A blow-by-blow of Texas State Senator Wendy Davis' filibuster of SB5 (the abortion bill) and everything that happened around it. I watched it live from about 9pm on and it's still a riveting read.
