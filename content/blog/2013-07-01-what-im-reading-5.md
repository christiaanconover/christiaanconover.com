---
title: "What I'm Reading"
date: 2013-07-01T14:47:41.000Z
slug: what-im-reading-5
image:
  src: /files/blog/what-im-reading-5/scrivener.png
  alt: Stained glass

tags:
  - google
  - jeff jarvis
  - journalism
  - nsa
  - "what i'm reading"
---

[**There are no journalists**](http://buzzmachine.com/2013/06/30/there-are-no-journalists-there-is-only-journalism/)<br>
Jeff Jarvis argues that defining the term "journalist" creates boundaries that run counter to the purpose of journalism.

[**The NSA vs. Democracy**](http://www.washingtonpost.com/blogs/wonkblog/wp/2013/06/28/the-nsa-vs-democracy/)<br>
Intelligence services are by nature in conflict with the principles of democracy - transparency and accountability for and by the people. Ezra Klein makes a case similar to the one [I made a few weeks ago](/blog/were-missing-the-bigger-picture-with-edward-snowden/), but with more charts and figures.

[**Google+ Turns Two: Why I'm Becoming a Convert**](http://readwrite.com/2013/06/28/google-plus-second-anniversary#awesm=~oalR9WEF7nqwdF)<br>
Better sharing & posting tools and higher quality discussion. Maybe it's not such a ghost town.
