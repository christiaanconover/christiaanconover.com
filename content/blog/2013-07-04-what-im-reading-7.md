---
title: "What I'm Reading"
date: 2013-07-04T16:37:02.000Z
slug: what-im-reading-7
image:
  src: /files/blog/what-im-reading-7/jefferson-memorial.jpg
  alt: Jefferson Memorial

tags:
  - declaration of independence
  - fisa
  - fourth of july
  - npr
  - "what i'm reading"
---

Happy Fourth of July! Today's post includes a story that isn't so much something I'm reading, as something to which I'm listening. NPR's Morning Edition has a long-standing tradition to read the Declaration of Independence on-air every July 4th. This year is no different, though it has a bit of a twist. Rather than the usual cast of hosts and correspondents reading in turn, this year they have visitors to the National Archives doing the reading. It's worth a listen, and brings to life words that we may not fully have appreciated before. There's fire in those pages which is even more evident when you hear the Declaration read aloud in modern diction.

**[The Declaration: What Does Independence Mean To You?](http://www.npr.org/2013/07/04/197722022/the-declaration-what-does-independence-mean-to-you)**

[Underwater Art Galleries Keep Memory of Ships Alive](http://news.usni.org/2013/07/03/underwater-art-galleries-keeps-memory-of-ships-alive)

[Business school students learn leadership from Battle of Gettysburg](http://www.marketplace.org/topics/life/business-school-students-learn-leadership-battle-gettysburg)

[Yahoo Keeps Buying Startups That Don't Make Their Own App](http://readwrite.com/2013/07/03/yahoo-qwiki-lawsuit-chaotic-moon#awesm=~oaE1rZspRALfZd)

[Ex-FISA Court Judge Reflects: After 9/11, 'Bloodcurdling' Briefings](http://www.npr.org/2013/07/03/198329788/fisa-court-judge-reflects-after-sept-11-bloodcurdling-meetings-and-briefings)

[U.S. Postal Service Logging All Mail for Law Enforcement](http://www.nytimes.com/2013/07/04/us/monitoring-of-snail-mail.html)
