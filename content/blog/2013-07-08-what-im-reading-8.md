---
title: "What I'm Reading"
date: 2013-07-08T19:36:56.000Z
slug: what-im-reading-8
image:
  src: /files/blog/what-im-reading-8/field.jpg
  alt: Field

tags:
  - bmw
  - edward snowden
  - google glass
  - bmw m3
  - prism
  - "what i'm reading"
---

[The BMW M3 Coupe Is Officially Dead](http://jalopnik.com/the-bmw-m3-coupe-is-officially-dead-677228405)

[If PRISM Is Good Policy, Why Stop With Terrorism?](http://www.theatlantic.com/politics/archive/2013/07/if-prism-is-good-policy-why-stop-with-terrorism/277531/)

[What Snowden Needs Now Is a Good Lawyer](http://swampland.time.com/2013/07/07/what-snowden-needs-now-is-a-good-lawyer/)

['I filmed the first fight and arrest through Google Glass'](http://venturebeat.com/2013/07/05/i-filmed-the-first-fight-and-arrest-through-google-glass/)

[How Clothes Should Fit](http://howclothesshouldfit.com/)
