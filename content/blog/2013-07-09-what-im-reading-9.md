---
title: "What I'm Reading"
date: 2013-07-09T16:55:09.000Z
slug: what-im-reading-9
image:
  src: /files/blog/what-im-reading-9/river-sunset.jpg
  alt: Sunset over a river

tags:
  - android
  - nsa
  - "what i'm reading"
---

[9 ways a billion dollar new mobile company might be created](http://bubba.vc/2013/07/08/9-ways-a-billion-dollar-new-mobile-company-might-be-created/)

[In Secret, Court Vastly Broadens Powers of N.S.A.](http://www.nytimes.com/2013/07/07/us/in-secret-court-vastly-broadens-powers-of-nsa.html)

[Android distribution numbers updated for July, Jelly Bean now the most used version](http://9to5google.com/2013/07/08/android-distribution-numbers-updated-for-july-jelly-bean-now-the-most-used-version/)
