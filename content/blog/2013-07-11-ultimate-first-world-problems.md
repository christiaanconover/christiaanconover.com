---
title: Ultimate First World Problems
date: 2013-07-11T17:20:17.000Z
slug: ultimate-first-world-problems
image:
  src: /files/blog/ultimate-first-world-problems/phone.jpg
  alt: My phone with a cracked screen

tags:
  - rant
---

Last night I had a first-time experience: dropping my phone and the screen cracking. I was a bit in shock, and a bit pissed off. All I could think was how much it was going to cost to rectify the problem. The crack doesn't cover the whole screen, but it's enough to be really annoying. That's immaterial though, as nobody wants their phone screen to crack...or so I thought.

I went on Twitter to gripe about having broken it. Narcissistic, yes, but I needed to vent my frustration. I got the following tweet in reply:

{{< tweet 355135347035873281 >}}

Assuming it was a joke I played along, asking why on Earth somebody would actually want their screen cracked?! The [answer I got was bonkers](http://articles.washingtonpost.com/2013-05-17/lifestyle/39333160_1_screen-iphone-lofton).

I must be getting old, because I've been completely unaware of this trend of teenagers and young 20-somethings having cracked phones to make a statement. I can't fathom wanting to ruin a piece of equipment costing hundreds of dollars for the sake of looking "tough" or...something. I'm having a hard enough time reading things in the small area of my phone affected by the single crack, let alone having cracks all over. Doesn't it sort of defeat the purpose of a device with a high-quality screen if you've gone to lengths to destroy it?

Besides, what kind of toughness does anybody think it conveys that their expensive handheld computer for privileged people with money for such luxuries managed to take a few hits and not shatter into a million pieces? The phone should be proud of its resilience, but the moron holding it should feel - well, they'll probably just be feeling cuts in their fingers as they gleefully swipe through Etsy on broken glass.

End curmudgeonly rant. Carry on.