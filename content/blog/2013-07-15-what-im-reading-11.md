---
title: "What I'm Reading"
date: 2013-07-15T15:47:58.000Z
slug: what-im-reading-11
image:
  src: /files/blog/what-im-reading-11/annapolis.jpg
  alt: Ego Alley in Annapolis

tags:
  - annapolis
  - coding
  - drinking
  - pixar
  - "what i'm reading"
---

[**Bros Get Wasted; Girls Get Tipsy: Why Boozy Talk Matters**](http://www.npr.org/blogs/health/2013/07/10/200779580/bros-get-wasted-girls-get-tipsy-why-boozy-talk-matters)<br>
I've known plenty of girls who use the same drinking language as guys, but apparently different language indicates different attitudes about alcohol.

[**The Pixar Theory**](http://jonnegroni.com/2013/07/11/the-pixar-theory/)<br>
All Pixar movies exist on a timeline in the same universe. Mind-blowing.

[**Why Citizen Developers Are The Future Of Programming**](http://readwrite.com/2013/07/15/the-rise-of-the-citizen-developer?awesm=readwr.it_dIw#awesm=~obFXcOoxvWXHa1)<br>
Degrees aren't as important to programming as many people think.
