---
title: "What I'm Reading"
date: 2013-07-18T14:34:28.000Z
slug: what-im-reading-12
image:
  src: /files/blog/what-im-reading-12/alfa.jpg
  alt: Alfa Romeo

tags:
  - blogging
  - boston marathon bombing
  - medium
  - terrorism
  - twitter
  - "what i'm reading"
---

[The Family That Tweets Together Stays Together](http://www.npr.org/blogs/health/2013/07/16/202646035/the-family-that-tweets-together-stays-together) [NPR: Shots]

[Silicon Valley's All Twttr](http://gigaom.com/2006/07/15/valleys-all-twttr/) [GigaOM]

[The terrorist as rock star](http://dave.smallpict.com/2013/07/17/theTerroristRockStar) [Dave Fargo]

[The Problem With Medium](https://medium.com/on-medium/336300490cbb) [Medium]

[Mission Creep: When Everything Is Terrorism](http://www.theatlantic.com/politics/archive/2013/07/mission-creep-when-everything-is-terrorism/277844/) [The Atlantic]
