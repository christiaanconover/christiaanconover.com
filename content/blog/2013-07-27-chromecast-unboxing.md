---
title: Chromecast Unboxing
date: 2013-07-27T13:30:31.000Z
slug: chromecast-unboxing
image:
  src: /files/blog/chromecast-unboxing/01.jpg
  alt: Chromecast

tags:
  - chromecast
  - google
  - google chrome
  - netflix
  - streaming
  - unboxing
  - youtube
---

My [Chromecast](http://en.wikipedia.org/wiki/Chromecast) arrived yesterday! I was lucky enough to order it via Amazon Prime before [they sold out](http://www.usatoday.com/story/tech/personal/2013/07/26/google-chromecast-amazon-best-buy/2589011/), so I didn't end up with a weeks-long wait. For those unfamiliar, Chromecast is a new device for televisions to stream content directly from the cloud, controlled by your phone, tablet or computer. It's about the size of a USB thumb drive and plugs right into an HDMI port, allowing you to stream just about any content on the Internet, straight from the source. The best part: it's $35.

The box is pretty simple. There's an outer sleeve which shows the device and some highlights about it, such as the ability to stream video from YouTube and Netflix.

![Chromecast Box - Back and Side](/files/blog/chromecast-unboxing/02.png)

Once you remove the sleeve you're presented with the box itself, featuring a side-hinged lid with the Chromecast logo on top.

![Chromecast Box - Removing the Sleeve](/files/blog/chromecast-unboxing/03.jpg)

The inside of the lid shows the steps for setting up the device, with the Chromecast itself sitting in a plastic tray on top of the other accessories. All in all very Apple-like packaging, though that's unsurprising given that many consumer electronics are packaged this way nowadays.

![Chromecast Box - Inside the Box](/files/blog/chromecast-unboxing/04.png)

Inside you'll find a micro USB cable (used to provide power, since HDMI can't power the device by itself), an HDMI extender in case your TV's ports are in a tight space, a USB power supply, and a small instruction manual. Depending on your TV/AV setup you may not need the power supply since many modern TVs have USB ports you can use.

![Chromecast Accessories](/files/blog/chromecast-unboxing/05.jpg)

Popping the device out of its tray is as easy as pushing on the open spot on the back. The Chromecast has protective plastic around the shiny edge which you need to peel off before using, as it covers the micro USB port. As I mentioned before, it's about the size of a thumb drive and there isn't much to it. The HDMI connector is at one end, the logo is on the front, some technical details are on the back, and the LED status light and micro USB port are at the other end.

![Chromecast](/files/blog/chromecast-unboxing/06.jpg)

That's all there is to it. The presentation is attractive, and for a $35 dongle it's surprisingly robust. As of this writing I haven't even set it up yet, so I'll have a review posted in a few days once I've played around with all its features.
