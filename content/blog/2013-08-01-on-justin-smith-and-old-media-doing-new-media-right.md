---
title: 'On Justin Smith, and Old Media Doing New Media Right'
date: 2013-08-01T20:24:06.000Z
slug: on-justin-smith-and-old-media-doing-new-media-right
image:
  src: /files/blog/on-justin-smith-and-old-media-doing-new-media-right/justin-smith.jpg
  alt: Justin Smith

tags:
  - justin smith
  - npr
  - the atlantic
  - the verge
---

Atlantic Media president Justin Smith has announced [he'll be leaving to become CEO of Bloomberg Media Group](http://gigaom.com/2013/07/28/atlantic-media-loses-one-of-its-digital-architects-as-president-justin-smith-joins-bloomberg/). It's an interesting move, and one that indicates Bloomberg wants to become more of a consumer brand. After all, when you [look at Smith's history](http://buzzmachine.com/2013/07/29/all-hail-justin-smith/) it's pretty clear that he has a knack for creating strong publications with direct consumer appeal - even reinventing ones that have lost their caché, like [The Atlantic](http://www.theatlantic.com/).

Case in point: I'd never heard of The Atlantic until about a year ago when I came across a couple of articles on their web site within a few weeks of each other. I figured I'd discovered a great new blog, and it wasn't until I sent a link to a friend of mine familiar with the magazine that I learned otherwise.

So what gave me the impression I'd found something new? The delivery method. Rather than a site skeumorphically designed to mimic its print counterpart, this site felt like it started on the web. Unlike many print-to-web publications that bombard you with all variety of obnoxious popups, overlays and traffic caps in an effort to make you a paid subscriber, The Atlantic just put the content right in front of you - like, you know, a proper web site. There are banner and sidebar ads, and that's great because they don't get in the way but still provide a revenue model that doesn't involve pissing off the user within the first three seconds.

Justin Smith took his skills straight to the web when he launched [Quartz](http://qz.com/). It's well designed and responsive, highly visual, and publishes engaging content. Oh, and it runs on WordPress. Clearly this is a guy who understands putting the content where people are, with as little barrier to entry as possible.

NPR has pulled off a similar feat using similar tactics. They design for the web, they make their content readily available, and they engage their audience. In fact, what NPR has been doing online the past few years is a case study in how to adapt to new media. Their site puts the content first, and keeps you connected to their traditional delivery method - radio - through their on-air shows in a way that makes sense for web users. They've launched a number of blogs focusing in on specific subject areas that have great content and a modern feel.

NPR is a special case since their revenue comes from fundraising listener support (and [a portion from government funding](http://www.npr.org/about-npr/178660742/public-radio-finances)). Yet their web strategy doesn't appear to differ too drastically from The Atlantic.

These are great examples of old media "getting" new media. Let's contrast this with the opposite experience on a publication I came across today: Foreign Policy. I think my tweet about it says it all:

{{< tweet 362979911411060736 >}}

This is a perfect example of how to kill your long-term business model with your short-term user experience. A friend sent me a link to an article on their site, and as soon as I got there I was shown a horrendous overlay, and the only way to get rid of it was to sign in or sign up. No option whatsoever to browse as a guest - which is doubly irritating when they say you can sign up for free. The only way they're ready to show me an article is if they can market to me incessantly or use me as a promotional mouthpiece to my friends. No thanks. Put your collective egos away, I _guarantee_ the article isn't good enough for that hassle. As a result, Foreign Policy just lost me as a reader for good. If they'd considered user experience as a feature instead of a liability I might've added them to my list of outlets worth reading.

Instead, I read sites like The Atlantic. And NPR and [their blogs](http://www.npr.org/about-npr/198341814/npr-blog-directory). And [The Verge](http://www.theverge.com/). Sites that get what it means to publish on the web and put user experience above everything else. Justin Smith and Atlantic Media figured out how to do that and be profitable. Let's see if others can do the same.