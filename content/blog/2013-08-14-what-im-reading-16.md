---
title: "What I'm Reading"
date: 2013-08-14T14:38:27.000Z
slug: what-im-reading-16
image:
  src: /files/blog/what-im-reading-16/chesapeake-sunset.jpg
  alt: Sunset on the Chesapeake

tags:
  - android
  - edward snowden
  - ku klux klan
  - om malik
  - paul stamatiou
  - "what i'm reading"
---

**[Whistleblowing Is the New Civil Disobedience: Why Edward Snowden Matters](http://www.zephoria.org/thoughts/archives/2013/07/19/edward-snowden-whistleblower.html)**<br>
"The US government, far from deterring future whistleblowers, has just incentivized a new generation of them by acting like a megalomaniac."

[**Why Silicon Valley Should Save Drive-in Movies**](http://pandodaily.com/2013/08/13/why-silicon-valley-should-save-drive-in-movies/)<br>
Drive-in theaters are the perfect alternative to indoor theaters for people who want the freedom to use their phone or other device while watching. Also, I'd love to see drive-in movies become popular again.

[**Android is Better**](http://paulstamatiou.com/android-is-better)<br>
Long-time Apple and iOS proponent Paul Stamatiou explains the reasons he's sticking with Android.

[**iAMerican**](http://om.co/2013/08/13/iamerican/)<br>
GigaOm founder and father of tech news blogging Om Malik was sworn in as an American citizen yesterday, and explains what that means to him. Congratulations Om!

[**A Day in the Life of the Ku Klux Klan, Uncensored**](http://www.slate.com/blogs/behold/2013/08/13/anthony_s_karen_a_photojournalist_s_unrestricted_access_to_the_ku_klux_klan.html)<br>
A photojournalistic peek into one of the most secretive and baffling organizations in the nation.
