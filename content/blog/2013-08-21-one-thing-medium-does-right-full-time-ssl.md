---
title: 'One Thing Medium Does Right: Full-Time SSL'
date: 2013-08-21T19:47:45.000Z
slug: one-thing-medium-does-right-full-time-ssl

tags:
  - medium
  - ssl
---

Since [Medium](https://medium.com/about/9e53ca408c48)'s launch earlier this year, there have been a ton of posts about [why Medium sucks](http://www.codingjohnson.com/medium-sucks#.UhTZRWRAQ2E), [what it's doing wrong](https://medium.com/on-medium/336300490cbb), [why it should fail](http://www.elezea.com/2013/08/medium-is-eating-all-the-content/), and [on](http://kennethreitz.org/why-i-left-medium/) and [on](https://www.tbray.org/ongoing/When/201x/2013/06/30/Medium) and [on](https://medium.com/medium-ideas/55251e013516). I agree with every single one I've read. Medium is trying to provide a 1990s answer to a 2010s problem. They're building a content farm, and aside from some 30 second visibility to passers-by, will ultimately do nothing to build the brands of contributors. If you've got something to say, start your own blog.

There is one thing Medium is doing right, though. Their site runs [full-time SSL](https://en.wikipedia.org/wiki/Secure_Socket_Layer) and so far as I can tell offers no plain text option. Outside of financial institutions and sites like Gmail and Facebook, that's pretty unusual. Yet the benefits of this are [increasingly apparent](http://online.wsj.com/article/SB10001424127887324108204579022874091732470.html), while the [costs are plummeting](https://www.imperialviolet.org/2010/06/25/overclocking-ssl.html). In fact, you can get yourself a fully valid SSL certificate for [the low cost of zero dollars](https://startssl.com/). Even if you're a blogger with no revenue, that expense will fit in your budget.

As so many others [have already been saying](http://techcrunch.com/2013/08/20/now-is-the-time-for-all-good-nerds-to-come-to-the-aid-of-the-internet/), it's time to have encryption by default. Say what you will about Medium and their business model - at least they've gotten that part right.
