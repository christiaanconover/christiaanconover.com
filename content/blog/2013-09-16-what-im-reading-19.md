---
title: "What I'm Reading"
date: 2013-09-16T18:20:45.000Z
slug: what-im-reading-19
image:
  src: /files/blog/what-im-reading-19/butterfly.jpg
  alt: Butterfly

tags:
  - apple
  - ios7
  - iphone
  - journalism
  - keith alexander
  - medium
  - nsa
  - "what i'm reading"
---

[Apple's Fingerprint ID May Mean You Can't 'Take the Fifth'](http://www.wired.com/opinion/2013/09/the-unexpected-result-of-fingerprint-authentication-that-you-cant-take-the-fifth/)

[A shield law for journalists might seem like a good idea, but it isn't -- it's actually a terrible idea](http://paidcontent.org/2013/09/13/a-shield-law-for-journalists-might-seem-like-a-good-idea-but-it-isnt-its-actually-a-terrible-idea/)

[The Secret War](http://www.wired.com/threatlevel/2013/06/general-keith-alexander-cyberwar/all/)

[Why Generation Y Yuppies Are Unhappy](http://www.huffingtonpost.com/wait-but-why/generation-y-unhappy_b_3930620.html)

[What Medium Is](http://dashes.com/anil/2013/08/what-medium-is.html)
