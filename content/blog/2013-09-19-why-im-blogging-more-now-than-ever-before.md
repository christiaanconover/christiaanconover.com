---
title: "Why I'm Blogging More Now Than Ever Before"
date: 2013-09-19T14:21:29.000Z
slug: why-im-blogging-more-now-than-ever-before
image:
  src: /files/blog/why-im-blogging-more-now-than-ever-before/wp-editor.png
  alt: WordPress editor

tags:
  - blogging
---

> **"Blogging is dead, [social network du jour] is where it's at."**

You've likely heard that phrase at least ~~seven times~~ once, whether you're a blogger or not. It's a popular refrain any time a new service comes along that gets hugely popular and becomes the place where everybody publishes all their stuff. Usually this mentality is driven not by an affinity for the service in question, but an impression of the issues around maintaining a blog - it's too much work, nobody will read it, it has to have brilliant posts on a regular basis, it's too narcissistic. Most of these are misconceptions, some of which stem from watching the pro bloggers do their thing. People feel intimidated by these prolific writers with thousands of readers, so they give up. That's like deciding there's no point to learning how to play guitar because rock stars exist.

I'm not going to pretend that writing a blog post doesn't require a little more effort than a Facebook status update - but only a little. In fact, I have a number of Facebook friends who post status updates that may as well be blog posts, so the line is blurred. However, there are some key distinctions that swing in favor of maintaining a blog.

For me, the biggest selling point is that blogging is enjoyable. It's like going to the gym for my brain. I keep my thinking and writing skills sharp by using them regularly to articulate ideas. Unlike the exhausting and irritating writing exercises we were all forced to do in school, writing a blog post is entirely up to you. You pick the topic, you decide how long or short the post is, you make the language as formal or informal as you like, you (or somebody you ask) does the editing, and you have final say in what gets published. Blogging is to academic writing what a fun Sunday drive is to the driver's license test.

I value keeping control of what I publish. Putting my thoughts straight onto Facebook or the like may engender a few extra likes or comments, but if Facebook suffers a data loss or - hard as it is to imagine - goes out of business, my content disappears. Publishing on a site I control removes that risk. I've also found very little difference in engagement between posting on my own site with a link on Facebook, and publishing directly to Facebook. Portability is the other side of that coin. I can post a link to a blog post on every social network and sharing service under the sun, and it all comes back to one single location. I'm not jumping through hoops to bring people from Twitter to my Facebook page - I'm using the web the way it ought to be.

Blog posts have permanence in a way that posts on social networks don't. If I write a post on my site it has a [permalink](https://en.wikipedia.org/wiki/Permalink) - a canonical URL that points to the same location on my site forever, easily found again later. A post on most social networks, by contrast, may have a permanent URL, but unless you've saved that address somewhere it can be near impossible to find an old post later. That's by design - social networks are about the here and now, about connecting with what your friends are doing at any given moment. Blogging is about sharing ideas, and preserving those ideas for posterity. I can go back and look at a post I wrote years ago with nearly the same ease as finding one I wrote yesterday. That searchable, indexable, easily navigable archive of your ideas becomes increasingly valuable as time goes on and your knowledge and experience grows.

If you're not into writing, not a problem. Maybe photography is your cup of tea, or you really enjoy video to tell your stories. Photo or video blogging offers nearly all the same benefits while allowing you to work in the medium you prefer. Flexibility is the name of the game.

Blogging is [a cornerstone of the modern web](http://wordpress.org/news/2013/05/ten-good-years/), and for good reason. It's a decentralized publishing model with almost no barrier to entry, and no restrictions on usage. It's a way to share ideas seamlessly and openly, and with permanence. I get far greater satisfaction out of writing a blog post than a Facebook status or tweet, because it's all mine to share with the world. Social networks and content farms will come and go, but the venerable blog will survive them all. It's up to those of us who believe in all forms of blogging to make sure it thrives.
