---
title: Serendipitous / Unfortunate
date: 2013-09-20T15:15:51.000Z
slug: netmag-lost-articles-self-publish
tags:
  - net magazine
  - blogging
  - jeff jarvis
---

Not 24 hours after I wrote [my post about blogging](/blog/why-im-blogging-more-now-than-ever-before/), _.Net Magazine_ announced they were closing down their site and [migrating their top 500 articles to Creative Bloq](http://www.creativebloq.com/net/creative-bloq-welcomes-readers-net-magazine-9134344). That's a far cry from the close to 10,000 articles on their site, spanning a long history of the web and its evolution. These articles were a de facto historical archive of how the web has changed over the years, and techniques that have come and gone. What's more, plenty of them were still relevant and useful to developers and designers, and [deserved to be kept available](http://www.smashingmagazine.com/2013/09/19/good-content-is-too-valuable-to-die/).

The editors of _.Net_ claimed that [it wasn't financially feasible to keep a legacy site online](https://twitter.com/netmag/statuses/380687914482401281). They are preserving a portion of those articles on another site - why not all of them?

This situation brings us back to one of my prior arguments: if you have something good, publish it yourself. If the contributors to _.Net_ had self-published on their own blogs, their articles would still be on the Internet.

{{< tweet 380729053353086976 >}}

If you write for a living and need to publish with a third party to make money, so be it. I've been seeing a new trend recently that allows you to do both: publish your article with whatever organization is paying you for it, and then publish it on your own site for archival purposes. Jeff Jarvis did that with [a recent _Guardian_ editorial he wrote](http://buzzmachine.com/2013/08/24/what-are-you-thinking-mr-president/). He didn't republish it right away, but waited a few days until the _Guardian_ had circulated it themselves. Jeff's words are now forever preserved online, even if the _Guardian_ goes the way of _.Net_. Hell, [I've done it before](/blog/nas-oceana/) and I'll do it again for anything I publish elsewhere.

Don't trust others with your hard work. The publication may be their product, but your work is your brand.
