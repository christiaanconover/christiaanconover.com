---
title: "What I'm Reading"
description: "The best articles I've read in the past week."
date: 2013-09-23T19:46:16.000Z
slug: what-im-reading-20
image:
  src: /files/blog/what-im-reading-20/roadboat.jpg
  alt: "Big ol' roadboat"

tags:
  - freelance
  - jeff bezos
  - joint strike fighter
  - "what i'm reading"
---

[**Will It Fly?**](http://www.vanityfair.com/politics/2013/09/joint-strike-fighter-lockheed-martin)<br>
An in-depth look at the history of, and problems plaguing, the Joint Strike Fighter.

[**Freelancers: Work for free**](https://medium.com/design-ux/f85dc3457d04)<br>
Ok, not all the time or you'd be broke, but Dann Petty explains why some of his most rewarding jobs were ones he did for free.

[**The 20 Smartest Things Jeff Bezos Has Ever Said**](http://www.fool.com/investing/general/2013/09/09/the-25-smartest-things-jeff-bezos-has-ever-said.aspx)<br>
There's a reason Amazon.com is so wildly successful.

[**Why It's Time to Ditch Your Office**](http://www.linkedin.com/today/post/article/20130916171740-5853751-why-it-s-time-to-ditch-your-office)<br>
I really wish more companies would embrace this philosophy. It's time to start working like it's 2013.
