---
title: "What I'm Reading"
description: 'Crash course in government shutdown, Breaking Bad as a lesson in business, and more.'
date: 2013-09-30T16:04:12.000Z
slug: what-im-reading-21
image:
  src: /files/blog/what-im-reading-21/big-sur-bridge.jpg
  alt: Big Sur bridge

tags:
  - breaking bad
  - football
  - nfl
  - "what i'm reading"
---

**[Everything you need to know about how a government shutdown works](http://www.washingtonpost.com/blogs/wonkblog/wp/2013/09/24/everything-you-need-to-know-about-a-government-shutdown/)**<br>
With mere hours until the deadline for a government funding bill, here's a crash course in what will happen if an agreement can't be reached.

**[The "Breaking Bad" School](http://www.economist.com/news/business/21586801-best-show-television-also-first-rate-primer-business-breaking-bad-school?fsrc=scn/gp/wl/pe/thebreakingbadschool)**<br>
Breaking Bad, besides being phenomenal television, is also a great lesson in business.

**[What's In A College Degree? Maybe Not As Much As You Think](http://readwrite.com/2013/09/25/online-education-college-alternative-getting-a-job#awesm=~oiWv80yML1NKUG)**<br>
Online courses are changing the landscape of higher education, and employers are starting to follow the trend.

**[How the NFL Fleeces Taxpayers](http://www.theatlantic.com/magazine/archive/2013/10/how-the-nfl-fleeces-taxpayers/309448/)**<br>
It's the most profitable professional sports enterprise in the world, yet the NFL manages to get millions in funding and tax breaks to subsidize its operations.
