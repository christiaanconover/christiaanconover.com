---
title: 'Government Shutdown: The Facts, With a Few Opinion Sprinkles on Top'
description: "The government has shut down. Here's a bunch of information about how that works and what it means."
date: 2013-10-01T16:47:30.000Z
slug: government-shutdown-the-facts-with-a-few-opinion-sprinkles-on-top
image:
  src: /files/blog/government-shutdown-the-facts-with-a-few-opinion-sprinkles-on-top/capitol-closed.png
  alt: Capitol is closed

tags:
  - congress
  - government shutdown
  - liveblog
  - obamacare
---

Well folks, it's happened. Congress failed to pass a continuing resolution to fund the federal government, and [as of this morning the government has shut down](http://www.washingtonpost.com/politics/government-shutdown-begins-senate-expected-to-reject-latest-house-proposal/2013/10/01/ef464556-2a88-11e3-97a3-ff2758228523_story.html), all non-essential employees are at home, and essential and non-essential employees alike are without pay.

If you're a government employee deemed "essential" then the good news is you'll receive back pay for the time you work during the shutdown once the government resumes operations. If you're non-essential, then...not so much. However, requiring essential employees to work without pay [is not sustainable for very long](http://www.slate.com/blogs/moneybox/2013/10/01/essential_services_how_long_can_the_government_keep_running_without_appropriations.html).

[Let's review everything that happens during a government shutdown](http://www.washingtonpost.com/blogs/wonkblog/wp/2013/09/24/everything-you-need-to-know-about-a-government-shutdown/). While most agencies are reduced to a skeleton crew or close altogether, certain functions of the government will continue. Uniformed military personnel will remain on duty, border patrol and prison guards will still be working, Social Security checks will continue to go out, and a few others. Until Congress can pass legislation to fund the government again, these agencies remain in a state of limbo.

[Guess who continues to get paid? Congressmen and Senators](http://www.washingtonpost.com/blogs/wonkblog/wp/2013/10/01/congress-gets-paid-during-a-shutdown-while-staffers-dont-heres-why/). Their staffers are a different story though. As absurd as this seems, it has to do with the way elected officials are paid. Their salary (currently $174,000 a year) is written into permanent law. The shutdown only affects employees paid through annual appropriations, so politicians are unaffected. All their staff are in the annual budget though, so those folks will be going without pay until this all gets sorted out.

The District of Columbia's municipal government funding is affected by the federal budget, so D.C. government employees fall into the same situation as federal employees. Interestingly, [Mayor Vincent Gray declared all city government employees as "essential"](http://www.washingtonpost.com/local/dc-politics/mayor-gray-designates-all-of-district-government-essential-to-avoid-shutdown/2013/09/25/3df4047a-25f4-11e3-ad0d-b7c8d2a594b9_story.html) to avoid any interruption in services, so District employees are all still going to work, albeit without pay.

The government shutdown has a much broader impact than the paychecks and operations of the federal government. [IHS Inc. estimates that this shutdown will cost the U.S. economy $300 million a day](http://www.bloomberg.com/news/2013-10-01/shutdown-would-cost-u-s-economy-300-million-a-day-ihs-says.html).

Why did it come to this at all? [It's really as simple as Republicans opposing the Affordable Care Act](http://maddowblog.msnbc.com/_news/2013/10/01/20771002-why-republicans-shut-down-the-government). What's more, not all Republicans agreed with this brinksmanship strategy and [the charge was led by a small faction of House Republicans](http://www.theatlantic.com/politics/archive/2013/09/your-false-equivalence-guide-to-the-days-ahead/280062/). It's a game of chicken between politicians, complicated by division in the ranks of a political party.

So how will this end? Well, if you take [the words of Republican Congressman Raúl Labrador of Idaho as any indicator](http://www.slate.com/blogs/weigel/2013/09/30/house_republican_cr_shutdown_will_likely_end_with_short_term_gop_surrender.html), the GOP will have to compromise on Obamacare. Who knows how long that will take, though.

*I'm posting additional links, in the liveblog below, to shutdown articles as I come across them.*

# Liveblog

## <time datetime="2013-10-01T19:00:00+00:00">October 1, 2013 3:00pm</time>

The reason Republicans are okay with taking the government to the brink? [They don't think it will affect them](http://www.slate.com/articles/news_and_politics/politics/2013/09/house_republicans_and_the_government_shutdown_why_is_the_gop_not_bothered.html).

## <time datetime="2013-10-01T19:03:00+00:00">October 1, 2013 3:03pm</time>

Are you a Navy or Air Force football fan? [You might be SOL this weekend](http://news.usni.org/2013/10/01/shutdown-pentagon-restrictions-cancel-navy-air-force-football-game).

## <time datetime="2013-10-01T19:54:00+00:00">October 1, 2013 3:54pm</time>

It seems that a lot of people might actually be [interested in getting affordable health insurance](http://www.washingtonpost.com/blogs/wonkblog/wp/2013/10/01/obamacares-marketplaces-are-overwhelmed-with-traffic/).

## <time datetime="2013-10-01T20:40:00+00:00">October 1, 2013 4:40pm</time>

![House of Turds](/files/blog/government-shutdown-the-facts-with-a-few-opinion-sprinkles-on-top/house-of-turds.png)

## <time datetime="2013-10-01T20:45:00+00:00">October 1, 2013 4:45pm</time>

So Republicans [plan to restore funding to parts of the government piece by piece](http://online.wsj.com/article/SB10001424052702304373104579109083001071104.html). Seems like they're quickly realizing how unsustainable and stupid this shutdown really is.

## <time datetime="2013-10-01T20:54:00+00:00">October 1, 2013 4:54pm</time>

[istheusgovernmentshutdown.com](http://istheusgovernmentshutdown.com/)

## <time datetime="2013-10-02T04:05:00+00:00">October 2, 2013 12:05am</time>

![The federal government is still shut down](/files/blog/government-shutdown-the-facts-with-a-few-opinion-sprinkles-on-top/snl-govt-still-shut-down.png)

## <time datetime="2013-10-02T04:34:00+00:00">October 2, 2013 12:34am</time>

Jonathan Cohn, Senior Editor at the New Republic, did a [Reddit AMA Tuesday morning about Obamacare](http://www.reddit.com/r/IAmA/comments/1niguh/im_jon_cohn_a_senior_editor_at_the_new_republic/). He's an expert in the ins and outs of ACA, so it's highly worth reading through the questions and answers.

## <time datetime="2013-10-02T16:02:00+00:00">October 2, 2013 12:02pm</time>

There's a way the Democrats can end the shutdown, with the help of more moderate Republicans, [using something called a discharge petition](http://www.washingtonpost.com/blogs/wonkblog/wp/2013/10/02/democrats-can-use-this-one-weird-trick-to-end-the-government-shutdown/).

## <time datetime="2013-10-03T16:53:00+00:00">October 3, 2013 12:53pm</time>

![The federal government is still shut down](/files/blog/government-shutdown-the-facts-with-a-few-opinion-sprinkles-on-top/compromising.jpg)

We're now on day 3 of the shutdown, and this image seems to summarize the hardline Republican position pretty well.

## <time datetime="2013-10-03T17:01:00+00:00">October 3, 2013 1:01pm</time>

As if it wasn't enough of an insult to federal workers to suspend their pay over a legislative temper tantrum, [this Congressman has the gall to blame the Park Ranger working without pay](http://www.nbcwashington.com/news/local/Congressman-Confronts-Park-Ranger-Over-Closed-WWII-Memorial-226209781.html) for the WWII Memorial being closed. What a jackass.

## <time datetime="2013-10-03T17:56:00+00:00">October 3, 2013 1:56pm</time>

A little humor, web service-style, to lighten the mood.

[usgovernment.statuspage.io](http://usgovernment.statuspage.io/)

## <time datetime="2013-10-03T19:37:00+00:00">October 3, 2013 3:37pm</time>

The House now has [enough Republicans ready to join the Democrats in voting for a clean continuing resolution for it to pass](http://www.huffingtonpost.com/2013/10/02/clean-funding-bill_n_4031784.html), but Boehner won't bring it to a vote.

## <time datetime="2013-10-16T14:39:00+00:00">October 16, 2013 10:39am</time>

There might **finally** be a [deal that can pass the Senate and the House](http://www.nytimes.com/2013/10/17/us/congress-budget-debate.html?_r=0), and none too soon either.