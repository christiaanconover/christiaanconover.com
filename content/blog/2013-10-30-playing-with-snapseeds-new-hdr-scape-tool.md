---
title: "Playing with Snapseed's New \"HDR Scape\" Tool"
description: "Snapseed's new HDR filter works pretty well. Here are my results."
date: 2013-10-30T19:02:10.000Z
slug: playing-with-snapseeds-new-hdr-scape-tool
image:
  src: /files/blog/playing-with-snapseeds-new-hdr-scape-tool/01.jpg
  alt: Boat on a Travelift

tags:
  - bmw
  - boston island
  - gisela
  - hdr
  - photography
  - snapseed
---

[Snapseed](https://plus.google.com/+Snapseed), a popular image manipulation tool for Android and iOS, has added a feature to turn any image into an [HDR image](https://en.wikipedia.org/wiki/High-dynamic-range_imaging). I did some tests, and here are my results.

![An HDR version of a photo I posted earlier in the year.](/files/blog/playing-with-snapseeds-new-hdr-scape-tool/02.jpg)<br>
An HDR version of [a photo I posted earlier in the year](/blog/virginia-beach-sunrise-13may13/).

![Our house in Maine.](/files/blog/playing-with-snapseeds-new-hdr-scape-tool/03.jpg)<br>
Our house in Maine.

![Gisela and me at a photo shoot.](/files/blog/playing-with-snapseeds-new-hdr-scape-tool/04.jpg)<br>
Gisela and me at a photo shoot.

![Another one of Gisela at the photo shoot, this time with much heavier HDR filtering.](/files/blog/playing-with-snapseeds-new-hdr-scape-tool/05.jpg)<br>
Another one of Gisela at the photo shoot, this time with much heavier HDR filtering.

![Counter Protesters against the Westboro Baptist Church.](/files/blog/playing-with-snapseeds-new-hdr-scape-tool/06.jpg)<br>
Counter Protesters against the Westboro Baptist Church.

![Boats on moorings in Maine at sunrise.](/files/blog/playing-with-snapseeds-new-hdr-scape-tool/07.jpg)<br>
Boats on moorings in Maine at sunrise.

For as simple as it is to use, it yields pretty good results. Obviously the quality isn't nearly as good as you see from professional photographers who use tools like Photoshop and spend tons of time tweaking the effects. Yet for the amateur looking for a quick way to spice up their image, it'll get you 90% of the way there. I'm usually opposed to these sorts of filter effects, but this is one that I can actually get behind.
