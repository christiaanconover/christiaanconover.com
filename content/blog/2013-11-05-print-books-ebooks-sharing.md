---
title: "I Bought a Print Book Today, But I Didn't Want To"
description: "One major flaw in the current eBook model has forced me to begrudgingly buy print books, a format that I don't want to own."
date: 2013-11-05T23:43:27.000Z
slug: print-books-ebooks-sharing
image:
  src: /files/blog/print-books-ebooks-sharing/nobooks.png
  alt: No books

tags:
  - amazon
  - amazon kindle
  - apple
  - ebooks
  - google
---

I'm a relatively recent convert to reading books. I wasn't much of a reader growing up, and it wasn't until I got my first eBook reader that I found a love for books. For me, reading on a screen is much easier than reading on a printed page. Then there are the added benefits of automatic bookmarking/syncing, lack of physical objects to store, instant delivery, and highlighting and notes that, for those of us with atrocious handwriting, are actually legible. As a result, I've bought - and actually read - a significant number of books in the past few years.

Yet I still find myself frustrated with eBooks from time to time. It has nothing to do with the format, but the lack of any simple way to loan a book to a friend. That happened to me today, when I decided I really wanted the [latest book by the founders of 37Signals](http://www.amazon.com/gp/product/0804137501/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=0804137501&linkCode=as2&tag=christconove-20). I was going to buy the Kindle version, but realized I'd probably want to share it with other people, which would be nigh impossible short of loaning them my tablet. So I bit the bullet and ordered the hardcover, despite not wanting to and being irked by that decision even after I made the purchase.

Before anyone points out that they still got my money when I bought the print book: you're right, in this specific instance I bought the book in a format I don't want. More often than not I'll just buy the eBook version (if I even decide to buy it at all), and just live with the fact that I can't loan it to anyone. If the publisher doesn't even offer an eBook version I absolutely won't buy the book.

This is not an insurmountable problem. There's a way publishers and distributors can offer the option to loan books to friends that makes them and their customers happy. Barnes & Noble has done this somewhat with their LendMe program, but it's restricted only to certain books. Amazon doesn't appear to offer this at all. Neither do Google and Apple, to my knowledge.

Please, publishers, don't make me buy things I don't want - or resort to finding what I want on Bittorrent. Just let me share my damn books.
