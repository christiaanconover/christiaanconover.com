---
title: 'My First WordPress Plugin: Relogo'
description: "I've released my first WordPress plugin, which lets you easily add support for rel=\"logo\" to your site."
date: 2013-11-13T14:30:36.000Z
slug: my-first-wordpress-plugin-relogo

tags:
  - relogo
  - wordpress
  - wordpress plugins
---

Today I released [my first WordPress plugin, called Relogo](https://github.com/cconover/wp-relogo). It's a simple plugin that lets you easily add support for `rel="logo"` to your WordPress site, in accordance with the spec published at [relogo.org](http://relogo.org).

The `rel="logo"` specification is a cool and simple way to make sure people always have the most up-to-date version of your logo without having to make any changes to their code.

So [check out my plugin](https://github.com/cconover/wp-relogo), and install it on your site! If you do, let me know what you think of it in the comments, and [please rate it](http://wordpress.org/plugins/relogo/).
