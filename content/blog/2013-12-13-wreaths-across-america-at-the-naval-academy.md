---
title: Wreaths Across America at the Naval Academy
description: 'Wreaths Across America came to the Naval Academy, so I snapped a few pictures.'
date: 2013-12-13T16:42:37.000Z
slug: wreaths-across-america-at-the-naval-academy
image:
  src: /files/blog/wreaths-across-america-at-the-naval-academy/01.jpg
  alt: Wreaths Across America

tags:
  - veterans
  - wreaths across america
---

[Wreaths Across America](https://www.wreathsacrossamerica.org/) came to the U.S. Naval Academy today, to lay wreaths on graves at the Naval Academy cemetery. I snapped a few photos while they were here.

![Wreaths waiting to be placed on graves at the Naval Academy cemetery.](/files/blog/wreaths-across-america-at-the-naval-academy/02.jpg)<br>
Wreaths waiting to be placed on graves at the Naval Academy cemetery.

![The procession of vehicles for Wreaths Across America, including motorcycles and vehicles from various police departments in Maine (where the program got its start).](/files/blog/wreaths-across-america-at-the-naval-academy/03.jpg)<br>
The procession of vehicles for Wreaths Across America, including motorcycles and vehicles from various police departments in Maine (where the program got its start).

![Volunteers lay wreaths on graves at the Naval Academy.](/files/blog/wreaths-across-america-at-the-naval-academy/04.jpg)<br>
Volunteers lay wreaths on graves at the Naval Academy.

<video width="640" height="360" controls="">
  <source src="/files/blog/wreaths-across-america-at-the-naval-academy/wreaths.mp4" type="video/mp4">
</video>
