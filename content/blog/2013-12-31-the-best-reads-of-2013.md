---
title: The Best Reads of 2013
description: The best articles I read throughout 2013.
date: 2013-12-31T18:35:50.000Z
slug: the-best-reads-of-2013
image:
  src: /files/blog/the-best-reads-of-2013/best-reads-2013.png
  alt: Best reads of 2013

tags:
  - "what i'm reading"
---

This year I started [a recurring segment on my blog](/tags/what-im-reading/), to catalog my favorite articles from around the web.

{{< tweet 349258950694285314 >}}

It seems fitting, then, that I look back on the year and pick out the highlights of what I read. Since this is a yearly summary I've got more than my usual 3 or 4 links to share, so I've broken them down by category.

# Technology

[The technologists' Hippocratic oath](http://buzzmachine.com/2013/12/09/16431/)<br>
[10 Rules of Internet](http://dashes.com/anil/2013/07/rules-of-internet.html)<br>
[The Secret War](http://www.wired.com/threatlevel/2013/06/general-keith-alexander-cyberwar/all/)<br>
[Android is Better](http://paulstamatiou.com/android-is-better)<br>
[Not in my house: how Vegas casinos wage a war on cheating](http://www.theverge.com/2013/1/14/3857842/las-vegas-casino-security-versus-cheating-technology)

# Business

[All is Fair in Love and Twitter](http://www.nytimes.com/2013/10/13/magazine/all-is-fair-in-love-and-twitter.html?pagewanted=1&_r=0&pagewanted=all)<br>
[The "Breaking Bad" school](http://www.economist.com/news/business/21586801-best-show-television-also-first-rate-primer-business-breaking-bad-school?fsrc=scn/gp/wl/pe/thebreakingbadschool)<br>
[Why It's Time to Ditch Your Office](http://www.linkedin.com/today/post/article/20130916171740-5853751-why-it-s-time-to-ditch-your-office)<br>
[The surprising reason we have a 40-hour work week (and why we should rethink it)](http://thenextweb.com/entrepreneur/2013/12/16/surprising-reason-40-hour-work-week-rethink/#!q0r3b)<br>
[From Subject Line to Signature: How To Do Work E-mail Right](http://www.forbes.com/sites/jacquelynsmith/2013/07/08/subject-line-to-signature-how-to-do-work-e-mail-right/)

# Lifestyle & Culture

[Any given Sunday: inside the chaos and spectacle of the NFL on Fox](http://www.theverge.com/2013/11/25/5141600/any-given-sunday-the-chaos-and-spectacle-of-nfl-on-fox)<br>
[Why Generation Y Yuppies are Unhappy](http://www.huffingtonpost.com/wait-but-why/generation-y-unhappy_b_3930620.html)<br>
[The Pixar Theory](http://jonnegroni.com/2013/07/11/the-pixar-theory/)<br>
[Why The Hell Does Your Drink Cost So Much?](http://deadspin.com/why-the-hell-does-your-drink-cost-so-much-1454231900)

# Humor

[Dear Guy Who Just Made My Burrito](https://medium.com/p/fd08c0babb57)<br>
[Please Just Give Him Five](http://www.patspulpit.com/2013/12/23/5237586/please-just-give-him-five)<br>
[Preferred Chat System](http://xkcd.com/1254/)