---
title: The Case Against Encrypted Text Messaging
description: "I'm all about encrypted messaging, but SMS is the wrong approach."
date: 2013-12-31T15:15:25.000Z
slug: the-case-against-encrypted-text-messaging

tags:
  - cyber security
  - encrypted messaging
  - encryption
  - messaging
  - sms
  - textsecure
  - xmpp
---

Yes, you read that title correctly. [I, of all people](/tags/cyber-security/), am advocating against increased security measures for texting.

Before we go any further, I want to be clear that [I am 100% in favor of encrypted messaging](https://guardianproject.info/apps/chatsecure/). Let's put encryption everywhere we can, and lots of it. Lest we forget, this site uses full-time [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) and there isn't even sensitive data being passed back and forth. There's a caveat to my security gusto, however: do it right, or don't bother. SMS encryption falls under the latter.

[Whisper Systems](https://whispersystems.org/) offers an app called TextSecure that supports encrypted [SMS messaging](https://en.wikipedia.org/wiki/Short_Message_Service), using your device's native SMS capability. On face value this seems like a win. Better security without needing a new protocol? What could be better? I tried it out with [an equally security-conscious friend of mine](http://johncardarelli.com/) and quickly noticed major flaws, all due to the nature of the SMS protocol.

SMS is inseparably tied to your phone number, and there's no way around that. Because of this, all the juicy metadata associated with phone calls is also associated with text messages: sender, recipient, time, and potentially even location. Everything about text messages is traceable, and attached to the physical world. Anonymity and texting will never coexist, [even if you're using a burner](http://gigaom.com/2013/10/14/feds-reportedly-used-phone-database-to-track-burner-use/).

> _"Hey Christiaan, email and other hosted messaging systems suffer from the same issue, right?"_

If you asked that question then first off, good for you for thinking through the logic chain. In fact, many other messaging protocols _do have a similar problem_ - but it's not the same. Let's use email as an example, since it's as ubiquitous as texting. Even if an email message is encrypted, some metadata is still visible. After all, the mail servers have to know things about the message so they can deliver it. However, email is a completely open and decentralized protocol, needing almost none of the same physical links and verifications that are required by cell carriers. Sure, the IP address of the mail server is public information, but that's equivalent to the address of your local post office. Masking the IP address and location of the end user is [relatively trivial](https://www.torproject.org/). Still, email can't be considered fully secure for the same sorts of reasons as texting.

However, even with protocols that pass plain text metadata you can send the message through an encrypted tunnel. It does rely on support for Transport Layer Security on every device involved in message transmission and reception, but that's not a technical hurdle so much as a configuration problem. Many existing real-time messaging platforms have TLS enabled by default.

Encrypted text messaging requires a smartphone to run the app for encryption. It also requires that you use the app in lieu of your standard SMS interface. The practicalities of using it are effectively no different from using an Internet-based secure messaging app, but with less benefit and more restrictions.

It's evident that there's demand for secure communications tools, especially in light of the information [leaked by Edward Snowden](/blog/were-missing-the-bigger-picture-with-edward-snowden/) this year. I'm happy to see developers taking up the charge for secure messaging, but I'd much rather see their efforts go in a direction that result in truly secure, easy-to-use, and preferably open source software. We've seen [hints at this](/blog/heml-is-has-a-shot-at-succeeding-where-other-encrypted-messaging-systems-have-failed/), but so far nothing that checks all the boxes. Let's do it right, and stop building a house of cards on [a standard that's over the hill](http://blog.chron.com/techblog/2012/11/sms-use-declines-but-you-arent-sending-fewer-texts-wait-what/).
