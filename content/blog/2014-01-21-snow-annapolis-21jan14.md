---
title: 'Winter Storm in Annapolis [Live Blog]'
date: 2014-01-21T17:25:04.000Z
slug: snow-annapolis-21jan14

tags:
  - annapolis
  - liveblog
  - snow
  - weather
---

Annapolis is expecting its first significant snow of the year today, with a forecasted 6-10" of accumulation. As usual, I'll be putting up pictures and videos throughout the day.

# Liveblog

## <time datetime="2014-01-21T17:26:00+00:00">January 21, 2014 12:26pm</time>

The start of the snow, just before noon.

![Start of the snow](/files/blog/snow-annapolis-21jan14/snow01.jpg)

I set up a time lapse camera in my living room to capture the accumulation. I'll have footage from that up later tonight or tomorrow morning.

## <time datetime="2014-01-21T17:50:00+00:00">January 21, 2014 12:50pm</time>

The snowfall has picked up, and so has the wind.

<video preload="metadata" controls="">
  <source src="/files/blog/snow-annapolis-21jan14/snowfall.mp4" type="video/mp4">
</video>

## <time datetime="2014-01-21T23:08:00+00:00">January 21, 2014 6:08pm</time>

Still very much in the thick of it.

![Weather radar](/files/blog/snow-annapolis-21jan14/radar01.png)

## <time datetime="2014-01-22T01:56:00+00:00">January 21, 2014 8:56pm</time>

I just got back from taking a bunch of pictures & videos around downtown Annapolis, which I'll be posting here shortly.

## <time datetime="2014-01-22T02:30:00+00:00">January 21, 2014 9:30pm</time>

I put together a time-lapse of the snow accumulation outside my apartment.

<video preload="metadata" controls="">
  <source src="/files/blog/snow-annapolis-21jan14/accumulation-time-lapse.mp4" type="video/mp4">
</video>

## <time datetime="2014-01-22T15:31:00+00:00">January 22, 2014 10:31am</time>

My commute to work this morning is a perfect example of why companies should be shifting to telecommuting.