---
title: Snow of the Titans
date: 2014-03-03T04:31:01.000Z
slug: winter-storm-titan
image:
  src: /files/blog/winter-storm-titan/accumulation04.jpg
  alt: Winter Storm Titan snow accumulation
tags:
  - liveblog
  - snow
  - winter storm titan
---

This is without doubt the [snowiest](/blog/first-snow-of-2014/) [winter](/blog/snow-annapolis-21jan14/) in recent memory. Yet the snow we've received thus far hasn't been more than a few inches each time, so while it's enough to slow things down in the DC-Baltimore region, it's not sizeable by any stretch.

This time, we're expecting [anywhere between 6 and 14 inches from Winter Storm Titan](http://www.wunderground.com/news/winter-storm-titan-snow-ice-rain-totals-20140302), depending on who you ask. For us in the Annapolis area, it's expected to be closer to the upper end of that estimate.

As usual, I'll be live blogging during the storm, which will hopefully produce enough snow to make it fun!

I'd also recommend following local news outlets for information during the storm, such as [Eye on Annapolis](http://www.eyeonannapolis.net/2014/03/02/anne-arundel-snowstorm-titan-live-blog/).

# Liveblog

## <time datetime="2014-03-03T04:41:00+00:00">March 2, 2014 11:41pm</time>

Just to start this off, before there's even snow falling: as much as the weather geek in me enjoys snow, I'm really ready for this nonsense to be over and reliably warm weather to make an entrance.

## <time datetime="2014-03-03T04:44:00+00:00">March 2, 2014 11:44pm</time>

The City of Annapolis [opened all four city garages to residents](http://www.annapolis.gov/government/headlines/2014/03/02/city-garages-open-for-residents-to-park-at-5-p.m.-today-as-city-prepares-for-storm) for the duration of the storm, free of charge.

## <time datetime="2014-03-03T05:32:00+00:00">March 3, 2014 12:32am</time>

Winter Storm Warning started at midnight, but so far just rain.

## <time datetime="2014-03-03T07:07:00+00:00">March 3, 2014 2:07am</time>

Apparently this storm has a name: Winter Storm Titan. With a name like that, I'm expecting Michael Bay levels of snowpocalypse.

I'm updating the non-live portions of this post to reflect the storm's name.

## <time datetime="2014-03-03T08:26:00+00:00">March 3, 2014 3:26am</time>

The ice that was forecast has started to accumulate. Snow will doubtless follow shortly.

![Ice on the walkway](/files/blog/winter-storm-titan/ice01.jpg)

![Ice on the walkway](/files/blog/winter-storm-titan/ice02.jpg)

## <time datetime="2014-03-03T08:28:00+00:00">March 3, 2014 3:28am</time>

Ok, off to bed. More to follow when I wake up.

## <time datetime="2014-03-03T13:08:00+00:00">March 3, 2014 8:08am</time>

Good morning! We've got steady snowfall and accumulation.

![Snow accumulation](/files/blog/winter-storm-titan/accumulation01.jpg)

## <time datetime="2014-03-03T17:50:00+00:00">March 3, 2014 12:50pm</time>

I'm having a hard time buying the latest "6-10 inches of snow" prediction considering we have about 3″ right now:

![Snow accumulation](/files/blog/winter-storm-titan/accumulation02.jpg)

And this storm doesn't look like it'll be over us for too much longer.

![Weather radar](/files/blog/winter-storm-titan/radar01.png)

## <time datetime="2014-03-03T19:01:00+00:00">March 3, 2014 2:01pm</time>

We've still got steady snowfall, but it should be tapering off soon. It looks like we've got about 4-5 inches at this point.

![Snow accumulation](/files/blog/winter-storm-titan/accumulation03.jpg)

![Snow accumulation](/files/blog/winter-storm-titan/accumulation04.jpg)

## <time datetime="2014-03-03T20:02:00+00:00">March 3, 2014 3:02pm</time>

Well it looks like that's a wrap, ladies and gentlemen. So much for Titan living up to its name.

![Snow accumulation](/files/blog/winter-storm-titan/accumulation05.jpg)

![Snow accumulation](/files/blog/winter-storm-titan/accumulation06.jpg)

![Snow accumulation](/files/blog/winter-storm-titan/accumulation07.jpg)

## <time datetime="2014-03-03T23:34:00+00:00">March 3, 2014 6:34pm</time>

The patio in the center of my building has a path shoveled, so it's a little easier to see the snow depth now.

![Snow accumulation](/files/blog/winter-storm-titan/accumulation08.jpg)

## <time datetime="2014-03-04T00:22:00+00:00">March 3, 2014 7:22pm</time>

I made a time-lapse!

{{< youtube _o8L3uH5dsE >}}
