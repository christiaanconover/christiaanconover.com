---
title: 'DEF[x] Annapolis: Encourage the Innovators'
description: "Let's give the innovative thinkers the tools and encouragement they need to make the military better."
date: 2014-03-14T16:42:29.000Z
slug: defx-annapolis-encourage-the-innovators
image:
  src: /files/blog/defx-annapolis-encourage-the-innovators/defxsailboat.jpg
  alt: 'DEF[x] sailboat'

tags:
  - defense entrepreneurs forum
  - defx
  - defx Annapolis
  - innovation
---

*This post [originally appeared on USNI Blog](http://blog.usni.org/2014/03/07/defx-annapolis-encourage-the-innovators). I'm posting it here for my archives.*

The [Defense Entrepreneurs Forum](http://defenseentrepreneurs.org/) held their first locally organized event this past Saturday, called [DEF[x] Annapolis](http://defenseentrepreneurs.org/annapolis/) (think [TEDx](https://www.ted.com/tedx) vs TED). Organized by midshipmen at the Naval Academy, the goal was to bring together a group of people from around the region interested in furthering the discussion of innovation and disruption within the military.

This was the second DEF event, the [inaugural conference](http://blog.usni.org/2013/10/15/def-2013-verbing-ideas) having been held this past October in Chicago. Their format tries to emulate some of the lessons of TED, such as restricting speakers to a 20-30 minute window (including Q&A time) and bringing in people with a variety of experiences and perspectives. I was not at DEF in Chicago, so this was my first exposure to the DEF group.

There were a few major themes running throughout the speakers' talks: how private industry can help the military innovate, that the military is resistant to change and innovation, and how military service can prepare you (or not) to be an entrepreneur. Most of the speakers were currently serving, or had at some point served, in the military and were in various stages of starting their own venture. They shared great lessons from their experiences both as military officers and as entrepreneurs. I'm not going to go into detail about what they said, because that's not the focus of this post, and because (once the videos are online I'll update this post with a link) you can hear them in their own words.

What struck me as largely absent from the conversation, and I'm not the only one who noticed this, was discussion about how to foster innovation from within the military - not just from the outside in via startups. Being a software developer and someone who appreciates the value of an outside disruptor to force change in an industry, I wasn't terribly bothered by this absence. I noticed a lack of this type of discussion simply due to the nature of the event. BJ Armstrong rightfully raised the question though, both on Twitter and out loud during a session.

{{< tweet 439803118910709761 >}}

It's a valid concern, and it got me thinking: why is there such a conspicuous lack of discussion, and (from where I'm sitting) a general lack of interest, about spurring innovation from the inside? Does it have to do with the type of person to whom this kind of thinking and iterating appeals? Is it a symptom of a culture of "shut up, do as you're told, and don't make waves" that persists inside the military? Perhaps it's a combination of those factors?

I'm a lowly BM3, and a reservist at that, so my exposure to this type of thinking is far more limited than the members who are pushing this discussion further into the sunlight. My sense is that while the problem is probably a combination of the above factors, the scales tip further in the direction of a change-resistant culture. Perhaps more specifically, it's the _perception_ of the military at large being innovation-averse. The DEF[x] speakers are a perfect example: they saw something they felt was fundamentally wrong within the military, and they set out to correct it - by setting up their own company, not by working inside the system to push for change. Some of them may have been driven primarily by business opportunity, which is perfectly acceptable, but the sense I get is that most of them were genuinely interested in solving a problem for the betterment of the service.

My takeaway from DEF[x] was not that the answer to fixing the military's problems lies in startups. What I took was that the biggest problem for innovation lies not with a lack of smart people with good ideas, but a lack of opportunity for those people to execute on those ideas. Innovation is alive and well in the minds of those who see a better way forward, but we need to encourage them to voice those thoughts and experiment. [CRIC](http://blog.usni.org/tag/cric) is a great idea, but it needs to go from one small group to a service-wide program that reaches down to the smallest unit level. Give the smart, creative thinkers the tools they need to improve the service they love, starting with a willingness to listen.