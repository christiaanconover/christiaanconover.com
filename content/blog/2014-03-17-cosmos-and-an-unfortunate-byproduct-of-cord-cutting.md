---
title: 'Cosmos, and an Unfortunate Byproduct of Cord-Cutting'
description: "Cord-cutting means losing access to some content. It also ends up meaning that you assume you can't watch new stuff, and ignoring it."
date: 2014-03-17T13:26:24.000Z
slug: cosmos-and-an-unfortunate-byproduct-of-cord-cutting
image:
  src: /files/blog/cosmos-and-an-unfortunate-byproduct-of-cord-cutting/cord-cutting.png
  alt: Cord-cutting

tags:
  - aereo
  - cord-cutting
  - cosmos
---

Last summer I canceled my cable TV subscription, and went 100% Internet. In fact, I'd been using Internet services nearly exclusively for my content consumption for many months up to that point, but other circumstances required keeping my cable TV. Despite pretty much entirely ignoring cable programming I still had the mentality that, if something came along on TV that I really wanted to watch, I still could. Once I dropped cable that option went away.

I signed up for the [Aereo](http://aereo.com) beta for Baltimore when it became available, which conveniently was right in the heart of football season. It immediately provided benefit to me, since I realized that most of the TV shows I followed were on broadcast networks, so I wasn't missing much. Yet, I still thought of network TV shows as largely outside the scope of things I had access to, since broadcast only covers a fraction of the programming available. I adopted a mentality of "unavailable until proven otherwise" about television.

So when [Cosmos](https://en.wikipedia.org/wiki/Cosmos:_A_Spacetime_Odyssey) was announced I didn't pay it much mind. It sounded intriguing, but I assumed it would be on a network I couldn't watch. It was only after the series premiere aired and received massive attention across the Internet that I bothered to check whether I could get it, and only then discovered that it was, in fact, on broadcast television. Fantastic! A pleasant, unexpected surprise.

I'm not complaining that everything ever made isn't available at my fingertips whenever & wherever. I'm enough of a realist to understand this will probably never happen, and I know that cord-cutting means compromising on what's available to you. Yet the trend seems to be [moving toward ditching cable and satellite](http://www.digitaltrends.com/home-theater/morgan-stanley-report-us-tv-subscribers-to-cut-cord-2014/) TV subscriptions, and the entertainment industry seems to be (slowly and begrudgingly) moving to accommodate this shift. In the mean time, those of us who have given up on paying for cable TV will be left assuming that the new stuff is out of our reach, and ignore it - until it shows up on Netflix.
