---
title: 'My Love/Hate Relationship with "Read it Later" Apps'
description: I love Pocket. I hate my Pocket queue.
date: 2014-03-18T15:49:37.000Z
slug: my-lovehate-relationship-with-read-it-later-apps
image:
  src: /files/blog/my-lovehate-relationship-with-read-it-later-apps/abandoned-library.jpg
  alt: Abandoned library

tags:
  - pocket
  - read it later
---

I read a lot. Not many books ([though more than I used to](/blog/print-books-ebooks-sharing/)), but a ton of articles and blog posts.

Actually, let me rephrase that: I have a lot of articles _in my reading queue_. I do, in fact, read plenty of things on a daily basis, but the majority of interesting posts I come across get sent to [Pocket](http://getpocket.com/), so I can read them later that day when I have some down time. It makes sense: I stand a much better chance of thoroughly reading, understanding and enjoying something when I can give it my undivided attention, rather than skimming it for thirty seconds while I'm standing in the checkout line at the grocery store. What an awesome idea! I can pick and choose the stuff I really care about, and consume it when I'm ready. In practice, however, it doesn't quite work out that way.

The problem is two-fold: first, I encounter a ton of good stuff that I want to read. If I had to guess, I'd say I find between 30 and 50 posts on a daily basis I think are deserving of my time. About half of those I open immediately, skim through the first paragraph or so, and decide whether to continue on/add them to Pocket. The other half end up going straight to Pocket without any vetting. It's fantastic that there are so many smart people sharing worthwhile ideas that are readily available for consumption. You can have too much of a good thing though, so moderation is important. But when I've come across something neat that I want to hang on to, I just want to save it and worry about moderating my consumption later.

Which brings me to the other problem. Pocket and its ilk encourage procrastination, which begets further procrastination. If I've put myself in the mindset that it's acceptable - beneficial, even - to put off reading something until later, then I've given myself permission to put it off indefinitely. That's exactly what has happened to me with Pocket, as my queue is now over 100 items. What good is that doing me? I'm never going to sit down and read all those articles, and the idea of sifting through the ones I _really_ want to read, and ones that just looked neat at the time, is daunting. Now I've come full circle, and turned a tool that's supposed to let me better manage my time into another thing on my to-do list.

I certainly deserve most of the blame for my packrat usage of Pocket, but I can't imagine I'm alone in this. I love Pocket; it's a great solution for a self-curated reading list. It's my Pocket queue that frustrates me.
