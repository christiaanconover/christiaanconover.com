---
title: Technology We Love to Hate
description: Why are we as a society so determined to be barriers to technology itself instead of contributing constructively to its responsible development?
date: 2014-03-25T19:59:39.000Z
slug: technology-we-love-to-hate
image:
  src: /files/blog/technology-we-love-to-hate/technophobia.jpg
  alt: Technophobia

tags:
  - lockitron
  - nih
  - robotics
  - technophobia
---

I am amazed by the frequency with which I see the announcement of a technological innovation followed immediately by a barrage of naysayers proclaiming all manner of ways this new technology is bad for humanity, society, their national population, or what have you.

Let's take a recent example. A friend of mine posted this video on Facebook about a robot at the National Institute of Health:

<iframe width="853" height="480" src="https://www.youtube.com/embed/ra0e97Wiqds" frameborder="0" allowfullscreen="">
</iframe>

In case you didn't bother to watch the video, here's the major takeaway: the robot can do in one week what would take a scientist working 8 hours a day, 7 days a week for 12 years. _One week_. The technology itself isn't new or revolutionary, it's just implementing existing systems (primarily an assembly line robot) in a new and unique way. The implications of that new capability, however, could be staggering. "Surely," you might think, "everyone in the world is in favor of this!"

Apparently not. The first comment my friend's post received came from somebody bemoaning this as a job-killer. What this person took away from the video was not the potential for curing diseases and saving untold lives, but that somebody might not be paid to do a job that, very obviously, could be automated. Oh, and let's not forget that in this instance the now-unpaid person is somebody who has enough education and intelligence that they very certainly ought to be spending their time working on more challenging problems than performing repetitive test patterns.

This is just one of a myriad of instances when I've encountered this sort of attitude. Whether it be something as mundane as a [WiFi-connected deadbolt](https://lockitron.com/) or a self-driving car, I have to work harder to find someone excited about it than I do to find ten people who are all too happy to point out the problems they see with it.

Why do we love to hate technology?

Fear of the unknown is the most oft-used example that's trotted out, and while it's an understandable reaction (to a point), it's also a cop-out. If human beings had allowed fear to prevent every technological innovation in history, we'd still be nomadic hunter-gatherers with a life expectancy of 20 years. We need technology to move our species forward. It's a critical part of how we have evolved. In fact, people in the developed world depend on technology in various forms for nearly everything in their daily lives, and their lives are better for it. In this era of unprecedented knowledge and scientific discovery, combined with that knowledge being more readily accessible than ever before, there's no excuse for people to allow fear of the unknown to drive their decisions.

Fear of technology as a jobs killer is short-sighted, and fighting progress to favor legacy processes is irrational and a losing battle. Jobs, first and foremost, are about money. More specifically, they're about the balance of money - the money being made by the employee balanced against the money made from that employee's work by the employer. If an employee (or multiple employees) can be replaced by a cheaper, more reliable machine then it's absolutely going to happen - it's only a matter of time. That's just smart business. What's more, we as a society should be embracing this opportunity to apply brain power to more challenging problems than the tasks that can be done by machines. Rather than lamenting the loss of jobs that keep people stuck in dangerous or repetitive positions, we should be giving people the knowledge they need to tackle tough, intellectually challenging issues. Giving any job we can to a machine instead of a person is an opportunity for the long term that outweighs the loss in the short term.

I'm not arguing that fear should be suppressed and ignored. Fear is an important part of the conversation when it's addressed in rational, objective terms. A knee-jerk reaction for cynicism benefits nobody, and only ends up putting everyone on the defensive. Constructive criticism and perspective, offered for consideration in a larger discussion about ethical and responsible innovation, benefits everyone.

The problem, I think, boils down to ignorance. Instead of tolerating, and to some degree celebrating, ignorance about the countless innovations upon which we depend, we should be dismissing uneducated fear mongering and expecting a higher degree of understanding about the world around us from the populace. It should be socially unacceptable to proudly proclaim, "I don't know anything about this computer box thingy, I just want my email damnit!" That's not the mark of a powerful society, it's a sign of one that's determined to be left behind.

Now bring the flames, I've got my bunker gear.
