---
title: Death to the Email Signature
description: Email signatures are the truck nuts of the Internet.
date: 2014-04-23T02:52:30.000Z
slug: death-to-the-email-signature
image:
  src: /files/blog/death-to-the-email-signature/facepalm.jpg
  alt: Picard facepalm

tags:
  - email signature
  - rant
---

I'm not going to use an email signature anymore. It's an arcane practice that provides no real value to anyone.

Let's examine [what a signature actually is](https://en.wikipedia.org/wiki/Signature). The signature is a form of identity verification, whereby the signatory validates that they are, in fact, the person they claim to be by putting pen to paper and swirling their hand around to form squiggles that they alone are the best at creating. When signatures were first used, there weren't many options to prove your identity to somebody with whom you weren't face-to-face. Not only does the "signature" block in an email not actually contain a signature, it provides no actual identity validation. There are a number of well established protocols available to authenticate email messages that are all far more secure and reliable than an arbitrary drawing. In fact, the most important aspect of a signature - conveying who the signatory is - already gets handled in email headers using, at minimum, the sender's email address but usually also includes their name. You know exactly who sent the email before you even open it.

Does it provide that "personal touch" for the recipient? Not in the slightest. Anybody who uses a signature block has it set up in their email client to automatically insert at the end of every single email they send. It's the same exact signature block every time, and zero thought is put into it after it's set up. When you receive a hand-written letter in the mail, every element on that page that came from the sender's pen was thought out and required effort on their part. It feels personal because it is personal. An email signature block feels like superfluous, impersonal garbage because that's what it is.

> "It's great for making sure the other person has all my contact info, my awe-inspiring job title, my logo, my company's disclaimer and terms of service, an inspirational quote, the forty web addresses I want people to visit, and how I take my coffee!"

> _- That annoying person whose signature block is a cafeteria casserole of clichés_

Technically that's correct, but it's also bullshit. It stands to reason that if I know you well enough to be maintaining correspondence, I know the other relevant details about you that you're throwing at the bottom of every email. If I don't know that information, I can ask you for it when it becomes relevant. I won't be looking at that chunk of text with gratitude for your considerate information overload because you've saved me from having to go through the ordeal of asking you for your phone number and address when we're arranging to having a meeting at your office. I either have the information already, or I'll ask you for it when I need it. Otherwise, I don't care.

> "How else will people know that they've reached the end of my message?"

> _- Person using the Internet for the first time ever_

Once again, thank you for your consideration. If I didn't run face-first into that overwrought, self-indulgent bunch of space-wasting that you call a signature I might never you know you were done typing. Nevermind the fact that there's nothing left to read and I can't scroll anymore, or that an email won't be delivered unless the entire message is received at the other end. I'm just going to sit here drooling on myself and hoping the rest of the message will appear some day. However, if you insist on holding my hand through to the end of the email, the signature block will definitely signal the stopping point, because I'll know there's no need to read whatever drivel is hanging like a dingleberry at the bottom of the page.

That typo-filled/vastly inadequate/lazy email you just dashed off to me was "sent from your iPhone?" Fun fact: that doesn't excuse your terrible email etiquette. Proof read your email, provide a real response (or tell me you'll get back to me later), or don't bother at all. A crap email is worse than no email, and throwing out automated excuses makes you look like an even bigger fool.

Here's the one thing email signatures are really good for: screwing up automated systems that handle email as an input. Does your company have a project management system with a messaging tool you can post to via email? How awesome that your email signature can add 8 additional lines of distracting cruft to the sentence or two you actually wanted to contribute to the conversation.

Email signatures are beyond useless - they're annoying and wasteful. Let's all stop pretending our emails are amazing pieces of literature that require their own credits. I either already know who you are, or I don't care.
