---
title: "The FCC's Proposal for an Internet \"Slow Lane\" and Why You Should Care"
date: 2014-05-13T15:29:04.000Z
slug: the-fccs-proposal-for-an-internet-slow-lane-and-why-you-should-care
image:
  src: /files/blog/the-fccs-proposal-for-an-internet-slow-lane-and-why-you-should-care/fcc-net-neutrality.jpg
  alt: Net neutrality

tags:
  - fcc
  - net neutrality
  - tom wheeler
---

FCC Chairman and former cable industry lobbyist [Tom Wheeler](https://en.wikipedia.org/wiki/Tom_Wheeler) has proposed new regulations for the Internet that would [allow Internet Service Providers to charge more to site operators and content providers in order to get faster throughput to end users](http://www.theguardian.com/technology/2014/apr/28/internet-service-providers-charging-premium-access), and slow traffic to any sites and services that don't pay up.

If these rules go through, it will break the current model of the Internet as an open, unfiltered platform for communication. [We've seen this sort of thing before](/blog/todays-net-neutrality-decision-screws-us-all/), but this time the risk is much greater because it will be explicitly legal for ISPs to charge providers for access to their customers. ISPs shouldn't have the right to decide which businesses succeed or fail, or to [censor sites that can't pay](http://boingboing.net/2014/04/28/gutting-net-neutrality-also-gu.html) their extortion racket. If Wheeler has his way though, that's exactly what will happen.

We all need to [fight this](http://boingboing.net/2014/05/13/how-to-fight-for-net-neutralit.html). Go to [Stop The Slow Lane](http://www.stoptheslowlane.com/), and share this message wherever you can. Contact your Congress Critter and tell them to take action against these rules. The Internet is too critical to our lives and our future to let it be torn apart by greedy executives for a quick buck.
