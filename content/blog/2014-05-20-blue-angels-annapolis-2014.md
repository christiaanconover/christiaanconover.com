---
title: Blue Angels in Annapolis for the 2014 Naval Academy Commissioning
description: "They're baaaaaack!"
date: 2014-05-20T16:15:44.000Z
slug: blue-angels-annapolis-2014
image:
  src: /files/blog/blue-angels-annapolis-2014/blue-angels-annapolis.jpg
  alt: Blue Angels in Annapolis

tags:
  - annapolis
  - blue angels
  - us naval academy
---

They're back! After a three year hiatus ([they last flew here in 2010](/blog/blue-angels-live-streaming-this-afternoon)), the tradition of the Blue Angels doing a show for Commissioning Week has been revived. Today (Tuesday) is practice day and tomorrow is the actual show.

I'll be posting pictures and videos here of the Blue Angels over the next couple of days. Stay tuned!

# Updates

## <time>May 21, 2014 5:19pm</time>

Highlights from the show.

{{< youtube mzCl6CTh3EY >}}

## <time>May 20, 2014 12:29pm</time>

Video of the Blue Angels practicing.

<video controls="">
  <source src="/files/blog/blue-angels-annapolis-2014/blue-angels-practice.mp4" type="video/mp4">
</video>