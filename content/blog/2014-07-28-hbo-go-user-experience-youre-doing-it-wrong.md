---
title: "HBO Go: User Experience, You're Doing it Wrong"
description: 'If your streaming service is frustrating to use, your content will suffer.'
date: 2014-07-28T13:46:17.000Z
slug: hbo-go-user-experience-youre-doing-it-wrong
image:
  src: /files/blog/hbo-go-user-experience-youre-doing-it-wrong/hbogo-android.png
  alt: HBO Go on Android

tags:
  - hbo
  - hbo go
  - netflix
  - streaming
---

I recently modified my Comcast subscription to add cable TV - almost entirely so I could get HBO. That of course means I have access to HBO Go, by far the most appealing addition as a result of the change. As soon as I was able, I signed into HBO Go on my phone and started watching [The Newsroom](http://www.imdb.com/title/tt1870479/) (which I highly recommend, by the way).

My excitement quickly waned the longer I dealt with HBO Go's frustrating user experience. Finding a show is easy enough - just search for its name - but I then have to dig for the episode I'm looking for. What's worse, I have to remember for myself which episode I left off on, and hope that it might remember where in that episode I was. The app acts more like the horrendous On Demand menu on my cable box than an easy-to-use video streaming platform. It's the same on their website too.

Contrast this with Netflix, which puts my recently watched items at the top, and all I have to do is tap the one I want to watch and it starts playing _exactly_ where I left off, synced across all my devices. Everything about the user experience is geared toward getting out of my way and get to what I want. HBO Go seems to have taken the opposite approach, putting many layers of work between me and my viewing experience.

HBO has great content. In fact, their stuff is the only reason [I gave in](/blog/cosmos-and-an-unfortunate-byproduct-of-cord-cutting/) and got a cable TV subscription again. Their platform is annoying enough, though, that when I go back to Netflix after using HBO it's like a breath of fresh air.
