---
title: Too Soon
description: "\"Too soon\" is not a real thing. There's no statute of limitations on humor."
date: 2014-08-08T16:42:02.000Z
slug: too-soon
image:
  src: /files/blog/too-soon/haha.jpg
  alt: Nelson from the Simpsons

tags:
  - rant
  - too soon
---

We've all heard it. Somebody makes a joke that riffs off a recent occurrence, and someone else says "too soon" to cover their offense at the joke and/or shame at laughing.

I'm of the opinion that "too soon" isn't a real thing. Something is either funny right away, or it's never funny. When a person says a joke is "too soon" what they really mean is, "I'm offended but won't admit it." If you're offended by a joke, either speak up about being offended, or shut up and live with it.

September 11th? Never funny. [Terry Schiavo](https://en.wikipedia.org/wiki/Terri_Schiavo_case)? Say what you will, but the "state vegetable of Florida" jokes were funny from day one.
