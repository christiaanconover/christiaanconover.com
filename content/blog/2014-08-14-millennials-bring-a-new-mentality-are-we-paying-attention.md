---
title: 'Millennials Bring a New Mentality: Are We Paying Attention?'
date: 2014-08-14T13:20:34.000Z
slug: millennials-bring-a-new-mentality-are-we-paying-attention
image:
  src: /files/blog/millennials-bring-a-new-mentality-are-we-paying-attention/commandant-with-the-millenials.jpg
  alt: 'Coast Guard Commandant with "the millenials"'

tags:
  - leadership
  - military
  - millennials
  - us coast guard
---

The August 2014 issue of _Proceedings_ contains an op-ed titled "[Millennials Bring a New Mentality: Does it Fit?](http://www.usni.org/magazines/proceedings/2014-08/now-hear-millennials-bring-new-mentality-does-it-fit)" written by a CDR Darcie Cunningham, USCG. She makes the argument that the [Millennial generation](https://en.wikipedia.org/wiki/Millennials) doesn't respect tradition and authority, is only willing to do the bare minimum required, expects constant praise and promotions on their own schedule, and generally need a course correction to fit inside the military structure. For a point-by-point analysis of her article, I suggest you read [LT Matt Hipple's fantastic post](http://cimsec.org/is-there-a-military-millennial-problem-twelve-responses-to-cdr-darcie-cunningham/12481).

I generally avoid addressing these sorts of generational abstractions as they usually serve little purpose beyond instigating a shouting match. The accusations leveled here against Millennials as a whole, however, have compelled me to respond. As long as we're talking in generalizations, I'll point out a few of my own.

With the exception of those at either end of the generation's age range, most Millennials have a unique perspective resulting from having never served in the military in a time of peace, while at the same time having spent a considerable portion of their formative years in a pre-9/11 world. Those in uniform have served in the longest ongoing conflict in U.S. history, with no expectation of returning to a true peacetime status during their career.

This generation is [often included in the term "digital native"](https://en.wikipedia.org/wiki/Digital_native) and has an expectation that technology should be used to simplify processes and make information more accessible.

Millennials [tend to be socially-minded activists](http://mic.com/articles/30658/are-millennials-better-activists-than-baby-boomers) who push for change where they see something broken. This is a generation that sees injustice and malfeasance as a call to action - exactly the sort of mind the military should be encouraging and putting to work.

Generational friction is nothing new, and I'm sure the military leadership had the same complaints when CDR Cunningham's generation were the new kids on the block. [CDR Salamander pointed out](http://cdrsalamander.blogspot.com/2014/08/pro-tip-millenials-are-not-your-problem.html) that many of her issues have been heard for decades, and are not unique to Millennials.

> Texting is becoming the primary mode of communication. It has already become a means of jumping the chain of command as a condoned communication tool.

You could replace "texting" with email, telephone, or any other disruptive tool for communication and find an example of somebody saying that at some point in history. Texting is just a more efficient tool for communicating, not the root cause of any disregard for chain of command. With a limit of 160 characters I wouldn't expect the complement of greetings and salutations that are standard in an email or letter. They're not texting out of disrespect, they're texting so they can get their job done faster and better. I'm sure they've found it to be much more effective than relying on the exercise in frustration we know as the Coast Guard Standard Workstation III.

> First, we must educate them on the importance of patience in our systems. Promotion and advancement boards are designed to ensure that members are not advanced until they have the maturity and experience demanded of good leaders. Waiting one more year will only serve them better in the end, as they'll be more equipped to take on increased levels of responsibility. If this doesn't sit well with a young member, he or she should be subtly reminded of the current economy and associated unemployment rate.

I doubt that telling a junior member, "suck it up and deal with it - besides, you won't do any better as a civilian" will instill confidence in, or foster respect for, senior leadership. If we want to keep the right people - as has been discussed in countless articles, conferences and forums - we should probably avoid pointing out that the civilian market has a bias towards efficiency and speed. Once these members start looking at their options, they may decide their talents will be better applied elsewhere. A perception of bureaucracy for the sake of it is a powerful way to crush the motivation of those upon whom you depend. Oh, and wielding a poor economy as your stick? [That may not be the best strategy these days](http://online.wsj.com/articles/u-s-jobs-report-unemployment-falls-to-6-1-as-288-000-positions-added-1404390904).

Millennials, like every up-and-coming generation prior, have a lot to offer to the military. The best thing we can do is not attempt to force them into a cookie-cutter image of the last generation, but give them the tools they need to help move their services forward, and listen to what they have to say.
