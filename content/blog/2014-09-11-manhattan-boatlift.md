---
title: Manhattan Boatlift
description: The largest-ever maritime rescue effort started with a bunch of working boats in New York harbor taking it upon themselves to help whoever they could.
date: 2014-09-11T13:32:03.000Z
slug: manhattan-boatlift
image:
  src: /files/blog/manhattan-boatlift/boatlift.jpg
  alt: Manhattan Boatlift

tags:
  - manhattan boatlift
  - september 11
  - us coast guard
---

On this 13th anniversary of the September 11 terrorist attacks, I'd like to share a story I learned about recently.

During the course of that fateful day, boat captains from around New York harbor headed toward Manhattan to help however they could. Seeing the influx of volunteer boats and the number of people trying to get off the island, the Coast Guard organized what became the largest maritime rescue effort in history. Nearly half a million people were carried off of the island of Manhattan by vessels of all shapes and sizes, all in the span of less than nine hours. The video below, narrated by Tom Hanks, does a great job of telling the story.

{{< youtube 18lsxFcDrjo >}}