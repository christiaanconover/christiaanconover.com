---
title: Elected Officials Should Talk About NFL Problems
description: 'If teams demand public funding, the public should demand a voice.'
date: 2014-09-16T18:33:07.000Z
slug: elected-officials-should-talk-about-nfl-problems
image:
  src: /files/blog/elected-officials-should-talk-about-nfl-problems/levis-stadium.jpg
  alt: "Levi's Stadium"

tags:
  - football
  - nfl
---

Like millions of other Americans, I am a [fan of professional football](/blog/afc-championship-2012-disappointing-outcome-amazing-experience/). I've bought into the excitement and drama, and I ride the emotional roller coaster of watching every game my team plays. I've spent more money than I'd like to admit on being part of the football experience. It's [the only part of fall that doesn't suck](/blog/fall-is-the-monday-of-seasons/).

In the past few years there have been a string of high-profile incidents regarding unethical, immoral, and [in some cases illegal](https://en.wikipedia.org/wiki/Aaron_Hernandez#2012_Boston_double_homicide), conduct by NFL players. In each case, politicians have voiced their opinions about the situation and what action should be taken. In the past few weeks we've heard from [Minnesota governor Mark Dayton about Adrian Peterson hitting a child](http://www.sbnation.com/nfl/2014/9/16/6232527/adrian-peterson-suspension-child-abuse-minnesota-governor-statement-vikings), and [President Obama about Ray Rice knocking his fiancé unconscious](http://www.politico.com/story/2014/09/president-barack-obama-ray-rice-video-110738.html). Really though, who cares what they say about the NFL?

I'm fine with elected officials weighing in on NFL issues. It's not because the NFL is really that important to public policy or citizens' daily lives, because it isn't - it's entertainment pure and simple. However, [NFL teams demand hundreds of millions of dollars in public funding](http://www.theatlantic.com/magazine/archive/2013/10/how-the-nfl-fleeces-taxpayers/309448/) for stadiums, subsidies, and other taxpayer-funded incentives so that a city/region can be graced with the presence of their privately held corporation. It seems reasonable that if the public drops that much coin on an organization that's synonymous with the area with whom they identify, that their officials speak up when something happens that casts their locality in a poor light.

I'm not saying NFL teams should be owned by their local governments - they have no place as government entities. I am saying that if NFL teams insist on public funding, we as the public should insist on a seat at the table, or at least a share in the profits. Something tells me that no city in America would turn down some extra money for schools, or to help cover the burden on infrastructure and services from having an NFL team.
