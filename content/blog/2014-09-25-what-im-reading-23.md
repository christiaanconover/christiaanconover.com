---
title: "What I'm Reading"
description: 'The blight of Internet ads and a better way, health benefits of drinking, the problems surrounding civil forfeiture, and more.'
date: 2014-09-25T18:23:46.000Z
slug: what-im-reading-23
image:
  src: /files/blog/what-im-reading-23/mall-at-umd.jpg
  alt: Mall at University of Maryland

tags:
  - anil dash
  - blogging
  - civil forfeiture
  - drinking
  - "what i'm reading"
---

It's [been a while](/blog/what-im-reading-22/) since I've done one of these, so I figured I'd have another go.

[**15 Lessons from 15 Years of Blogging**](http://dashes.com/anil/2014/09/15-lessons-from-15-years-of-blogging.html)<br>
Anil Dash, one of my favorite bloggers and all-around brilliant Internet statesman on the practicalities of blogging he's learned over the years.

[**Taken**](http://www.newyorker.com/magazine/2013/08/12/taken)<br>
The insidious, and largely unknown, policy of civil forfeiture allows police departments to strip people of their property without due process.

[**The Truth We Won't Admit: Drinking Is Healthy**](http://www.psmag.com/navigation/health-and-behavior/truth-wont-admit-drinking-healthy-87891/)<br>
Alcohol very likely gives most of us a higher life expectancy, even if we drink more than is recommended. So why does this information get buried?

[**The Internet's Original Sin**](http://www.theatlantic.com/technology/archive/2014/08/advertising-is-the-internets-original-sin/376041/?single_page=true)<br>
Advertising is the scourge of the Internet. It's time to build a better Web.

[**The Look Of Funny: How The Onion's Art Department Works**](http://www.fastcodesign.com/3033865/the-look-of-funny-how-the-onions-art-department-work)<br>
A look behind the curtain of the top satirical news outlet's graphics production.
