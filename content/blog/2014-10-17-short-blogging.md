---
title: Short Blogging
description: 'Short posts, more fun.'
date: 2014-10-17T13:19:02.000Z
slug: short-blogging

tags:
  - short stuff
---

Gina Trapani [posted yesterday](http://scribbling.net/2014/10/16/short-form-blogging/) about the concept of short-form blogging, in conjunction with Andy Baio's [post on "middling"](http://waxy.org/2014/10/middling/). Their conclusion (that short, to-the-point blog posts can be better) is one that [I came to a while back](/blog/shorter-posts-are-better/) as well, but I've done a poor job implementing it. I still get hung up on the need for blog posts to have lasting meaning, or make an impactful statement, or have measurable value to others. The problem is that, if I approach writing and blogging with that attitude, nothing will measure up and it will all feel too canned and polished. So I'm going to relax, and go back to writing for fun.

I agree with most of the blogging rules Gina put in her post. I don't know that I'll be able to take her "Simplify, simplify" axiom to the level that she is, but the sentiment behind it is spot-on: less is more. [Ryan Erickson](http://ryanerickson.com/) has been doing a great job of simple, concise posts. He's in the middle of a blogging challenge, but I don't sign on to those anymore because they feel too much like work, and that's not what I'm looking for.

Here's to short blogging/short-form blogging/middling - whatever you want to call it. Less pressure, fewer words, more fun. I'm in.
