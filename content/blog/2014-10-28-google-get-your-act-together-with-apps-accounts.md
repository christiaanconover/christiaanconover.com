---
title: 'Google, Get Your Act Together with Apps Accounts'
description: How are Apps accounts still second class citizens in the Google world?
date: 2014-10-28T14:02:00.000Z
slug: google-get-your-act-together-with-apps-accounts

tags:
  - google
  - google apps
  - inbox by gmail
---

If you're not a geek like me, you may have missed [Google's announcement of their new email management tool, Inbox](http://googleblog.blogspot.com/2014/10/an-inbox-that-works-for-you.html). As soon as I heard about it I signed up for an invite, and eagerly awaited my chance to try it. A way to manage my email that works like Google Now? Sign me up! A few days later, I got the email telling me it was my turn. Hooray! I downloaded the app, chose the account I wanted to use when it opened, and got slapped in the face with this message:

![Inbox by Gmail Apps Account Error](/files/blog/google-get-your-act-together-with-apps-accounts/inbox-apps-screenshot.png)

I understand that Inbox is a new tool, and not everything may work as intended right now. Account issues are nothing new with Google though. This same thing happened when Google+ launched a few years ago. If you have multiple accounts, you've likely run into the frustration of signing into more than one at a time that myself and [others](https://plus.google.com/+JeffJarvis/posts/2SrjmnByzzX) have had.

What's worse, Apps customers are paying Google for their accounts (with the exception of the small group whose free accounts are grandfathered in). They're spending their dollars every month to get access to Google's services using their own domain. Yet, when a new product launches that they can't use because they chose this path, they get penalized. That's bad customer service, no matter which way you cut it.

I love my Google Apps account, but this shouldn't be a hard problem for a company like Google to solve. This needs to be fixed, and soon.
