---
title: '30 Day Challenge: From Chrome to Firefox'
date: 2015-07-02T13:57:33.000Z
slug: 30-day-challenge-from-chrome-to-firefox
image:
  src: /files/blog/30-day-challenge-from-chrome-to-firefox/firefox-ubuntu.png
  alt: Firefox on Ubuntu

tags:
  - 30 day challenge
  - chrome
  - firefox
---

I've been a died-in-the-wool Chrome fan pretty much [since it was released](http://googleblog.blogspot.com/2008/09/fresh-take-on-browser.html). When it came out it was a breath of fresh air compared to the nightmare of Internet Explorer and the increasingly slow and bloated Firefox. Everything was so _fast_. The UI was so sparse and clean. It was such a departure from the way browsers had been set up.

In the years since, Chrome has grown in size and reach to a point where it's starting to feel a little heavy and, in some ways, losing its direction. By contrast, Firefox has been working to slim down and improve performance. I also prefer many of Firefox's security implementations, such as certificate revocation. Plus, there's the perennial tugging at my open source sensibilities that has [tended more toward Firefox recently](http://arstechnica.com/security/2015/06/not-ok-google-chromium-voice-extension-pulled-after-spying-concerns/).

So, I'm taking a page out of [Matt Cutts' book](https://www.mattcutts.com/blog/type/30-days/) and turning this into a 30 Day Challenge. For the next 30 days I'll be using Firefox as my default browser on both my computer and my phone. I'm excited to see how it stacks up these days against Chrome.
