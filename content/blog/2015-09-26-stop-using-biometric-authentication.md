---
title: "No, Fingerprint Login Isn't \"Better than Nothing\""
description: "Password gets leaked? Change it and move on. Fingerprint data gets stolen? Nothing you can do."
date: 2015-09-26T14:30:52.000Z
slug: stop-using-biometric-authentication
image:
  src: /files/blog/stop-using-biometric-authentication/no-more-fingerprints.jpg
  alt: No more fingerprints

tags:
  - 5th amendment
  - biometrics
  - cyber security
  - fingerprint
  - iphone
---

This week we learned that [5.6 million people's fingerprints were part of the stolen data](https://www.washingtonpost.com/news/the-switch/wp/2015/09/23/opm-now-says-more-than-five-million-fingerprints-compromised-in-breaches/) from OPM earlier this year. Samsung and HTC have come under fire for [their (atrocious) implementation of fingerprint authentication that left fingerprint data unprotected](http://www.popsci.com/how-samsung-and-htcs-fingerprint-security-was-hacked) on users' devices. Apple's Touch ID is arguably the most secure (and widely used) consumer fingerprint authentication system, but even it has [flaws that have been exploited](https://blog.lookout.com/blog/2013/09/23/why-i-hacked-apples-touchid-and-still-think-it-is-awesome/).

There's no such thing as bugless or unhackable software. NASA is one of the few organizations on the planet with the resources, time and requirements that can come close to achieving that goal, and I'd bet that even they discover bugs from time to time. With enough time and effort, any system created by man can be exploited. Given that, we should not be depending on our ability to keep secrets, but to render them obsolete and irrelevant once exposed. In other words, when sensitive information is lost, we need to be able to change it so the lost data is effectively useless.

{{< tweet 646811379312279552 >}}

If your password gets stolen, you can change it. If your fingerprints get stolen, you have no recourse. The importance of this distinction cannot be overstated. You have no way to prevent that fingerprint data from being used or shared without your consent, yet you are still responsible for anything attached to that data - after all, they're your fingerprints.

Consider the weight that fingerprints carry in our society. If you're ever arrested for a crime, your fingerprints can be used as physical evidence against you. If you apply for a job with any sort of security clearance requirement, your fingerprints will be taken and kept on file (hence the OPM's possession of fingerprint records in the first place). We place a level of trust and authority in our fingerprints which is never bestowed upon almost any other form of identification for the same reason they are so risky for casual use: fingerprints are inextricably linked to their owner.

Let's come back to the criminal justice angle for a moment. Your fingerprints aren't just useful to forensics technicians with powder and tape lifting oily prints off of smooth surfaces. If you stand accused of a crime in the United States, [you can be compelled to unlock your phone for the police if it's secured with your fingerprint - but not if you use a password or PIN](http://time.com/3558936/fingerprint-password-fifth-amendment/). A password is something you _know_, which is protected by the [5th Amendment](https://www.law.cornell.edu/wex/fifth_amendment). A fingerprint is something you _have_, which is physical evidence and therefore not protected. By opting for the convenience of tapping once instead of four times, you have waived your right not to self-incriminate if your phone contains damning evidence.

Passwords are a terrible form of authentication. They're annoying, hard to remember, and dependent upon somebody else to keep them safe. Fingerprints may be easier, but depending on them can be far costlier.
