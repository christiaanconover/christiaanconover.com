---
title: WordPress Needs to Take PHP Upgrades Seriously
description: The time is long past due for WordPress to move up its minimum PHP requirement.
date: 2016-03-05T18:11:41.000Z
slug: wordpress-needs-to-take-php-upgrades-seriously
image:
  src: /files/blog/wordpress-needs-to-take-php-upgrades-seriously/charge-ahead.jpg
  alt: Charge ahead
tags:
  - matt mullenweg
  - php
  - wordcamp
  - wordcamp us
  - wordcamp us 2015
  - wordpress
---

WordPress powers [25% of the web](http://venturebeat.com/2015/11/08/wordpress-now-powers-25-of-the-web/). It is arguably the most influential open source PHP project, and claims a massive community and developer base. **It's not handling PHP upgrades responsibly**.

This is not a new issue. The [push from the community](http://planetozh.com/blog/2014/01/why-wordpress-should-drop-php-5-2/) for WordPress to raise the minimum required version of PHP has been happening for years. It was brought up again with Matt Mullenweg at WordCamp US 2015\. [Take a look](https://youtu.be/PqTwwT0cvA0?t=1h34m55s).

Matt's response, at face value, makes sense. The burden is on hosting providers to upgrade PHP, not users. The problem is that when something goes wrong because of an unpatched PHP exploit, or a business loses a customer because the site is too slow, the users will be the ones who pay the penalty. To that end, WordPress has a responsibility to inform users and encourage them to lean on their host to upgrade PHP.

This has become an even bigger issue with this recent announcement:

{{< tweet 702227207767330816 >}}

WordPress plugin developers have been able to take advantage of features in later PHP releases within their own code, and add dependency checking inside their plugin's code to make sure the server had a compatible version of PHP. The only disadvantage here--and likely the one the .org plugin repository is trying to prevent--is that users on older PHP versions download these plugins, only to be told upon activation that they aren't compatible. However, this seems like further opportunity for the community to encourage users and hosting providers to upgrade PHP. Actively preventing developers from modernizing their codebase is further evidence of WordPress' irresponsible behavior around PHP version dependency.

In the video above, Mullenweg refers to "the last time" WordPress forced a PHP version upgrade. That indicates part of the problem: there isn't a standard in the WordPress community for upgrading the minimum required version of PHP. The PHP project [publishes an end-of-life schedule](https://secure.php.net/eol.php) for each PHP release. It's clearly documented the exact date after which a version will no longer be supported. WordPress should adopt a standard based off of PHP's schedule. Give everyone a specified window of time from when a version of PHP is end-of-life, after which the next release of WordPress will no longer support that deprecated version of PHP. For example, PHP 5.4 was end-of-life on September 3, 2015\. If the WordPress standard was 1 year of support for a release after it's end-of-life, then the next WordPress release after September 3, 2016 would require PHP 5.5 and later. Whatever that time interval is, it should be codified in WordPress' release cycle plan, so that everybody knows what it is, and can plan around it.

For a project of the size and impact of WordPress, the weight they carry can move the industry forward. WordPress is too big to ignore, so when they make a change the web reacts. With that kind of influence and power, it is not acceptable to let the lazy few hold back and put at risk the responsible majority.
