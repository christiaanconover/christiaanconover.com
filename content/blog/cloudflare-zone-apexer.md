---
title: "Protecting Sites When Using CloudFlare's CNAME Setup"
description: "How to ensure you're not leaving any gaps in security when using a partial CloudFlare implementation."
slug: "cloudflare-zone-apexer"
date: 2018-06-01T09:48:42-04:00
tags:
    - cloudflare
    - cyber security
---
In my current job I work with a number of clients who want CloudFlare in front of their sites, but are unable or unwilling to make CloudFlare their [authoritative DNS provider](https://www.dnsknowledge.com/whatis/authoritative-name-server/). CloudFlare supports this, through something they call [CNAME Setup](https://support.cloudflare.com/hc/en-us/articles/200168706-How-do-I-do-CNAME-setup-). Basically, every subdomain you'd like protected by CloudFlare is configured with a `CNAME` record in your authoritative DNS to point to a CloudFlare subdomain, which then routes through CloudFlare to your site.

This is great, and it works well. If you're reading carefully though, you'll notice I explicitly said "every *subdomain* you'd like to protect." Due to how the DNS specification is written, you cannot have a `CNAME` record for the top level record of a domain (known as the zone apex). The reasoning is sound: a `CNAME` record is an alias; it points to somewhere else. No matter how the rest of a domain's DNS is configured, you at least want to make sure that the zone apex points to an actual IP address and is directly routable. Therefore, no `CNAME` records at the zone apex. When using CloudFlare CNAME Setup, this potentially leaves a chink in the armor.

## The Problem
A key part of how CloudFlare is able to ensure your site is protected is that they mask your server's IP address, and instead show their network's IP addresses for your site's DNS. This means that no matter what layer an adversary attempts to use to attack your site, they'll just be hitting a CloudFlare server.

However, if you use CNAME Setup, the IP address for your domain's zone apex is exposed. Not ideal if the IP of the zone apex is your actual origin server you're trying to protect.

## The Solution
We instead need an IP address that is _different_ from our origin server to leave exposed. To do this we'll use a cheap cloud server from DigitalOcean, Vultr, AWS, etc. - whoever is your preferred cloud provider.

The only function this server will have is to redirect all HTTP traffic from your domain's zone apex to the `www` subdomain. If you're using NGINX, the virtual host configuration looks like this:

```nginx
server {
    server_name example.com;
    listen 80;
    listen [::]:80;

    return 301 http://www.example.com$request_uri;
}
```

_This is assuming that `www` is the subdomain where your site lives._

That's it! That's all you need.

Configure your DNS to use the IP of this new server as your zone apex `A` record (and `AAAA` if you have IPv6 support). You're all set! If you get targeted for a DDoS or other attack, the only IP that is exposed is one that only provides a redirect to your CloudFlare-protected site.

At our office we've started referring to this type of server as a "zone apexer" since its only function is to handle unprotected zone apex traffic. It's a use case we encounter frequently enough that it warrants its own shorthand.

It's a bit of a hack, admittedly, and this new server can be taken offline if it is subject to a DDoS since it's not protected. However, since that server doesn't host your actual site, the damage from an attack is minimized.
