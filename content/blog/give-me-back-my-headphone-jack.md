---
title: "Give Me Back My Headphone Jack"
description: "Bluetooth is an inferior substitute for the standard headphone jack in so many ways."
slug: "give-me-back-my-headphone-jack"
date: 2018-10-01T16:08:37-04:00
image:
    src: "/files/blog/give-me-back-my-headphone-jack/iphone-headphone-jack.jpg"
    alt: "Headphone jack on the iPhone"
tags:
    - headphones
    - 3.5mm
    - bluetooth
    - android
    - iphone
    - rant
---
_This is one of my [rants](/tags/rant/), so if you're not into snarky griping with your facts, this might not be for you. Okay, let's do this._

I'm sick of the trend to remove the headphone jack from phones (and surely other devices). There's nothing wrong, and plenty right, with the 3.5mm headphone jack, and replacing it with Bluetooth is totally self-serving for vendors with no benefit to consumers. I'm not going to say anything here that hasn't been [said](https://fossbytes.com/apple-removes-headphone-jack-bad-terrible-decision/) [countless](https://www.theverge.com/circuitbreaker/2016/6/21/11991302/iphone-no-headphone-jack-user-hostile-stupid) [times](https://www.androidauthority.com/no-headphone-jacks-bad-idea-opinion-805797/) before, but I want to add my voice to the choir of the perturbed and get this off my chest.

Let's start with the biggest pain point: existing accessories. The [3.5mm audio connection](https://en.wikipedia.org/wiki/Phone_connector_(audio)) has been a standard for decades, and is so ubiquitous that nobody even thought about it as optional until Apple decided to omit it from the iPhone. Every car stereo made since, oh I don't know, 2003 has had an auxiliary input that uses the standard headphone jack. Most speakers (even ones marketed primarily as Bluetooth speakers) have an auxiliary input. Basically any audio input or output device has used the headphone jack for decades. It just works. With everything. All the time.

I've been using the [Essential Phone](https://en.wikipedia.org/wiki/Essential_Phone) for nearly a year. In so many ways it's a great phone. In one major, glaring way it's terrible. Yup, you guessed it: no headphone jack. With all of my previous phones, I could plug the aux cord in my car into my phone and go. I could grab my [favorite headphones](https://www.audio-technica.com/cms/headphones/99aff89488ddd6b1/index.html) that are decidedly not Bluetooth, and plug them directly into my phone. Not anymore. Now, I either need a dongle or a Bluetooth adapter. Both solutions are more complicated and less reliable.

_"But Christiaan, I like Bluetooth better, and I hate wires, so nobody should have wires EVER! WAAAAAAAAAAAH!!!"_ Well, there's good news: YOUR PHONE CAN DO BOTH. In fact, it's been able to do both for years. Having a headphone jack has no impact whatsoever on your ability to use Bluetooth the way you want to. It doesn't mean you have to use wires. It just means that you _also_ can use an audio cable when you need it.

Bluetooth has reliability problems that a wired connection never will. Have you ever tried to use WiFi in a crowded place with a lot of other devices? How about trying to load Facebook on your phone over the cell network at a stadium? Wireless signals are subject to interference and congestion. I have issues occassionally with Bluetooth even in places where there aren't a lot of other people for any number of reasons, from other RF interference to my own body messing with the signal. Bluetooth devices also require their own power, which adds one more thing I need to remember to charge. I have never had any of these problems with things that use a headphone jack.

_"You can just use a dongle, what's the big deal?"_ The big deal is that dongles suck. It's one more thing I have to remember to bring with me, and that I have to plug in. Oh, and it's one more thing I can lose. On a more technical level, USB-C audio has [its own set of problems with different implementations, which means it's not actually universal](https://youtu.be/Ly-bSBHOSIo). Most dongles are just audio, so you can't listen to music and charge your phone at the same time. That is, unless you want to spend $30-$45 for a dongle that will do power and audio and isn't completely unreliable cheap trash. Man, what a great solution to a problem that didn't exist until WE CREATED IT ON PURPOSE.

I'd like to think that enough people will push back on this nonsense that phones will start being made with a headphone jack again. I doubt it though. I'll probably have to live with being constantly pissed off at the obsession with inferior solutions for the sake of "progress" and fashion.

<small>[_Image source: Fossbytes_](https://fossbytes.com/apple-removes-headphone-jack-bad-terrible-decision/)</small>
