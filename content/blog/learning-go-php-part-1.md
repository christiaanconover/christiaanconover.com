---
title: "Learning Go as a PHP Developer: The Journey Begins"
date: 2016-12-01T00:42:36-05:00
description: "Why Go is an appealing language for the PHP developer that yearns for something more."

slug: learning-go-php-part-1
tags:
    - go
    - learning go
    - php
    - programming
---

_This is part 1 of a [multi-part series](/tags/learning-go/) about my experience learning Go._

I've been writing software almost exclusively in PHP since I first started to program. In the early to mid 2000s, before the advent of cheap cloud computing, shared hosting was the most viable option for somebody with a small (read: non-existent) budget, which meant PHP was the language to learn if I wanted to write software for the web. PHP was also the language used by pretty much all of the applications that interested me, such as blogging and forum software. Thus began my journey into writing software, and making computers do my bidding.

I enjoy writing PHP code, [for all of its shortcomings](https://eev.ee/blog/2012/04/09/php-a-fractal-of-bad-design/). Maybe it's my familiarity and experience with PHP, and maybe it's the massive community and open source ecosystem that exists around the language. However, I've had a growing curiosity about [Go](https://golang.org/). I've read [article](https://medium.com/@jamesotoole/golang-and-why-it-matters-1710b3af96f7#.8i9lk2iis) after [article](https://eng.uber.com/go-geofence/) espousing the benefits of Go, especially as it pertains to writing web applications. I decided it was time to take the plunge and learn Go.

### Why Go?

There are tons of languages purpose-built for web development that I could have chosen to learn that would broaden my skills and offer features to fill in PHP's gaps. Node.js is all the rage right now. Ruby has been a staple of web applications for years. So why choose Go?

The main attraction for me is performance. Go is a compiled language. It gets turned into machine code, and the resulting application has no external runtime dependencies. Compare that to PHP, which is an interpreted language, and has to have the PHP runtime and all sorts of extensions to run even basic scripts. There's a ton of stuff that a computer has to do just to output "Hello world!" in PHP. That overhead, both in terms of computation and systems management, is felt pretty quickly as an application scales.

I'm also attracted to development in a language that forces me to be stricter in my programming practices, and places a premium on code simplicity and efficiency.

### First impressions

I'm still learning some of the basics which, coming from a language without some of Go's core concepts, are foreign to me. Strict typing is something I've never had to think about before. I've never used a `struct` before in my life. Despite these hurdles, I'm already really enjoying programming in Go.

The PHP world has pretty much standardized on frameworks as the way to build any production application. Nobody writes a real PHP application from scratch; they almost always start with something like [Symfony](https://symfony.com/) or [Laravel](https://laravel.com/). As a result, there's a whole lot of prep work that must be done before you can actually start building your application. The "Go Way" is completely different. Frameworks appear to be largely discouraged, and instead the use of packages is the recommended approach. Keep your code simple, use only the packages you need, and don't get carried away with a whole bunch of features you aren't using. The result is faster development and iteration, as well as lighter weight code.

Go's [highly opinionated nature](https://golang.org/doc/effective_go.html) is ironically freeing. I don't have to develop my own coding standards, or adjust to different developers' various coding styles. All Go programs must conform to the standards prescribed by the language, or they may not compile.

I'm still struggling a bit to get used to the syntax, and how it differs (in some ways drastically) from PHP. There's enough overlap in the core concepts, though, that syntax confusion boils down to shaking off muscle memory more than having trouble understanding purpose.

### Next steps

Once I'm confident in my understanding of the basic syntax and structure of a Go program, I will be moving on to building API providers and web applications. I'll document my findings, and some more technical comparisions between Go and PHP, in [a future post](/tags/learning-go/).
