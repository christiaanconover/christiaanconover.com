---
title: "Learning Go as a PHP Developer: Comparing Features, Terminology and Syntax"
date: 2017-03-26T22:20:40-04:00
description: "Go introduces a whole slew of terms and concepts that are foreign to the PHP world. Let's figure them out."

slug: learning-go-php-part-2
tags:
    - go
    - learning go
    - php
    - programming
draft: true
---

_This is part 2 of a [multi-part series](/tags/learning-go/) about my experience learning Go._

As [I've discussed previously](/blog/learning-go-php-part-1/), I decided to learn the Go language after more than a decade of almost exclusively developing in PHP. The hardest part of getting started is making sense of all the terminology and concepts in Go that don't exist, or work very differently, in PHP.
