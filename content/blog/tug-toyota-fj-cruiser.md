---
title: 'Tug, the FJ Cruiser: My Latest Vehicle'
date: 2017-02-04T22:32:00.000Z
slug: tug-toyota-fj-cruiser
image:
  src: /files/blog/tug-toyota-fj-cruiser/offroad1.jpg
  alt: Tug offroading
tags:
  - my cars
  - tug
  - toyota
  - offroading
  - rausch creek park
---

Meet Tug, my newest vehicle! Tug is a 2010 Toyota FJ Cruiser. It's a bit of a departure from [all of my previous cars](/tags/my-cars/), but I absolutely love it.

This post is actually many months late, since I bought Tug in June 2016\. I've liked the FJ Cruiser from the first time I saw and sat in one at an auto show in early 2008\. Not thinking of myself as much of a truck guy, and being all in on BMW, I'd never given any serious thought to owning one until the spring and summer of 2016\. [Gisela](/blog/gisela/) had been in and out of the shop a number of times in the previous months, and there were still some issues that needed fixing, and hints at bigger problems in the future. That, combined with some other life circumstances, had me thinking about getting something a little bigger and more utilitarian. The FJ was back on my radar. I did some searching, and found Tug at a used car dealer only a half hour from my house, with every single criteria I'd come up with after researching FJ Cruisers extensively. Only a few days later, I was driving Tug home!

As I mentioned earlier, Tug is a 2010 FJ Cruiser. I narrowed my search to 2010 and later, since I'd found a lot of information that Toyota made some changes to the FJ in 2010 that addressed a few weaknesses in the original design. Black and yellow were my two top color choices - yellow because I like that the FJ looks like a Tonka truck in that color, and black because I've always liked black cars. I opted for an automatic transmission this time around, since I thought there was a good chance of other people driving it, and I thought it best to keep things simple. I also definitely wanted selectable 4 wheel drive, and the FJ with manual transmission was only offered with all-wheel drive. As an added bonus, Tug came equipped with the 3 gauge cluster on top of the dashboard, and a backup camera (which turns out to be pretty critical given the rear visibility of the FJ).

![Tug right after purchase](/files/blog/tug-toyota-fj-cruiser/first.jpg)

Within only a couple of weeks of purchase, I took Tug on our first road trip together, up to [my family's place in Maine](/tags/boston-island/). Immediately the roof rack and cargo space came in handy, and made the trip much more comfortable since everything fit without having to crowd the passengers at all. We also discovered that the setup of the passenger compartment, specifically all of the cup holders and storage compartments, make road trip snacking and entertainment easy. My girlfriend found that the arrangement of the seats and door panels are pretty good for sleeping.

![Items on the roof rack](/files/blog/tug-toyota-fj-cruiser/road-trip-roof-rack.jpg)

On the way home from Maine, we stopped at my girlfriend's family summer house in Scituate, MA. Their house is right on the beach, and you have to drive on the beach to get there, so it gave me the first opportunity to test Tug's 4 wheel drive off road. It worked flawlessly, and Tug feels right at home on the sand!

![Tug in Scituate](/files/blog/tug-toyota-fj-cruiser/scituate.jpg)

Tug's first real off-roading adventure came in December, at [Rausch Creek Off-Road Park](http://rc4x4.org/) in Pennsylvania. Having never done any proper off-roading before, I did a group class to learn the basics and better understand how Tug's various off-road systems work. In addition to the selectable high and low range 4WD, Tug is equipped with Toyota's off-road traction control system called A-TRAC, which basically pushes power to the wheels with the most traction and prevents the wheels with less traction from spinning freely. There is also a push-button rear differential locker. We navigated all variety of trails and obstacles, including steep hill climbs and descents, off-balance loose terrain driving, and even fording water! Once again, Tug handled everything without issue - in some cases better than the Jeeps in the group!

![Tug fording a pond](/files/blog/tug-toyota-fj-cruiser/offroad2.jpg)

{{< youtube qtzdxJ-nByQ >}}

As a result, I've been bitten by the off-roading bug. I definitely see myself doing a lot more of it in the future, and I've already started planning out upgrades and modifications I'm going to make to Tug to increase my off-roading capabilities.

In January I got the chance to test out Tug in the snow. While in the Hampton Roads area of Virginia, a winter storm came through that dropped 6-8 inches of snow. Once again, it was time to see how Tug handled the conditions. Between the 4WD and the all-terrain tires I put on shortly after I bought the truck, I was able to drive all over the area, on roads that hadn't seen any plowing, without missing a beat. In fact, Tug made such short work of navigating the slippery conditions that it was sometimes easy to forget that the roads had snow on them at all.

![Tug in the snow during the storm](/files/blog/tug-toyota-fj-cruiser/snow1.jpg)

![Tug in the snow by the York river](/files/blog/tug-toyota-fj-cruiser/snow2.jpg)

So far I haven't been able to find a single situation where this truck doesn't excel. It handles well both on and off road, it's comfortable even on long trips, it runs well and reliably (it's a Toyota truck, after all), and has an impressive set of features.

So, why the name Tug? First, one of reasons I started looking at buying an FJ was for towing, specifically our boat in Maine. We'd like to be able to have it back home in the off season, and none of us owned a vehicle that could tow it. The FJ has a 5,000 pound towing capacity, which is more than enough for our boat. Second, my sister and I noted that the high vantage point, the rather utilitarian interior, and the view through the low, wide windshield and windows all make you feel as though you're in a tugboat. The name came naturally.

I couldn't be happier with my choice to buy this truck. I'll be sharing my adventures with Tug in the future, so keep an eye out.
