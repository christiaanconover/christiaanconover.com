---
title: Moving from WordPress to a Static Site Generator
date: 2016-11-28T01:43:14.000Z
description: null
image:
  alt: null
  src: null

slug: wordpress-to-static-site
tags:
  - hugo
  - wordpress
  - howto
draft: true
---

I've been a [WordPress user, and developer, for nearly a decade](/tags/wordpress/). It has met my needs and provided a user-friendly experience. Recently, however, I've wanted a change, and decided to take a look at static site generators. Boy am I glad I did.

### Why a static site?
My site is just a blog, meaning that there isn't anything dynamic or interactive - just posts and pages. The only interactivity is the comments, and that's handled by Disqus. WordPress, for all of its benefits, suffers from one major flaw: it's heavy. To load a simple blog post, the entire codebase needs to be run, and that means that it very quickly becomes inefficient as you add plugins, and can't scale up to meet heavy traffic without a lot of optimizations and tweaking. I don't have much interest in doing all of that for my blog.

A static site, by contrast, is as lightweight as you can get. No databases to maintenance, no dependencies to manage, no optimizations needed. All you need is a basic web server.

Oh, one more big benefit: no security vulnerabilities in my CMS to worry about.

### Picking a static site generator
I had heard a lot of good things about [Jekyll](https://jekyllrb.com/), but not being a Ruby developer, it didn't have much appeal. In looking for alternatives, I came across [Sculpin](https://sculpin.io/), a PHP static site generator that largely mimics the structure of Jekyll. Sculpin proved to be a good solution initially, but I started getting restless again and wanted to find a site generator with even fewer complexities and dependencies. This coincided with my learning [Go](https://golang.org/), and came across [Hugo](https://gohugo.io/).

Hugo is written in Go, which means that it's self-contained (no dependencies need to be installed), and since it's compiled, it's fast. Generating my site in Sculpin would take about 10-15 seconds; generating my site in Hugo takes about 400-600ms. Its feature set and options are a little more mature as well.

### Migrating site content