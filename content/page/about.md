---
title: About
slug: about
menu: "main"
---

Hi! I'm Christiaan. I [write code](/code/), [blog](/blog/), and [tinker with cars](/tags/my-cars/).

## Coding

The first code I ever wrote was some very basic HTML with [Sean Routt](https://seanroutt.com) in middle school on an old Compaq desktop. I was hooked pretty much immediately, captivated by the fact that I could quickly make pages that, at the time, resembled some actual websites I used (this was 2002). From there I started learning more about HTML, CSS, and a little later started dabbling in PHP. I developed an interest in servers and networking as part of my effort to share the web pages I was making, and ended up with multiple old computers in my bedroom running all variety of services.

In high school I discovered WordPress, which I used as a springboard to learn a lot more about PHP and web development. I wrote themes and plugins for WordPress, a few of which gained modest popularity. This led to learning how to write full-blown web applications.

Those experiences have evolved into a career as a web developer and systems engineer. I still write some WordPress code, though not very often. These days I'm focusing more on web application development, as well as learning new programming languages and techniques to broaden my skill set and improve as a programmer.

## Blogging

I've been maintaining this blog, in various forms, since 2007. It's changed focus a few times to match what I was up to. These days, I post mainly about tech topics and personal experiences.
