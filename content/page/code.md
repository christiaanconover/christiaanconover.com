---
title: Code
slug: code
---

## Wordpress

### Plugins

[Author Customization](https://wordpress.org/plugins/author-customization/)\
[Featured Image Caption](https://wordpress.org/plugins/featured-image-caption/)\
[Relogo](https://wordpress.org/plugins/relogo/)\
[WP Fragmention](https://wordpress.org/plugins/wp-fragmention/)

## YOURLS

[QR Code Generator](https://github.com/cconover/yourls-qrgenerator)
