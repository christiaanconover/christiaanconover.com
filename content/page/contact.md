---
title: Contact
slug: contact
menu: footer
---

This is where you used to find my contact form, but I removed it since I mostly only got spam. Sorry about that. I'm working on another way for people to get in touch with me, stay tuned.
