---
title: Content License
slug: license
---

[![Creative Commons License](http://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/) This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/). Permissions beyond the scope of this license may be available at [christiaanconover.com/license](https://www.christiaanconover.com/license/).

Use of content from my site is welcomed and encouraged! If you choose to use anything from this site, I ask that you do so in compliance with the listed Creative Commons license and my own stipulations. Thanks!

## Site-Specific Stipulations

1. Any content used elsewhere, either in original or modified form, must attribute content to this site by name and with a **link to the specific page** where the content is located.
2. Any media containing a watermark or other branding must retain these items or, if image is modified, equivalent branding (which includes this web site's address [ChristiaanConover.com](https://www.christiaanconover.com)) as **part of the visible image** in compliance with Creative Commons attribution requirements. Text or other attribution alone **is not sufficient**.

_If you are interested in using any of my site's content in a situation that falls outside the parameters of the content license, or are unsure whether your intended use may be within the above stated terms, please feel free to [contact me](/contact/) and I'll be happy to discuss it. I'm likely to agree to your request, I'd simply prefer that you ask._
