---
title: W3RTX
slug: w3rtx
---

W3RTX is the amateur radio [callsign belonging to Christiaan Conover](http://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=3334417).

Christiaan has been a ham radio operator since August 5, 2005 after passing the [Technician class](http://en.wikipedia.org/wiki/Amateur_radio_licensing_in_the_United_States#Current_license_classes) test at the [Boy Scout National Jamboree](http://en.wikipedia.org/wiki/2005_National_Scout_Jamboree), and was issued the callsign [KB3MMY](http://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=2739157). In November 2011 Christiaan changed his callsign to W3RTX.

Christiaan got his [first radio](/blog/new-radio-icom-ict90a/) in 2007, and was a member of the [Falmouth Amateur Radio Association](http://www.falara.org/) through 2009.

**When operating, Christiaan typically monitors the following frequencies**:\
[2M national simplex calling](http://www.arrl.org/band-plan): `146.52`\
[APRS VoiceAlert](http://www.aprs.org/VoiceAlert3.html): `144.39 PL 100`

W3RTX Echolink node: `647512`
