// Include Gulp
var gulp = require('gulp');

// Include our plugins
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

// Compile Sass
gulp.task('sass', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass())
        .on('error', swallowError)
        .pipe(rename('christiaanconover.css'))
        .pipe(gulp.dest('static/css'));
});

// Watch files for changes
gulp.task('watch', function() {
    gulp.watch('src/scss/*.scss', ['sass']);
});

// Handle errors
function swallowError (error) {
    // Output error details to the console
    console.log(error.toString());

    this.emit('end');
}

// Default task
gulp.task('default', ['sass', 'watch'])
